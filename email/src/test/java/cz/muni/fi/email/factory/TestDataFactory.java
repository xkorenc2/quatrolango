package cz.muni.fi.email.factory;

import cz.muni.fi.commons.dto.course.CourseDto;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;

import java.util.UUID;

public class TestDataFactory {
    public static CourseDto createSampleCourseDto() {
        CourseDto courseDto = new CourseDto();
        courseDto.setId(UUID.randomUUID());
        courseDto.setName("English course");
        courseDto.setProficiencyLevel(ProficiencyLevel.A1);
        courseDto.setCapacity(20);
        courseDto.setLanguage(Language.ENGLISH);

        return courseDto;
    }
}
