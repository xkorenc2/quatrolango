package cz.muni.fi.email.service;

import cz.muni.fi.commons.dto.course.CourseDto;
import cz.muni.fi.email.factory.TestDataFactory;
import jakarta.mail.internet.MimeMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

import java.util.List;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EmailServiceTest {

    @Mock
    private JavaMailSender mockMailSender;

    @Mock
    private CourseApiService mockCourseApiService;

    @Mock
    private ITemplateEngine mockThymeleafTemplateEngine;

    @InjectMocks
    private EmailService emailService;


    @Test
    void sendCourseAdEmail_ValidInput_EmailSent() {
        String toEmail = "example@example.com";
        List<CourseDto> courses = List.of(TestDataFactory.createSampleCourseDto());

        MimeMessage mimeMessage = mock(MimeMessage.class);
        when(mockMailSender.createMimeMessage()).thenReturn(mimeMessage);

        when(mockCourseApiService.findAllCourses(any(Pageable.class))).thenReturn(courses);
        when(mockThymeleafTemplateEngine.process(eq("course_ad_template.html"), any(Context.class)))
                .thenReturn("mocked HTML content");

        emailService.sendCourseAdEmail(toEmail);

        verify(mockMailSender, times(1)).createMimeMessage(); // Ensure createMimeMessage is called
        verify(mockMailSender, times(1)).send(any(MimeMessage.class)); // Ens
    }

    @Test
    void sendFileViaEmail_validParameters_emailSentWithAttachment() {
        String toEmail = "recipient@example.com";
        String body = "Email body";
        MimeMessage mimeMessage = mock(MimeMessage.class);
        when(mockMailSender.createMimeMessage()).thenReturn(mimeMessage);
        MultipartFile file = createMockMultipartFile("file.txt", "Hello, World!");

        emailService.sendFileViaEmail(toEmail, body, file);

        verify(mockMailSender).send(any(MimeMessage.class));
    }


    private MultipartFile createMockMultipartFile(String fileName, String content) {
        return new MockMultipartFile(fileName, content.getBytes());
    }
}