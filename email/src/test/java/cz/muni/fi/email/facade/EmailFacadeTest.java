package cz.muni.fi.email.facade;

import cz.muni.fi.email.service.EmailService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class EmailFacadeTest {

    @Mock
    private EmailService emailService;

    @InjectMocks
    private EmailFacade emailFacade;

    @Test
    void sendEmail_ValidInput_EmailSent() {

        String toEmail = "recipient@example.com";

        emailFacade.sendCourseAdEmail(toEmail);

        verify(emailService, times(1)).sendCourseAdEmail(toEmail);
    }

    @Test
    void sendFileViaEmail_validArguments_emailServiceCalledWithCorrectArguments() {
        MockMultipartFile file = new MockMultipartFile("file", "test.txt", "text/plain", "Hello, World!".getBytes());

        emailFacade.sendFileViaEmail("recipient@example.com", "Test email body", file);

        verify(emailService).sendFileViaEmail("recipient@example.com", "Test email body", file);
    }
}
