package cz.muni.fi.email.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.email.facade.EmailFacade;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(EmailRestController.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WithMockUser(username = "admin", authorities = {"SCOPE_test_1", "SCOPE_test_2", "SCOPE_test_3"})
class EmailRestControllerTest {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    @MockBean
    private EmailFacade emailFacade;
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;

    @BeforeAll
    void setup() {
        objectMapper.findAndRegisterModules();
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    void sendCourseAdEmail_ValidInput_EmailSent() throws Exception {
        String toEmail = "valdobaldo@fi.muni.cz";

        mockMvc.perform(post("/emails/ad/").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("toEmail", toEmail))
                .andExpect(status().isOk());

        verify(emailFacade, times(1)).sendCourseAdEmail(toEmail);
    }

    @Test
    void sendFileViaEmail_validRequest_successResponse() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "test.txt", MediaType.TEXT_PLAIN_VALUE, "Hello, World!".getBytes());

        mockMvc.perform(multipart("/emails/file/")
                        .file(file)
                        .param("toEmail", "recipient@example.com")
                        .param("body", "Test email body").with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().string("Email sent successfully."));

        verify(emailFacade).sendFileViaEmail("recipient@example.com", "Test email body", file);
    }

    @Test
    void sendFileViaEmail_invalidRequest_badRequestResponse() throws Exception {
        mockMvc.perform(multipart("/emails/file/").with(csrf()))
                .andExpect(status().isInternalServerError());

        verifyNoInteractions(emailFacade);
    }

    @Test
    void sendFileViaEmail_exceptionThrown_internalServerErrorResponse() throws Exception {
        doThrow(new RuntimeException("Failed to send email")).when(emailFacade).sendFileViaEmail(any(), any(), any());

        MockMultipartFile file = new MockMultipartFile("file", "test.txt", MediaType.TEXT_PLAIN_VALUE, "Hello, World!".getBytes());

        mockMvc.perform(multipart("/emails/file/")
                        .file(file)
                        .param("toEmail", "recipient@example.com")
                        .param("body", "Test email body").with(csrf()))
                .andExpect(status().isInternalServerError())
                .andExpect(content().string("Failed to send email: Failed to send email"));

        verify(emailFacade).sendFileViaEmail("recipient@example.com", "Test email body", file);
    }
}
