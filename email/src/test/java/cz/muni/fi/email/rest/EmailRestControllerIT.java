package cz.muni.fi.email.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.commons.dto.course.CourseDto;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import cz.muni.fi.email.service.CourseApiService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WithMockUser(username = "admin", authorities = {"SCOPE_test_1", "SCOPE_test_2", "SCOPE_test_3"})
class EmailRestControllerIT {
    @Autowired
    private JavaMailSender mockMailSender;
    @MockBean
    private CourseApiService courseApiService;
    @Autowired
    private ObjectMapper objectMapper;
    private CourseDto course;
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;

    @BeforeAll
    void setup() {
        objectMapper.findAndRegisterModules();

        course = new CourseDto();
        course.setId(UUID.randomUUID());
        course.setName("Sample Course");
        course.setCapacity(10);
        course.setLanguage(Language.GERMAN);
        course.setProficiencyLevel(ProficiencyLevel.A2);

        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    void sendEmail_validBody_isOk() throws Exception {
        String toEmail = "valdobaldo@fi.muni.cz";
        List<CourseDto> courses = List.of(course);


        when(courseApiService.findAllCourses(Pageable.ofSize(10))).thenReturn(courses);


        mockMvc.perform(post("/emails/ad/").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("toEmail", toEmail))
                .andExpect(status().isOk());
    }
}
