package cz.muni.fi.email.service;

import cz.muni.fi.commons.dto.CustomPageImpl;
import cz.muni.fi.commons.dto.course.CourseDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.AbstractOAuth2Token;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Objects;

@Service
public class CourseApiService {
    private final WebClient client;

    public CourseApiService(@Value("${course.api.url}") String baseUrl) {
        this.client = WebClient.create(baseUrl);
    }

    public List<CourseDto> findAllCourses(Pageable pageable) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        AbstractOAuth2Token token = (AbstractOAuth2Token) authentication.getCredentials();

        Mono<CustomPageImpl<CourseDto>> courseDtoFlux = client
                .get()
                .uri("/?page=" + pageable.getPageNumber() + "&size=" + pageable.getPageSize())
                .headers(httpHeaders -> httpHeaders.setBearerAuth(token.getTokenValue()))
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });

        return Objects.requireNonNull(courseDtoFlux.block()).getContent();
    }


}
