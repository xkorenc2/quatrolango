package cz.muni.fi.email.rest;

import cz.muni.fi.email.facade.EmailFacade;
import io.micrometer.observation.annotation.Observed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(path = "/emails", produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Email service", description = "Service for sending emails from Quatrolango application")
public class EmailRestController {
    private final EmailFacade emailFacade;

    @Autowired
    public EmailRestController(EmailFacade emailFacade) {
        this.emailFacade = emailFacade;
    }

    @Observed(name = "sendAdEmail")
    @PostMapping(path = "/ad/")
    @Operation(summary = "Sends ad email",
            description = "Sends ad email to provided email address with available courses",
            security = @SecurityRequirement(name = "Bearer")
    )
    public ResponseEntity<String> sendCourseAdEmail(@Valid @RequestParam String toEmail) {
        if (toEmail.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing required parameters.");
        }
        try {
            emailFacade.sendCourseAdEmail(toEmail);
            return ResponseEntity.ok("Email sent successfully.");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to send email: " + e.getMessage());
        }
    }

    @Observed(name = "sendEmailWithFile")
    @PostMapping(path = "/file/", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "Sends file via email", security = @SecurityRequirement(name = "Bearer"),
            description = "Sends file as attachment via email to provided email address"
    )
    public ResponseEntity<String> sendFileViaEmail(@Valid @RequestParam String toEmail,
                                                   @Valid @RequestParam String body,
                                                   @Valid @RequestParam MultipartFile file
    ) {
        if (toEmail.isEmpty() || body.isEmpty() || file == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing required parameters.");
        }
        try {
            emailFacade.sendFileViaEmail(toEmail, body, file);
            return ResponseEntity.ok("Email sent successfully.");
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to send email: " + exception.getMessage());
        }
    }
}
