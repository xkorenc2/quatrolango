package cz.muni.fi.email.facade;

import cz.muni.fi.email.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class EmailFacade {
    private final EmailService emailService;

    @Autowired
    public EmailFacade(EmailService emailService) {
        this.emailService = emailService;
    }

    public void sendCourseAdEmail(String toEmail) {
        emailService.sendCourseAdEmail(toEmail);
    }

    public void sendFileViaEmail(String toEmail, String body, MultipartFile file) {
        emailService.sendFileViaEmail(toEmail, body, file);
    }
}
