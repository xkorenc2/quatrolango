package cz.muni.fi.email.service;


import cz.muni.fi.commons.dto.course.CourseDto;
import cz.muni.fi.commons.exceptions.EmailException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

import java.util.List;
import java.util.Locale;
import java.util.Objects;

@Service
public class EmailService {

    private final JavaMailSender mailSender;
    private final CourseApiService courseApiService;
    private final ITemplateEngine thymeleafTemplateEngine;

    @Autowired
    public EmailService(
            JavaMailSender mailSender,
            CourseApiService courseApiService,
            @Qualifier("templateEngine") ITemplateEngine thymeleafTemplateEngine
    ) {
        this.mailSender = mailSender;
        this.courseApiService = courseApiService;
        this.thymeleafTemplateEngine = thymeleafTemplateEngine;
    }

    @Transactional
    public void sendCourseAdEmail(String toEmail) {
        List<CourseDto> availableCourses = findAvailableCourses();
        final Context ctx = new Context(Locale.ENGLISH);
        ctx.setVariable("courses", availableCourses);

        final MimeMessage mimeMessage = mailSender.createMimeMessage();
        try {
            final MimeMessageHelper message =
                    new MimeMessageHelper(mimeMessage, true, "UTF-8");
            message.setSubject("Quatrolango - available courses");
            message.setTo(toEmail);

            final String htmlContent = thymeleafTemplateEngine.process("course_ad_template.html", ctx);
            message.setText(htmlContent, true);

            mailSender.send(mimeMessage);
        } catch (Exception e) {
            throw new EmailException(e.getLocalizedMessage());
        }
    }

    @Transactional
    public void sendFileViaEmail(String toEmail, String body, MultipartFile file) {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        try {
            final MimeMessageHelper message =
                    new MimeMessageHelper(mimeMessage, true, "UTF-8");
            message.setTo(toEmail);
            message.setSubject("Quatrolango - certificate");

            message.setText(body, true);
            message.addAttachment(Objects.requireNonNull(file.getOriginalFilename()), file);

            mailSender.send(mimeMessage);
        } catch (Exception e) {
            throw new EmailException(e.getLocalizedMessage());
        }
    }


    public List<CourseDto> findAvailableCourses() {
        return courseApiService.findAllCourses(Pageable.ofSize(3));
    }

}
