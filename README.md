# Quatrolango

### Description

System for language school lectures management. Project consists
of 4 main modules **[Core](#core-module)**,**[OIDC-Client](#oidc-client-module)**,
**[User - Student & Lecturer](#user-module)**,
**[Email](#email-module)**, **[Certificate](#certificates-module)** and single **[Commons](#commons-module)** module
containing shared functionality across
the project.

Main modules serve as microservices and can be run independently. Each module runs on a different port
mentioned in detailed description below. Each module also uses n-tier architecture.

Project uses _PostgreSQL_ database. **[Core](#core-module)**,
**[User - Student & Lecturer](#user-module)**,
**[Certificate](#certificates-module)** use their own databases. Each database runs on different port as well.
More information in [run section](#run-project).

## Commons module

This module contains common functionality that is shared across other modules. Most importantly
there are DTOs, thanks to which there are no dependencies between main modules whatsoever
and these modules can take full advantage of their microservice-like properties.

Commons also holds exceptions, RestExceptionHandling (which is used in all rest endpoints in the project)
, enums and other shared classes and methods.

## Core module

This module contains the following core entities: [Lecture, Exercise, Course, CourseEnrollment, LectureEnrollment].
It provides CRUD operations for all the contained entities.

### Lecture

This entity represents a single lecture session within a course. It contains information such as the lecture
name, description, start and end times, lecturer ID, capacity, and enrollment details.

### Course

This entity represents a particular educational course offered within the system. It contains information such as
the course name, capacity, language of instruction, proficiency level, and associated lectures, exercises, and
enrollments.

### Exercise

This entity represents a specific exercise associated with a course. It includes details such as the exercise
name, description, difficulty level, and the course it belongs to.

### CourseEnrollment and LectureEnrollment

These entities represent a specific student being enrolled in Course/Lecture

## User module

This module is responsible for managing both **Student** and **Lecturer** entity. The User entity holds common
attributes for both entities. Namely, those are name, middle name, surname, email and date of birth.

### Student

Student is an extension of the User class. Except all the properties of User, it has the IsActive property, which
specifies whether the student is actively studying or not.

### Lecturer

Lecturer is an extension of the Student Class. Except all the properties of Student it has the properties
isNativeSpeaker and list of languages the lecturer can teach.

## Certificates module

This module provides CRD operations for Certificates. It is used to generate certificates for students that have passed
a course.

## Email module

This module provides the ability to send emails to the students and lecturers within the system.

## OIDC-Client module

This module provides a way for users to authenticate via MUNI and get their access token.

## Run project

#### Modules run on following ports:

- **Core** - 8080
- **Email** - 8081
- **User** - 8082
- **Certificate** - 8083
- **OIDC-Client** - 8084

#### Databases run on following ports with names:

- **Core** - 5433 quatro_core_db
- **User** - 5434 quatro_user_db
- **Certificate** - 5435 quatro_certificate_db

#### Database credentials

- **username**: _quatro_
- **password**: _4jazyky_

### Run using docker

Project is using Java JDK version **21** and Maven.

1. Clean and install dependencies using maven: `mvn clean install`
2. Start token client in docker container by running `make run-token-client`
    - **OBTAINING TOKEN**: after `make run-token-client` builds and runs the token client, go to http://localhost:8084/. Log in with your MUNI credentials, copy the token and place it at the end of the .env file in the root of the project.
2. Start server in docker container by running: `make run`
    - if you want to include seeding of the DB, run the container with profile dev: `make run ENV=dev`
    - **for successful seeding, token must be already stored in .env** 
    - if you decide to remove data from DB, run: `make drop-seeding`
3. Server will be running on `http://localhost:808x` where x is specific port number
   - to access swagger, use `http://localhost:808x/swagger-ui/index.html#/`
4. Databases will be running on specified ports
5. To shut down and remove containers, run: `make down`

To connect to database we suggest using desktop application [pgAdmin](https://www.pgadmin.org/) or terminal
based admin [psql](https://www.postgresql.org/docs/current/app-psql.html). Fill in provided
database credentials to see database locally.

## Showcase
Before starting showcase script, ensure ALL CONTAINERS are running.

#### Usage
- Run `make run-showcase` to build and run the script.
- Go to the quatrolango-showcase service in docker container and check the logs. If there was any error, the script throws exception and ends with code 1
- Logs describe what is happening in the script and also provide state of the DB using GET calls.
- On success, the script ends with a success log at the end. 
- Note, that logs are much clearer and better readable if the script is run with clean database (without seeding). For that, you can utilize command `make drop-seeding`, that will remove all data in the DBs.
- Script also shows a functionality of sending certificates to student's email. If you want to verify the behaviour, specify your email address in the .env file. The script will send you an email with a pdf certificate for passing the course in students name.

#### Observability
- to start prometheus and grafana, run `make run-observe`
- log in to grafana with username `admin` and password `admin`
- grafana: `http://localhost:3000`
- prometheus: `http://localhost:9090`


#### Functionality

The script executes the following sequence of events:

1. **Registers New Lecturer**:
    - Registers a new lecturer in the Quatrolango Showcase system.

2. **Get Lecturers**:
    - Retrieves a list of all registered lecturers.

3. **Register New Student**:
    - Registers a new student in the Quatrolango Showcase system.

4. **Create Course**:
    - Creates a new course in the system.

5. **Create Lectures**:
    - Creates lectures for the created course and new lecturer assigned.

6. **Get Lectures**:
    - Retrieves a list of all lectures for the created course.

7. **Create Exercises**:
    - Creates exercises for the course.

8. **Get Exercises**:
    - Retrieves a list of all exercises for the created course.

9. **Create Course Enrollment**:
    - Enrolls the student in the created course.

10. **Create Lectures Enrollments**:
    - Enrolls the student in the lectures of the created course.

11. **Change Course State for Student**:
    - Changes the state of the course for the student from `enrolled` to `passed`.

12. **Generate Certificate of Passing Course**:
    - Generates a certificate of passing the course for the student and sends an email with certificate to student.

## Use case diagram

![QuatroLango.jpg](resources%2FQuatroLango.jpg)

## Class diagram

![QuatrolingoClass.jpg](resources%2FQuatrolangoClass.jpg)