import argparse
import os

import requests
import json
from datetime import datetime, timedelta

# Base URLs for the services
# if running without docker compose, replace user/core/certificate with localhost
base_url_user_service = 'http://user:8082'
base_url_core_service = 'http://core:8080'
base_url_certificate_service = 'http://certificate:8083'


class QuatrolangoShowcase:
    def __init__(self, token):
        self.headers = {
            'Authorization': f'Bearer {token}',
            'Content-Type': 'application/json'
        }
        self.lecturer = None
        self.course = None
        self.student = None
        self.lectures = []
        self.course_enrollments = []
        self.lecture_enrollments = []

    def register_new_lecturer(self):
        url = f"{base_url_user_service}/lecturers/"
        data = {
            "firstName": "Jennifer",
            "secondName": "Benson",
            "middleName": "Andrea",
            "email": "jennifer.benson@mail.com",
            "dateOfBirth": "2000-12-23",
            "languages": [
                {"language": "ENGLISH", "proficiencyLevel": "C1"}
            ],
            "nativeSpeaker": True
        }
        response = requests.post(url, json=data, headers=self.headers)
        if response.status_code == 201:
            self.lecturer = response.json()
            id = self.lecturer["id"]
            print("POST /lecturers/")
            print(f"Created new lecturer with ID: {id}")
            print(json.dumps(response.json(), indent=4))
        else:
            raise ValueError("Failed to register lecturer")

    def get_lecturers(self):
        url = f"{base_url_user_service}/lecturers/"
        response = requests.get(url, headers=self.headers)
        print("GET /lecturers/")
        print(json.dumps(response.json(), indent=4))

    # Function to register a new student
    def register_new_student(self, data):
        url = f"{base_url_user_service}/students/"
        response = requests.post(url, json=data, headers=self.headers)
        if response.status_code == 201:
            self.student = response.json()
            print("Created new student with ID:", self.student['id'])
            print(json.dumps(response.json(), indent=4))
        else:
            raise ValueError("Failed to register student")

    # Function to create a course
    def create_course(self):
        url = f"{base_url_core_service}/courses/"
        data = {
            "name": "English for IT specialists",
            "capacity": 100,
            "language": "ENGLISH",
            "proficiencyLevel": "B1"
        }
        response = requests.post(url, json=data, headers=self.headers)
        if response.status_code == 201:
            data = response.json()
            self.course = data
            course_id = self.course["id"]
            print(f"Created new course with ID: {course_id}")
        else:
            raise ValueError("Failed to create course")

    def create_lectures(self):
        for i in range(5):
            start_time = datetime.fromisoformat("2024-05-30T18:00:00.000")
            current_start_time = start_time + timedelta(weeks=i)
            current_end_time = current_start_time + timedelta(hours=2)  # Assuming each lecture is 2 hours long

            course_id = self.course["id"]
            url = f"{base_url_core_service}/lectures/{course_id}"
            data = {
                "name": "IT English practice: Lecture " + str(i),
                "description": "Learn English by practicing conversations under the supervision of skilled lecturer.",
                "startTime": current_start_time.isoformat() + "Z",
                "endTime": current_end_time.isoformat() + "Z",
                "courseId": self.course["id"],
                "lecturerId": self.lecturer["id"],
                "capacity": 100
            }
            response = requests.post(url, json=data, headers=self.headers)
            if response.status_code == 201:
                resp_data = response.json()
                self.lectures.append(resp_data)
                lecture_id = resp_data["id"]
                print(f"Created new lecture with ID: {lecture_id}")
            else:
                raise ValueError("Failed to create lecture")

    def get_lectures(self):
        url = f"{base_url_core_service}/lectures/"
        response = requests.get(url, headers=self.headers)
        print("GET /lectures/")
        print(json.dumps(response.json(), indent=4))

    def create_exercises(self):
        # List of exercise difficulties
        difficulties = ["EASY", "MEDIUM", "HARD"]

        for i in range(3):  # Create 3 exercises
            course_id = self.course["id"]
            url = f"{base_url_core_service}/exercises/{course_id}"
            data = {
                "name": f"Exercise {i + 1}",
                "description": f"Practice exercise {i + 1}: {difficulties[i]} level",
                "exerciseDifficulty": difficulties[i]
            }
            response = requests.post(url, json=data, headers=self.headers)
            if response.status_code == 201:
                resp_data = response.json()
                print(f"Created new exercise with ID: {resp_data['id']}")
            else:
                raise ValueError("Failed to create exercise")

    def get_exercises(self):
        url = f"{base_url_core_service}/exercises/"
        response = requests.get(url, headers=self.headers)
        print("GET /exercises/")
        print(json.dumps(response.json(), indent=4))

    def create_course_enrollment(self):
        url = f"{base_url_core_service}/course-enrollments/"
        timestamp = datetime.utcnow().isoformat(timespec='milliseconds') + 'Z'
        data = {
            "studentDto": self.student,
            "courseDto": self.course,
            "updatedAt": timestamp,
            "createdAt": timestamp,
            "enrollmentState": "ENROLLED"
        }
        response = requests.post(url, json=data, headers=self.headers)
        if response.status_code == 201:
            data = response.json()
            self.course_enrollments.append(data)
            enrollment_id = self.course["id"]
            print(f"Created new course enrollment with ID: {enrollment_id}")
        else:
            raise ValueError("Failed to create course enrollment")

    def create_lectures_enrollments(self):
        for lecture in self.lectures:
            url = f"{base_url_core_service}/lecture-enrollments/"
            timestamp = datetime.utcnow().isoformat(timespec='milliseconds') + 'Z'
            data = {
                "studentDto": self.student,
                "lectureDto": lecture,
                "updatedAt": timestamp,
                "createdAt": timestamp,
            }
            response = requests.post(url, json=data, headers=self.headers)
            if response.status_code == 201:
                resp_data = response.json()
                self.lecture_enrollments.append(data)
                enrollment_id = resp_data["id"]
                print(f"Created new lecture enrollment with ID: {enrollment_id}")
            else:
                raise ValueError("Failed to create lecture enrollment")

    def change_course_state_for_student(self):
        enrollment_id = self.course_enrollments[0]["id"]
        url = f"{base_url_core_service}/course-enrollments/{enrollment_id}"
        params = {"enrollmentState": "PASSED"}
        response = requests.patch(url, params=params, headers=self.headers)
        if response.status_code == 200:
            resp_data = response.json()
            self.course_enrollments[0] = resp_data
            enrollment_id = resp_data["id"]
            print(f"Updated course enrollment state with ID: {enrollment_id} to state PASSED")
        else:
            raise ValueError("Failed to update course enrollment state.")

    def generate_certificate_of_passing_course(self):
        student_id = self.student["id"]
        course_id = self.course["id"]
        url = f"{base_url_certificate_service}/certificates/students/{student_id}/courses/{course_id}"
        response = requests.post(url, data="{}", headers=self.headers)
        if response.status_code == 200:
            certificate_id = response.json()["id"]
            print(f"Created new certificate with ID: {certificate_id}")
        else:
            raise ValueError("Failed to create certificate.")


def run(token, email):
    showcase = QuatrolangoShowcase(token)
    showcase.register_new_lecturer()
    showcase.get_lecturers()
    student_data = {
            "firstName": "John",
            "secondName": "Smith",
            "middleName": "Andrew",
            "email": email if email != None and email != '' else "andrew.smith@mail.com",
            "dateOfBirth": "1995-12-23",
            "active": True
        }
    showcase.register_new_student(student_data)
    showcase.create_course()
    if showcase.course:
        showcase.create_lectures()
        showcase.get_lectures()
        showcase.create_exercises()
        showcase.get_exercises()
        showcase.create_course_enrollment()
        showcase.create_lectures_enrollments()
        showcase.change_course_state_for_student()
        showcase.generate_certificate_of_passing_course()
        print("\n\n\n\n\n")
        print("SHOWCASE SCRIPT ENDED SUCCESSFULLY")
        print("\n\n\n\n\n")
    else:
        print("No course created, skipping lectures, exercises and enrollments.")


if __name__ == "__main__":
    # Parse command line arguments
    token = os.getenv('TOKEN')
    email = os.getenv('SHOWCASE_EMAIL')
    if not token or token == '':
        raise ValueError("No token provided. Set the TOKEN environment variable in .env file.")

    run(token, email)
