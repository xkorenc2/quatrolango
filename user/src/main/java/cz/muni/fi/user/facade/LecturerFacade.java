package cz.muni.fi.user.facade;

import cz.muni.fi.commons.dto.user.lecturer.LecturerDto;
import cz.muni.fi.commons.dto.user.student.LecturerCreateDto;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.user.data.mapper.LecturerDtoMapper;
import cz.muni.fi.user.data.mapper.LecturerLanguageMapper;
import cz.muni.fi.user.service.LecturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class LecturerFacade {
    private final LecturerDtoMapper mapper;
    private final LecturerLanguageMapper languageMapper;
    private final LecturerService lecturerService;

    @Autowired
    public LecturerFacade(LecturerService lecturerService, LecturerDtoMapper mapper, LecturerLanguageMapper languageMapper) {
        this.lecturerService = lecturerService;
        this.mapper = mapper;
        this.languageMapper = languageMapper;
    }

    @Transactional(readOnly = true)
    public LecturerDto findById(UUID id) {
        return mapper.toDto(lecturerService.findById(id));
    }

    @Transactional(readOnly = true)
    public Page<LecturerDto> findAll(Pageable pageable) {
        return mapper.toDtoPage(lecturerService.findAll(pageable));
    }

    @Transactional(readOnly = true)
    public List<LecturerDto> findByLanguage(Language language) {
        return mapper.toDtoList(lecturerService.findByLanguage(language));
    }

    @Transactional(readOnly = true)
    public List<LecturerDto> findByIds(List<UUID> ids) {
        return mapper.toDtoList(lecturerService.findByIds(ids));
    }

    @Transactional(readOnly = true)
    public LecturerDto findByEmail(String email) {
        return mapper.toDto(lecturerService.findByEmail(email));
    }

    @Transactional
    public LecturerDto create(LecturerCreateDto lecturer) {
        return mapper.toDto(lecturerService.create(mapper.toEntityFromCreate(lecturer)));
    }

    @Transactional
    public LecturerDto update(LecturerCreateDto lecturer, UUID id) {
        return mapper.toDto(lecturerService.update(mapper.toEntityFromCreate(lecturer), id));
    }

    @Transactional
    public LecturerDto remove(UUID id) {
        return mapper.toDto(lecturerService.remove(id));
    }
}
