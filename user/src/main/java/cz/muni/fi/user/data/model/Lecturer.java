package cz.muni.fi.user.data.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@Entity
@EqualsAndHashCode(callSuper = false)
@Table(name = "Lecturer")
public class Lecturer extends User {

    @Column(name = "is_native_speaker")
    private boolean isNativeSpeaker;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE})
    private List<LecturerLanguage> languages = new ArrayList<>();

    public boolean isNativeSpeaker() {
        return isNativeSpeaker;
    }

    public void setNativeSpeaker(boolean nativeSpeaker) {
        isNativeSpeaker = nativeSpeaker;
    }
}
