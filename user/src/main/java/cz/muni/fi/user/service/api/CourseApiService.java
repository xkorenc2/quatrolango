package cz.muni.fi.user.service.api;

import cz.muni.fi.commons.dto.course.CourseEnrollmentDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.AbstractOAuth2Token;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Service
public class CourseApiService {
    private final WebClient client;

    public CourseApiService(@Value("${course.enrollment.api.url}") String baseUrl) {
        this.client = WebClient.create(baseUrl);
    }

    public Set<CourseEnrollmentDto> getCourseEnrollmentsByStudentId(UUID studentId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        AbstractOAuth2Token token = (AbstractOAuth2Token) authentication.getCredentials();

        var courses = client
                .get()
                .uri("/students/" + studentId)
                .headers(httpHeaders -> httpHeaders.setBearerAuth(token.getTokenValue()))
                .retrieve()
                .bodyToFlux(CourseEnrollmentDto.class);
        return new HashSet<>(Objects.requireNonNull(courses.collectList().block()));
    }
}

