package cz.muni.fi.user.config;

import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

import static cz.muni.fi.commons.consts.OAuthConsts.ADMIN_SCOPE;
import static cz.muni.fi.commons.consts.OAuthConsts.LECTURER_SCOPE;
import static cz.muni.fi.commons.consts.OAuthConsts.SECURITY_SCHEME_BEARER;
import static cz.muni.fi.commons.consts.OAuthConsts.STUDENT_SCOPE;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(x -> x
                        .requestMatchers("/actuator/**").permitAll()
                        .requestMatchers(HttpMethod.POST, "/lecturers/**").hasAuthority(ADMIN_SCOPE)
                        .requestMatchers(HttpMethod.DELETE, "/lecturers/**").hasAuthority(ADMIN_SCOPE)
                        .requestMatchers(HttpMethod.PUT, "/lecturers/**").hasAuthority(ADMIN_SCOPE)
                        .requestMatchers(HttpMethod.GET, "/lecturers/**").hasAnyAuthority(ADMIN_SCOPE, LECTURER_SCOPE, STUDENT_SCOPE)
                        .requestMatchers(HttpMethod.POST, "/students/**").hasAnyAuthority(ADMIN_SCOPE, LECTURER_SCOPE)
                        .requestMatchers(HttpMethod.DELETE, "/students/**").hasAnyAuthority(ADMIN_SCOPE, LECTURER_SCOPE)
                        .requestMatchers(HttpMethod.GET, "/students/**").hasAnyAuthority(ADMIN_SCOPE, LECTURER_SCOPE, STUDENT_SCOPE)
                        .requestMatchers(HttpMethod.PUT, "/students/**").hasAnyAuthority(ADMIN_SCOPE, LECTURER_SCOPE)
                        .requestMatchers("/swagger-ui/**").permitAll()
                        .requestMatchers("/v3/api-docs/**").permitAll()
                )
                .oauth2ResourceServer(oauth2 -> oauth2.opaqueToken(Customizer.withDefaults()));
        return http.build();
    }

    @Bean
    public OpenApiCustomizer openAPICustomizer() {
        return openApi -> {
            openApi.getComponents().addSecuritySchemes(SECURITY_SCHEME_BEARER,
                    new SecurityScheme()
                            .type(SecurityScheme.Type.HTTP)
                            .scheme("bearer")
                            .description("provide a valid access token"));
        };
    }
}
