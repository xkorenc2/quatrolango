package cz.muni.fi.user.service;

import cz.muni.fi.commons.dto.course.CourseEnrollmentDto;
import cz.muni.fi.commons.dto.lecture.LectureEnrollmentDto;
import cz.muni.fi.commons.dto.user.student.StudentDetailedDto;
import cz.muni.fi.commons.exceptions.ResourceNotFoundException;
import cz.muni.fi.user.data.model.Student;
import cz.muni.fi.user.data.repository.StudentRepository;
import cz.muni.fi.user.service.api.CourseApiService;
import cz.muni.fi.user.service.api.LectureApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class StudentService {
    private final StudentRepository studentRepository;
    private final CourseApiService courseApiService;
    private final LectureApiService lectureApiService;

    @Autowired
    public StudentService(StudentRepository repository, CourseApiService courseApiService, LectureApiService lectureApiService) {
        this.studentRepository = repository;
        this.courseApiService = courseApiService;
        this.lectureApiService = lectureApiService;
    }

    @Transactional
    public Page<Student> findAll(Pageable pageable) {
        return studentRepository.findAll(pageable);
    }

    @Transactional
    public Student findById(UUID id) {
        var student = studentRepository.findById(id);
        if (student.isEmpty()) {
            throw new ResourceNotFoundException("Student with id [" + id + "] was not found.");
        }
        return student.get();
    }

    @Transactional
    public List<Student> findByIds(Set<UUID> ids) {
        List<Student> students = new ArrayList<>();
        ids.forEach(id -> students.add(this.findById(id)));
        return students;
    }

    @Transactional
    public Student removeById(UUID id) {
        Optional<Student> student = studentRepository.findById(id);
        if (student.isEmpty()) {
            throw new ResourceNotFoundException("Student with id [" + id + "] was not present.");
        }
        studentRepository.deleteById(id);
        return student.get();
    }

    public StudentDetailedDto studentToDetailed(StudentDetailedDto student) {
        student.setCourses(courseApiService.getCourseEnrollmentsByStudentId(student.getId())
                .stream()
                .map(CourseEnrollmentDto::getCourseDto)
                .collect(Collectors.toSet()));
        student.setLectures(lectureApiService.getLectureEnrollmentsByStudentId(student.getId())
                .stream()
                .map(LectureEnrollmentDto::getLectureDto)
                .collect(Collectors.toSet()));
        return student;
    }

    @Transactional
    public Student updateStudent(Student student) {
        var oldStudent = studentRepository.findById(student.getId());
        var timestamp = LocalDateTime.now();
        if (oldStudent.isPresent()) {
            student.setCreatedAt(oldStudent.get().getCreatedAt());
        } else {
            student.setCreatedAt(timestamp);
        }
        student.setUpdatedAt(timestamp);

        return studentRepository.save(student);
    }

    @Transactional
    public Student registerStudent(Student student) {
        student.setId(UUID.randomUUID());
        var timestamp = LocalDateTime.now();
        student.setCreatedAt(timestamp);
        student.setUpdatedAt(timestamp);
        student.setActive(true);
        return studentRepository.save(student);
    }
}
