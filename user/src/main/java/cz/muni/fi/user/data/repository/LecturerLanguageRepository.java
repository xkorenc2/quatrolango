package cz.muni.fi.user.data.repository;

import cz.muni.fi.user.data.model.LecturerLanguage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface LecturerLanguageRepository extends JpaRepository<LecturerLanguage, UUID> {
}