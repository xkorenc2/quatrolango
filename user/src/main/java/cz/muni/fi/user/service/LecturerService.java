package cz.muni.fi.user.service;

import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.exceptions.ResourceNotFoundException;
import cz.muni.fi.user.data.model.Lecturer;
import cz.muni.fi.user.data.repository.LecturerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class LecturerService {
    private final LecturerRepository lecturerRepository;

    @Autowired
    public LecturerService(LecturerRepository lecturerRepository) {
        this.lecturerRepository = lecturerRepository;
    }

    @Transactional
    public Page<Lecturer> findAll(Pageable pageable) {
        return lecturerRepository.findAll(pageable);
    }

    @Transactional
    public List<Lecturer> findByIds(List<UUID> ids) {
        return lecturerRepository.findByIds(ids);
    }

    @Transactional
    public Lecturer findById(UUID id) {
        return lecturerRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Lecturer with id: " + id + " was not found.")
        );
    }

    @Transactional
    public Lecturer findByEmail(String email) {
        return lecturerRepository.findLecturerByEmail(email).orElseThrow(
                () -> new ResourceNotFoundException("Lecturer with email: " + email + " was not found.")
        );
    }

    @Transactional
    public List<Lecturer> findByLanguage(Language language) {
        return lecturerRepository.findByLanguage(language);
    }

    @Transactional
    public Lecturer create(Lecturer lecturer) {
        LocalDateTime currentTimeStamp = LocalDateTime.now();
        lecturer.setUpdatedAt(currentTimeStamp);
        lecturer.setCreatedAt(currentTimeStamp);
        lecturer.setId(UUID.randomUUID());
        return lecturerRepository.save(lecturer);
    }

    @Transactional
    public Lecturer update(Lecturer updateData, UUID id) {
        LocalDateTime currentTimeStamp = LocalDateTime.now();
        Optional<Lecturer> original = lecturerRepository.findById(id);
        if (original.isEmpty()) {
            updateData.setCreatedAt(currentTimeStamp);
        } else {
            updateData.setCreatedAt(original.get().getCreatedAt());
        }
        updateData.setUpdatedAt(currentTimeStamp);
        updateData.setId(id);
        return lecturerRepository.save(updateData);
    }

    @Transactional
    public Lecturer remove(UUID id) {
        Optional<Lecturer> lecturer = lecturerRepository.findById(id);
        if (lecturer.isEmpty()) {
            throw new ResourceNotFoundException("Lecturer with id: " + id + " was not found and could not be deleted.");
        }
        lecturerRepository.deleteById(id);
        return lecturer.get();
    }
}
