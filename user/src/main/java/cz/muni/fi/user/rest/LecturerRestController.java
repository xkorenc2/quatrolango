package cz.muni.fi.user.rest;


import cz.muni.fi.commons.dto.user.lecturer.LecturerDto;
import cz.muni.fi.commons.dto.user.student.LecturerCreateDto;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.user.facade.LecturerFacade;
import io.micrometer.observation.annotation.Observed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;


@RestController
@Tag(name = "Lecturer REST API", description = "REST API for Lecturers")
@RequestMapping(path = "/lecturers", produces = MediaType.APPLICATION_JSON_VALUE)
public class LecturerRestController {
    private final LecturerFacade lecturerFacade;

    @Autowired
    public LecturerRestController(LecturerFacade lecturerFacade) {
        this.lecturerFacade = lecturerFacade;
    }

    @GetMapping(path = "/")
    @Operation(summary = "Get all lecturers", security = @SecurityRequirement(name = "Bearer"), description = "Get all lecturers from the database")
    @Observed(name = "findAllLecturers")
    public ResponseEntity<Page<LecturerDto>> findAll(Pageable pageable) {
        return ResponseEntity.ok(lecturerFacade.findAll(pageable));
    }

    @GetMapping(path = "/language/{language}")
    @Operation(summary = "Get all lecturers by language", security = @SecurityRequirement(name = "Bearer"), description = "Get all lecturers by language from the database")
    @Observed(name = "findLecturersByLanguage")
    public ResponseEntity<List<LecturerDto>> findByLanguage(@PathVariable String language) {
        Language languageEnum = Language.valueOf(language);
        return ResponseEntity.ok(lecturerFacade.findByLanguage(languageEnum));
    }

    @PostMapping(path = "/ids")
    @Operation(summary = "Get lecturers by ids", security = @SecurityRequirement(name = "Bearer"), description = "Get lecturers by ids from the database")
    @Observed(name = "findLecturersByIds")
    public ResponseEntity<List<LecturerDto>> findByIds(@RequestBody List<UUID> ids) {
        return ResponseEntity.ok(lecturerFacade.findByIds(ids));
    }

    @GetMapping(path = "/email/{email}")
    @Operation(summary = "Get lecturer by email", security = @SecurityRequirement(name = "Bearer"), description = "Get lecturer by email from the database")
    @Observed(name = "findLecturerByEmail")
    public ResponseEntity<LecturerDto> findByEmail(@PathVariable String email) {
        return ResponseEntity.ok(lecturerFacade.findByEmail(email));
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Get lecturer by id", security = @SecurityRequirement(name = "Bearer"), description = "Get lecturer by id from the database")
    @Observed(name = "findLecturerById")
    public ResponseEntity<LecturerDto> findById(@PathVariable UUID id) {
        return Optional.ofNullable(lecturerFacade.findById(id))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping(path = "/")
    @Operation(summary = "Create lecturer", security = @SecurityRequirement(name = "Bearer"), description = "Create lecturer in the database")
    @Observed(name = "createLecturer")
    public ResponseEntity<LecturerDto> create(@Valid @RequestBody LecturerCreateDto lecturer) {
        return new ResponseEntity<>(lecturerFacade.create(lecturer), HttpStatus.CREATED);
    }

    @PutMapping(path = "/{id}")
    @Operation(summary = "Update lecturer", security = @SecurityRequirement(name = "Bearer"), description = "Update lecturer in the database")
    @Observed(name = "updateLecturer")
    public ResponseEntity<LecturerDto> update(@PathVariable UUID id, @Valid @RequestBody LecturerCreateDto lecturer) {
        return ResponseEntity.ok(lecturerFacade.update(lecturer, id));
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete lecturer", security = @SecurityRequirement(name = "Bearer"), description = "Delete lecturer from the database")
    @Observed(name = "deleteLecturer")
    public ResponseEntity<LecturerDto> delete(@PathVariable UUID id) {
        return Optional.ofNullable(lecturerFacade.remove(id))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }
}
