package cz.muni.fi.user.data.repository;

import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.user.data.model.Lecturer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface LecturerRepository extends JpaRepository<Lecturer, UUID> {

    @Query("SELECT l FROM Lecturer l WHERE l.email = :email")
    Optional<Lecturer> findLecturerByEmail(String email);

    @Query("SELECT l FROM Lecturer l WHERE l.id IN :ids")
    List<Lecturer> findByIds(@Param("ids") List<UUID> ids);

    @Query("SELECT l FROM Lecturer l JOIN l.languages ll WHERE ll.language = :language")
    List<Lecturer> findByLanguage(@Param("language") Language language);
}
