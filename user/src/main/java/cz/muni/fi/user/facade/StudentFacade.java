package cz.muni.fi.user.facade;

import cz.muni.fi.commons.dto.user.student.StudentDetailedDto;
import cz.muni.fi.commons.dto.user.student.StudentDto;
import cz.muni.fi.commons.dto.user.student.UserCreateDto;
import cz.muni.fi.commons.exceptions.ServiceApiException;
import cz.muni.fi.user.data.mapper.StudentDtoMapper;
import cz.muni.fi.user.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class StudentFacade {
    private final StudentService studentService;
    private final StudentDtoMapper studentDtoMapper;

    @Autowired
    public StudentFacade(
            StudentService studentService,
            StudentDtoMapper studentDtoMapper
    ) {
        this.studentService = studentService;
        this.studentDtoMapper = studentDtoMapper;
    }

    public Page<StudentDto> findAll(Pageable pageable) {
        return studentDtoMapper.toDtoPage(studentService.findAll(pageable));
    }

    public List<StudentDto> findByIds(Set<UUID> ids) {
        return studentDtoMapper.toDtoList(studentService.findByIds(ids));
    }

    public StudentDto findById(UUID id) {
        return studentDtoMapper.toDto(studentService.findById(id));
    }

    public StudentDetailedDto findByIdDetailed(UUID id) {
        var student = studentService.findById(id);

        if (student == null) {
            throw new ServiceApiException("Student with id [" + id + "] was not found.");
        }
        try {
            return studentService.studentToDetailed(studentDtoMapper.mapToDetailedDto(studentDtoMapper.toDto(student)));
        } catch (Exception e) {
            throw new ServiceApiException(e.getMessage());
        }
    }

    public StudentDto removeById(UUID id) {
        return studentDtoMapper.toDto(studentService.removeById(id));
    }

    public StudentDto updateStudent(StudentDto student) {
        return studentDtoMapper.toDto(studentService.updateStudent(studentDtoMapper.toStudent(student)));
    }

    public StudentDto registerStudent(UserCreateDto userCreateDto) {
        return studentDtoMapper.toDto(studentService.registerStudent(studentDtoMapper.userCreateDtotoStudent(userCreateDto)));
    }
}
