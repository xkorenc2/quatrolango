package cz.muni.fi.user.data.mapper;

import cz.muni.fi.commons.dto.user.lecturer.LecturerLanguageDto;
import cz.muni.fi.user.data.model.LecturerLanguage;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface LecturerLanguageMapper {
    LecturerLanguageDto mapToDto(LecturerLanguage lecturerLanguage);

}
