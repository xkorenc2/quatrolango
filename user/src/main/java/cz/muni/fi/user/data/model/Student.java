package cz.muni.fi.user.data.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@EqualsAndHashCode(callSuper = false)
@Table(name = "Student")
public class Student extends User {

    @Column(name = "is_active")
    boolean isActive;
}
