package cz.muni.fi.user.data.seeding;

import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import cz.muni.fi.user.data.model.Lecturer;
import cz.muni.fi.user.data.model.LecturerLanguage;
import cz.muni.fi.user.data.model.Student;
import cz.muni.fi.user.data.repository.LecturerLanguageRepository;
import cz.muni.fi.user.data.repository.LecturerRepository;
import cz.muni.fi.user.data.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

@Profile("dev")
@Component
public class UserDataSeeder implements CommandLineRunner {
    private static final String[] firstNames = {"James", "Mary", "John", "Patricia", "Robert", "Jennifer", "Michael", "Linda", "William", "Elizabeth"};
    private static final String[] secondNames = {"Smith", "Johnson", "Williams", "Brown", "Jones", "Garcia", "Miller", "Davis", "Rodriguez", "Martinez"};
    private static final String[] middleNames = {"Allen", "Lee", null, "Marie", "Joseph", "Jean", null, "Lynn", "Andrew", null}; // Includes null for optional middle names
    private final List<LecturerLanguage> languagesC1 = new ArrayList<>();
    private final List<LecturerLanguage> languagesC2 = new ArrayList<>();
    private final LecturerLanguageRepository lecturerLanguageRepository;
    private final StudentRepository studentRepository;
    private final LecturerRepository lecturerRepository;

    @Autowired
    public UserDataSeeder(LecturerLanguageRepository lecturerLanguageRepository,
                          StudentRepository studentRepository,
                          LecturerRepository lecturerRepository) {
        this.lecturerRepository = lecturerRepository;
        this.lecturerLanguageRepository = lecturerLanguageRepository;
        this.studentRepository = studentRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        seedLanguages(); // not actual seeding to repo, just preparing data

        System.out.println("SEEDING STUDENTS");
        seedStudents(50);
        System.out.printf("Created %d students.%n", studentRepository.count());

        System.out.println("SEEDING LECTURERS");
        seedLecturers(10);
        System.out.printf("Created %d lecturers.%n", lecturerRepository.count());
    }

    public void seedLecturers(int lecturerCount) {
        for (int i = 0; i < lecturerCount; i++) {
            createLecturer();
        }
    }

    public void seedStudents(int studentsCount) {
        for (int i = 0; i < studentsCount; i++) {
            createStudent(i % 3 != 0); // make third of students inactive
        }
    }

    public void seedLanguages() {
        for (Language l: Language.values()) {
            LecturerLanguage lectLanguageC1 = new LecturerLanguage();
            lectLanguageC1.setLanguage(l);
            lectLanguageC1.setProficiencyLevel(ProficiencyLevel.C1);

            LecturerLanguage lectLanguageC2 = new LecturerLanguage();
            lectLanguageC2.setLanguage(l);
            lectLanguageC2.setProficiencyLevel(ProficiencyLevel.C2);

            languagesC1.add(lectLanguageC1);
            languagesC2.add(lectLanguageC2);
        }
    }

    public void createLecturer() {
        Lecturer lecturer = new Lecturer();
        lecturer.setId(UUID.randomUUID());
        int nameIdx = ThreadLocalRandom.current().nextInt(firstNames.length);
        lecturer.setFirstName(firstNames[nameIdx]);
        lecturer.setMiddleName(middleNames[nameIdx]); // May be null
        lecturer.setSecondName(secondNames[ThreadLocalRandom.current().nextInt(firstNames.length)]);
        lecturer.setEmail(generateEmail(lecturer.getFirstName(), lecturer.getSecondName()));
        lecturer.setDateOfBirth(generateRandomDateOfBirth());
        lecturer.setNativeSpeaker(ThreadLocalRandom.current().nextBoolean());

        var selectedLanguage1 = languagesC1.get(ThreadLocalRandom.current().nextInt(languagesC1.size()));
        selectedLanguage1.setId(UUID.randomUUID());
        var l1 = lecturerLanguageRepository.save(selectedLanguage1);

        var selectedLanguage2 = languagesC2.get(ThreadLocalRandom.current().nextInt(languagesC2.size()));
        selectedLanguage2.setId(UUID.randomUUID());
        var l2 = lecturerLanguageRepository.save(selectedLanguage2);

        lecturer.setLanguages(List.of(l1, l2));
        lecturerRepository.save(lecturer);
    }

    public void createStudent(boolean active) {
        Student student = new Student();
        int nameIdx = ThreadLocalRandom.current().nextInt(firstNames.length);
        student.setFirstName(firstNames[nameIdx]);
        student.setMiddleName(middleNames[nameIdx]); // May be null
        student.setSecondName(secondNames[ThreadLocalRandom.current().nextInt(firstNames.length)]);
        student.setEmail(generateEmail(student.getFirstName(), student.getSecondName()));
        student.setDateOfBirth(generateRandomDateOfBirth());
        student.setActive(active);

        studentRepository.save(student);
    }

    private static String generateEmail(String firstName, String secondName) {
        String domain = "@gmail.com";
        return firstName.toLowerCase() + "." + secondName.toLowerCase() + domain;
    }

    private static LocalDate generateRandomDateOfBirth() {
        int year = ThreadLocalRandom.current().nextInt(1980, 2003);
        int month = ThreadLocalRandom.current().nextInt(1, 13);
        int day = ThreadLocalRandom.current().nextInt(1, 29); // Simplified to avoid invalid dates
        return LocalDate.of(year, month, day);
    }
}
