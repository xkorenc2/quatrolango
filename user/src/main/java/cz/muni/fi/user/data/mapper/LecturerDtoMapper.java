package cz.muni.fi.user.data.mapper;

import cz.muni.fi.commons.dto.user.lecturer.LecturerDto;
import cz.muni.fi.commons.dto.user.student.LecturerCreateDto;
import cz.muni.fi.user.data.model.Lecturer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LecturerDtoMapper {
    LecturerDto toDto(Lecturer lecturer);

    Lecturer toEntity(LecturerDto lecturerDto);

    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "id", ignore = true)
    Lecturer toEntityFromCreate(LecturerCreateDto lecturerCreateDto);

    List<LecturerDto> toDtoList(List<Lecturer> lecturers);

    default Page<LecturerDto> toDtoPage(Page<Lecturer> lecturers) {
        return new PageImpl<>(toDtoList(lecturers.getContent()), lecturers.getPageable(), lecturers.getTotalPages());
    }
}
