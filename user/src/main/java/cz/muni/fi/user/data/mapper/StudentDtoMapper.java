package cz.muni.fi.user.data.mapper;

import cz.muni.fi.commons.dto.user.student.StudentDetailedDto;
import cz.muni.fi.commons.dto.user.student.StudentDto;
import cz.muni.fi.commons.dto.user.student.UserCreateDto;
import cz.muni.fi.user.data.model.Student;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StudentDtoMapper {
    @Mapping(target = "lectures", ignore = true)
    @Mapping(target = "courses", ignore = true)
    StudentDetailedDto mapToDetailedDto(StudentDto studentDto);

    List<StudentDto> toDtoList(List<Student> students);

    Student toStudent(StudentDto dto);

    default Student userCreateDtotoStudent(UserCreateDto userCreateDto) {
        Student student = new Student();
        student.setDateOfBirth(userCreateDto.getDateOfBirth());
        student.setFirstName(userCreateDto.getFirstName());
        student.setMiddleName(userCreateDto.getMiddleName());
        student.setSecondName(userCreateDto.getSecondName());
        student.setEmail(userCreateDto.getEmail());
        return student;
    }

    StudentDto toDto(Student student);

    default Page<StudentDto> toDtoPage(Page<Student> students) {
        return new PageImpl<>(toDtoList(students.getContent()), students.getPageable(), students.getTotalPages());
    }
}