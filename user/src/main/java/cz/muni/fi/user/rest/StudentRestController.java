package cz.muni.fi.user.rest;

import cz.muni.fi.commons.dto.user.student.StudentDto;
import cz.muni.fi.commons.dto.user.student.UserCreateDto;
import cz.muni.fi.user.facade.StudentFacade;
import io.micrometer.observation.annotation.Observed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@RestController
@Tag(name = "Student service", description = "Service that handles users")
@RequestMapping(path = "/students", produces = MediaType.APPLICATION_JSON_VALUE)
public class StudentRestController {
    private final StudentFacade studentFacade;

    @Autowired
    public StudentRestController(StudentFacade studentFacade) {
        this.studentFacade = studentFacade;
    }

    @Operation(summary = "Get student by id", security = @SecurityRequirement(name = "Bearer"), description = "Fetches a student with the specified ID")
    @GetMapping(path = "/{id}")
    @Observed(name = "findStudentById")
    public ResponseEntity<StudentDto> findById(@PathVariable UUID id) {
        return new ResponseEntity<>(studentFacade.findById(id), HttpStatus.OK);
    }

    @Operation(summary = "Get a detailed view of student by id", security = @SecurityRequirement(name = "Bearer"), description = "Fetches a detailed view of student with the specified ID")
    @GetMapping(path = "/detailed/{id}")
    @Observed(name = "findDetailedStudentById")
    public ResponseEntity<StudentDto> findByIdDetailed(@PathVariable UUID id) {
        return new ResponseEntity<>(studentFacade.findByIdDetailed(id), HttpStatus.OK);
    }

    @Operation(summary = "Gets all students", security = @SecurityRequirement(name = "Bearer"), description = "Fetches all created students")
    @GetMapping(path = "/")
    @Observed(name = "findAllStudents")
    public ResponseEntity<Page<StudentDto>> findAll(Pageable pageable) {
        return new ResponseEntity<>(studentFacade.findAll(pageable), HttpStatus.OK);
    }

    @Operation(summary = "Registers a new student", security = @SecurityRequirement(name = "Bearer"), description = "Registers a new student")
    @PostMapping(path = "/")
    @Observed(name = "registerStudent")
    public ResponseEntity<StudentDto> registerStudent(@RequestBody @Valid UserCreateDto userCreateDto) {
        return new ResponseEntity<>(studentFacade.registerStudent(userCreateDto), HttpStatus.CREATED);
    }

    @Operation(summary = "Get students with the specific Ids", security = @SecurityRequirement(name = "Bearer"), description = "Fetches all students that match the ids provided")
    @PostMapping(path = "/ids")
    @Observed(name = "findStudentsByIds")
    public ResponseEntity<List<StudentDto>> findStudentsByIds(@RequestBody Set<UUID> ids) {
        return new ResponseEntity<>(studentFacade.findByIds(ids), HttpStatus.OK);
    }

    @Operation(summary = "Updates a student", security = @SecurityRequirement(name = "Bearer"), description = "Finds a student with the ID specified and updates it with the data")
    @PutMapping(path = "/{id}")
    @Observed(name = "updateStudent")
    public ResponseEntity<StudentDto> updateStudent(@PathVariable UUID id, @Valid @RequestBody StudentDto studentDto) {
        studentDto.setId(id);
        return ResponseEntity.ok(studentFacade.updateStudent(studentDto));
    }

    @Operation(summary = "Deletes a student", security = @SecurityRequirement(name = "Bearer"), description = "Deletes a student with the specified id")
    @DeleteMapping("/{id}")
    @Observed(name = "deleteStudent")
    public ResponseEntity<StudentDto> deleteStudent(@PathVariable UUID id) {
        return new ResponseEntity<>(studentFacade.removeById(id), HttpStatus.OK);
    }
}
