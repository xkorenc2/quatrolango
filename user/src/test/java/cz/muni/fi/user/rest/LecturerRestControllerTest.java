package cz.muni.fi.user.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.commons.dto.user.lecturer.LecturerDto;
import cz.muni.fi.commons.dto.user.student.LecturerCreateDto;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.user.facade.LecturerFacade;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static cz.muni.fi.user.factory.TestDataFactory.createSampleLecturerCreateDto;
import static cz.muni.fi.user.factory.TestDataFactory.createSampleLecturerDto;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(LecturerRestController.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WithMockUser(username = "admin", authorities = {"SCOPE_test_1", "SCOPE_test_2", "SCOPE_test_3"})
class LecturerRestControllerTest {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    @MockBean
    private LecturerFacade lecturerFacade;
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;

    @BeforeAll
    void setup() {
        objectMapper.findAndRegisterModules();
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    void findAll_lecturersFound_isOk() throws Exception {
        LecturerDto lecturerDto1 = createSampleLecturerDto();
        LecturerDto lecturerDto2 = createSampleLecturerDto();

        List<LecturerDto> lecturerDtoList = new ArrayList<>();
        lecturerDtoList.add(lecturerDto1);
        lecturerDtoList.add(lecturerDto2);
        when(lecturerFacade.findAll(any(Pageable.class))).thenReturn(new PageImpl<>(lecturerDtoList));

        mockMvc.perform(get("/lecturers/").with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void findAll_findsNothing_isOk() throws Exception {
        when(lecturerFacade.findAll(any(Pageable.class))).thenReturn(new PageImpl<>(new ArrayList<>()));

        mockMvc.perform(get("/lecturers/").with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void findByLanguage_lecturersFound_isOk() throws Exception {
        String language = "ENGLISH";
        LecturerDto lecturerDto = createSampleLecturerDto();
        List<LecturerDto> lecturerDtoList = Collections.singletonList(lecturerDto);
        when(lecturerFacade.findByLanguage(Language.valueOf(language))).thenReturn(lecturerDtoList);

        mockMvc.perform(get("/lecturers/language/{language}", language).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].id").exists());
    }

    @Test
    void findByLanguage_findsNothing_returnsEmptyList() throws Exception {
        Language language = Language.ENGLISH;

        when(lecturerFacade.findByLanguage(language)).thenReturn(new ArrayList<>());

        mockMvc.perform(MockMvcRequestBuilders.get("/lecturers/language/{language}", language).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void findByIds_findsNothing_returnsEmptyList() throws Exception {
        when(lecturerFacade.findByIds(new ArrayList<>())).thenReturn(new ArrayList<>());

        mockMvc.perform(MockMvcRequestBuilders.post("/lecturers/ids").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(new ArrayList<>())))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void findByIds_lecturersFound_isOk() throws Exception {
        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();

        LecturerDto lecturerDto1 = createSampleLecturerDto();
        lecturerDto1.setId(id1);
        LecturerDto lecturerDto2 = createSampleLecturerDto();
        lecturerDto2.setId(id2);

        List<UUID> ids = Arrays.asList(id1, id2);
        List<LecturerDto> lecturerDtoList = new ArrayList<>();
        lecturerDtoList.add(lecturerDto1);
        lecturerDtoList.add(lecturerDto2);
        when(lecturerFacade.findByIds(ids)).thenReturn(lecturerDtoList);

        mockMvc.perform(post("/lecturers/ids").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(ids)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].id").exists());
    }

    @Test
    void findByEmail_lecturersFound_isOk() throws Exception {
        String email = "test@example.com";
        LecturerDto lecturerDto = createSampleLecturerDto();
        when(lecturerFacade.findByEmail(email)).thenReturn(lecturerDto);

        mockMvc.perform(get("/lecturers/email/{email}", email).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    void findById_lecturerFound_isOk() throws Exception {
        UUID id = UUID.randomUUID();
        LecturerDto lecturerDto = createSampleLecturerDto();
        when(lecturerFacade.findById(id)).thenReturn(lecturerDto);

        mockMvc.perform(get("/lecturers/{id}", id).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    void create_lecturerCreated_returnsCreated() throws Exception {
        LecturerDto lecturerDto = createSampleLecturerDto();
        LecturerCreateDto lecturerCreateDto = createSampleLecturerCreateDto();
        when(lecturerFacade.create(lecturerCreateDto)).thenReturn(lecturerDto);

        mockMvc.perform(post("/lecturers/").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(lecturerDto)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    void update_lecturerUpdated_returnsLecturerDto() throws Exception {
        UUID id = UUID.randomUUID();
        LecturerDto lecturerDto = createSampleLecturerDto();
        LecturerCreateDto lecturerCreateDto = createSampleLecturerCreateDto();
        lecturerDto.setId(id);
        when(lecturerFacade.update(lecturerCreateDto, id)).thenReturn(lecturerDto);

        mockMvc.perform(put("/lecturers/{id}", id).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(lecturerDto)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(id.toString()));
    }

    @Test
    void delete_deletedLecturer_returnsLecturerDto() throws Exception {
        LecturerDto lecturerDto = createSampleLecturerDto();
        UUID id = lecturerDto.getId();

        when(lecturerFacade.remove(id)).thenReturn(lecturerDto);

        mockMvc.perform(delete("/lecturers/{id}", id).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(id.toString()));
    }

    @Test
    void delete_nonExistingLecturer_returnsNotFound() throws Exception {
        UUID nonExistingId = UUID.randomUUID();

        when(lecturerFacade.remove(nonExistingId)).thenReturn(null);

        mockMvc.perform(MockMvcRequestBuilders.delete("/lecturers/{id}", nonExistingId).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void create_exceptionOccurs_returnsBadRequest() throws Exception {
        LecturerCreateDto lecturerCreateDto = createSampleLecturerCreateDto();
        doThrow(RuntimeException.class).when(lecturerFacade).create(lecturerCreateDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/lecturers/").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(lecturerCreateDto)))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }
}
