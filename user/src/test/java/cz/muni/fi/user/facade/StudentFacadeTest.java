package cz.muni.fi.user.facade;

import cz.muni.fi.commons.dto.user.student.StudentDetailedDto;
import cz.muni.fi.commons.dto.user.student.StudentDto;
import cz.muni.fi.commons.dto.user.student.UserCreateDto;
import cz.muni.fi.commons.exceptions.ServiceApiException;
import cz.muni.fi.user.data.mapper.StudentDtoMapper;
import cz.muni.fi.user.data.model.Student;
import cz.muni.fi.user.service.StudentService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static cz.muni.fi.user.factory.TestDataFactory.createSampleStudent;
import static cz.muni.fi.user.factory.TestDataFactory.createSampleStudentDetailedDto;
import static cz.muni.fi.user.factory.TestDataFactory.createSampleStudentDto;
import static cz.muni.fi.user.factory.TestDataFactory.createSampleStudentDtoFromStudent;
import static cz.muni.fi.user.factory.TestDataFactory.createSampleUserCreateDto;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StudentFacadeTest {
    @Mock
    private StudentService studentService;
    @Mock
    private StudentDtoMapper studentMapper;
    @InjectMocks
    private StudentFacade studentFacade;

    @Test
    void findAll_existingStudents_returnListOfStudentDto() {
        List<Student> students = List.of(createSampleStudent(), createSampleStudent());
        List<StudentDto> expectedStudents = new ArrayList<>();
        Pageable pageable = Pageable.unpaged();

        for (Student student : students) {
            var studentDto = createSampleStudentDtoFromStudent(student);
            expectedStudents.add(studentDto);
        }

        Page<Student> lecturerPage = new PageImpl<>(students);
        Page<StudentDto> lecturerDtos = new PageImpl<>(expectedStudents);

        when(studentService.findAll(pageable)).thenReturn(lecturerPage);
        when(studentMapper.toDtoPage(lecturerPage)).thenReturn(lecturerDtos);

        var result = studentFacade.findAll(pageable);
        assertEquals(lecturerDtos, result);
    }


    @Test
    void findById_existingStudent_returnStudentDto() {
        Student student = createSampleStudent();
        StudentDto expectedStudent = createSampleStudentDtoFromStudent(student);

        when(studentService.findById(student.getId())).thenReturn(student);
        when(studentMapper.toDto(student)).thenReturn(expectedStudent);

        assertEquals(expectedStudent, studentFacade.findById(student.getId()));
    }

    @Test
    void findById_nonExisting_returnNull() {
        UUID id = UUID.randomUUID();

        when(studentService.findById(id)).thenReturn(null);

        assertNull(studentFacade.findById(id));
    }

    @Test
    void findByIds_existingStudents_returnListOfStudentDto() {
        List<Student> students = List.of(createSampleStudent(), createSampleStudent());
        List<StudentDto> expectedStudents = new ArrayList<>();
        for (Student student : students) {
            expectedStudents.add(createSampleStudentDtoFromStudent(student));
        }

        when(studentService.findByIds(Set.of(students.get(0).getId(), students.get(1).getId()))).thenReturn(students);
        when(studentMapper.toDtoList(students)).thenReturn(expectedStudents);

        assertEquals(expectedStudents, studentFacade.findByIds(Set.of(students.get(0).getId(), students.get(1).getId())));
    }

    @Test
    void findByIds_nonExisting_returnEmptyList() {
        when(studentService.findByIds(any())).thenReturn(new ArrayList<>());

        assertEquals(new ArrayList<>(), studentFacade.findByIds(Set.of(UUID.randomUUID(), UUID.randomUUID())));
    }

    @Test
    void removeById_existingStudent_returnStudentDto() {
        Student student = createSampleStudent();
        StudentDto expectedStudent = createSampleStudentDtoFromStudent(student);

        when(studentService.removeById(student.getId())).thenReturn(student);
        when(studentMapper.toDto(student)).thenReturn(expectedStudent);

        assertEquals(expectedStudent, studentFacade.removeById(student.getId()));
    }

    @Test
    void removeById_nonExisting_returnNull() {
        UUID id = UUID.randomUUID();

        when(studentService.removeById(id)).thenReturn(null);

        assertNull(studentFacade.removeById(id));
    }

    @Test
    void updateStudent_existingStudent_returnStudentDto() {
        StudentDto studentDto = createSampleStudentDto();

        Student expectedStudent = createSampleStudent();

        when(studentService.updateStudent(any())).thenReturn(expectedStudent);
        when(studentMapper.toDto(any())).thenReturn(studentDto);

        StudentDto result = studentFacade.updateStudent(studentDto);

        assertEquals(studentDto, result);
    }

    @Test
    void updateStudent_nonExistingStudent_returnNull() {
        Student student = createSampleStudent();
        StudentDto expectedStudent = createSampleStudentDtoFromStudent(student);

        when(studentService.updateStudent(any())).thenReturn(null);

        assertNull(studentFacade.updateStudent(expectedStudent));
    }

    @Test
    void createStudent_newStudent_returnStudentDto() {
        UserCreateDto userCreateDto = createSampleUserCreateDto();
        Student student = createSampleStudent();
        StudentDto studentDto = createSampleStudentDtoFromStudent(student);

        when(studentMapper.userCreateDtotoStudent(userCreateDto)).thenReturn(student);
        when(studentService.registerStudent(any())).thenReturn(student);
        when(studentMapper.toDto(student)).thenReturn(studentDto);

        assertEquals(studentDto, studentFacade.registerStudent(userCreateDto));
    }

    @Test
    void createStudent_existingStudent_returnNull() {
        UserCreateDto userCreateDto = createSampleUserCreateDto();

        when(studentService.registerStudent(any())).thenReturn(null);

        assertNull(studentFacade.registerStudent(userCreateDto));
    }

    @Test
    void createStudent_nonExistingStudent_returnNull() {
        UserCreateDto userCreateDto = createSampleUserCreateDto();

        when(studentService.registerStudent(any())).thenReturn(null);

        assertNull(studentFacade.registerStudent(userCreateDto));
    }

    @Test
    void findByIdDetailed_existingStudent_returnStudentDetailedDto() {
        StudentDto studentDto = createSampleStudentDto();
        Student student = createSampleStudent();
        StudentDetailedDto expectedStudentDto = createSampleStudentDetailedDto();
        expectedStudentDto.setId(studentDto.getId());

        when(studentService.findById(studentDto.getId())).thenReturn(student);
        when(studentService.studentToDetailed(expectedStudentDto)).thenReturn(expectedStudentDto);
        when(studentMapper.mapToDetailedDto(any())).thenReturn(expectedStudentDto);

        assertEquals(expectedStudentDto, studentFacade.findByIdDetailed(studentDto.getId()));
    }

    @Test
    void findByIdDetailed_nonExisting_returnNull() {
        UUID id = UUID.randomUUID();

        when(studentService.findById(id)).thenReturn(null);

        assertThrows(ServiceApiException.class, () -> studentFacade.findByIdDetailed(id));
    }

    @Test
    void findByIdDetailed_studentNotFound_throwServiceApiException() {
        UUID studentId = UUID.randomUUID();
        when(studentService.findById(studentId)).thenReturn(null);

        assertThrows(ServiceApiException.class, () -> studentFacade.findByIdDetailed(studentId));
    }

    @Test
    void findByIdDetailed_mappingError_throwServiceApiException() {
        UUID studentId = UUID.randomUUID();
        Student student = new Student();
        when(studentService.findById(studentId)).thenReturn(student);

        when(studentMapper.mapToDetailedDto(studentMapper.toDto(student))).thenThrow(new RuntimeException("Mapping error"));

        assertThrows(ServiceApiException.class, () -> studentFacade.findByIdDetailed(studentId));
    }
}

