package cz.muni.fi.user.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.commons.dto.user.student.StudentDetailedDto;
import cz.muni.fi.commons.dto.user.student.StudentDto;
import cz.muni.fi.commons.dto.user.student.UserCreateDto;
import cz.muni.fi.commons.exceptions.ResourceNotFoundException;
import cz.muni.fi.user.facade.StudentFacade;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static cz.muni.fi.user.factory.TestDataFactory.createSampleStudentDetailedDto;
import static cz.muni.fi.user.factory.TestDataFactory.createSampleStudentDto;
import static cz.muni.fi.user.factory.TestDataFactory.createSampleUserCreateDto;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@WebMvcTest(StudentRestController.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WithMockUser(username = "admin", authorities = {"SCOPE_test_1", "SCOPE_test_2", "SCOPE_test_3"})
class StudentRestControllerTest {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    @MockBean
    private StudentFacade studentFacade;
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;

    @BeforeAll
    void setup() {
        objectMapper.findAndRegisterModules();
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    void findAll_studentsFound_studentsReturned() throws Exception {
        List<StudentDto> studentDtos = new ArrayList<>();
        studentDtos.add(createSampleStudentDto());

        when(studentFacade.findAll(Pageable.unpaged())).thenReturn(new PageImpl<>(studentDtos));

        mockMvc.perform(MockMvcRequestBuilders.get("/students/").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void findById_studentFound_studentReturned() throws Exception {
        StudentDto studentDto = createSampleStudentDto();

        when(studentFacade.findById(studentDto.getId())).thenReturn(studentDto);

        mockMvc.perform(MockMvcRequestBuilders.get("/students/" + studentDto.getId()).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void findById_studentNotFound_notFoundReturned() throws Exception {
        UUID id = UUID.randomUUID();

        when(studentFacade.findById(id)).thenThrow(new ResourceNotFoundException("Student with id [" + id + "] was not found."));

        mockMvc.perform(MockMvcRequestBuilders.get("/students/" + id).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }


    @Test
    void registerStudent_studentCreated_studentReturned() throws Exception {
        UserCreateDto userCreateDto = createSampleUserCreateDto();
        StudentDto studentDto = createSampleStudentDto();

        when(studentFacade.registerStudent(userCreateDto)).thenReturn(studentDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/students/").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(studentDto)))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    void updateStudent_studentUpdated_studentReturned() throws Exception {
        StudentDto studentDto = createSampleStudentDto();

        when(studentFacade.updateStudent(studentDto)).thenReturn(studentDto);

        mockMvc.perform(MockMvcRequestBuilders.put("/students/" + studentDto.getId()).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(studentDto)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void updateStudent_studentNotFound_notFoundReturned() throws Exception {
        StudentDto studentDto = createSampleStudentDto();
        UUID studentId = UUID.randomUUID();
        studentDto.setId(studentId);

        when(studentFacade.updateStudent(studentDto)).thenThrow(new ResourceNotFoundException("Student with id [" + studentDto.getId() + "] was not found."));

        mockMvc.perform(MockMvcRequestBuilders.put("/students/" + studentId).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(studentDto)))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void deleteStudent_studentDeleted_studentReturned() throws Exception {
        StudentDto studentDto = createSampleStudentDto();

        when(studentFacade.removeById(studentDto.getId())).thenReturn(studentDto);

        mockMvc.perform(MockMvcRequestBuilders.delete("/students/" + studentDto.getId()).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void deleteStudent_studentNotFound_notFoundReturned() throws Exception {
        UUID id = UUID.randomUUID();

        when(studentFacade.removeById(id)).thenThrow(new ResourceNotFoundException("Student with id [" + id + "] was not present."));

        mockMvc.perform(MockMvcRequestBuilders.delete("/students/" + id).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void findStudentsByIds_studentsFound_studentsReturned() throws Exception {
        List<StudentDto> studentDtos = new ArrayList<>();
        studentDtos.add(createSampleStudentDto());

        when(studentFacade.findByIds(Set.of(UUID.randomUUID()))).thenReturn(studentDtos);

        mockMvc.perform(MockMvcRequestBuilders.post("/students/ids").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(Set.of(UUID.randomUUID())))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void findStudentsByIds_studentsNotFound_notFoundReturned() throws Exception {
        UUID id = UUID.randomUUID();

        when(studentFacade.findByIds(Set.of(id))).thenThrow(new ResourceNotFoundException("Student with id [" + id + "] was not found."));

        mockMvc.perform(MockMvcRequestBuilders.post("/students/ids").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(Set.of(id)))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void updateStudent_unexpectedException_badRequestReturned() throws Exception {
        StudentDto studentDto = createSampleStudentDto();
        UUID studentId = UUID.randomUUID();
        studentDto.setId(studentId);

        doThrow(new RuntimeException("Unexpected error occurred"))
                .when(studentFacade).updateStudent(studentDto);

        mockMvc.perform(MockMvcRequestBuilders.put("/students/" + studentId).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(studentDto)))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }

    @Test
    void findStudentsDetailedById_studentsFound_studentsReturned() throws Exception {
        StudentDetailedDto studentDto = createSampleStudentDetailedDto();
        studentDto.setId(UUID.randomUUID());

        when(studentFacade.findById(UUID.randomUUID())).thenReturn(studentDto);

        mockMvc.perform(MockMvcRequestBuilders.get("/students/detailed/" + studentDto.getId()).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(studentDto.getId()))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void findStudentsDetailedById_studentsNotFound_notFoundReturned() throws Exception {
        UUID id = UUID.randomUUID();

        when(studentFacade.findByIdDetailed(id)).thenThrow(new ResourceNotFoundException("Student with id [" + id + "] was not found."));

        mockMvc.perform(MockMvcRequestBuilders.get("/students/detailed/" + id).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(id))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

}