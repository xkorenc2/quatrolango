package cz.muni.fi.user.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.user.data.mapper.LecturerDtoMapper;
import cz.muni.fi.user.data.model.Lecturer;
import cz.muni.fi.user.data.repository.LecturerRepository;
import cz.muni.fi.user.factory.TestDataFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;
import java.util.UUID;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WithMockUser(username = "admin", authorities = {"SCOPE_test_1", "SCOPE_test_2", "SCOPE_test_3"})
public class LecturerRestControllerIT {
    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private LecturerRepository lecturerRepository;
    @Autowired
    private LecturerDtoMapper lecturerDtoMapper;
    private Lecturer lecturer;

    @BeforeAll
    void setup() {
        objectMapper.findAndRegisterModules();
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @BeforeEach
    void beforeEach() {
        lecturerRepository.deleteAll();
        lecturer = lecturerRepository.save(TestDataFactory.createSampleLecturer());
    }

    @Test
    void findAll_returnsListOfLecturers() throws Exception {
        mockMvc.perform(get("/lecturers/").with(csrf()))
                .andExpect(status().isOk());
    }

    @Test
    void findById_returnsLecturer() throws Exception {
        mockMvc.perform(get("/lecturers/{id}", lecturer.getId()).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(lecturerDtoMapper.toDto(lecturer))));
    }

    @Test
    void findById_notFound_returns404() throws Exception {
        mockMvc.perform(get("/lecturers/{id}", UUID.randomUUID()).with(csrf()))
                .andExpect(status().isNotFound());
    }

    @Test
    void create_validLecturer_returnsCreated() throws Exception {
        mockMvc.perform(post("/lecturers/").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(lecturerDtoMapper.toDto(lecturer))))
                .andExpect(status().isCreated());
    }

    @Test
    void update_existingIdAndValidDto_returnsOk() throws Exception {
        mockMvc.perform(put("/lecturers/{id}", UUID.randomUUID()).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(lecturerDtoMapper.toDto(lecturer))))
                .andExpect(status().isOk());
    }

    @Test
    void delete_existingId_returnsOk() throws Exception {
        mockMvc.perform(delete("/lecturers/{id}", lecturer.getId()).with(csrf()))
                .andExpect(status().isOk());
    }

    @Test
    void delete_nonExistingId_returns404() throws Exception {
        mockMvc.perform(delete("/lecturers/{id}", UUID.randomUUID()).with(csrf()))
                .andExpect(status().isNotFound());
    }

    @Test
    void findByLanguage_returnsListOfLecturers() throws Exception {
        mockMvc.perform(get("/lecturers/language/ENGLISH").with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(Collections.singletonList(lecturerDtoMapper.toDto(lecturer)))));
    }

    @Test
    void findByIds_returnsListOfLecturers() throws Exception {
        mockMvc.perform(post("/lecturers/ids").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(Collections.singletonList(lecturer.getId()))))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(Collections.singletonList(lecturerDtoMapper.toDto(lecturer)))));
    }

    @Test
    void findByEmail_returnsLecturer() throws Exception {
        mockMvc.perform(get("/lecturers/email/{email}", lecturer.getEmail()).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(lecturerDtoMapper.toDto(lecturer))));
    }

}
