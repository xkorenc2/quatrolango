package cz.muni.fi.user.factory;

import cz.muni.fi.commons.dto.exercise.ExerciseDto;
import cz.muni.fi.commons.dto.lecture.LectureDto;
import cz.muni.fi.commons.dto.user.lecturer.LecturerDto;
import cz.muni.fi.commons.dto.user.lecturer.LecturerLanguageCreateDto;
import cz.muni.fi.commons.dto.user.lecturer.LecturerLanguageDto;
import cz.muni.fi.commons.dto.course.CourseDetailedDto;
import cz.muni.fi.commons.dto.course.CourseDto;
import cz.muni.fi.commons.dto.user.student.LecturerCreateDto;
import cz.muni.fi.commons.dto.user.student.StudentDetailedDto;
import cz.muni.fi.commons.dto.user.student.StudentDto;
import cz.muni.fi.commons.dto.user.student.UserCreateDto;
import cz.muni.fi.commons.enums.ExerciseDifficulty;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import cz.muni.fi.user.data.model.Lecturer;
import cz.muni.fi.user.data.model.LecturerLanguage;
import cz.muni.fi.user.data.model.Student;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class TestDataFactory {

    private static final UUID USER_UUID = UUID.randomUUID();

    private TestDataFactory() {
    }


    public static UUID getExistingUUID() {
        return UUID.fromString("aed7bf53-2047-4090-9b54-321c5527862a");
    }

    public static UserCreateDto createSampleUserCreateDto() {
        UserCreateDto userCreateDto = new UserCreateDto();
        userCreateDto.setFirstName("John");
        userCreateDto.setSecondName("Doe");
        userCreateDto.setEmail("test@gmail.com");
        userCreateDto.setDateOfBirth(LocalDate.of(2000, 1, 1));
        return userCreateDto;
    }

    public static CourseDetailedDto getCourseDetailedDtoFactory() {
        CourseDetailedDto courseDto = new CourseDetailedDto();
        courseDto.setId(USER_UUID);
        courseDto.setName("English course");
        courseDto.setProficiencyLevel(ProficiencyLevel.A1);
        courseDto.setCapacity(20);
        courseDto.setLanguage(Language.ENGLISH);

        return courseDto;
    }

    public static List<CourseDto> createSampleListOfCourseDtos() {
        List<CourseDto> courseDtos = new ArrayList<>();
        CourseDto courseDto = new CourseDto();
        courseDto.setId(UUID.randomUUID());
        courseDto.setName("English course");
        courseDto.setProficiencyLevel(ProficiencyLevel.A1);
        courseDto.setCapacity(20);
        courseDto.setLanguage(Language.ENGLISH);


        CourseDto courseDto2 = new CourseDto();
        courseDto2.setId(UUID.randomUUID());
        courseDto2.setName("German course");
        courseDto2.setProficiencyLevel(ProficiencyLevel.B1);
        courseDto2.setCapacity(40);
        courseDto2.setLanguage(Language.GERMAN);

        courseDtos.add(courseDto);
        courseDtos.add(courseDto2);

        return courseDtos;
    }

    public static StudentDto createSampleStudentDto() {
        StudentDto student = new StudentDto();
        student.setId(UUID.randomUUID());
        student.setFirstName("John");
        student.setSecondName("Doe");
        student.setEmail("test@gmail.com");
        student.setDateOfBirth(LocalDate.of(2000, 1, 1));
        return student;
    }

    public static UserCreateDto createSampleCreateStudentDto() {
        UserCreateDto student = new UserCreateDto();
        student.setFirstName("John");
        student.setSecondName("Doe");
        student.setEmail("test@gmail.com");
        student.setMiddleName("ferko");
        student.setDateOfBirth(LocalDate.of(2000, 1, 1));
        return student;
    }

    public static LectureDto createSampleLectureDto() {
        LectureDto lectureDto = new LectureDto();
        lectureDto.setName("Sample Lecture");
        lectureDto.setDescription("This is a sample lecture description.");
        lectureDto.setStartTime(LocalDateTime.now());
        lectureDto.setEndTime(LocalDateTime.now().plusHours(1));
        lectureDto.setCourseId(UUID.randomUUID());
        lectureDto.setLecturerId(UUID.randomUUID());
        lectureDto.setCapacity(50);
        return lectureDto;
    }

    public static Student createSampleStudent() {
        Student student = new Student();
        student.setId(UUID.randomUUID());
        student.setFirstName("John");
        student.setSecondName("Doe");
        student.setCreatedAt(LocalDateTime.now());
        student.setUpdatedAt(LocalDateTime.now());
        student.setActive(true);
        student.setDateOfBirth(LocalDate.ofYearDay(2000, 5));
        student.setEmail("John@Mail.com");
        return student;
    }

    public static StudentDto createSampleStudentDtoFromStudent(Student student) {
        StudentDto studentDto = new StudentDto();
        studentDto.setId(student.getId());
        studentDto.setFirstName(student.getFirstName());
        studentDto.setSecondName(student.getSecondName());
        return studentDto;
    }

    public static StudentDetailedDto createSampleStudentDetailedDto() {
        StudentDetailedDto student = new StudentDetailedDto();
        student.setFirstName("John");
        student.setSecondName("Doe");
        Set<LectureDto> lecturesList = Set.of(createSampleLectureDto());
        student.setLectures(lecturesList);
        Set<CourseDto> coursesList = Set.of(createSampleCourseDto());
        student.setCourses(coursesList);
        return student;
    }

    public static CourseDto createSampleCourseDto() {
        CourseDto courseDto = new CourseDto();
        courseDto.setId(USER_UUID);
        courseDto.setName("English course");
        courseDto.setProficiencyLevel(ProficiencyLevel.A1);
        courseDto.setCapacity(20);
        courseDto.setLanguage(Language.ENGLISH);
        return courseDto;
    }

    public static Lecturer createSampleLecturer() {
        Lecturer lecturer = new Lecturer();
        lecturer.setId(UUID.randomUUID());
        lecturer.setFirstName("John");
        lecturer.setSecondName("Doe");
        lecturer.setMiddleName("Middle");
        lecturer.setEmail("john.doe@example.com");
        lecturer.setDateOfBirth(LocalDate.of(1990, 1, 1));
        lecturer.setCreatedAt(LocalDateTime.now());
        lecturer.setUpdatedAt(LocalDateTime.now());
        lecturer.setNativeSpeaker(true);

        List<LecturerLanguage> languages = new ArrayList<>();
        LecturerLanguage language = new LecturerLanguage();
        language.setLanguage(Language.ENGLISH);
        language.setProficiencyLevel(ProficiencyLevel.A1);
        languages.add(language);
        lecturer.setLanguages(languages);

        return lecturer;
    }

    public static LecturerDto createSampleLecturerDto() {
        LecturerDto lecturerDto = new LecturerDto();
        lecturerDto.setId(UUID.randomUUID());
        lecturerDto.setFirstName("John");
        lecturerDto.setSecondName("Doe");
        lecturerDto.setMiddleName("Middle");
        lecturerDto.setEmail("john.doe@example.com");
        lecturerDto.setDateOfBirth(LocalDate.of(1990, 1, 1));
        lecturerDto.setCreatedAt(LocalDateTime.now());
        lecturerDto.setUpdatedAt(LocalDateTime.now());
        lecturerDto.setNativeSpeaker(true);

        List<LecturerLanguageDto> languages = new ArrayList<>();
        LecturerLanguageDto language = new LecturerLanguageDto();
        language.setLanguage(Language.ENGLISH);
        language.setProficiencyLevel(ProficiencyLevel.A1);
        languages.add(language);
        lecturerDto.setLanguages(languages);
        return lecturerDto;
    }

    public static LecturerCreateDto createSampleLecturerCreateDto() {
        LecturerCreateDto lecturerDto = new LecturerCreateDto();
        lecturerDto.setFirstName("John");
        lecturerDto.setSecondName("Doe");
        lecturerDto.setMiddleName("Middle");
        lecturerDto.setEmail("john.doe@example.com");
        lecturerDto.setDateOfBirth(LocalDate.of(1990, 1, 1));
        lecturerDto.setNativeSpeaker(true);
        List<LecturerLanguageCreateDto> languages = new ArrayList<>();
        LecturerLanguageCreateDto language = new LecturerLanguageCreateDto();
        language.setLanguage(Language.ENGLISH);
        language.setProficiencyLevel(ProficiencyLevel.A1);
        languages.add(language);
        lecturerDto.setLanguages(languages);
        return lecturerDto;
    }

    public static ExerciseDto createSampleExerciseDto() {
        ExerciseDto eDto = new ExerciseDto();
        eDto.setId(UUID.randomUUID());
        eDto.setCourseId(UUID.randomUUID());
        eDto.setName("Sample Exercise");
        eDto.setDescription("This is a sample exercise description.");
        eDto.setExerciseDifficulty(ExerciseDifficulty.MEDIUM);
        return eDto;
    }

    public static CourseDetailedDto createSampleCourseDetailedViewDto() {
        CourseDetailedDto cd = new CourseDetailedDto();
        cd.setId(UUID.randomUUID());
        cd.setName("English course");
        cd.setProficiencyLevel(ProficiencyLevel.A1);
        cd.setCapacity(20);
        cd.setLanguage(Language.ENGLISH);
        cd.setStudents(new ArrayList<>());
        cd.setExercises(new ArrayList<>());
        cd.setLectures(new ArrayList<>());
        return cd;
    }

    public static Set<ExerciseDto> getExerciseDtos() {
        ExerciseDto exercise1 = createSampleExerciseDto();
        ExerciseDto exercise2 = createSampleExerciseDto();
        ExerciseDto exercise3 = createSampleExerciseDto();

        return Set.of(exercise1, exercise2, exercise3);
    }
}
