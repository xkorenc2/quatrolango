package cz.muni.fi.user.facade;

import cz.muni.fi.commons.dto.user.lecturer.LecturerDto;
import cz.muni.fi.commons.dto.user.student.LecturerCreateDto;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.user.data.mapper.LecturerDtoMapper;
import cz.muni.fi.user.data.model.Lecturer;
import cz.muni.fi.user.service.LecturerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static cz.muni.fi.user.factory.TestDataFactory.createSampleLecturer;
import static cz.muni.fi.user.factory.TestDataFactory.createSampleLecturerCreateDto;
import static cz.muni.fi.user.factory.TestDataFactory.createSampleLecturerDto;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LecturerFacadeTest {

    @Mock
    private LecturerService lecturerService;

    @Mock
    private LecturerDtoMapper lecturerMapper;

    @InjectMocks
    private LecturerFacade lecturerFacade;

    @Test
    void findById_existingId_returnsLecturerDto() {
        UUID existingId = UUID.randomUUID();
        Lecturer expectedLecturer = createSampleLecturer();
        expectedLecturer.setId(existingId);

        when(lecturerService.findById(existingId)).thenReturn(expectedLecturer);

        LecturerDto result = lecturerFacade.findById(existingId);

        assertEquals(lecturerMapper.toDto(expectedLecturer), result);
    }

    @Test
    void findAll_existingLecturers_returnsListOfLecturerDtos() {
        List<Lecturer> existingLecturers = new ArrayList<>();
        existingLecturers.add(createSampleLecturer());
        existingLecturers.add(createSampleLecturer());
        Pageable pageable = Pageable.unpaged();

        List<LecturerDto> expectedLecturerDtos = new ArrayList<>();
        for (Lecturer lecturer : existingLecturers) {
            expectedLecturerDtos.add(lecturerMapper.toDto(lecturer));
        }

        Page<Lecturer> lecturerPage = new PageImpl<>(existingLecturers);
        Page<LecturerDto> lecturerDtos = new PageImpl<>(expectedLecturerDtos);

        when(lecturerService.findAll(pageable)).thenReturn(lecturerPage);
        when(lecturerMapper.toDtoPage(lecturerPage)).thenReturn(lecturerDtos);

        Page<LecturerDto> result = lecturerFacade.findAll(pageable);

        assertEquals(lecturerDtos, result);
    }

    @Test
    void create_validLecturerDto_returnsCreatedLecturerDto() {
        LecturerCreateDto lecturerCreateDto = createSampleLecturerCreateDto();
        LecturerDto lecturerDto = createSampleLecturerDto();

        Lecturer expectedLecturer = lecturerMapper.toEntity(lecturerDto);

        when(lecturerService.create(expectedLecturer)).thenReturn(expectedLecturer);
        when(lecturerMapper.toDto(expectedLecturer)).thenReturn(lecturerDto);

        LecturerDto result = lecturerFacade.create(lecturerCreateDto);

        assertEquals(lecturerDto, result);
    }

    @Test
    void update_validLecturerDto_returnsUpdatedLecturerDto() {
        LecturerCreateDto lecturerCreateDto = createSampleLecturerCreateDto();
        LecturerDto lecturerDto = createSampleLecturerDto();
        Lecturer expectedLecturer = lecturerMapper.toEntityFromCreate(lecturerCreateDto);

        UUID id = UUID.randomUUID();
        when(lecturerService.update(expectedLecturer, id)).thenReturn(expectedLecturer);
        when(lecturerMapper.toDto(expectedLecturer)).thenReturn(lecturerDto);

        LecturerDto result = lecturerFacade.update(lecturerCreateDto, id);
        assertEquals(lecturerDto, result);
    }

    @Test
    void findById_nonExistingId_returnsNull() {
        UUID nonExistingId = UUID.randomUUID();

        when(lecturerService.findById(nonExistingId)).thenReturn(null);

        LecturerDto result = lecturerFacade.findById(nonExistingId);

        assertNull(result);
    }

    @Test
    void findByLanguage_existingLanguage_returnsListOfLecturerDtos() {
        Language language = Language.ENGLISH;
        List<Lecturer> existingLecturers = new ArrayList<>();
        existingLecturers.add(createSampleLecturer());
        existingLecturers.add(createSampleLecturer());

        List<LecturerDto> expectedLecturerDtos = new ArrayList<>();
        for (Lecturer lecturer : existingLecturers) {
            expectedLecturerDtos.add(lecturerMapper.toDto(lecturer));
        }

        when(lecturerService.findByLanguage(language)).thenReturn(existingLecturers);
        when(lecturerMapper.toDtoList(existingLecturers)).thenReturn(expectedLecturerDtos);

        List<LecturerDto> result = lecturerFacade.findByLanguage(language);

        assertEquals(expectedLecturerDtos, result);
    }

    @Test
    void findByIds_existingIds_returnsListOfLecturerDtos() {
        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();
        List<UUID> ids = Arrays.asList(id1, id2);
        List<Lecturer> existingLecturers = new ArrayList<>();
        Lecturer lecturer1 = createSampleLecturer();
        Lecturer lecturer2 = createSampleLecturer();
        lecturer1.setId(id1);
        lecturer2.setId(id2);
        existingLecturers.add(lecturer1);
        existingLecturers.add(lecturer2);

        List<LecturerDto> expectedLecturerDtos = new ArrayList<>();
        for (Lecturer lecturer : existingLecturers) {
            expectedLecturerDtos.add(lecturerMapper.toDto(lecturer));
        }

        when(lecturerService.findByIds(ids)).thenReturn(existingLecturers);
        when(lecturerMapper.toDtoList(existingLecturers)).thenReturn(expectedLecturerDtos);

        List<LecturerDto> result = lecturerFacade.findByIds(ids);

        assertEquals(expectedLecturerDtos, result);
    }

    @Test
    void findByEmail_existingEmail_returnsLecturerDto() {
        String email = "test@example.com";
        Lecturer lecturer = createSampleLecturer();
        LecturerDto expectedLecturerDto = lecturerMapper.toDto(lecturer);

        when(lecturerService.findByEmail(email)).thenReturn(lecturer);

        LecturerDto result = lecturerFacade.findByEmail(email);

        assertEquals(expectedLecturerDto, result);
    }

    @Test
    void findByEmail_nonExistingEmail_returnsNull() {
        String nonExistingEmail = "nonexisting@example.com";

        when(lecturerService.findByEmail(nonExistingEmail)).thenReturn(null);

        LecturerDto result = lecturerFacade.findByEmail(nonExistingEmail);

        assertNull(result);
    }

    @Test
    void remove_existingId_callsServiceAndReturnsDto() {
        UUID existingId = UUID.randomUUID();
        Lecturer expectedLecturer = createSampleLecturer();
        expectedLecturer.setId(existingId);
        LecturerDto expectedLecturerDto = createSampleLecturerDto();

        when(lecturerService.remove(existingId)).thenReturn(expectedLecturer);
        when(lecturerMapper.toDto(expectedLecturer)).thenReturn(expectedLecturerDto);

        LecturerDto result = lecturerFacade.remove(existingId);

        assertEquals(expectedLecturerDto, result);
        verify(lecturerService).remove(existingId);
    }

    @Test
    void remove_nonExistingId_callsServiceAndReturnsNull() {
        UUID nonExistingId = UUID.randomUUID();

        when(lecturerService.remove(nonExistingId)).thenReturn(null);

        LecturerDto result = lecturerFacade.remove(nonExistingId);

        assertNull(result);
        verify(lecturerService).remove(nonExistingId);
    }

}
