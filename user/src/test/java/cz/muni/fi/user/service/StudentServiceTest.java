package cz.muni.fi.user.service;

import cz.muni.fi.commons.dto.course.CourseEnrollmentDto;
import cz.muni.fi.commons.dto.lecture.LectureDto;
import cz.muni.fi.commons.dto.lecture.LectureEnrollmentDto;
import cz.muni.fi.commons.dto.course.CourseDto;
import cz.muni.fi.commons.dto.user.student.StudentDetailedDto;
import cz.muni.fi.commons.exceptions.ResourceNotFoundException;
import cz.muni.fi.user.data.model.Student;
import cz.muni.fi.user.data.repository.StudentRepository;
import cz.muni.fi.user.service.api.CourseApiService;
import cz.muni.fi.user.service.api.LectureApiService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static cz.muni.fi.user.factory.TestDataFactory.createSampleCourseDto;
import static cz.muni.fi.user.factory.TestDataFactory.createSampleLectureDto;
import static cz.muni.fi.user.factory.TestDataFactory.createSampleStudent;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StudentServiceTest {
    @Mock
    private StudentRepository studentRepository;
    @Mock
    private CourseApiService courseApiService;
    @Mock
    private LectureApiService lectureApiService;
    @InjectMocks
    private StudentService studentService;

    @Test
    void findAll_existingStudents_returnsListOfStudents() {
        Pageable pageable = Pageable.unpaged();

        List<Student> students = new ArrayList<>();
        students.add(createSampleStudent());
        students.add(createSampleStudent());
        Page<Student> stringPage = new PageImpl<>(students);

        when(studentRepository.findAll(pageable)).thenReturn(stringPage);

        Page<Student> result = studentService.findAll(pageable);
        assertEquals(stringPage, result);
    }

    @Test
    void findById_existingStudent_returnsStudent() {
        Student student = createSampleStudent();
        UUID studentId = student.getId();

        when(studentRepository.findById(studentId)).thenReturn(Optional.of(student));

        Student result = studentService.findById(studentId);
        assertEquals(student, result);
    }

    @Test
    void findById_nonExistingStudent_throwsException() {
        UUID studentId = UUID.randomUUID();

        when(studentRepository.findById(studentId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> studentService.findById(studentId));
    }

    @Test
    void findByIds_existingStudents_returnsListOfStudents() {
        Student student1 = createSampleStudent();
        Student student2 = createSampleStudent();
        UUID studentId1 = student1.getId();
        UUID studentId2 = student2.getId();

        when(studentRepository.findById(studentId1)).thenReturn(Optional.of(student1));
        when(studentRepository.findById(studentId2)).thenReturn(Optional.of(student2));

        List<Student> result = studentService.findByIds(Set.of(studentId1, studentId2));
        assertThat(List.of(student1, student2)).hasSameElementsAs(result);
    }

    @Test
    void removeById_existingStudent_returnsStudent() {
        Student student = createSampleStudent();
        UUID studentId = student.getId();

        when(studentRepository.findById(studentId)).thenReturn(Optional.of(student));

        Student result = studentService.removeById(studentId);
        assertEquals(student, result);
    }

    @Test
    void removeById_nonExistingStudent_throwsException() {
        UUID studentId = UUID.randomUUID();

        when(studentRepository.findById(studentId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> studentService.removeById(studentId));
    }

    @Test
    void updateStudent_existingStudent_returnsStudent() {
        Student student = createSampleStudent();
        UUID studentId = student.getId();

        when(studentRepository.findById(studentId)).thenReturn(Optional.of(student));
        when(studentRepository.save(student)).thenReturn(student);

        Student result = studentService.updateStudent(student);
        assertEquals(student, result);
    }

    @Test
    void updateStudent_nonExistingStudent_returnsStudent() {
        Student student = createSampleStudent();
        UUID studentId = student.getId();

        when(studentRepository.findById(studentId)).thenReturn(Optional.empty());
        when(studentRepository.save(student)).thenReturn(student);

        Student result = studentService.updateStudent(student);
        assertEquals(student, result);
    }

    @Test
    void createStudent_newStudent_returnsStudent() {
        Student student = createSampleStudent();

        when(studentRepository.save(student)).thenReturn(student);

        Student result = studentService.registerStudent(student);
        assertEquals(student, result);
    }

    @Test
    void studentToDetailed() {
        UUID studentId = UUID.randomUUID();
        StudentDetailedDto student = new StudentDetailedDto();
        student.setId(studentId);

        CourseDto courseDto = createSampleCourseDto();
        LectureDto lectureDto = createSampleLectureDto();

        CourseEnrollmentDto courseEnrollmentDto = new CourseEnrollmentDto();
        courseEnrollmentDto.setStudentDto(student);
        courseEnrollmentDto.setCourseDto(courseDto);

        LectureEnrollmentDto lectureEnrollmentDto = new LectureEnrollmentDto();
        lectureEnrollmentDto.setStudentDto(student);
        lectureEnrollmentDto.setLectureDto(lectureDto);

        when(courseApiService.getCourseEnrollmentsByStudentId(studentId)).thenReturn(Set.of(courseEnrollmentDto));

        when(lectureApiService.getLectureEnrollmentsByStudentId(studentId)).thenReturn(Set.of(lectureEnrollmentDto));

        StudentDetailedDto detailedStudent = studentService.studentToDetailed(student);

        assertEquals(1, detailedStudent.getCourses().size());
        assertEquals(1, detailedStudent.getLectures().size());
    }
}