package cz.muni.fi.user.repository;

import cz.muni.fi.user.data.model.Student;
import cz.muni.fi.user.data.repository.StudentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
class StudentRepositoryTest {

    @Autowired
    private StudentRepository studentRepository;

    @BeforeEach
    void setUp() {
        Student student1 = new Student();
        student1.setFirstName("Alice");
        student1.setSecondName("Doe");
        student1.setEmail("alice@example.com");
        studentRepository.save(student1);

        Student student2 = new Student();
        student2.setFirstName("Bob");
        student2.setSecondName("Smith");
        student2.setEmail("bob@example.com");
        studentRepository.save(student2);
    }

    @Test
    void findById_correctId_returnsStudent() {
        List<Student> students = studentRepository.findAll();
        UUID id = students.get(0).getId();

        Student foundStudent = studentRepository.findById(id).orElse(null);
        assertNotNull(foundStudent);
        assertEquals(id, foundStudent.getId());
    }

    @Test
    void findById_incorrectId_returnsNull() {
        List<Student> students = studentRepository.findAll();
        UUID id = UUID.randomUUID();

        Student foundStudent = studentRepository.findById(id).orElse(null);
        assertNull(foundStudent);
    }

    @Test
    void findAll_twoStudentsExist_returnsTwoStudents() {
        List<Student> students = studentRepository.findAll();
        assertEquals(2, students.size());
    }

    @Test
    void delete_correctId_deletesStudent() {
        List<Student> students = studentRepository.findAll();
        UUID id = students.get(0).getId();
        UUID otherId = students.get(1).getId();
        studentRepository.deleteById(id);

        List<Student> remainingStudents = studentRepository.findAll();
        assertEquals(1, remainingStudents.size());
        assertEquals(otherId, remainingStudents.get(0).getId());
    }
}
