package cz.muni.fi.user.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.commons.dto.user.student.StudentDto;
import cz.muni.fi.commons.dto.user.student.UserCreateDto;
import cz.muni.fi.user.data.mapper.StudentDtoMapper;
import cz.muni.fi.user.data.model.Student;
import cz.muni.fi.user.data.repository.StudentRepository;
import cz.muni.fi.user.factory.TestDataFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WithMockUser(username = "admin", authorities = {"SCOPE_test_1", "SCOPE_test_2", "SCOPE_test_3"})
class StudentRestControllerIT {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private StudentDtoMapper studentDtoMapper;

    private UUID existingID;
    private Student student;

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;

    @BeforeAll
    void setup() {
        objectMapper.findAndRegisterModules();
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @BeforeEach
    void beforeEach() {
        studentRepository.deleteAll();
        student = studentRepository.save(TestDataFactory.createSampleStudent());
        existingID = student.getId();
    }

    @Test
    void findById_existingId_returnsOk() throws Exception {
        var actual = studentDtoMapper.toDto(student);

        mockMvc.perform(get("/students/{id}", existingID).with(csrf()))
                .andExpect(status().isOk()).andExpect(content().json(objectMapper.writeValueAsString(actual)));
    }

    @Test
    void findById_nonExistingId_returnsNotFound() throws Exception {
        mockMvc.perform(get("/students/{id}", UUID.randomUUID()).with(csrf()))
                .andExpect(status().isNotFound());
    }

    @Test
    void findAll_entitiesExist_returnsEntities() throws Exception {
        var actual = studentDtoMapper.toDto(student);

        mockMvc.perform(get("/students/").with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content[0].id").value(actual.getId().toString()))
                .andExpect(jsonPath("$.content[0].firstName").value(actual.getFirstName()));

    }

    @Test
    void findByIds_entitiesExist_returnsEntities() throws Exception {
        var actual = studentDtoMapper.toDto(student);
        mockMvc.perform(post("/students/ids").contentType(MediaType.APPLICATION_JSON).with(csrf()).content(objectMapper.writeValueAsBytes(Set.of(existingID))))
                .andExpect(status().isOk()).andExpect(content().json(objectMapper.writeValueAsString(List.of(actual))));
    }

    @Test
    void findByIds_entitiesNotExist_isNotFound() throws Exception {
        mockMvc.perform(post("/students/ids").contentType(MediaType.APPLICATION_JSON).with(csrf()).content(objectMapper.writeValueAsBytes(Set.of(UUID.randomUUID()))))
                .andExpect(status().isNotFound());
    }

//    @Test
//    void findByIdDetailed_existingId_returnsOk() throws Exception {
//        mockMvc.perform(get("/students/detailed/{id}", existingID))
//                .andExpect(status().isOk());
//    }

    @Test
    void findByIdDetailed_nonExistingId_returnsNotFound() throws Exception {
        mockMvc.perform(get("/students/detailed/{id}", UUID.randomUUID()).with(csrf()))
                .andExpect(status().isNotFound());
    }

    @Test
    void registerStudent_validUserCreateDto_returnsCreated() throws Exception {
        UserCreateDto userCreateDto = TestDataFactory.createSampleCreateStudentDto();
        var result = mockMvc.perform(post("/students/").with(csrf()).content(objectMapper.writeValueAsBytes(userCreateDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn().getResponse().getContentAsString();

        var created = objectMapper.readValue(result, StudentDto.class);
        assertEquals(userCreateDto.getDateOfBirth(), created.getDateOfBirth());
        assertEquals(userCreateDto.getFirstName(), created.getFirstName());
        assertEquals(userCreateDto.getMiddleName(), created.getMiddleName());
        assertEquals(userCreateDto.getSecondName(), created.getSecondName());
        assertEquals(userCreateDto.getEmail(), created.getEmail());
    }

    @Test
    void updateStudent_existingIdAndValidDto_returnsOk() throws Exception {
        StudentDto studentDto = TestDataFactory.createSampleStudentDto();
        var result = mockMvc.perform(put("/students/{id}", existingID).with(csrf()).content(objectMapper.writeValueAsBytes(studentDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        var created = objectMapper.readValue(result, StudentDto.class);
        assertEquals(studentDto.getDateOfBirth(), created.getDateOfBirth());
        assertEquals(studentDto.getFirstName(), created.getFirstName());
        assertEquals(studentDto.getMiddleName(), created.getMiddleName());
        assertEquals(studentDto.getSecondName(), created.getSecondName());
        assertEquals(studentDto.getEmail(), created.getEmail());
    }

    @Test
    void updateStudent_nonExistingId_isOk() throws Exception {
        StudentDto studentDto = TestDataFactory.createSampleStudentDto();
        var result = mockMvc.perform(put("/students/{id}", UUID.randomUUID()).with(csrf()).content(objectMapper.writeValueAsBytes(studentDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        var created = objectMapper.readValue(result, StudentDto.class);
        assertEquals(studentDto.getDateOfBirth(), created.getDateOfBirth());
        assertEquals(studentDto.getFirstName(), created.getFirstName());
        assertEquals(studentDto.getMiddleName(), created.getMiddleName());
        assertEquals(studentDto.getSecondName(), created.getSecondName());
        assertEquals(studentDto.getEmail(), created.getEmail());
    }

    @Test
    void deleteStudent_existingId_returnsOk() throws Exception {
        var actual = studentDtoMapper.toDto(student);
        mockMvc.perform(delete("/students/{id}", existingID).with(csrf()))
                .andExpect(status().isOk()).andExpect(content().json(objectMapper.writeValueAsString(actual)));
    }

    @Test
    void deleteStudent_nonExistingId_returnsNotFound() throws Exception {
        mockMvc.perform(delete("/students/{id}", UUID.randomUUID()).with(csrf()))
                .andExpect(status().isNotFound());
    }
}
