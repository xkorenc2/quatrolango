package cz.muni.fi.user.service;

import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.exceptions.ResourceNotFoundException;
import cz.muni.fi.user.data.model.Lecturer;
import cz.muni.fi.user.data.repository.LecturerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static cz.muni.fi.user.factory.TestDataFactory.createSampleLecturer;
import static cz.muni.fi.user.factory.TestDataFactory.getExistingUUID;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LecturerServiceTest {
    @Mock
    private LecturerRepository lecturerRepository;

    @InjectMocks
    private LecturerService lecturerService;

    @Test
    void findAll_allLecturersFound_returnsAllLecturers() {
        Pageable pageable = Pageable.unpaged();

        List<Lecturer> lecturers = Arrays.asList(createSampleLecturer(), createSampleLecturer());
        when(lecturerRepository.findAll(pageable)).thenReturn(new PageImpl<>(lecturers));

        Page<Lecturer> result = lecturerService.findAll(pageable);

        assertEquals(2, result.getTotalElements());
        verify(lecturerRepository).findAll(pageable);
    }

    @Test
    void findById_lecturerFound_returnsLecturer() {
        Lecturer lecturer = createSampleLecturer();
        UUID id = lecturer.getId();

        when(lecturerRepository.findById(id)).thenReturn(Optional.of(lecturer));

        Lecturer result = lecturerService.findById(id);

        assertNotNull(result);
        assertEquals(result.getId(), lecturer.getId());
        verify(lecturerRepository).findById(id);
    }

    @Test
    void findById_lecturerNotFound_throwsResourceNotFoundException() {
        UUID id = UUID.randomUUID();
        when(lecturerRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> lecturerService.findById(id));
        verify(lecturerRepository).findById(id);
    }

    @Test
    void findByIds_allLecturersFound_returnsAllLecturers() {
        List<UUID> ids = Arrays.asList(UUID.randomUUID(), UUID.randomUUID());
        List<Lecturer> lecturers = Arrays.asList(new Lecturer(), new Lecturer());
        when(lecturerRepository.findByIds(ids)).thenReturn(lecturers);

        List<Lecturer> result = lecturerService.findByIds(ids);

        assertEquals(2, result.size());
        verify(lecturerRepository).findByIds(ids);
    }

    @Test
    void findByEmail_lecturerFound_returnsLecturer() {
        String email = "test@example.com";
        Lecturer lecturer = createSampleLecturer();
        when(lecturerRepository.findLecturerByEmail(email)).thenReturn(Optional.of(lecturer));

        Lecturer result = lecturerService.findByEmail(email);

        assertNotNull(result);
        verify(lecturerRepository).findLecturerByEmail(email);
    }

    @Test
    void findByEmail_lecturerNotFound_throwsResourceNotFoundException() {
        String email = "test@example.com";
        when(lecturerRepository.findLecturerByEmail(email)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> lecturerService.findByEmail(email));
        verify(lecturerRepository).findLecturerByEmail(email);
    }

    @Test
    void findByLanguage_lecturersFound_returnsLecturers() {
        Language language = Language.ENGLISH;
        List<Lecturer> lecturers = Arrays.asList(createSampleLecturer(), createSampleLecturer());
        when(lecturerRepository.findByLanguage(language)).thenReturn(lecturers);

        List<Lecturer> result = lecturerService.findByLanguage(language);

        assertEquals(2, result.size());
        verify(lecturerRepository).findByLanguage(language);
    }

    @Test
    void create_lecturerCreated_returnsCreatedLecturer() {
        Lecturer lecturer = createSampleLecturer();
        when(lecturerRepository.save(lecturer)).thenReturn(lecturer);

        Lecturer result = lecturerService.create(lecturer);

        assertNotNull(result.getId());
        verify(lecturerRepository).save(lecturer);
    }

    @Test
    void update_existingLecturerUpdated_returnsUpdatedLecturer() {
        Lecturer lecturer = createSampleLecturer();
        UUID id = lecturer.getId();

        when(lecturerRepository.findById(id)).thenReturn(Optional.of(lecturer));
        when(lecturerRepository.save(lecturer)).thenReturn(lecturer);

        Lecturer result = lecturerService.update(lecturer, id);

        assertEquals(id, result.getId());
        verify(lecturerRepository).findById(id);
        verify(lecturerRepository).save(lecturer);
    }

    @Test
    void update_nonExistingLecturer_returnsCreatedLecturer() {
        Lecturer lecturer = createSampleLecturer();
        UUID id = lecturer.getId();

        when(lecturerRepository.findById(id)).thenReturn(Optional.empty());
        when(lecturerRepository.save(lecturer)).thenReturn(lecturer);

        Lecturer result = lecturerService.update(lecturer, id);

        assertEquals(id, result.getId());
    }

    @Test
    void remove_existingLecturerRemoved_isCalledOnce() {
        Lecturer lecturer = createSampleLecturer();
        UUID id = getExistingUUID();

        when(lecturerRepository.findById(id)).thenReturn(Optional.of(lecturer));

        lecturerService.remove(id);

        verify(lecturerRepository, times(1)).deleteById(id);
    }

    @Test
    void remove_nonExistingLecturer_throwsResourceNotFoundException() {
        UUID id = UUID.randomUUID();
        when(lecturerRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> lecturerService.remove(id));
        verify(lecturerRepository).findById(id);
    }
}
