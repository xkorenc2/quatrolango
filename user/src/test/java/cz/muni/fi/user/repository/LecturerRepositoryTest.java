package cz.muni.fi.user.repository;

import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import cz.muni.fi.user.data.model.Lecturer;
import cz.muni.fi.user.data.model.LecturerLanguage;
import cz.muni.fi.user.data.repository.LecturerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
class LecturerRepositoryTest {

    @Autowired
    private LecturerRepository lecturerRepository;

    @BeforeEach
    void setUp() {
        LecturerLanguage lecturerLanguage1 = new LecturerLanguage();
        lecturerLanguage1.setLanguage(Language.ENGLISH);
        lecturerLanguage1.setProficiencyLevel(ProficiencyLevel.A1);

        LecturerLanguage lecturerLanguage2 = new LecturerLanguage();
        lecturerLanguage2.setLanguage(Language.SPANISH);
        lecturerLanguage2.setProficiencyLevel(ProficiencyLevel.B1);

        Lecturer lecturer1 = new Lecturer();
        lecturer1.setEmail("lecturer1@example.com");
        lecturer1.setNativeSpeaker(true);
        lecturer1.setFirstName("John");
        lecturer1.setMiddleName("A");
        lecturer1.setSecondName("Doe");
        lecturer1.setLanguages(List.of(lecturerLanguage1));
        lecturerRepository.save(lecturer1);

        Lecturer lecturer2 = new Lecturer();
        lecturer2.setEmail("lecturer2@example.com");
        lecturer2.setNativeSpeaker(false);
        lecturer2.setFirstName("Jimmy");
        lecturer2.setMiddleName("notA");
        lecturer2.setSecondName("Doe");
        lecturer2.setLanguages(List.of(lecturerLanguage2));
        lecturerRepository.save(lecturer2);
    }

    @Test
    void findLecturerByEmail_correctEmail_returnsLecturer() {
        String email = "lecturer1@example.com";
        Lecturer lecturer = lecturerRepository.findLecturerByEmail(email).orElse(null);
        assertNotNull(lecturer);
        assertEquals(lecturer.getEmail(), email);
    }

    @Test
    void findLecturerByEmail_incorrectEmail_returnsNull() {
        String email = "nonexistent@example.com";
        Lecturer lecturer = lecturerRepository.findLecturerByEmail(email).orElse(null);
        assertNull(lecturer);
    }

    @Test
    void findByIds_correctIds_returnsLecturers() {
        List<Lecturer> lecturers = lecturerRepository.findAll();
        List<UUID> ids = new ArrayList<>();
        for (Lecturer lecturer : lecturers) {
            ids.add(lecturer.getId());
        }
        List<Lecturer> lecturersByIds = lecturerRepository.findByIds(ids);
        assertEquals(2, lecturersByIds.size());
    }

    @Test
    void findByIds_incorrectIds_returnsNone() {
        List<UUID> ids = new ArrayList<>();
        ids.add(UUID.randomUUID());
        ids.add(UUID.randomUUID());
        List<Lecturer> lecturersByIds = lecturerRepository.findByIds(ids);
        assertEquals(0, lecturersByIds.size());
    }

    @Test
    void findByLanguage_existingLanguage_returnsLecturer() {
        Language language = Language.ENGLISH;

        List<Lecturer> lecturers = lecturerRepository.findByLanguage(language);
        assertEquals(1, lecturers.size());
        assertEquals(language, lecturers.get(0).getLanguages().get(0).getLanguage());
    }

    @Test
    void findByLanguage_invalidLanguage_returnsNone() {
        Language language = Language.GERMAN;

        List<Lecturer> lecturers = lecturerRepository.findByLanguage(language);
        assertEquals(0, lecturers.size());
    }

    @Test
    void findAll_returnsTwoLecturers() {
        List<Lecturer> lecturers = lecturerRepository.findAll();
        assertEquals(2, lecturers.size());
    }

    @Test
    void delete_existingLecturer_deletesLecturer() {
        List<Lecturer> lecturers = lecturerRepository.findAll();
        assertEquals(2, lecturers.size());
        lecturerRepository.delete(lecturers.get(0));
        List<Lecturer> remainingLecturers = lecturerRepository.findAll();
        assertEquals(1, remainingLecturers.size());
    }
}
