package cz.muni.fi.certificate.service.api;

import cz.muni.fi.commons.dto.user.student.StudentDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.AbstractOAuth2Token;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.util.Optional;
import java.util.UUID;

@Service
public class StudentApiService {
    private final WebClient client;

    public StudentApiService(@Value("${student.api.url}") String baseUrl) {
        this.client = WebClient.create(baseUrl);
    }

    public Optional<StudentDto> findStudentById(UUID id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        AbstractOAuth2Token token = (AbstractOAuth2Token) authentication.getCredentials();

        Flux<StudentDto> student = client
                .get()
                .uri("/" + id)
                .headers(httpHeaders -> httpHeaders.setBearerAuth(token.getTokenValue()))
                .retrieve()
                .bodyToFlux(StudentDto.class);
        return Optional.ofNullable(student.blockFirst());
    }
}
