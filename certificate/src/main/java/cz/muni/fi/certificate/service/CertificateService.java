package cz.muni.fi.certificate.service;

import cz.muni.fi.certificate.data.model.Certificate;
import cz.muni.fi.certificate.data.repository.CertificateRepository;
import cz.muni.fi.commons.exceptions.StudentDidNotPassCourseException;
import cz.muni.fi.certificate.service.api.CourseApiService;
import cz.muni.fi.certificate.service.api.CourseEnrollmentApiService;
import cz.muni.fi.certificate.service.api.EmailApiService;
import cz.muni.fi.certificate.service.api.StudentApiService;
import cz.muni.fi.commons.dto.course.CourseEnrollmentDto;
import cz.muni.fi.commons.dto.course.CourseDto;
import cz.muni.fi.commons.dto.user.student.StudentDto;
import cz.muni.fi.commons.enums.EnrollmentState;
import cz.muni.fi.commons.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

import java.io.File;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
public class CertificateService {
    private final CertificateRepository certificateRepository;
    private final CourseApiService courseApiService;
    private final StudentApiService studentApiService;
    private final CourseEnrollmentApiService courseEnrollmentApiService;
    private final EmailApiService emailApiService;
    private final ITemplateEngine thymeleafTemplateEngine;
    private final CertificatePDFService certificatePDFService;


    @Autowired
    public CertificateService(CertificateRepository certificateRepository,
                              CourseApiService courseApiService,
                              StudentApiService studentApiService,
                              CourseEnrollmentApiService courseEnrollmentApiService,
                              EmailApiService emailApiService,
                              @Qualifier("templateEngine") ITemplateEngine thymeleafTemplateEngine,
                              CertificatePDFService certificatePDFService
    ) {
        this.certificateRepository = certificateRepository;
        this.courseApiService = courseApiService;
        this.studentApiService = studentApiService;
        this.courseEnrollmentApiService = courseEnrollmentApiService;
        this.emailApiService = emailApiService;
        this.thymeleafTemplateEngine = thymeleafTemplateEngine;
        this.certificatePDFService = certificatePDFService;
    }

    @Transactional
    public Page<Certificate> findAll(Pageable pageable) {
        return certificateRepository.findAll(pageable);
    }

    @Transactional
    public Certificate findById(UUID id) {
        return certificateRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Certificate with id " + id + " not found"));
    }

    @Transactional
    public List<Certificate> findByIds(Set<UUID> ids) {
        return certificateRepository.findByIds(ids);
    }

    @Transactional
    public Certificate findCertificateForStudentAndCourse(UUID studentId, UUID courseId) {
        checkIfCourseExists(courseId);
        checkIfStudentExists(studentId);
        return certificateRepository.findCertificateForStudentAndCourse(studentId, courseId);
    }

    @Transactional
    public List<Certificate> findByCourseId(UUID passedCourseId) {
        if (passedCourseId == null) {
            throw new IllegalArgumentException("Passed course id cannot be null");
        }

        checkIfCourseExists(passedCourseId);
        return certificateRepository.findByPassedCourseId(passedCourseId);
    }

    @Transactional
    public List<Certificate> findByStudentId(UUID studentId) {
        if (studentId == null) {
            throw new IllegalArgumentException("Student id cannot be null");
        }

        checkIfStudentExists(studentId);
        return certificateRepository.findByStudentId(studentId);
    }


    @Transactional
    public Certificate create(UUID studentId, UUID courseId) {
        CourseDto course = checkIfCourseExists(courseId);
        StudentDto student = checkIfStudentExists(studentId);
        checkIfStudentPassedCourse(courseId, studentId);

        if (certificateRepository.existsCertificateForStudentAndCourse(studentId, courseId)) {
            throw new IllegalArgumentException("Student already has a certificate for this course");
        }

        Certificate certificate = new Certificate();
        certificate.setStudentId(studentId);
        certificate.setPassedCourseId(courseId);
        certificate.setName("Certificate of passing " + course.getName());
        certificate.setFileName("certificate_" + course.getName() + "_" + student.getId());
        String description = "Student: " + "firstName=" + student.getFirstName() +
                ", secondName=" + student.getSecondName() +
                ", middleName=" + student.getMiddleName() +
                ", email=" + student.getEmail() +
                "Course: " + "name=" + course.getName() +
                ", language=" + course.getLanguage() +
                ", proficiencyLevel=" + course.getProficiencyLevel();
        certificate.setDescription(description);
        certificate.setId(UUID.randomUUID());
        certificate.setValidFrom(LocalDate.now().atStartOfDay());

        File createdCertificate = certificatePDFService.generateCertificatePDF(
                student.getFirstName(),
                student.getMiddleName(),
                student.getSecondName(),
                course,
                certificate.getValidFrom().toLocalDate().toString(),
                certificate.getFileName()
        );

        final Context ctx = new Context(Locale.ENGLISH);
        ctx.setVariable("recipient", student.getFirstName());
        final String htmlContent = thymeleafTemplateEngine.process("certificate_email_template.html", ctx);

        emailApiService.sendFileViaEmail(student.getEmail(), htmlContent, createdCertificate);


        return certificateRepository.save(certificate);
    }


    @Transactional
    public Certificate delete(UUID id) {
        Certificate certificate = certificateRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Certificate with id: " + id + " was not found."));

        certificateRepository.deleteById(id);
        return certificate;

    }

    CourseDto checkIfCourseExists(UUID courseId) {
        return courseApiService.findCourseById(courseId)
                .orElseThrow(() -> new ResourceNotFoundException("Course with id: " + courseId + " was not found."));
    }

    StudentDto checkIfStudentExists(UUID studentId) {
        return studentApiService.findStudentById(studentId)
                .orElseThrow(() -> new ResourceNotFoundException("Student with id: " + studentId + " was not found."));
    }

    void checkIfStudentPassedCourse(UUID courseId, UUID studentId) {
        Optional<CourseEnrollmentDto> courseEnrollmentDto =
                courseEnrollmentApiService.findCourseEnrollmentByCourseIdAndStudentId(courseId, studentId);

        if (courseEnrollmentDto.isEmpty()) {
            throw new ResourceNotFoundException(
                    "CourseEnrollment for course id: "
                            + courseId
                            + " and student id: "
                            + studentId
                            + " was not found."
            );
        }

        EnrollmentState enrollmentState = courseEnrollmentDto.get().getEnrollmentState();
        if (!enrollmentState.equals(EnrollmentState.PASSED)) {
            throw new StudentDidNotPassCourseException(
                    "Student with id: " + studentId + " did not pass course with id: " + courseId
            );
        }
    }


}
