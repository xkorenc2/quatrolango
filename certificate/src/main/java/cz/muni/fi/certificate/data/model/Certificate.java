package cz.muni.fi.certificate.data.model;

import cz.muni.fi.commons.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Entity
@ToString
@EqualsAndHashCode(callSuper = true)
@Table(name = "Certificate")
public class Certificate extends BaseEntity {
    @NotNull
    @Column(name = "name")
    private String name;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "description")
    private String description;

    @Column(name = "valid_from")
    private LocalDateTime validFrom;

    @Column(name = "student_id")
    private UUID studentId;

    @Column(name = "passed_course_id")
    private UUID passedCourseId;
}
