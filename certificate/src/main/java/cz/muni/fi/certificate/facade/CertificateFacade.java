package cz.muni.fi.certificate.facade;

import cz.muni.fi.certificate.data.mapper.CertificateMapper;
import cz.muni.fi.commons.dto.CertificateDto;
import cz.muni.fi.certificate.service.CertificateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class CertificateFacade {
    private final CertificateService certificateService;
    private final CertificateMapper mapper;


    @Autowired
    public CertificateFacade(CertificateService certificateService, CertificateMapper certificateMapper) {
        this.certificateService = certificateService;
        this.mapper = certificateMapper;
    }

    @Transactional(readOnly = true)
    public CertificateDto findById(UUID id) {
        return mapper.toDto(certificateService.findById(id));
    }

    @Transactional(readOnly = true)
    public List<CertificateDto> findByStudentId(UUID studentId) {
        return mapper.toDtoList(certificateService.findByStudentId(studentId));
    }

    @Transactional(readOnly = true)
    public List<CertificateDto> findByCourseId(UUID courseId) {
        return mapper.toDtoList(certificateService.findByCourseId(courseId));
    }

    @Transactional(readOnly = true)
    public CertificateDto findCertificateForStudentAndCourse(UUID studentId, UUID courseId) {
        return mapper.toDto(certificateService.findCertificateForStudentAndCourse(studentId, courseId));
    }

    @Transactional(readOnly = true)
    public List<CertificateDto> findByIds(Set<UUID> ids) {
        return mapper.toDtoList(certificateService.findByIds(ids));
    }

    @Transactional(readOnly = true)
    public Page<CertificateDto> findAll(Pageable pageable) {
        return mapper.toDtoPage(certificateService.findAll(pageable));
    }

    @Transactional
    public CertificateDto create(UUID studentId, UUID courseId) {
        return mapper.toDto(certificateService.create(studentId, courseId));
    }

    @Transactional
    public CertificateDto delete(UUID id) {
        return mapper.toDto(certificateService.delete(id));
    }
    
}
