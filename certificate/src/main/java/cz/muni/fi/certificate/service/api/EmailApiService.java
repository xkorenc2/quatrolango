package cz.muni.fi.certificate.service.api;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.AbstractOAuth2Token;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.File;

@Service
public class EmailApiService {
    private final WebClient client;


    public EmailApiService(@Value("${email.api.url}") String baseUrl) {
        this.client = WebClient.create(baseUrl);
    }

    public void sendFileViaEmail(String toEmail, String body, File file) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        AbstractOAuth2Token token = (AbstractOAuth2Token) authentication.getCredentials();


        MultiValueMap<String, Object> emailBody = new LinkedMultiValueMap<>();
        emailBody.add("toEmail", toEmail);
        emailBody.add("body", body);
        emailBody.add("file", new FileSystemResource(file));

        client.post()
                .uri("/file/")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .headers(httpHeaders -> httpHeaders.setBearerAuth(token.getTokenValue()))
                .body(BodyInserters.fromMultipartData(emailBody))
                .retrieve()
                .bodyToMono(String.class)
                .doOnError(error -> System.err.println("Failed to send email: " + error.getMessage()))
                .subscribe(response -> System.out.println("Email sent successfully: " + response));
    }

}
