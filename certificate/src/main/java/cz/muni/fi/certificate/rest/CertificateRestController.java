package cz.muni.fi.certificate.rest;

import cz.muni.fi.certificate.facade.CertificateFacade;
import cz.muni.fi.commons.dto.CertificateDto;
import io.micrometer.observation.annotation.Observed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@RestController
@Tag(name = "Certificate service", description = "Operations with certificates")
@RequestMapping(value = "/certificates", produces = MediaType.APPLICATION_JSON_VALUE)
public class CertificateRestController {
    private final CertificateFacade certificateFacade;

    @Autowired
    public CertificateRestController(CertificateFacade certificateFacade) {
        this.certificateFacade = certificateFacade;
    }

    @Observed(name = "getAllCertificates")
    @Operation(summary = "Get all certificates", security = @SecurityRequirement(name = "Bearer"), description = "Get all certificates from the system")
    @GetMapping(value = "/")
    public ResponseEntity<Page<CertificateDto>> getAllCertificates(Pageable pageable) {
        return ResponseEntity.ok(certificateFacade.findAll(pageable));
    }

    @Observed(name = "getCertificateById")
    @Operation(summary = "Get certificate by id", security = @SecurityRequirement(name = "Bearer"), description = "Get certificate by id")
    @GetMapping(value = "/{id}")
    public ResponseEntity<CertificateDto> getCertificateById(@PathVariable UUID id) {
        return Optional.ofNullable(certificateFacade.findById(id))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @Observed(name = "getCertificatesByIds")
    @Operation(summary = "Get certificates by ids", security = @SecurityRequirement(name = "Bearer"), description = "Get certificates by ids")
    @PostMapping(value = "/ids")
    public ResponseEntity<List<CertificateDto>> getCertificatesByIds(@RequestBody Set<UUID> ids) {
        return ResponseEntity.ok(certificateFacade.findByIds(ids));
    }

    @Operation(summary = "Get student's certificates", security = @SecurityRequirement(name = "Bearer"), description = "Get certificates by student id")
    @GetMapping(value = "/students/{studentId}")
    @Observed(name = "getCertificatesByStudentId")
    public ResponseEntity<List<CertificateDto>> getCertificatesByStudentId(@PathVariable UUID studentId) {
        return ResponseEntity.ok(certificateFacade.findByStudentId(studentId));
    }

    @Operation(summary = "Get certificates by course id", security = @SecurityRequirement(name = "Bearer"), description = "Get certificates by course id")
    @GetMapping(value = "/courses/{courseId}")
    @Observed(name = "getCertificatesByCourseId")
    public ResponseEntity<List<CertificateDto>> getCertificatesByCourseId(@PathVariable UUID courseId) {
        return ResponseEntity.ok(certificateFacade.findByCourseId(courseId));
    }

    @Operation(summary = "Get certificate by student and course id", security = @SecurityRequirement(name = "Bearer"), description = "Get certificate by student and course id")
    @GetMapping(value = "/students/{studentId}/courses/{courseId}")
    @Observed(name = "getCertificateByStudentAndCourseId")
    public ResponseEntity<CertificateDto> getCertificateByStudentAndCourseId(@PathVariable UUID studentId, @PathVariable UUID courseId) {
        return ResponseEntity.ok(certificateFacade.findCertificateForStudentAndCourse(studentId, courseId));
    }

    @Operation(summary = "Generate certificate of passing course", security = @SecurityRequirement(name = "Bearer"), description = "Create certificate")
    @PostMapping("/students/{studentId}/courses/{courseId}")
    @Observed(name = "createCertificate")
    public ResponseEntity<CertificateDto> createCertificate(@PathVariable UUID studentId, @PathVariable UUID courseId) {
        return ResponseEntity.ok(certificateFacade.create(studentId, courseId));
    }

    @Operation(summary = "Delete certificate", security = @SecurityRequirement(name = "Bearer"), description = "Delete certificate")
    @DeleteMapping(value = "/{id}")
    @Observed(name = "deleteCertificate")
    public ResponseEntity<CertificateDto> deleteCertificate(@PathVariable UUID id) {
        return Optional.ofNullable(certificateFacade.delete(id))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }
}
