package cz.muni.fi.certificate.service.api;

import cz.muni.fi.commons.dto.course.CourseDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.AbstractOAuth2Token;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.util.Optional;
import java.util.UUID;

@Service
public class CourseApiService {
    private final WebClient client;

    public CourseApiService(@Value("${course.api.url}") String baseUrl) {
        this.client = WebClient.create(baseUrl);
    }

    public Optional<CourseDto> findCourseById(UUID id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        AbstractOAuth2Token token = (AbstractOAuth2Token) authentication.getCredentials();

        Flux<CourseDto> course = client
                .get()
                .uri("/" + id)
                .headers(httpHeaders -> httpHeaders.setBearerAuth(token.getTokenValue()))
                .retrieve()
                .bodyToFlux(CourseDto.class);
        return Optional.ofNullable(course.blockFirst());
    }
}