package cz.muni.fi.certificate.data.mapper;

import cz.muni.fi.certificate.data.model.Certificate;
import cz.muni.fi.commons.dto.CertificateDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CertificateMapper {
    List<CertificateDto> toDtoList(List<Certificate> certificates);

    default Page<CertificateDto> toDtoPage(Page<Certificate> certificates) {
        return new PageImpl<>(toDtoList(certificates.getContent()), certificates.getPageable(), certificates.getTotalPages());
    }

    CertificateDto toDto(Certificate certificate);

    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    Certificate toEntity(CertificateDto certificateDto);
}
