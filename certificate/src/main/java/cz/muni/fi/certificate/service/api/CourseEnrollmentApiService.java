package cz.muni.fi.certificate.service.api;

import cz.muni.fi.commons.dto.course.CourseEnrollmentDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.AbstractOAuth2Token;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.util.Optional;
import java.util.UUID;

@Service
public class CourseEnrollmentApiService {
    private final WebClient client;

    public CourseEnrollmentApiService(@Value("${course.enrollment.api.url}") String baseUrl) {
        this.client = WebClient.create(baseUrl);
    }

    public Optional<CourseEnrollmentDto> findCourseEnrollmentByCourseIdAndStudentId(UUID courseId, UUID studentId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        AbstractOAuth2Token token = (AbstractOAuth2Token) authentication.getCredentials();


        Flux<CourseEnrollmentDto> course = client
                .get()
                .uri("/courses/" + courseId + "/students/" + studentId)
                .headers(httpHeaders -> httpHeaders.setBearerAuth(token.getTokenValue()))
                .retrieve()
                .bodyToFlux(CourseEnrollmentDto.class);
        return Optional.ofNullable(course.blockFirst());
    }

}
