package cz.muni.fi.certificate.service;

import cz.muni.fi.commons.exceptions.CertificatePDFGenerationException;
import cz.muni.fi.commons.dto.course.CourseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Locale;

@Service
public class CertificatePDFService {
    private final ITemplateEngine thymeleafTemplateEngine;
    ITextRenderer renderer;

    @Autowired
    public CertificatePDFService(@Qualifier("templateEngine") ITemplateEngine thymeleafTemplateEngine) {
        this.thymeleafTemplateEngine = thymeleafTemplateEngine;
        this.renderer = new ITextRenderer();
    }


    public File generateCertificatePDF(
            String recipientFirstName,
            String recipientLastName,
            String recipientMiddleName,
            CourseDto courseDto,
            String validFrom,
            String filename
    ) {
        final Context ctx = new Context(Locale.ENGLISH);
        ctx.setVariable("recipientFirstName", recipientFirstName);
        ctx.setVariable("recipientMiddleName", recipientMiddleName);
        ctx.setVariable("recipientLastName", recipientLastName);
        ctx.setVariable("courseName", courseDto.getName());
        ctx.setVariable("language", courseDto.getLanguage().name().toUpperCase());
        ctx.setVariable("proficiencyLevel", courseDto.getProficiencyLevel().name().toUpperCase());
        ctx.setVariable("validFrom", validFrom);
        final String htmlContent = thymeleafTemplateEngine.process("certificate_template.html", ctx);

        File file = new File(filename + ".pdf");
        try (OutputStream outputStream = new FileOutputStream(file)) {
            renderer.setDocumentFromString(htmlContent);
            renderer.layout();
            renderer.createPDF(outputStream);
        } catch (Exception e) {
            throw new CertificatePDFGenerationException(e.getLocalizedMessage());
        }
        return file;
    }
}
