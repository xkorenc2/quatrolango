package cz.muni.fi.certificate.data.repository;

import cz.muni.fi.certificate.data.model.Certificate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Repository
public interface CertificateRepository extends JpaRepository<Certificate, UUID> {
    @Query("SELECT c FROM Certificate c WHERE c.id IN :ids")
    List<Certificate> findByIds(@Param("ids") Set<UUID> ids);

    @Query("SELECT c FROM Certificate c WHERE c.studentId = :studentId")
    List<Certificate> findByStudentId(@Param("studentId") UUID studentId);

    @Query("SELECT c FROM Certificate c WHERE c.passedCourseId = :passedCourseId")
    List<Certificate> findByPassedCourseId(@Param("passedCourseId") UUID passedCourseId);

    @Query("SELECT c FROM Certificate c WHERE c.studentId = :studentId AND c.passedCourseId = :courseId")
    Certificate findCertificateForStudentAndCourse(@Param("studentId") UUID studentId, @Param("courseId") UUID courseId);

    @Query("SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM Certificate c WHERE c.studentId = :studentId AND c.passedCourseId = :courseId")
    boolean existsCertificateForStudentAndCourse(@Param("studentId") UUID studentId, @Param("courseId") UUID courseId);
}
