package cz.muni.fi.certificate.service;

import cz.muni.fi.commons.exceptions.CertificatePDFGenerationException;
import cz.muni.fi.certificate.factory.TestDataFactory;
import cz.muni.fi.commons.dto.course.CourseDto;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CertificatePDFServiceTest {
    @InjectMocks
    private CertificatePDFService certificatePDFService;
    @Mock
    private ITemplateEngine mockThymeleafTemplateEngine;
    @Mock
    private ITextRenderer iTextRenderer;

    private final String TEST_PDF_FILE = "test_certificate";

    @BeforeEach
    void setUp() {
        deleteTestPDFFile();
    }

    @AfterEach
    void tearDown() {
        deleteTestPDFFile();
    }

    private void deleteTestPDFFile() {
        try {
            Files.deleteIfExists(Paths.get(TEST_PDF_FILE + ".pdf"));
        } catch (IOException e) {
            new RuntimeException("Could not delete test pdf file: " + TEST_PDF_FILE, e);
        }
    }

    @Test
    void generateCertificate_validData_successfullyGeneratesPDF() {
        String recipientFirstName = "John";
        String recipientLastName = "Doe";
        String recipientMiddleName = "A";
        String date = "April 20, 2024";
        CourseDto courseDto = TestDataFactory.createSampleCourseDto();

        when(mockThymeleafTemplateEngine.process(eq("certificate_template.html"), any(Context.class)))
                .thenReturn("<html></html>");

        certificatePDFService.generateCertificatePDF(
                recipientFirstName,
                recipientLastName,
                recipientMiddleName,
                courseDto,
                date,
                TEST_PDF_FILE
        );

        assertTrue(Files.exists(Paths.get(TEST_PDF_FILE + ".pdf")), "Test PDF file should be created");
    }

    @Test
    void generateCertificate_exceptionThrown_certificatePDFGenerationExceptionThrown() {
        String recipientFirstName = "John";
        String recipientLastName = "Doe";
        String recipientMiddleName = "A";
        String date = "April 20, 2024";
        CourseDto courseDto = TestDataFactory.createSampleCourseDto();

        ITextRenderer mockRenderer = mock(ITextRenderer.class);
        doThrow(new RuntimeException("Mocked exception")).when(mockRenderer).createPDF(any(OutputStream.class));

        when(mockThymeleafTemplateEngine.process(eq("certificate_template.html"), any(Context.class)))
                .thenReturn("<html></html>");

        CertificatePDFService certificatePDFService = new CertificatePDFService(mockThymeleafTemplateEngine);

        certificatePDFService.renderer = mockRenderer;

        assertThrows(CertificatePDFGenerationException.class, () -> {
            certificatePDFService.generateCertificatePDF(
                    recipientFirstName,
                    recipientLastName,
                    recipientMiddleName,
                    courseDto,
                    date,
                    TEST_PDF_FILE
            );
        });
    }

}
