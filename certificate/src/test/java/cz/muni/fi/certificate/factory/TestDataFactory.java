package cz.muni.fi.certificate.factory;

import cz.muni.fi.certificate.data.model.Certificate;
import cz.muni.fi.commons.dto.CertificateDto;
import cz.muni.fi.commons.dto.course.CourseEnrollmentDto;
import cz.muni.fi.commons.dto.course.CourseDto;
import cz.muni.fi.commons.dto.user.student.StudentDto;
import cz.muni.fi.commons.enums.EnrollmentState;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class TestDataFactory {
    private TestDataFactory() {
    }

    public static Certificate createSampleCertificate() {
        Certificate certificate = new Certificate();

        certificate.setId(UUID.randomUUID());
        certificate.setName("Test cert");
        certificate.setDescription("Test cert desc");
        certificate.setValidFrom(LocalDateTime.of(2023, 10, 3, 4, 3, 2));
        certificate.setName("Test cert");
        certificate.setStudentId(UUID.randomUUID());
        certificate.setPassedCourseId(UUID.randomUUID());

        return certificate;
    }

    public static List<Certificate> createListOfCertificates(int count) {
        List<Certificate> certificates = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            certificates.add(createSampleCertificate());
        }
        return certificates;
    }

    public static CourseEnrollmentDto getCourseEnrollmentDto(EnrollmentState state) {
        CourseDto courseDto = createSampleCourseDto();
        StudentDto studentDto = createSampleStudentDto();

        CourseEnrollmentDto courseEnrollmentDto = new CourseEnrollmentDto();
        courseEnrollmentDto.setCourseDto(courseDto);
        courseEnrollmentDto.setId(UUID.randomUUID());
        courseEnrollmentDto.setStudentDto(studentDto);
        courseEnrollmentDto.setEnrollmentState(state);
        return courseEnrollmentDto;
    }

    public static List<Certificate> createListOfCertificatesWithIds(Set<UUID> ids) {
        return ids.stream().map(TestDataFactory::createSampleCertificateWithCustomId).toList();
    }

    public static List<CertificateDto> createListOfCertificateDtosWithIds(Set<UUID> ids) {
        return ids.stream().map(TestDataFactory::createSampleCertificateDto).toList();
    }

    public static CertificateDto createSampleCertificateDto(UUID id) {
        CertificateDto certificateDto = new CertificateDto();

        certificateDto.setId(id);
        certificateDto.setName("Test cert");
        certificateDto.setDescription("Test cert desc");
        certificateDto.setValidFrom(LocalDateTime.of(2023, 10, 3, 4, 3, 2));
        certificateDto.setName("Test cert");
        certificateDto.setStudentId(UUID.randomUUID());
        certificateDto.setPassedCourseId(UUID.randomUUID());

        return certificateDto;
    }

    public static Certificate createSampleCertificateWithCustomId(UUID id) {
        Certificate certificate = new Certificate();

        certificate.setId(id);
        certificate.setName("Test cert");
        certificate.setDescription("Test cert desc");
        certificate.setValidFrom(LocalDateTime.of(2023, 10, 3, 4, 3, 2));
        certificate.setName("Test cert");
        certificate.setStudentId(UUID.randomUUID());
        certificate.setPassedCourseId(UUID.randomUUID());

        return certificate;
    }

    public static CourseDto createSampleCourseDto() {
        CourseDto courseDto = new CourseDto();
        courseDto.setId(UUID.randomUUID());
        courseDto.setName("English course");
        courseDto.setProficiencyLevel(ProficiencyLevel.A1);
        courseDto.setCapacity(20);
        courseDto.setLanguage(Language.ENGLISH);

        return courseDto;
    }

    public static StudentDto createSampleStudentDto() {
        StudentDto student = new StudentDto();
        student.setId(UUID.randomUUID());
        student.setFirstName("John");
        student.setSecondName("Doe");
        student.setEmail("test@gmail.com");
        student.setDateOfBirth(LocalDate.of(2000, 1, 1));
        return student;
    }
}
