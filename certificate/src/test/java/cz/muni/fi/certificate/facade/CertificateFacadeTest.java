package cz.muni.fi.certificate.facade;

import cz.muni.fi.certificate.data.mapper.CertificateMapper;
import cz.muni.fi.certificate.data.model.Certificate;
import cz.muni.fi.commons.dto.CertificateDto;
import cz.muni.fi.certificate.factory.TestDataFactory;
import cz.muni.fi.certificate.service.CertificateService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CertificateFacadeTest {
    @Mock
    private CertificateService certificateService;

    @Mock
    private CertificateMapper mapper;

    @InjectMocks
    private CertificateFacade certificateFacade;

    @Test
    void findById_validId_returnsCertificateDto() {

        Certificate certificate = TestDataFactory.createSampleCertificate();
        UUID certificateId = certificate.getId();
        CertificateDto expectedDto = TestDataFactory.createSampleCertificateDto(certificateId);

        when(certificateService.findById(certificateId)).thenReturn(certificate);
        when(mapper.toDto(certificate)).thenReturn(expectedDto);

        CertificateDto result = certificateFacade.findById(certificateId);

        assertEquals(expectedDto, result);
    }

    @Test
    void findByStudentId_validStudentId_returnsListOfCertificateDto() {
        UUID studentId = UUID.randomUUID();
        UUID certificateId1 = UUID.randomUUID();
        UUID certificateId2 = UUID.randomUUID();

        List<Certificate> certificates = TestDataFactory.createListOfCertificatesWithIds(Set.of(certificateId1, certificateId2));
        List<CertificateDto> expectedDtos = TestDataFactory.createListOfCertificateDtosWithIds(Set.of(certificateId1, certificateId2));

        when(certificateService.findByStudentId(studentId)).thenReturn(certificates);
        when(mapper.toDtoList(certificates)).thenReturn(expectedDtos);

        List<CertificateDto> result = certificateFacade.findByStudentId(studentId);

        assertEquals(expectedDtos, result);
    }

    @Test
    void findByCourseId_validCourseId_returnsListOfCertificateDto() {
        UUID courseId = UUID.randomUUID();
        UUID certificateId1 = UUID.randomUUID();
        UUID certificateId2 = UUID.randomUUID();

        List<Certificate> certificates = TestDataFactory.createListOfCertificatesWithIds(Set.of(certificateId1, certificateId2));
        List<CertificateDto> expectedDtos = TestDataFactory.createListOfCertificateDtosWithIds(Set.of(certificateId1, certificateId2));

        when(certificateService.findByCourseId(courseId)).thenReturn(certificates);
        when(mapper.toDtoList(certificates)).thenReturn(expectedDtos);

        List<CertificateDto> result = certificateFacade.findByCourseId(courseId);

        assertEquals(expectedDtos, result);
    }

    @Test
    void findCertificateForStudentAndCourse_validIds_returnsCertificateDto() {
        UUID studentId = UUID.randomUUID();
        UUID courseId = UUID.randomUUID();
        Certificate certificate = TestDataFactory.createSampleCertificate();
        CertificateDto expectedDto = TestDataFactory.createSampleCertificateDto(certificate.getId());

        when(certificateService.findCertificateForStudentAndCourse(studentId, courseId)).thenReturn(certificate);
        when(mapper.toDto(certificate)).thenReturn(expectedDto);

        CertificateDto result = certificateFacade.findCertificateForStudentAndCourse(studentId, courseId);

        assertEquals(expectedDto, result);
    }

    @Test
    void findByIds_validIds_returnsListOfCertificateDto() {
        UUID certificateId1 = UUID.randomUUID();
        UUID certificateId2 = UUID.randomUUID();

        Set<UUID> ids = new HashSet<>(Arrays.asList(certificateId1, certificateId2));


        List<Certificate> certificates = TestDataFactory.createListOfCertificatesWithIds(ids);
        List<CertificateDto> expectedDtos = TestDataFactory.createListOfCertificateDtosWithIds(ids);

        when(certificateService.findByIds(ids)).thenReturn(certificates);
        when(mapper.toDtoList(certificates)).thenReturn(expectedDtos);

        List<CertificateDto> result = certificateFacade.findByIds(ids);

        assertEquals(expectedDtos, result);
    }

    @Test
    void findAll_validIds_returnsListOfCertificateDto() {
        UUID certificateId1 = UUID.randomUUID();
        UUID certificateId2 = UUID.randomUUID();
        Pageable pageable = Pageable.unpaged();
        List<Certificate> certificates = TestDataFactory.createListOfCertificatesWithIds(Set.of(certificateId1, certificateId2));
        List<CertificateDto> expectedDtos = TestDataFactory.createListOfCertificateDtosWithIds(Set.of(certificateId1, certificateId2));

        Page<Certificate> certificatePage = new PageImpl<>(certificates);
        when(certificateService.findAll(pageable)).thenReturn(certificatePage);

        Page<CertificateDto> expectedDtoPage = new PageImpl<>(expectedDtos);
        when(mapper.toDtoPage(certificatePage)).thenReturn(expectedDtoPage);

        Page<CertificateDto> result = certificateFacade.findAll(pageable);

        assertEquals(expectedDtoPage, result);
    }

    @Test
    void create_validInput_returnsCertificateDto() {
        UUID studentId = UUID.randomUUID();
        UUID courseId = UUID.randomUUID();
        Certificate certificate = TestDataFactory.createSampleCertificate();
        CertificateDto expectedDto = TestDataFactory.createSampleCertificateDto(certificate.getId());

        when(certificateService.create(studentId, courseId)).thenReturn(certificate);
        when(mapper.toDto(certificate)).thenReturn(expectedDto);

        CertificateDto result = certificateFacade.create(studentId, courseId);

        assertEquals(expectedDto, result);
    }

    @Test
    void delete_validId_returnsCertificateDto() {
        Certificate certificate = TestDataFactory.createSampleCertificate();
        UUID certificateId = certificate.getId();
        CertificateDto expectedDto = TestDataFactory.createSampleCertificateDto(certificateId);

        when(certificateService.delete(certificateId)).thenReturn(certificate);
        when(mapper.toDto(certificate)).thenReturn(expectedDto);

        CertificateDto result = certificateFacade.delete(certificateId);

        assertEquals(expectedDto, result);
    }
}
