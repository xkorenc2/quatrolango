package cz.muni.fi.certificate.service;

import cz.muni.fi.certificate.data.model.Certificate;
import cz.muni.fi.certificate.data.repository.CertificateRepository;
import cz.muni.fi.commons.exceptions.StudentDidNotPassCourseException;
import cz.muni.fi.certificate.factory.TestDataFactory;
import cz.muni.fi.certificate.service.api.CourseApiService;
import cz.muni.fi.certificate.service.api.CourseEnrollmentApiService;
import cz.muni.fi.certificate.service.api.EmailApiService;
import cz.muni.fi.certificate.service.api.StudentApiService;
import cz.muni.fi.commons.dto.course.CourseEnrollmentDto;
import cz.muni.fi.commons.dto.course.CourseDto;
import cz.muni.fi.commons.dto.user.student.StudentDto;
import cz.muni.fi.commons.enums.EnrollmentState;
import cz.muni.fi.commons.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.thymeleaf.ITemplateEngine;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CertificateServiceTest {
    @Mock
    CertificateRepository certificateRepository;
    @Mock
    StudentApiService studentApiService;
    @Mock
    CourseApiService courseApiService;
    @Mock
    EmailApiService emailApiService;
    @Mock
    CourseEnrollmentApiService courseEnrollmentApiService;
    @InjectMocks
    CertificateService certificateService;
    @Mock
    private ITemplateEngine mockThymeleafTemplateEngine;
    @Mock
    private CertificatePDFService certificatePDFService;


    @Test
    void findAll_certificatesFound_returnsCertificates() {
        Pageable pageable = Pageable.unpaged();

        List<Certificate> certificates = TestDataFactory.createListOfCertificates(3);

        Page<Certificate> certificatePage = new PageImpl<>(certificates);
        when(certificateRepository.findAll(pageable)).thenReturn(certificatePage);

        Page<Certificate> result = certificateService.findAll(pageable);
        assertEquals(certificatePage, result);

    }

    @Test
    void findAll_emptyList_returnsEmptyList() {
        Pageable pageable = Pageable.unpaged();
        Page<Certificate> certificatePage = new PageImpl<>(Collections.emptyList());


        when(certificateRepository.findAll(pageable)).thenReturn(certificatePage);

        Page<Certificate> result = certificateService.findAll(pageable);

        assertTrue(result.isEmpty());
    }

    @Test
    void findById_certificateFound_returnsCertificate() {
        UUID certificateId = UUID.randomUUID();
        Certificate certificate = TestDataFactory.createSampleCertificate();
        when(certificateRepository.findById(certificateId)).thenReturn(Optional.of(certificate));

        Certificate result = certificateService.findById(certificateId);

        assertEquals(certificate, result);
    }

    @Test
    void findById_certificateNotFound_throwsResourceNotFoundException() {
        UUID nonExistingId = UUID.randomUUID();
        when(certificateRepository.findById(nonExistingId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> certificateService.findById(nonExistingId));
    }

    @Test
    void findByIds_certificatesFound_returnsCertificates() {
        Set<UUID> ids = new HashSet<>(Arrays.asList(UUID.randomUUID(), UUID.randomUUID()));
        List<Certificate> certificates = TestDataFactory.createListOfCertificatesWithIds(ids);
        when(certificateRepository.findByIds(ids)).thenReturn(certificates);

        List<Certificate> result = certificateService.findByIds(ids);

        assertEquals(certificates, result);
    }

    @Test
    void findByIds_emptySet_returnsEmptyList() {
        Set<UUID> ids = Collections.emptySet();
        when(certificateRepository.findByIds(ids)).thenReturn(Collections.emptyList());

        List<Certificate> result = certificateService.findByIds(ids);

        assertTrue(result.isEmpty());
    }

    @Test
    void findCertificateForStudentAndCourse_validIds_returnsCertificate() {

        StudentDto studentDto = TestDataFactory.createSampleStudentDto();
        CourseDto courseDto = TestDataFactory.createSampleCourseDto();

        UUID studentId = studentDto.getId();
        UUID courseId = courseDto.getId();
        Certificate certificate = TestDataFactory.createSampleCertificate();

        when(certificateRepository.findCertificateForStudentAndCourse(studentId, courseId)).thenReturn(certificate);
        when(studentApiService.findStudentById(studentId)).thenReturn(Optional.of(studentDto));
        when(courseApiService.findCourseById(courseId)).thenReturn(Optional.of(courseDto));

        Certificate result = certificateService.findCertificateForStudentAndCourse(studentId, courseId);

        assertEquals(certificate, result);
    }


    @Test
    void findCertificateForStudentAndCourse_courseDoesNotExist_throwsException() {
        UUID studentId = UUID.randomUUID();
        UUID courseId = UUID.randomUUID();

        when(courseApiService.findCourseById(courseId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> certificateService.findCertificateForStudentAndCourse(studentId, courseId));
    }

    @Test
    void findCertificateForStudentAndCourse_studentDoesNotExist_throwsException() {
        UUID studentId = UUID.randomUUID();
        UUID courseId = UUID.randomUUID();

        when(courseApiService.findCourseById(courseId)).thenReturn(Optional.of(new CourseDto()));
        when(studentApiService.findStudentById(studentId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> certificateService.findCertificateForStudentAndCourse(studentId, courseId));
    }

    @Test
    void findByCourseId_validCourseId_returnsCertificates() {
        CourseDto courseDto = TestDataFactory.createSampleCourseDto();

        List<Certificate> certificates = TestDataFactory.createListOfCertificates(2);
        when(courseApiService.findCourseById(courseDto.getId())).thenReturn(Optional.of(courseDto));
        when(certificateRepository.findByPassedCourseId(courseDto.getId())).thenReturn(certificates);
        List<Certificate> result = certificateService.findByCourseId(courseDto.getId());

        assertEquals(certificates, result);
    }

    @Test
    void findByCourseId_nullCourseId_throwsIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> certificateService.findByCourseId(null));
    }

    @Test
    void findByStudentId_validStudentId_returnsCertificates() {
        StudentDto studentDto = TestDataFactory.createSampleStudentDto();
        List<Certificate> certificates = TestDataFactory.createListOfCertificates(2);
        when(certificateRepository.findByStudentId(studentDto.getId())).thenReturn(certificates);
        when(studentApiService.findStudentById(studentDto.getId())).thenReturn(Optional.of(TestDataFactory.createSampleStudentDto()));

        List<Certificate> result = certificateService.findByStudentId(studentDto.getId());

        assertEquals(certificates, result);
    }

    @Test
    void findByStudentId_nullStudentId_throwsIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> certificateService.findByStudentId(null));
    }

    @Test
    void create_validInput_createsCertificate() {
        CourseDto course = TestDataFactory.createSampleCourseDto();
        CourseEnrollmentDto courseEnrollmentDto = TestDataFactory.getCourseEnrollmentDto(EnrollmentState.PASSED);
        StudentDto student = TestDataFactory.createSampleStudentDto();
        Certificate certificate = TestDataFactory.createSampleCertificate();
        UUID studentId = student.getId();
        UUID courseId = course.getId();
        certificate.setName("Certificate of passing " + course.getName());
        String description = "Student: " + "firstName=" + student.getFirstName() +
                ", secondName=" + student.getSecondName() +
                ", middleName=" + student.getMiddleName() +
                ", email=" + student.getEmail() +
                "Course: " + "name=" + course.getName() +
                ", language=" + course.getLanguage() +
                ", proficiencyLevel=" + course.getProficiencyLevel();
        certificate.setDescription(description);

        certificate.setPassedCourseId(courseId);
        certificate.setStudentId(studentId);

        when(studentApiService.findStudentById(studentId)).thenReturn(Optional.of(student));
        when(courseApiService.findCourseById(courseId)).thenReturn(Optional.of(course));
        when(courseEnrollmentApiService.findCourseEnrollmentByCourseIdAndStudentId(courseId, studentId)).thenReturn(Optional.of(courseEnrollmentDto));
        when(certificateRepository.existsCertificateForStudentAndCourse(studentId, courseId)).thenReturn(false);

        when(certificateRepository.save(any(Certificate.class))).thenReturn(certificate);

        Certificate createdCertificate = certificateService.create(studentId, courseId);

        assertNotNull(createdCertificate);
        assertEquals(studentId, createdCertificate.getStudentId());
        assertEquals(courseId, createdCertificate.getPassedCourseId());
        assertTrue(createdCertificate.getName().startsWith("Certificate of passing"));
        assertNotNull(createdCertificate.getDescription());
        assertNotNull(createdCertificate.getId());
        assertNotNull(createdCertificate.getValidFrom());

        verify(certificateRepository, times(1)).save(any(Certificate.class));
    }

    @Test
    void create_existingCertificate_throwsIllegalArgumentException() {
        CourseDto course = TestDataFactory.createSampleCourseDto();
        CourseEnrollmentDto courseEnrollmentDto = TestDataFactory.getCourseEnrollmentDto(EnrollmentState.PASSED);
        StudentDto student = TestDataFactory.createSampleStudentDto();
        UUID studentId = student.getId();
        UUID courseId = course.getId();
        when(studentApiService.findStudentById(studentId)).thenReturn(Optional.of(student));
        when(courseApiService.findCourseById(courseId)).thenReturn(Optional.of(course));
        when(courseEnrollmentApiService.findCourseEnrollmentByCourseIdAndStudentId(courseId, studentId)).thenReturn(Optional.of(courseEnrollmentDto));


        when(certificateRepository.existsCertificateForStudentAndCourse(studentId, courseId)).thenReturn(true);

        assertThrows(IllegalArgumentException.class, () -> certificateService.create(studentId, courseId));
        verify(certificateRepository, never()).save(any(Certificate.class));
    }

    @Test
    void delete_validId_deletesCertificate() {
        UUID certificateId = UUID.randomUUID();
        Certificate certificate = TestDataFactory.createSampleCertificate();
        when(certificateRepository.findById(certificateId)).thenReturn(java.util.Optional.of(certificate));

        Certificate deletedCertificate = certificateService.delete(certificateId);

        assertNotNull(deletedCertificate);
        assertEquals(certificate, deletedCertificate);
        verify(certificateRepository, times(1)).deleteById(certificateId);
    }

    @Test
    void delete_nonExistingId_throwsResourceNotFoundException() {
        UUID nonExistingId = UUID.randomUUID();
        when(certificateRepository.findById(nonExistingId)).thenReturn(java.util.Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> certificateService.delete(nonExistingId));
        verify(certificateRepository, never()).deleteById(any(UUID.class));
    }


    @Test
    void checkIfCourseExists_existingCourse_returnsCourseDto() {
        CourseDto expectedCourse = TestDataFactory.createSampleCourseDto();
        UUID courseId = expectedCourse.getId();
        when(courseApiService.findCourseById(courseId)).thenReturn(Optional.of(expectedCourse));

        CourseDto result = certificateService.checkIfCourseExists(courseId);

        assertEquals(expectedCourse, result);
    }

    @Test
    void checkIfCourseExists_nonExistingCourse_throwsResourceNotFoundException() {
        UUID courseId = UUID.randomUUID();
        when(courseApiService.findCourseById(courseId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> certificateService.checkIfCourseExists(courseId));
    }

    @Test
    void checkIfStudentExists_existingStudent_returnsStudentDto() {
        StudentDto expectedStudent = TestDataFactory.createSampleStudentDto();
        UUID studentId = expectedStudent.getId();
        when(studentApiService.findStudentById(studentId)).thenReturn(Optional.of(expectedStudent));

        StudentDto result = certificateService.checkIfStudentExists(studentId);

        assertEquals(expectedStudent, result);
    }

    @Test
    void checkIfStudentExists_nonExistingStudent_throwsResourceNotFoundException() {
        UUID studentId = UUID.randomUUID();
        when(studentApiService.findStudentById(studentId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> certificateService.checkIfStudentExists(studentId));
    }

    @Test
    void checkIfStudentPassedCourse_courseEnrollmentNotFound_throwResourceNotFoundException() {
        UUID courseId = UUID.randomUUID();
        UUID studentId = UUID.randomUUID();
        when(courseEnrollmentApiService.findCourseEnrollmentByCourseIdAndStudentId(courseId, studentId))
                .thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class,
                () -> certificateService.checkIfStudentPassedCourse(courseId, studentId));

        verify(courseEnrollmentApiService).findCourseEnrollmentByCourseIdAndStudentId(courseId, studentId);
        verifyNoMoreInteractions(courseEnrollmentApiService);
    }

    @Test
    void checkIfStudentPassedCourse_studentDidNotPassCourse_throwStudentDidNotPassCourseException() {
        CourseEnrollmentDto courseEnrollmentDto = TestDataFactory.getCourseEnrollmentDto(EnrollmentState.FAILED);
        UUID courseId = courseEnrollmentDto.getCourseDto().getId();
        UUID studentId = courseEnrollmentDto.getStudentDto().getId();

        when(courseEnrollmentApiService.findCourseEnrollmentByCourseIdAndStudentId(courseId, studentId))
                .thenReturn(Optional.of(courseEnrollmentDto));

        assertThrows(StudentDidNotPassCourseException.class,
                () -> certificateService.checkIfStudentPassedCourse(courseId, studentId));

        verify(courseEnrollmentApiService).findCourseEnrollmentByCourseIdAndStudentId(courseId, studentId);
        verifyNoMoreInteractions(courseEnrollmentApiService);
    }

    @Test
    void checkIfStudentPassedCourse_studentPassedCourse_noExceptionThrown() {
        CourseEnrollmentDto courseEnrollmentDto = TestDataFactory.getCourseEnrollmentDto(EnrollmentState.PASSED);
        UUID courseId = courseEnrollmentDto.getCourseDto().getId();
        UUID studentId = courseEnrollmentDto.getStudentDto().getId();
        when(courseEnrollmentApiService.findCourseEnrollmentByCourseIdAndStudentId(courseId, studentId))
                .thenReturn(Optional.of(courseEnrollmentDto));

        assertDoesNotThrow(() -> certificateService.checkIfStudentPassedCourse(courseId, studentId));

        verify(courseEnrollmentApiService).findCourseEnrollmentByCourseIdAndStudentId(courseId, studentId);
        verifyNoMoreInteractions(courseEnrollmentApiService);
    }
}
