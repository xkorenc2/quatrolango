package cz.muni.fi.certificate.repository;

import cz.muni.fi.certificate.data.model.Certificate;
import cz.muni.fi.certificate.data.repository.CertificateRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
class CertificateRepositoryTest {

    @Autowired
    private CertificateRepository certificateRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    private Certificate certificate1;
    private Certificate certificate2;
    private Certificate certificate3;

    @BeforeEach
    void initData() {
        certificate1 = createCertificate("Cert1", "Desc1", LocalDateTime.of(2022, 11, 13, 4, 23, 2));
        certificate2 = createCertificate("Cert2", "Desc3", LocalDateTime.of(2021, 2, 23, 4, 3, 2));
        certificate3 = createCertificate("Cert3", "Desc3", LocalDateTime.of(2023, 10, 3, 4, 3, 2));
    }

    @AfterEach
    public void tearDown() {
        certificateRepository.delete(certificate1);
        certificateRepository.delete(certificate2);
        certificateRepository.delete(certificate3);
    }

    private Certificate createCertificate(String name, String description, LocalDateTime validFrom) {
        Certificate certificate = new Certificate();
        certificate.setName(name);
        certificate.setDescription(description);
        certificate.setValidFrom(validFrom);
        certificate.setStudentId(UUID.randomUUID());
        certificate.setPassedCourseId(UUID.randomUUID());

        return testEntityManager.persistAndFlush(certificate);
    }

    @Test
    @Transactional
    void findByIds_validIds_returnsCertificates() {
        Set<UUID> ids = Set.of(certificate1.getId(), certificate2.getId(), certificate3.getId());

        List<Certificate> result = certificateRepository.findByIds(ids);

        assertEquals(3, result.size());
        assertTrue(result.contains(certificate1));
        assertTrue(result.contains(certificate2));
        assertTrue(result.contains(certificate3));
    }

    @Test
    @Transactional
    void findByIds_invalidIds_returnsNone() {
        Set<UUID> ids = Set.of(UUID.randomUUID(), UUID.randomUUID());

        List<Certificate> result = certificateRepository.findByIds(ids);

        assertEquals(0, result.size());
    }


    @Test
    void findByStudentId_validStudentId_returnsCertificates() {
        UUID studentId = certificate1.getStudentId();

        List<Certificate> result = certificateRepository.findByStudentId(studentId);

        assertEquals(1, result.size());
        assertEquals(certificate1, result.get(0));
    }

    @Test
    void findByStudentId_invalidStudentId_returnsNone() {
        List<Certificate> result = certificateRepository.findByStudentId(UUID.randomUUID());

        assertEquals(0, result.size());
    }

    @Test
    void findByPassedCourseId_validCourseId_returnsCertificates() {
        UUID courseId = certificate1.getPassedCourseId();

        List<Certificate> result = certificateRepository.findByPassedCourseId(courseId);

        assertEquals(1, result.size());
        assertEquals(certificate1, result.get(0));
    }

    @Test
    void findByPassedCourseId_invalidCourseId_returnsNone() {
        List<Certificate> result = certificateRepository.findByPassedCourseId(UUID.randomUUID());

        assertEquals(0, result.size());
    }

    @Test
    void findCertificateForStudentAndCourse_validIds_returnsCertificate() {
        UUID studentId = certificate1.getStudentId();
        UUID courseId = certificate1.getPassedCourseId();

        Certificate result = certificateRepository.findCertificateForStudentAndCourse(studentId, courseId);

        assertEquals(certificate1, result);
    }

    @Test
    void findCertificateForStudentAndCourse_invalidStudentId_returnsNone() {
        UUID courseId = certificate1.getPassedCourseId();

        Certificate result = certificateRepository.findCertificateForStudentAndCourse(UUID.randomUUID(), courseId);

        assertNull(result);
    }

    @Test
    void findCertificateForStudentAndCourse_invalidCourseId_returnsNone() {
        UUID studentId = certificate1.getStudentId();

        Certificate result = certificateRepository.findCertificateForStudentAndCourse(studentId, UUID.randomUUID());

        assertNull(result);
    }

    @Test
    void findCertificateForStudentAndCourse_invalidBothIds_returnsNone() {
        Certificate result = certificateRepository.findCertificateForStudentAndCourse(UUID.randomUUID(), UUID.randomUUID());

        assertNull(result);
    }

    @Test
    void existsCertificateForStudentAndCourse_certificateExists_returnsTrue() {
        UUID studentId = certificate1.getStudentId();
        UUID courseId = certificate1.getPassedCourseId();

        boolean result = certificateRepository.existsCertificateForStudentAndCourse(studentId, courseId);

        assertTrue(result);
    }

    @Test
    void existsCertificateForStudentAndCourse_invalidStudentId_returnsFalse() {
        UUID studentId = UUID.randomUUID();
        UUID courseId = certificate1.getPassedCourseId();

        boolean result = certificateRepository.existsCertificateForStudentAndCourse(studentId, courseId);

        assertFalse(result);
    }

    @Test
    void existsCertificateForStudentAndCourse_invalidCourseId_returnsFalse() {
        UUID studentId = certificate1.getStudentId();
        UUID courseId = UUID.randomUUID();

        boolean result = certificateRepository.existsCertificateForStudentAndCourse(studentId, courseId);

        assertFalse(result);
    }

    @Test
    void existsCertificateForStudentAndCourse_invalidBothIds_returnsFalse() {
        UUID studentId = UUID.randomUUID();
        UUID courseId = UUID.randomUUID();

        boolean result = certificateRepository.existsCertificateForStudentAndCourse(studentId, courseId);

        assertFalse(result);
    }


}
