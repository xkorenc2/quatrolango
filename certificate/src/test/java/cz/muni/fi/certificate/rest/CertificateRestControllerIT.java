package cz.muni.fi.certificate.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import cz.muni.fi.certificate.data.mapper.CertificateMapper;
import cz.muni.fi.certificate.data.model.Certificate;
import cz.muni.fi.certificate.data.repository.CertificateRepository;
import cz.muni.fi.certificate.service.CertificatePDFService;
import cz.muni.fi.certificate.service.api.CourseApiService;
import cz.muni.fi.certificate.service.api.CourseEnrollmentApiService;
import cz.muni.fi.certificate.service.api.EmailApiService;
import cz.muni.fi.certificate.service.api.StudentApiService;
import cz.muni.fi.commons.dto.CertificateDto;
import cz.muni.fi.commons.dto.course.CourseDto;
import cz.muni.fi.commons.dto.course.CourseEnrollmentDto;
import cz.muni.fi.commons.dto.user.student.StudentDto;
import cz.muni.fi.commons.enums.EnrollmentState;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WithMockUser(username = "admin", authorities = {"SCOPE_test_2", "SCOPE_test_3"})
class CertificateRestControllerIT {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CertificateRepository certificateRepository;

    @Autowired
    private CertificateMapper mapper;

    @MockBean
    private CourseApiService courseApiService;

    @MockBean
    private StudentApiService studentApiService;

    @MockBean
    private CourseEnrollmentApiService courseEnrollmentApiService;

    @MockBean
    private CertificatePDFService certificatePDFService;

    @MockBean
    private EmailApiService emailApiService;

    private StudentDto student;
    private CourseDto course;
    private CourseEnrollmentDto courseEnrollmentDto;
    private CertificateDto certificateDto;

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;

    @BeforeAll
    public void setupStudentAndCourse() {
        objectMapper.findAndRegisterModules();
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        // Create a course
        course = new CourseDto();
        course.setId(UUID.randomUUID());
        course.setName("Sample Course");
        course.setCapacity(10);
        course.setLanguage(Language.GERMAN);
        course.setProficiencyLevel(ProficiencyLevel.A2);

        // Create a student
        student = new StudentDto();
        student.setId(UUID.randomUUID());
        student.setFirstName("John");
        student.setSecondName("Doe");
        student.setEmail("john.doe@gmail.com");
        student.setDateOfBirth(LocalDate.of(1990, 1, 1));

        courseEnrollmentDto = new CourseEnrollmentDto();
        courseEnrollmentDto.setEnrollmentState(EnrollmentState.PASSED);
        courseEnrollmentDto.setId(UUID.randomUUID());
        courseEnrollmentDto.setStudentDto(student);
        courseEnrollmentDto.setCourseDto(course);

        certificateDto = new CertificateDto();
        certificateDto.setName("Sample Certificate");
        certificateDto.setDescription("This is a sample certificate description.");
        certificateDto.setStudentId(student.getId());
        certificateDto.setPassedCourseId(course.getId());
        certificateDto.setValidFrom(LocalDateTime.now());

    }

    @BeforeEach
    public void setup() {
        // Clean up the database before each test
        certificateRepository.deleteAll();

        // Mock endpoints for Student and Course
        when(courseApiService.findCourseById(course.getId())).thenReturn(Optional.ofNullable(course));
        when(studentApiService.findStudentById(student.getId())).thenReturn(Optional.ofNullable(student));
        when(courseEnrollmentApiService.findCourseEnrollmentByCourseIdAndStudentId(course.getId(), student.getId())).thenReturn(Optional.ofNullable(courseEnrollmentDto));

        Certificate certificate = mapper.toEntity(certificateDto);
        Certificate certi = certificateRepository.save(certificate);
        certificateRepository.flush();
        certificateDto.setId(certi.getId());
    }

    @Test
    void getAllCertificates_certificatesExist_returnsListOfCertificates() throws Exception {
        mockMvc.perform(get("/certificates/").with(csrf()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].id").value(certificateDto.getId().toString()))
                .andExpect(jsonPath("$.content[0].name").value(certificateDto.getName()));
    }

    @Test
    void getAllCertificates_noCertificatesExist_returnsEmptyList() throws Exception {
        // Arrange
        certificateRepository.deleteAll();

        mockMvc.perform(get("/certificates/").with(csrf()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content").isEmpty());

    }


    @Test
    void getCertificateById_certificateExists_returnsCertificate() throws Exception {
        // Arrange
        UUID certificateId = certificateDto.getId();

        // Act
        String response = mockMvc.perform(get("/certificates/" + certificateId).with(csrf()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        // Assert

        objectMapper.registerModule(new JavaTimeModule());

        CertificateDto actualCertificate = objectMapper.readValue(response, CertificateDto.class);

        assertEquals(certificateDto, actualCertificate, "The returned certificate does not match the expected certificate");
    }

    @Test
    void getCertificateById_incorrectId_returnsNotFound() throws Exception {
        // Arrange
        UUID certificateId = UUID.randomUUID();

        // Act
        mockMvc.perform(get("/certificates/" + certificateId).with(csrf()))
                .andExpect(status().isNotFound());
    }

    @Test
    void getCertificatesByIds_certificatesExist_returnsListOfCertificates() throws Exception {
        // Arrange
        CertificateDto certificate1 = new CertificateDto();
        certificate1.setName("Sample Certificate 1");
        certificate1.setDescription("This is a sample certificate description 1.");
        certificate1.setStudentId(student.getId());
        certificate1.setPassedCourseId(course.getId());
        certificate1.setValidFrom(LocalDateTime.now());

        var saveResult = certificateRepository.save(mapper.toEntity(certificate1));
        certificate1.setId(saveResult.getId());

        CertificateDto certificate2 = new CertificateDto();
        certificate2.setName("Sample Certificate 2");
        certificate2.setDescription("This is a sample certificate description 2.");
        certificate2.setStudentId(student.getId());
        certificate2.setPassedCourseId(course.getId());
        certificate2.setValidFrom(LocalDateTime.now());

        saveResult = certificateRepository.save(mapper.toEntity(certificate2));
        certificate2.setId(saveResult.getId());

        List<CertificateDto> expectedCertificates = Arrays.asList(certificateDto, certificate1, certificate2);

        // Act
        String response = mockMvc.perform(post("/certificates/ids").with(csrf())
                        .content(objectMapper.writeValueAsString(Arrays.asList(certificateDto.getId(), certificate1.getId(), certificate2.getId())))
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        // Assert
        objectMapper.registerModule(new JavaTimeModule());

        List<CertificateDto> actualCertificates = Arrays.asList(objectMapper.readValue(response, CertificateDto[].class));

        assertEquals(expectedCertificates.size(), actualCertificates.size(), "The returned list of certificates does not match the length of expected list");
        for (int i = 0; i < 3; i++) {
            assertEquals(expectedCertificates.stream().map(CertificateDto::getId).sorted().toList(),
                    actualCertificates.stream().map(CertificateDto::getId).sorted().toList(),
                    "The returned list of certificates does not match the expected list");
        }
    }

    @Test
    void getCertificatesByStudentId_certificatesExist_returnsListOfCertificates() throws Exception {
        // Arrange
        List<CertificateDto> expectedCertificates = Collections.singletonList(certificateDto);
        assert (certificateDto.getStudentId() == student.getId());

        // Act
        String response = mockMvc.perform(get("/certificates/students/" + certificateDto.getStudentId()).with(csrf()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        // Assert
        objectMapper.registerModule(new JavaTimeModule());

        List<CertificateDto> actualCertificates = Arrays.asList(objectMapper.readValue(response, CertificateDto[].class));

        assertEquals(expectedCertificates, actualCertificates, "The returned list of certificates does not match the expected list");
    }

    @Test
    void getCertificatesByStudentId_studentDoesNotExist_returnsNotFound() throws Exception {
        // Arrange
        UUID studentId = UUID.randomUUID();

        // Act
        mockMvc.perform(get("/certificates/students/" + studentId).with(csrf()))
                .andExpect(status().isNotFound());
    }

    @Test
    void getCertificatesByCourseId_certificatesExist_returnsListOfCertificates() throws Exception {
        // Arrange
        UUID courseId = certificateDto.getPassedCourseId();
        List<CertificateDto> expectedCertificates = Collections.singletonList(certificateDto);

        // Act
        String response = mockMvc.perform(get("/certificates/courses/" + courseId).with(csrf()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        // Assert
        objectMapper.registerModule(new JavaTimeModule());

        List<CertificateDto> actualCertificates = Arrays.asList(objectMapper.readValue(response, CertificateDto[].class));

        assertEquals(expectedCertificates, actualCertificates, "The returned list of certificates does not match the expected list");
    }

    @Test
    void getCertificatesByCourseId_courseDoesNotExist_returnsNotFound() throws Exception {
        // Arrange
        UUID courseId = UUID.randomUUID();

        // Act
        mockMvc.perform(get("/certificates/courses/" + courseId).with(csrf()))
                .andExpect(status().isNotFound());
    }

    @Test
    void getCertificateByStudentAndCourseId_certificateExists_returnsCertificate() throws Exception {
        // Arrange
        UUID studentId = certificateDto.getStudentId();
        UUID courseId = certificateDto.getPassedCourseId();

        // Act
        String response = mockMvc.perform(get("/certificates/students/" + studentId + "/courses/" + courseId).with(csrf()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        // Assert
        objectMapper.registerModule(new JavaTimeModule());

        CertificateDto actualCertificate = objectMapper.readValue(response, CertificateDto.class);

        assertEquals(certificateDto, actualCertificate, "The returned certificate does not match the expected certificate");
    }

    @Test
    void getCertificateByStudentAndCourseId_studentDoesNotExist_returnsNotFound() throws Exception {
        // Arrange
        UUID studentId = UUID.randomUUID();
        UUID courseId = certificateDto.getPassedCourseId();

        // Act
        mockMvc.perform(get("/certificates/students/" + studentId + "/courses/" + courseId).with(csrf()))
                .andExpect(status().isNotFound());
    }

    @Test
    void getCertificateByStudentAndCourseId_courseDoesNotExist_returnsNotFound() throws Exception {
        // Arrange
        UUID studentId = certificateDto.getStudentId();
        UUID courseId = UUID.randomUUID();

        // Act
        mockMvc.perform(get("/certificates/students/" + studentId + "/courses/" + courseId).with(csrf()))
                .andExpect(status().isNotFound());
    }

    @Test
    void createCertificate_certificateCreated_returnsCertificate() throws Exception {
        certificateRepository.deleteAll();
        doNothing().when(emailApiService).sendFileViaEmail(anyString(), anyString(), any(File.class));

        String response = mockMvc.perform(post("/certificates/students/" + certificateDto.getStudentId() + "/courses/" + certificateDto.getPassedCourseId()).with(csrf()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();


        objectMapper.registerModule(new JavaTimeModule());

        CertificateDto actualCertificate = objectMapper.readValue(response, CertificateDto.class);

        assertTrue(certificateRepository.existsById(actualCertificate.getId()), "The returned certificate does not exist in the repository");
        assertEquals(actualCertificate.getStudentId(), student.getId(), "The student id of the returned certificate does not match the expected student id");
        assertEquals(actualCertificate.getPassedCourseId(), course.getId(), "The course id of the returned certificate does not match the expected course id");
    }

    @Test
    void createCertificate_studentDoesNotExist_returnsNotFound() throws Exception {
        // Arrange
        UUID studentId = UUID.randomUUID();

        // Act
        mockMvc.perform(post("/certificates/students/" + studentId + "/courses/" + certificateDto.getPassedCourseId()).with(csrf()))
                .andExpect(status().isNotFound());
    }

    @Test
    void createCertificate_courseDoesNotExist_returnsNotFound() throws Exception {
        // Arrange
        UUID courseId = UUID.randomUUID();

        // Act
        mockMvc.perform(post("/certificates/students/" + certificateDto.getStudentId() + "/courses/" + courseId).with(csrf()))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteCertificate_certificateDeleted_returnsCertificate() throws Exception {
        // Arrange
        UUID certificateId = certificateDto.getId();

        // Act
        String response = mockMvc.perform(get("/certificates/" + certificateId).with(csrf()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        // Assert
        objectMapper.registerModule(new JavaTimeModule());

        CertificateDto actualCertificate = objectMapper.readValue(response, CertificateDto.class);

        assertEquals(certificateDto, actualCertificate, "The returned certificate does not match the expected certificate");
    }

    @Test
    void deleteCertificate_incorrectId_returnsNotFound() throws Exception {
        // Arrange
        UUID certificateId = UUID.randomUUID();

        // Act
        mockMvc.perform(get("/certificates/" + certificateId).with(csrf()))
                .andExpect(status().isNotFound());
    }
}
