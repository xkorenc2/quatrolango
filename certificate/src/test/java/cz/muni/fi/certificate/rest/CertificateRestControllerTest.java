package cz.muni.fi.certificate.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.certificate.facade.CertificateFacade;
import cz.muni.fi.certificate.factory.TestDataFactory;
import cz.muni.fi.commons.dto.CertificateDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CertificateRestController.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WithMockUser(username = "admin", authorities = {"SCOPE_test_1", "SCOPE_test_2", "SCOPE_test_3"})
class CertificateRestControllerTest {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    @MockBean
    private CertificateFacade certificateFacade;
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;

    @BeforeAll
    void setup() {
        objectMapper.findAndRegisterModules();
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }


    @Test
    void getAllCertificates_certificatesFound_returnsCertificates() throws Exception {
        Set<UUID> ids = new HashSet<>(Arrays.asList(UUID.randomUUID(), UUID.randomUUID()));
        Page<CertificateDto> certificates = new PageImpl<>(TestDataFactory.createListOfCertificateDtosWithIds(ids));


        when(certificateFacade.findAll(any(Pageable.class))).thenReturn(certificates);

        mockMvc.perform(MockMvcRequestBuilders.get("/certificates/").with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void getCertificateById_certificateFound_returnsCertificate() throws Exception {
        UUID certificateId = UUID.randomUUID();
        CertificateDto certificateDto = TestDataFactory.createSampleCertificateDto(certificateId);

        when(certificateFacade.findById(certificateId)).thenReturn(certificateDto);

        mockMvc.perform(MockMvcRequestBuilders.get("/certificates/{id}", certificateId).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(certificateId.toString()));
    }

    @Test
    void getCertificatesByIds_certificatesFound_returnsCertificates() throws Exception {
        Set<UUID> ids = new HashSet<>(Arrays.asList(UUID.randomUUID(), UUID.randomUUID()));
        List<CertificateDto> certificates = TestDataFactory.createListOfCertificateDtosWithIds(ids);


        when(certificateFacade.findByIds(ids)).thenReturn(certificates);

        mockMvc.perform(MockMvcRequestBuilders.post("/certificates/ids").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(ids)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(certificates.get(0).getId().toString()))
                .andExpect(jsonPath("$[1].id").value(certificates.get(1).getId().toString()));
    }

    @Test
    void getCertificatesByStudentId_certificatesFound_returnsCertificates() throws Exception {
        UUID studentId = UUID.randomUUID();
        CertificateDto certificateDto = TestDataFactory.createSampleCertificateDto(UUID.randomUUID());
        List<CertificateDto> certificates = Collections.singletonList(certificateDto);

        when(certificateFacade.findByStudentId(studentId)).thenReturn(certificates);

        mockMvc.perform(MockMvcRequestBuilders.get("/certificates/students/{studentId}", studentId).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(certificateDto.getId().toString()));
    }

    @Test
    void createCertificate_certificateCreated_returnsCreatedCertificate() throws Exception {
        UUID studentId = UUID.randomUUID();
        UUID courseId = UUID.randomUUID();
        CertificateDto certificateDto = TestDataFactory.createSampleCertificateDto(UUID.randomUUID());

        when(certificateFacade.create(studentId, courseId)).thenReturn(certificateDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/certificates/students/{studentId}/courses/{courseId}", studentId, courseId).with(csrf()))
                .andExpect(status().isOk());
    }

    @Test
    void deleteCertificate_certificateFound_returnsDeletedCertificate() throws Exception {
        UUID certificateId = UUID.randomUUID();
        CertificateDto certificateDto = TestDataFactory.createSampleCertificateDto(certificateId);

        when(certificateFacade.delete(certificateId)).thenReturn(certificateDto);

        mockMvc.perform(MockMvcRequestBuilders.delete("/certificates/{id}", certificateId).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(certificateDto.getId().toString()));
    }

    @Test
    void getCertificatesByCourseId_certificatesFound_returnsCertificates() throws Exception {
        UUID courseId = UUID.randomUUID();
        CertificateDto certificateDto = TestDataFactory.createSampleCertificateDto(UUID.randomUUID());
        List<CertificateDto> certificates = Collections.singletonList(certificateDto);

        when(certificateFacade.findByCourseId(courseId)).thenReturn(certificates);

        mockMvc.perform(MockMvcRequestBuilders.get("/certificates/courses/{courseId}", courseId).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(certificateDto.getId().toString()));
    }

    @Test
    void getCertificateByStudentAndCourseId_certificateFound_returnsCertificate() throws Exception {
        UUID studentId = UUID.randomUUID();
        UUID courseId = UUID.randomUUID();
        CertificateDto certificateDto = TestDataFactory.createSampleCertificateDto(UUID.randomUUID());

        when(certificateFacade.findCertificateForStudentAndCourse(studentId, courseId)).thenReturn(certificateDto);

        mockMvc.perform(MockMvcRequestBuilders.get("/certificates/students/{studentId}/courses/{courseId}", studentId, courseId).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(certificateDto.getId().toString()));
    }


    @Test
    void getCertificateById_certificateNotFound_returnsNotFound() throws Exception {
        UUID certificateId = UUID.randomUUID();

        when(certificateFacade.findById(certificateId)).thenReturn(null);

        mockMvc.perform(MockMvcRequestBuilders.get("/certificates/{id}", certificateId).with(csrf()))
                .andExpect(status().isNotFound());
    }

    @Test
    void getCertificatesByCourseId_noCertificatesFound_returnsEmptyList() throws Exception {
        UUID courseId = UUID.randomUUID();

        when(certificateFacade.findByCourseId(courseId)).thenReturn(Collections.emptyList());

        mockMvc.perform(MockMvcRequestBuilders.get("/certificates/courses/{courseId}", courseId).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    void deleteCertificate_certificateNotFound_returnsNotFound() throws Exception {
        UUID certificateId = UUID.randomUUID();

        when(certificateFacade.delete(certificateId)).thenReturn(null);

        mockMvc.perform(MockMvcRequestBuilders.delete("/certificates/{id}", certificateId).with(csrf()))
                .andExpect(status().isNotFound());
    }

}
