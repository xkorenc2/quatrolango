# include defined env variables
include .env
export

# Choose Docker Compose files based on the environment
ifeq ($(ENV), dev)
    DC_FILE = -f compose.yml -f compose.dev.yml
else
    DC_FILE = -f compose.yml
endif

# The default action when you just run `make`
all: build run

# Build the project using Maven
build:
	mvn clean install

# Start up services with Docker Compose
run:
	docker-compose $(DC_FILE)  up --build -d core core-db user user-db certificate certificate-db email

run-observe:
	docker-compose $(DC_FILE)  up --build -d prometheus grafana

run-showcase:
	docker-compose $(DC_FILE)  up --build -d showcase

run-token-client:
	docker-compose $(DC_FILE)  up --build -d oidc-client

integration-test:
	mvn failsafe:integration-test

# drop data from user-db
truncate-user-db:
	@docker-compose exec user-db psql -U $${DB_USER_NAME} -d $${USER_SERVICE_DB} -f /scripts/truncate.sql

# drop data from core-db
truncate-core-db:
	@docker-compose exec core-db psql -U $${DB_USER_NAME} -d $${CORE_SERVICE_DB} -f /scripts/truncate.sql

# drop all data from user-db and core-db (DBs that use seeding)
drop-seeding:
	make truncate-user-db
	make truncate-core-db

# Tear down running services and remove the created Docker images
down:
	docker-compose down --rmi all

.PHONY: all build run down drop-seeding truncate-core-db truncate-user-db integration-test run-showcase run-observe