package cz.muni.fi.commons.dto.user.student;

import cz.muni.fi.commons.dto.user.lecturer.LecturerLanguageCreateDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
public class LecturerCreateDto extends StudentCreateDto {

    @Schema(description = "Whether the lecturer is a native speaker of the language of instruction")
    private boolean isNativeSpeaker;

    @Schema(description = "The list of languages the lecturer speaks")
    private List<LecturerLanguageCreateDto> languages;
}
