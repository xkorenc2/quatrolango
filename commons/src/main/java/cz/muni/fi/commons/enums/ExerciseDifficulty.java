package cz.muni.fi.commons.enums;

public enum ExerciseDifficulty {
    EASY("easy"),
    MEDIUM("medium"),
    HARD("hard");

    private final String value;

    ExerciseDifficulty(String value) {
        this.value = value;
    }
}
