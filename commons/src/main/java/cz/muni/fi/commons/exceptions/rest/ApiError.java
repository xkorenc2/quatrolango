package cz.muni.fi.commons.exceptions.rest;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.Map;

@Setter
@Getter
public class ApiError {
    private LocalDateTime timeStamp;
    private HttpStatus httpStatus;
    private String message;
    private String path;
    private Map<String, String> errors;


    public ApiError(HttpStatus httpStatus, String message, String path) {
        this.timeStamp = LocalDateTime.now(Clock.systemUTC());
        this.httpStatus = httpStatus;
        this.message = message;
        this.path = path;
    }

    @Override
    public String toString() {
        return "ApiError{" +
                "timestamp=" + timeStamp +
                ", status=" + httpStatus +
                ", message='" + message + '\'' +
                ", path='" + path + '\'' +
                ", errors=" + errors +
                '}';
    }
}
