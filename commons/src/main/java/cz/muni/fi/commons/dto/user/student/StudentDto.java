package cz.muni.fi.commons.dto.user.student;

import cz.muni.fi.commons.dto.user.UserDto;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@EqualsAndHashCode(callSuper = false)
@ToString
@Schema(title = "Student", description = "Represents a student that can enroll in lectures")
public class StudentDto extends UserDto {
    @NotNull(message = "Boolean value checking if the student is active")
    @Schema(description = "Whether the student is active or not in school system")
    private boolean isActive;
}
