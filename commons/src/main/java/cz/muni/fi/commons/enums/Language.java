package cz.muni.fi.commons.enums;

public enum Language {
    ENGLISH("english"),
    GERMAN("german"),
    SPANISH("spanish"),
    MANDARIN("mandarin");

    private final String value;

    Language(String value) {
        this.value = value;
    }
}
