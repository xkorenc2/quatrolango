package cz.muni.fi.commons.dto.exercise;

import cz.muni.fi.commons.enums.ExerciseDifficulty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@Setter
@Getter
@ToString
@EqualsAndHashCode(of = "id")
@Schema(name = "ExerciseDTO")
public class ExerciseDto {

    @Schema(description = "Id of an exercise", requiredMode = Schema.RequiredMode.REQUIRED, accessMode = Schema.AccessMode.READ_ONLY)
    private UUID id;

    @Schema(description = "Id of a course", requiredMode = Schema.RequiredMode.REQUIRED, accessMode = Schema.AccessMode.READ_ONLY)
    private UUID courseId;

    @Schema(description = "Name of an exercise", requiredMode = Schema.RequiredMode.REQUIRED, maxLength = 100)
    @NotBlank(message = "Name is required")
    @Size(max = 100)
    private String name;

    @Schema(description = "Description of the exercise", maxLength = 255)
    @Size(max = 255)
    private String description;

    @Schema(description = "Difficulty of the exercise", requiredMode = Schema.RequiredMode.REQUIRED, enumAsRef = true)
    @NotNull(message = "Difficulty is required")
    private ExerciseDifficulty exerciseDifficulty;
}
