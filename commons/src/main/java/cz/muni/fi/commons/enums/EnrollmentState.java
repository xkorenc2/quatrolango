package cz.muni.fi.commons.enums;

public enum EnrollmentState {
    PASSED("passed"),
    ENROLLED("enrolled"),
    FAILED("failed");

    private final String value;

    EnrollmentState(String value) {
        this.value = value;
    }
}
