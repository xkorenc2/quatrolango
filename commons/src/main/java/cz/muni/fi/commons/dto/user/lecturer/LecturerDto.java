package cz.muni.fi.commons.dto.user.lecturer;

import cz.muni.fi.commons.dto.user.UserDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@ToString
@Getter
@Setter
@Schema(description = "Data transfer object that represents a lecturer")
public class LecturerDto extends UserDto {

    @Schema(description = "Whether the lecturer is a native speaker of the language of instruction")
    private boolean isNativeSpeaker;

    @Schema(description = "The list of languages the lecturer speaks")
    private List<LecturerLanguageDto> languages;
}
