package cz.muni.fi.commons.dto.course;

import cz.muni.fi.commons.dto.exercise.ExerciseDto;
import cz.muni.fi.commons.dto.lecture.LectureDto;
import cz.muni.fi.commons.dto.user.student.StudentDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Setter
@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title = "DetailedCourse")
public class CourseDetailedDto extends CourseDto {

    @Schema(description = "Set of students enrolled in this course")
    private List<StudentDto> students;

    @Schema(description = "Set of lectures available in this course")
    private List<LectureDto> lectures;

    @Schema(description = "Set of exercises available in this course")
    private List<ExerciseDto> exercises;
}
