package cz.muni.fi.commons.exceptions;

public class CertificatePDFGenerationException extends RuntimeException {
    public CertificatePDFGenerationException(String message) {
        super(message);
    }
}
