package cz.muni.fi.commons.exceptions;

public class UnprivilegedActionException extends RuntimeException {

    public UnprivilegedActionException() {
        super();
    }

    public UnprivilegedActionException(String message) {
        super(message);
    }

    public UnprivilegedActionException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnprivilegedActionException(Throwable cause) {
        super(cause);
    }

    protected UnprivilegedActionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
