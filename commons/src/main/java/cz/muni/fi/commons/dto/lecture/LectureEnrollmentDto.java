package cz.muni.fi.commons.dto.lecture;

import cz.muni.fi.commons.dto.user.student.StudentDto;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "id")
@Schema(title = "Lecture Enrollment", description = "Represents a student enrolled in lecture")
public class LectureEnrollmentDto {

    @Schema(description = "Id of a lecture enrollment", requiredMode = RequiredMode.REQUIRED, accessMode = AccessMode.READ_ONLY)
    private UUID id;

    @Schema(description = "Student enrolled in a lecture")
    private StudentDto studentDto;

    @Schema(description = "Lecture where student is enrolled")
    private LectureDto lectureDto;
}
