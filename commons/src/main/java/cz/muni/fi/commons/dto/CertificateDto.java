package cz.muni.fi.commons.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "id")
@Schema(title = "Certificate", description = "Represents a certificate that can be obtained by a student")
public class CertificateDto {

    @Schema(description = "Id of a certificate", requiredMode = Schema.RequiredMode.REQUIRED, accessMode = Schema.AccessMode.READ_ONLY)
    private UUID id;

    @Schema(description = "Name of the certificate", requiredMode = Schema.RequiredMode.REQUIRED, maxLength = 100)
    @NotBlank(message = "Name is required")
    private String name;

    @Schema(description = "Description of the certificate", maxLength = 255)
    private String description;

    @Schema(description = "Valid from", format = "date-time", accessMode = Schema.AccessMode.READ_ONLY)
    private LocalDateTime validFrom;

    @Schema(description = "Id of a student", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "Student is required")
    private UUID studentId;

    @Schema(description = "Id of a course", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "Course is required")
    private UUID passedCourseId;
}
