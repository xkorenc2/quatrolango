package cz.muni.fi.commons.dto.course;

import cz.muni.fi.commons.dto.user.student.StudentDto;
import cz.muni.fi.commons.enums.EnrollmentState;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "id")
@Schema(title = "Course Enrollment", description = "Represents a student enrolled in course")
public class CourseEnrollmentDto {

    @Schema(description = "Id of a course enrollment", requiredMode = RequiredMode.REQUIRED, accessMode = AccessMode.READ_ONLY)
    private UUID id;

    @Schema(description = "Student Dto enrolled in a course")
    private StudentDto studentDto;

    @Schema(description = "Course Dto where student is enrolled")
    private CourseDto courseDto;

    @Schema(description = "State of student's enrollment")
    private EnrollmentState enrollmentState;

    @Schema(description = "Time of enrolment change")
    private LocalDateTime updatedAt;

    @Schema(description = "Time of enrolment creation")
    private LocalDateTime createdAt;
}
