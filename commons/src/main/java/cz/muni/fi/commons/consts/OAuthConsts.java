package cz.muni.fi.commons.consts;

public class OAuthConsts {
    public static final String STUDENT_SCOPE = "SCOPE_test_1";
    public static final String LECTURER_SCOPE = "SCOPE_test_2";
    public static final String ADMIN_SCOPE = "SCOPE_test_3";
    public static final String SECURITY_SCHEME_BEARER = "Bearer";
    private static final String SECURITY_SCHEME_OAUTH2 = "MUNI";
    public static final String SECURITY_SCHEME_NAME = SECURITY_SCHEME_OAUTH2;
}
