package cz.muni.fi.commons.enums;

public enum ProficiencyLevel {
    A1("a1"),
    A2("a2"),
    B1("b1"),
    B2("b2"),
    C1("c1"),
    C2("c2");

    private final String value;

    ProficiencyLevel(String value) {
        this.value = value;
    }
}
