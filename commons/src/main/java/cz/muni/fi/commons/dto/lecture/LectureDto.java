package cz.muni.fi.commons.dto.lecture;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.UUID;


@Setter
@Getter
@EqualsAndHashCode(of = "id")
@ToString
@Schema(title = "Lecture", description = "Data transfer object that represents a lecture")
public class LectureDto {

    @Schema(description = "The unique identifier of the lecture", requiredMode = Schema.RequiredMode.REQUIRED, accessMode = Schema.AccessMode.READ_ONLY)
    private UUID id;

    @Schema(description = "The name of the lecture", requiredMode = Schema.RequiredMode.REQUIRED, minLength = 1, maxLength = 100)
    @NotBlank(message = "Lecture name cannot be blank")
    @Size(min = 1, max = 100, message = "Lecture name cannot be empty or exceed 100 characters")
    private String name;

    @Schema(description = "A brief description of the lecture", nullable = true, maxLength = 500)
    @Size(max = 500, message = "Description cannot exceed 500 characters")
    private String description;

    @Schema(description = "The start time of the lecture", format = "date-time", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "Start time cannot be null")
    private LocalDateTime startTime;

    @Schema(description = "The end time of the lecture", format = "date-time", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "End time cannot be null")
    private LocalDateTime endTime;

    @Schema(description = "The ID of course to which this lecture belongs", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "Course ID is required")
    private UUID courseId;

    @Schema(description = "The ID of lecturer conducting this lecture", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "Lecturer ID is required")
    private UUID lecturerId;

    @Schema(description = "The capacity of the lecture", minimum = "1", maximum = "10000")
    @Min(value = 1, message = "Lecture capacity must be at least 1")
    @Max(value = 10000, message = "Lecture capacity cannot exceed 10000")
    @NotNull(message = "Lecture capacity cannot be null")
    private int capacity;
}
