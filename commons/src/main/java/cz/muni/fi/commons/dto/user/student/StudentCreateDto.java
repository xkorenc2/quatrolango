package cz.muni.fi.commons.dto.user.student;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
public class StudentCreateDto extends UserCreateDto {

    @NotNull(message = "Boolean value checking if the student is active")
    @Schema(description = "Whether the student is active or not in school system")
    private boolean isActive;
}
