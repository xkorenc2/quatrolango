package cz.muni.fi.commons.dto.user.student;

import cz.muni.fi.commons.dto.lecture.LectureDto;
import cz.muni.fi.commons.dto.course.CourseDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Setter
@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title = "Detailed Student", description = "Also shows specific Lectures, Exercises, Courses.")
public class StudentDetailedDto extends StudentDto {

    @Schema(description = "Set of lectures the student is enrolled in")
    private Set<LectureDto> lectures;

    @Schema(description = "Set of courses the student is enrolled in")
    private Set<CourseDto> courses;
}

