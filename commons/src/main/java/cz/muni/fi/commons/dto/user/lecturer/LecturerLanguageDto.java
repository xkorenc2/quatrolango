package cz.muni.fi.commons.dto.user.lecturer;

import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "id")
@Schema(title = "Lecturer Language", description = "Represents a language a lecturer can speak")
public class LecturerLanguageDto {

    @Schema(description = "Id of a lecturer language", requiredMode = Schema.RequiredMode.REQUIRED, accessMode = Schema.AccessMode.READ_ONLY)
    private UUID id;

    @Schema(description = "language")
    private Language language;
    
    @Schema(description = "the proficiency level")
    private ProficiencyLevel proficiencyLevel;
}
