package cz.muni.fi.commons.exceptions;

public class ServiceApiException extends RuntimeException {
    public ServiceApiException(String message) {
        super(message);
    }
}
