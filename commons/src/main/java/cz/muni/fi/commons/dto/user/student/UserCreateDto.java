package cz.muni.fi.commons.dto.user.student;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class UserCreateDto {

    @NotBlank(message = "First name is required.")
    @Schema(description = "First name of the student", example = "John", maxLength = 64)
    private String firstName;

    @NotNull(message = "Second name is required.")
    @Schema(description = "Second name of the student", example = "Doe", maxLength = 64)
    private String secondName;

    @Schema(description = "Middle name of the student", example = "Fred", nullable = true)
    private String middleName;

    @NotBlank(message = "E-mail is required.")
    @Email(message = "Invalid email format")
    @Schema(description = "E-mail address of the user", example = "johndoe@mail.com")
    private String email;

    @Schema(description = "The date of birth of the user", format = "date", example = "2007-12-23")
    @NotNull(message = "Date of birth is required.")
    private LocalDate dateOfBirth;
}
