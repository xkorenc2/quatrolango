package cz.muni.fi.commons.dto.course;

import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Schema(title = "Course Update DTO", description = "Represents a update DTO for language course")
public class CourseUpdateDto {

    @Schema(description = "Name of a course", requiredMode = RequiredMode.REQUIRED, maxLength = 100)
    @NotBlank(message = "Name is required")
    @Size(max = 100)
    private String name;

    @Schema(description = "Capacity of a course", minimum = "1", maximum = "999", requiredMode = RequiredMode.REQUIRED)
    @NotNull(message = "Capacity is required")
    @Positive(message = "Capacity must be a positive number")
    @Max(999)
    @Min(1)
    private int capacity;

    @Schema(description = "Language of a course", requiredMode = RequiredMode.REQUIRED, enumAsRef = true)
    @NotNull(message = "Language is required")
    private Language language;

    @Schema(description = "Proficiency level of a course", requiredMode = RequiredMode.REQUIRED, enumAsRef = true)
    @NotNull(message = "Proficiency level is required")
    private ProficiencyLevel proficiencyLevel;
}
