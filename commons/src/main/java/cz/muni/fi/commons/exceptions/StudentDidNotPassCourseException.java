package cz.muni.fi.commons.exceptions;

public class StudentDidNotPassCourseException extends RuntimeException {
    public StudentDidNotPassCourseException(String message) {
        super(message);
    }

}
