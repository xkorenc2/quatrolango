package cz.muni.fi.commons.dto.course;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.UUID;

@Setter
@Getter
@Schema(title = "Course", description = "Represents a language course")
@EqualsAndHashCode(of = "id", callSuper = false)
@ToString
public class CourseDto extends CourseUpdateDto {

    @Schema(description = "Id of a course", requiredMode = RequiredMode.REQUIRED, accessMode = AccessMode.READ_ONLY)
    private UUID id;

    @Schema(type = "string", format = "date-time", description = "Time of creation", requiredMode = RequiredMode.REQUIRED, accessMode = AccessMode.READ_ONLY)
    private LocalDateTime createdAt;

    @Schema(type = "string", format = "date-time", description = "Time of update", requiredMode = RequiredMode.REQUIRED, accessMode = AccessMode.READ_ONLY)
    private LocalDateTime updatedAt;
}
