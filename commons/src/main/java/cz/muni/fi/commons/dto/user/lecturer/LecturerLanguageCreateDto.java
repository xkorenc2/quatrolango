package cz.muni.fi.commons.dto.user.lecturer;

import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode()
@Schema(title = "Lecturer Language create DTO", description = "Represents a language a lecturer can speak. Used for creation.")
public class LecturerLanguageCreateDto {

    @Schema(description = "language")
    private Language language;

    @Schema(description = "the proficiency level")
    private ProficiencyLevel proficiencyLevel;
}
