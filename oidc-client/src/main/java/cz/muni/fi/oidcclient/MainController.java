package cz.muni.fi.oidcclient;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Spring MVC Controller.
 * Handles HTTP requests by preparing data in model and passing it to Thymeleaf HTML templates.
 */
@Controller
public class MainController {
    @GetMapping("/")
    public String index(@AuthenticationPrincipal OidcUser user) {
        return user == null ? "index" : "redirect:/authscreen";
    }

    @GetMapping("/authscreen")
    public String authenticatedPage(Model model,
                                    @AuthenticationPrincipal OidcUser user,
                                    @RegisteredOAuth2AuthorizedClient OAuth2AuthorizedClient oauth2Client) {
        model.addAttribute("user", user);
        model.addAttribute("issuerName",
                "https://oidc.muni.cz/oidc/".equals(user.getIssuer().toString()) ? "MUNI" : "Google");
        model.addAttribute("authInfo", oauth2Client.getAccessToken().getTokenValue());
        return "auth";
    }
}