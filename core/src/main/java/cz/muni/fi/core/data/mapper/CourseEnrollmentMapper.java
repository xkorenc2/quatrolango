package cz.muni.fi.core.data.mapper;

import cz.muni.fi.commons.dto.course.CourseEnrollmentDto;
import cz.muni.fi.core.data.model.CourseEnrollment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CourseEnrollmentMapper {
    @Mapping(target = "studentDto", ignore = true)
    @Mapping(target = "courseDto", ignore = true)
    CourseEnrollmentDto mapToDto(CourseEnrollment courseEnrollment);

    @Mapping(source = "courseDto", target = "course")
    @Mapping(target = "course.lectures", ignore = true)
    @Mapping(target = "course.exercises", ignore = true)
    @Mapping(target = "course.courseEnrollments", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "studentId", source = "studentDto.id")
    CourseEnrollment mapToEntity(CourseEnrollmentDto courseDto);

    List<CourseEnrollmentDto> mapToDtoList(List<CourseEnrollment> courseEnrollments);

}
