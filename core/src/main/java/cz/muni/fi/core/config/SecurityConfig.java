package cz.muni.fi.core.config;

import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

import static cz.muni.fi.commons.consts.OAuthConsts.ADMIN_SCOPE;
import static cz.muni.fi.commons.consts.OAuthConsts.LECTURER_SCOPE;
import static cz.muni.fi.commons.consts.OAuthConsts.SECURITY_SCHEME_BEARER;
import static cz.muni.fi.commons.consts.OAuthConsts.STUDENT_SCOPE;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(x -> x
                        .requestMatchers("/swagger-ui/**").permitAll()
                        .requestMatchers("/v3/api-docs/**").permitAll()
                        .requestMatchers("/actuator/**").permitAll()
                        .requestMatchers(HttpMethod.GET, "/course-enrollments/**", "/lecture-enrollments/**").hasAnyAuthority(LECTURER_SCOPE, STUDENT_SCOPE, ADMIN_SCOPE)
                        .requestMatchers(HttpMethod.POST, "/course-enrollments/**", "/lecture-enrollments/**").hasAnyAuthority(LECTURER_SCOPE, STUDENT_SCOPE, ADMIN_SCOPE)
                        .requestMatchers(HttpMethod.DELETE, "/course-enrollments/**", "/lecture-enrollments/**").hasAnyAuthority(LECTURER_SCOPE, ADMIN_SCOPE)
                        .requestMatchers(HttpMethod.PATCH, "/course-enrollments/**", "/lecture-enrollments/**").hasAnyAuthority(LECTURER_SCOPE, ADMIN_SCOPE)
                        .requestMatchers(HttpMethod.GET, "/lectures/**", "/courses/**", "/exercises/**").hasAnyAuthority(STUDENT_SCOPE, LECTURER_SCOPE, ADMIN_SCOPE)
                        .requestMatchers(HttpMethod.PUT, "/lectures/**", "/courses/**", "/exercises/**").hasAnyAuthority(LECTURER_SCOPE, ADMIN_SCOPE)
                        .requestMatchers(HttpMethod.POST, "/lectures/**", "/courses/**", "/exercises/**").hasAnyAuthority(LECTURER_SCOPE, ADMIN_SCOPE)
                        .requestMatchers(HttpMethod.DELETE, "/lectures/**", "/courses/**", "/exercises/**").hasAnyAuthority(LECTURER_SCOPE, ADMIN_SCOPE)
                )
                .oauth2ResourceServer(oauth2 -> oauth2.opaqueToken(Customizer.withDefaults()));
        return http.build();
    }

    @Bean
    public OpenApiCustomizer openAPICustomizer() {
        return openApi -> {
            openApi.getComponents().addSecuritySchemes(SECURITY_SCHEME_BEARER,
                    new SecurityScheme()
                            .type(SecurityScheme.Type.HTTP)
                            .scheme("bearer")
                            .description("provide a valid access token"));
        };
    }
}
