package cz.muni.fi.core.rest;

import cz.muni.fi.commons.dto.course.CourseDetailedDto;
import cz.muni.fi.commons.dto.course.CourseDto;
import cz.muni.fi.commons.dto.course.CourseUpdateDto;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import cz.muni.fi.core.facade.CourseFacade;
import io.micrometer.observation.annotation.Observed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@RequestMapping(path = "/courses", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@Tag(name = "Course service", description = "Service for operations on Course model")
public class CourseRestController {
    private final CourseFacade courseFacade;

    @Autowired
    public CourseRestController(CourseFacade courseFacade) {
        this.courseFacade = courseFacade;
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Find Course by ID", security = @SecurityRequirement(name = "Bearer"), description = "Finds a course by the provided ID")
    @Observed(name = "findCourseById")
    public ResponseEntity<CourseDto> findById(@PathVariable("id") UUID id) {
        return Optional.ofNullable(courseFacade.findById(id))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping(path = "/detailed/{id}")
    @Operation(
            summary = "Find Detailed Course by ID",
            security = @SecurityRequirement(name = "Bearer"),
            description = "Finds a course by the provided ID and displays it in a detailed view"
    )
    @Observed(name = "findDetailedCourseById")
    public ResponseEntity<CourseDetailedDto> findByIdDetailed(@PathVariable("id") UUID id) {
        return ResponseEntity.ok(courseFacade.findByIdDetailed(id));
    }

    @GetMapping(path = "/")
    @Operation(summary = "Find All Courses", security = @SecurityRequirement(name = "Bearer"), description = "Finds all courses")
    @Observed(name = "findAllCourses")
    public ResponseEntity<Page<CourseDto>> findAll(Pageable pageable) {
        return ResponseEntity.ok(courseFacade.findAll(pageable));
    }

    @PostMapping(path = "/ids")
    @Operation(summary = "Find courses by IDs", security = @SecurityRequirement(name = "Bearer"), description = "Find courses by provided IDs")
    @Observed(name = "findCoursesByIds")
    public ResponseEntity<List<CourseDto>> findByIds(@RequestBody Set<UUID> ids) {
        return ResponseEntity.ok(courseFacade.findByIds(ids));
    }

    @GetMapping(path = "/levels/{proficiencyLevel}")
    @Operation(summary = "Find Courses by Proficiency Level", security = @SecurityRequirement(name = "Bearer"), description = "Finds courses by proficiency level")
    @Observed(name = "findCoursesByProficiencyLevel")
    public ResponseEntity<List<CourseDto>> findByProficiencyLevel(
            @PathVariable("proficiencyLevel") ProficiencyLevel proficiencyLevel
    ) {
        return ResponseEntity.ok(courseFacade.findByProficiencyLevel(proficiencyLevel));
    }

    @GetMapping(path = "/languages/{language}")
    @Operation(summary = "Find Courses by Language", security = @SecurityRequirement(name = "Bearer"), description = "Finds courses by language")
    @Observed(name = "findCoursesByLanguage")
    public ResponseEntity<List<CourseDto>> findByLanguage(@PathVariable("language") Language language) {
        return ResponseEntity.ok(courseFacade.findByLanguage(language));
    }

    @PostMapping(path = "/")
    @Operation(summary = "Create Course", security = @SecurityRequirement(name = "Bearer"), description = "Creates a new course")
    @Observed(name = "createCourse")
    public ResponseEntity<CourseDto> create(@Valid @RequestBody CourseDto courseDto) {
        return new ResponseEntity<>(courseFacade.create(courseDto), HttpStatus.CREATED);
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete Course by ID", security = @SecurityRequirement(name = "Bearer"), description = "Deletes a course by the provided ID")
    @Observed(name = "deleteCourse")
    public ResponseEntity<CourseDto> delete(@PathVariable UUID id) {
        return ResponseEntity.ok(courseFacade.delete(id));
    }

    @PutMapping(path = "/{id}")
    @Operation(summary = "Update Course by ID", security = @SecurityRequirement(name = "Bearer"), description = "Updates a course by the provided ID")
    @Observed(name = "updateCourse")
    public ResponseEntity<CourseDto> update(@PathVariable("id") UUID id, @Valid @RequestBody CourseUpdateDto courseUpdateDto) {
        return ResponseEntity.ok(courseFacade.update(id, courseUpdateDto));
    }
}
