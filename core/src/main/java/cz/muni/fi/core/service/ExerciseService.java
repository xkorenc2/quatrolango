package cz.muni.fi.core.service;

import cz.muni.fi.commons.enums.ExerciseDifficulty;
import cz.muni.fi.commons.exceptions.ResourceNotFoundException;
import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.data.model.Exercise;
import cz.muni.fi.core.data.repository.ExerciseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
public class ExerciseService {
    private final ExerciseRepository exerciseRepository;
    private final CourseService courseService;
    private final CourseEnrollmentService courseEnrollmentService;

    @Autowired
    public ExerciseService(
            ExerciseRepository exerciseRepository,
            CourseService courseService,
            CourseEnrollmentService courseEnrollmentService) {
        this.exerciseRepository = exerciseRepository;
        this.courseService = courseService;
        this.courseEnrollmentService = courseEnrollmentService;
    }

    @Transactional
    public Exercise findById(UUID id) {
        return exerciseRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Exercise with id: " + id + " was not found."));
    }

    @Transactional
    public Page<Exercise> findAll(Pageable pageable) {
        return exerciseRepository.findAll(pageable);
    }

    @Transactional
    public List<Exercise> findByIds(Set<UUID> ids) {
        return exerciseRepository.findByIds(ids);
    }

    @Transactional
    public List<Exercise> findByExerciseDifficulty(ExerciseDifficulty exerciseDifficulty) {
        return exerciseRepository.findByExerciseDifficulty(exerciseDifficulty);
    }

    @Transactional
    public Exercise getExerciseForStudent(UUID studentUuid, UUID exerciseUuid) {
        Exercise exercise = exerciseRepository.findById(exerciseUuid).orElseThrow(() -> new ResourceNotFoundException("Exercise with id: " + exerciseUuid +
                " was not found, exercise could not be picked"));
        UUID courseId = exercise.getCourse().getId();
        if (validateIfStudentIsEnrolled(studentUuid, courseId)) {
            return exercise;
        }
        throw new ResourceNotFoundException("Provided student is not enrolled in" +
                " the course this exercise belongs to");
    }

    @Transactional
    public boolean validateIfStudentIsEnrolled(UUID studentUuid, UUID courseUuId) {
        return courseEnrollmentService.checkIfStudentIsEnrolled(courseUuId, studentUuid);
    }

    @Transactional
    public Exercise delete(UUID id) {
        Optional<Exercise> exercise = exerciseRepository.findById(id);
        if (exercise.isEmpty()) {
            throw new ResourceNotFoundException("Exercise with id: " + id +
                    " was not found, exercise was not deleted");
        }
        exerciseRepository.deleteById(id);
        return exercise.get();
    }

    @Transactional
    public Exercise create(Exercise exercise, UUID courseUuid) {
        Course course = checkIfCourseExists(courseUuid);
        exercise.setCourse(course);
        exercise.setId(UUID.randomUUID());
        return exerciseRepository.save(exercise);
    }

    @Transactional
    public Exercise update(UUID id, Exercise updateData) {
        Optional<Exercise> exercise = exerciseRepository.findById(id);
        if (exercise.isEmpty()) {
            throw new ResourceNotFoundException("Exercise with id: " + id +
                    " was not found, exercise was not updated");
        }
        updateData.setId(id);
        updateData.setCourse(exercise.get().getCourse());
        return exerciseRepository.save(updateData);
    }

    Course checkIfCourseExists(UUID courseId) throws ResourceNotFoundException {
        return courseService.findById(courseId);
    }
}
