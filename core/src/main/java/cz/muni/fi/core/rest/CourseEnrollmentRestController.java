package cz.muni.fi.core.rest;

import cz.muni.fi.commons.dto.course.CourseEnrollmentDto;
import cz.muni.fi.commons.enums.EnrollmentState;
import cz.muni.fi.core.facade.CourseEnrollmentFacade;
import io.micrometer.observation.annotation.Observed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(path = "/course-enrollments", produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Course Enrollment service", description = "Service for operations on CourseEnrollment model")
public class CourseEnrollmentRestController {
    private final CourseEnrollmentFacade courseEnrollmentFacade;

    @Autowired
    public CourseEnrollmentRestController(CourseEnrollmentFacade courseEnrollmentFacade) {
        this.courseEnrollmentFacade = courseEnrollmentFacade;
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Find Course Enrollment by ID", security = @SecurityRequirement(name = "Bearer"), description = "Finds a course enrollment by the provided ID")
    @Observed(name = "findCourseEnrollmentById")
    public ResponseEntity<CourseEnrollmentDto> findById(@PathVariable("id") UUID id) {
        return Optional.ofNullable(courseEnrollmentFacade.findById(id))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping(path = "/students/{studentId}")
    @Operation(summary = "Find Course Enrollments by student IDs", security = @SecurityRequirement(name = "Bearer"), description = "Finds a course enrollments by the provided student ID")
    @Observed(name = "findCourseEnrollmentByStudentId")
    public ResponseEntity<List<CourseEnrollmentDto>> findByStudentId(@PathVariable("studentId") UUID studentId) {
        return ResponseEntity.ok(courseEnrollmentFacade.findByStudentId(studentId));

    }

    @GetMapping(path = "/courses/{courseId}/students/{studentId}")
    @Operation(
            summary = "Find Course Enrollment course ID and by student ID",
            security = @SecurityRequirement(name = "Bearer"),
            description = "Finds a course enrollment by the provided course ID and  student ID"
    )
    @Observed(name = "findByCourseIdAndStudentId")
    public ResponseEntity<CourseEnrollmentDto> findByCourseIdAndStudentId(
            @PathVariable("courseId") UUID courseId,
            @PathVariable("studentId") UUID studentId
    ) {
        return Optional.ofNullable(courseEnrollmentFacade.findByCourseIdAndStudentId(courseId, studentId))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping(path = "/")
    @Operation(summary = "Create Course Enrollment", security = @SecurityRequirement(name = "Bearer"), description = "Creates a new course enrollment")
    @Observed(name = "createCourseEnrollment")
    public ResponseEntity<CourseEnrollmentDto> create(@Valid @RequestBody CourseEnrollmentDto courseDto) {
        return new ResponseEntity<>(courseEnrollmentFacade.create(courseDto), HttpStatus.CREATED);
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete Course Enrollment by ID", security = @SecurityRequirement(name = "Bearer"), description = "Deletes a course enrollment by the provided ID")
    @Observed(name = "deleteCourseEnrollment")
    public ResponseEntity<CourseEnrollmentDto> delete(@PathVariable("id") UUID id) {
        return ResponseEntity.ok(courseEnrollmentFacade.delete(id));
    }

    @PatchMapping(path = "/{id}")
    @Operation(summary = "Update enrollment state", security = @SecurityRequirement(name = "Bearer"), description = "Updates an enrollment state for provided CourseEnrollment ID")
    @Observed(name = "updateCourseEnrollmentState")
    public ResponseEntity<CourseEnrollmentDto> updateEnrollmentState(
            @PathVariable("id") UUID id,
            @RequestParam EnrollmentState enrollmentState
    ) {
        return ResponseEntity.ok(courseEnrollmentFacade.updateEnrollmentState(id, enrollmentState));
    }

}
