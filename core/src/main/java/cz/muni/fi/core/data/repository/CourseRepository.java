package cz.muni.fi.core.data.repository;

import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import cz.muni.fi.core.data.model.Course;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Repository
public interface CourseRepository extends JpaRepository<Course, UUID> {
    @Query("SELECT c FROM Course c WHERE c.id IN :ids")
    List<Course> findByIds(@Param("ids") Set<UUID> ids);

    @Query("SELECT c FROM Course c WHERE c.proficiencyLevel = :proficiencyLevel")
    List<Course> findByProficiencyLevel(@Param("proficiencyLevel") ProficiencyLevel proficiencyLevel);

    @Query("SELECT c FROM Course c WHERE c.language = :language")
    List<Course> findByLanguage(@Param("language") Language language);
}
