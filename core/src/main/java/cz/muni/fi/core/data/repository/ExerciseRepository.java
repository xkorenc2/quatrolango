package cz.muni.fi.core.data.repository;


import cz.muni.fi.commons.enums.ExerciseDifficulty;
import cz.muni.fi.core.data.model.Exercise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Repository
public interface ExerciseRepository extends JpaRepository<Exercise, UUID> {

    @Query("SELECT e FROM Exercise e WHERE e.exerciseDifficulty = :exerciseDifficulty")
    List<Exercise> findByExerciseDifficulty(@Param("exerciseDifficulty") ExerciseDifficulty exerciseDifficulty);

    @Query("SELECT e FROM Exercise e WHERE e.id IN :ids")
    List<Exercise> findByIds(@Param("ids") Set<UUID> ids);
}
