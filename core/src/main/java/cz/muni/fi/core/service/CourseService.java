package cz.muni.fi.core.service;

import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import cz.muni.fi.commons.exceptions.ResourceNotFoundException;
import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.data.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;


@Service
public class CourseService {
    private final CourseRepository courseRepository;

    @Autowired
    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @Transactional
    public Course findById(UUID id) {
        return findCourse(id);
    }

    @Transactional
    public Page<Course> findAll(Pageable pageable) {
        return courseRepository.findAll(pageable);
    }

    @Transactional
    public List<Course> findByIds(Set<UUID> ids) {
        return courseRepository.findByIds(ids);
    }

    @Transactional
    public List<Course> findByProficiencyLevel(ProficiencyLevel proficiencyLevel) {
        return courseRepository.findByProficiencyLevel(proficiencyLevel);
    }

    @Transactional
    public List<Course> findByLanguage(Language language) {
        return courseRepository.findByLanguage(language);
    }

    @Transactional
    public Course delete(UUID id) {
        Optional<Course> course = courseRepository.findById(id);
        if (course.isEmpty()) {
            throw new ResourceNotFoundException("Course with id: " + id +
                    " was not found, course was not deleted");
        }
        courseRepository.deleteById(id);
        return course.get();
    }

    @Transactional
    public Course create(Course course) {
        return courseRepository.save(course);
    }

    @Transactional
    public Course update(UUID id, Course updateData) {
        if (!courseRepository.existsById(id)) {
            throw new ResourceNotFoundException("Course with id: " + id +
                    " was not found, course was not updated");
        }

        updateData.setId(id);
        return courseRepository.save(updateData);
    }

    private Course findCourse(UUID courseId) {
        return courseRepository.findById(courseId).orElseThrow(() ->
                new ResourceNotFoundException("Course with id: " + courseId +
                        " was not found."));
    }
}
