package cz.muni.fi.core.facade;

import cz.muni.fi.commons.dto.exercise.ExerciseDto;
import cz.muni.fi.commons.enums.ExerciseDifficulty;
import cz.muni.fi.core.data.mapper.ExerciseMapper;
import cz.muni.fi.core.service.ExerciseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class ExerciseFacade {
    private final ExerciseService exerciseService;
    private final ExerciseMapper exerciseMapper;

    @Autowired
    public ExerciseFacade(ExerciseService exerciseService, ExerciseMapper exerciseMapper) {
        this.exerciseService = exerciseService;
        this.exerciseMapper = exerciseMapper;
    }

    @Transactional(readOnly = true)
    public ExerciseDto findById(UUID id) {
        return exerciseMapper.mapToDto(exerciseService.findById(id));
    }

    @Transactional(readOnly = true)
    public Page<ExerciseDto> findAll(Pageable pageable) {
        return exerciseMapper.toDtoPage(exerciseService.findAll(pageable));
    }

    @Transactional(readOnly = true)
    public List<ExerciseDto> findByIds(Set<UUID> ids) {
        return exerciseMapper.toDtoList(exerciseService.findByIds(ids));
    }

    @Transactional(readOnly = true)
    public List<ExerciseDto> findByExerciseDifficulty(ExerciseDifficulty difficulty) {
        return exerciseMapper.toDtoList(exerciseService.findByExerciseDifficulty(difficulty));
    }

    @Transactional(readOnly = true)
    public ExerciseDto getExerciseForStudent(UUID studentUuid, UUID exerciseUuid) {
        return exerciseMapper.mapToDto(exerciseService.getExerciseForStudent(studentUuid, exerciseUuid));
    }

    @Transactional
    public ExerciseDto create(ExerciseDto exercise, UUID courseUuid) {
        return exerciseMapper.mapToDto(exerciseService.create(exerciseMapper.mapToEntity(exercise), courseUuid));
    }

    @Transactional
    public ExerciseDto delete(UUID id) {
        return exerciseMapper.mapToDto(exerciseService.delete(id));
    }

    @Transactional
    public ExerciseDto update(UUID id, ExerciseDto exercise) {
        return exerciseMapper.mapToDto(exerciseService.update(id, exerciseMapper.mapToEntity(exercise)));
    }
}
