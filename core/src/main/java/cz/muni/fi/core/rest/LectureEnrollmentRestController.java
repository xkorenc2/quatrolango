package cz.muni.fi.core.rest;

import cz.muni.fi.commons.dto.lecture.LectureEnrollmentDto;
import cz.muni.fi.core.facade.LectureEnrollmentFacade;
import io.micrometer.observation.annotation.Observed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(path = "/lecture-enrollments", produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Lecture Enrollment service", description = "Service for operations on LectureEnrollment model")
public class LectureEnrollmentRestController {
    private final LectureEnrollmentFacade lectureEnrollmentFacade;

    @Autowired
    public LectureEnrollmentRestController(LectureEnrollmentFacade lectureEnrollmentFacade) {
        this.lectureEnrollmentFacade = lectureEnrollmentFacade;
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Find Lecture Enrollment by ID", security = @SecurityRequirement(name = "Bearer"), description = "Finds a lecture enrollment by the provided ID")
    @Observed(name = "findLectureEnrollmentById")
    public ResponseEntity<LectureEnrollmentDto> findById(@PathVariable("id") UUID id) {
        return Optional.ofNullable(lectureEnrollmentFacade.findById(id))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping(path = "/students/{studentId}")
    @Operation(summary = "Find Lecture Enrollments by student IDs", security = @SecurityRequirement(name = "Bearer"), description = "Finds a lecture enrollments by the provided student ID")
    @Observed(name = "findLectureEnrollmentByStudentId")
    public ResponseEntity<List<LectureEnrollmentDto>> findByStudentId(@PathVariable("studentId") UUID studentId) {
        return ResponseEntity.ok(lectureEnrollmentFacade.findByStudentId(studentId));
    }

    @PostMapping(path = "/")
    @Operation(summary = "Create Lecture Enrollment", security = @SecurityRequirement(name = "Bearer"), description = "Creates a new lecture enrollment")
    @Observed(name = "createLectureEnrollment")
    public ResponseEntity<LectureEnrollmentDto> create(@Valid @RequestBody LectureEnrollmentDto lectureDtp) {
        return new ResponseEntity<>(lectureEnrollmentFacade.create(lectureDtp), HttpStatus.CREATED);
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete Lecture Enrollment by ID", security = @SecurityRequirement(name = "Bearer"), description = "Deletes a lecture enrollment by the provided ID")
    @Observed(name = "deleteLectureEnrollment")
    public ResponseEntity<LectureEnrollmentDto> delete(@PathVariable("id") UUID id) {
        return ResponseEntity.ok(lectureEnrollmentFacade.delete(id));
    }

    @GetMapping(path = "/students/{studentId}/lectures/{lectureId}")
    @Operation(summary = "Check if Student is enrolled in Lecture",
            security = @SecurityRequirement(name = "Bearer"),
            description = "Checks if student with provided student id is enrolled in lecture also provided as id")
    @Observed(name = "checkIfStudentIsEnrolledInLecture")
    public ResponseEntity<Boolean> checkIfStudentIsEnrolled(@PathVariable("studentId") UUID studentId, @PathVariable("lectureId") UUID lectureId) {
        return ResponseEntity.ok(lectureEnrollmentFacade.checkIfStudentIsEnrolled(studentId, lectureId));
    }
}
