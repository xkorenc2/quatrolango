package cz.muni.fi.core.service;

import cz.muni.fi.commons.exceptions.ResourceNotFoundException;
import cz.muni.fi.core.data.model.LectureEnrollment;
import cz.muni.fi.core.data.repository.LectureEnrollmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class LectureEnrollmentService {
    private final LectureEnrollmentRepository lectureEnrollmentRepository;

    @Autowired
    public LectureEnrollmentService(LectureEnrollmentRepository lectureEnrollmentRepository) {
        this.lectureEnrollmentRepository = lectureEnrollmentRepository;
    }

    @Transactional
    public LectureEnrollment findById(UUID id) {
        return lectureEnrollmentRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("LectureEnrollment with id: " + id + " was not found."));
    }

    public List<LectureEnrollment> findByStudentId(UUID studentId) {
        return lectureEnrollmentRepository.findByStudentId(studentId);
    }


    public LectureEnrollment create(LectureEnrollment courseEnrollment) {
        return lectureEnrollmentRepository.save(courseEnrollment);
    }

    public LectureEnrollment delete(UUID id) {
        LectureEnrollment lectureEnrollment = lectureEnrollmentRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("LectureEnrollment with id: " + id + " was not found."));


        lectureEnrollmentRepository.deleteById(id);
        return lectureEnrollment;
    }

    public boolean checkIfStudentIsEnrolled(UUID lectureId, UUID studentId) {
        return lectureEnrollmentRepository.checkIfStudentIsEnrolled(lectureId, studentId);
    }
}
