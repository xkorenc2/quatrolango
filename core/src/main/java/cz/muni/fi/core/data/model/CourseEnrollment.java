package cz.muni.fi.core.data.model;

import cz.muni.fi.commons.BaseEntity;
import cz.muni.fi.commons.enums.EnrollmentState;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
@Table(name = "CourseEnrollment")
public class CourseEnrollment extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "course_id", nullable = false)
    private Course course;

    @NotNull
    @Column(name = "student_id")
    private UUID studentId;

    @NotNull
    @Column(name = "enrollment_state")
    @Enumerated(EnumType.STRING)
    private EnrollmentState enrollmentState;
}
