package cz.muni.fi.core.data.mapper;

import cz.muni.fi.commons.dto.lecture.LectureEnrollmentDto;
import cz.muni.fi.core.data.model.LectureEnrollment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LectureEnrollmentMapper {
    @Mapping(target = "studentDto", ignore = true)
    @Mapping(target = "lectureDto", ignore = true)
    LectureEnrollmentDto mapToDto(LectureEnrollment lectureEnrollment);

    @Mapping(source = "lectureDto", target = "lecture")
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "studentId", source = "studentDto.id")
    LectureEnrollment mapToEntity(LectureEnrollmentDto courseDto);

    List<LectureEnrollmentDto> mapToDtoList(List<LectureEnrollment> lectureEnrollments);

}
