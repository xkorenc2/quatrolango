package cz.muni.fi.core.service.api;

import cz.muni.fi.commons.dto.CustomPageImpl;
import cz.muni.fi.commons.dto.user.lecturer.LecturerDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.AbstractOAuth2Token;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class LecturerApiService {

    private final WebClient client;

    public LecturerApiService(@Value("${lecturer.api.url}") String baseUrl) {
        this.client = WebClient.create(baseUrl);
    }

    public Optional<LecturerDto> findLecturerById(UUID lecturerId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        AbstractOAuth2Token token = (AbstractOAuth2Token) authentication.getCredentials();

        Mono<LecturerDto> lecturerMono = client
                .get()
                .uri("/" + lecturerId)
                .headers(httpHeaders -> httpHeaders.setBearerAuth(token.getTokenValue()))
                .retrieve()
                .bodyToMono(LecturerDto.class);
        return lecturerMono.blockOptional();
    }

    public List<LecturerDto> findAll(int size) {
        return client
                .get()
                .uri("/?page=0&size=" + size)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<CustomPageImpl<LecturerDto>>() {
                })
                .block()
                .getContent();
    }
}
