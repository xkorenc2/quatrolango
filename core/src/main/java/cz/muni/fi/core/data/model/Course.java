package cz.muni.fi.core.data.model;

import cz.muni.fi.commons.BaseEntity;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@Entity
@ToString
@Table(name = "Course")
@EqualsAndHashCode(callSuper = true)
public class Course extends BaseEntity {
    @NotNull
    @Column(name = "name", length = 50)
    private String name;

    @NotNull
    @Positive
    @Column(name = "capacity")
    private int capacity;

    @NotNull
    @Column(name = "language")
    @Enumerated(EnumType.STRING)
    private Language language;

    @NotNull
    @Column(name = "proficiency_level")
    @Enumerated(EnumType.STRING)
    private ProficiencyLevel proficiencyLevel;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "course", cascade = {CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE})
    @ToString.Exclude
    private List<Lecture> lectures;

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "course", cascade = {CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE})
    private List<Exercise> exercises;

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "course", cascade = {CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE})
    private List<CourseEnrollment> courseEnrollments;
}
