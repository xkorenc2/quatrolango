package cz.muni.fi.core.data.repository;

import cz.muni.fi.core.data.model.LectureEnrollment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface LectureEnrollmentRepository extends JpaRepository<LectureEnrollment, UUID> {
    List<LectureEnrollment> findByStudentId(UUID studentId);

    @Query("SELECT COUNT(le) > 0 FROM LectureEnrollment le WHERE le.lecture.id = :lectureId AND le.studentId = :studentId")
    boolean checkIfStudentIsEnrolled(@Param("lectureId") UUID courseId, @Param("studentId") UUID studentId);
}
