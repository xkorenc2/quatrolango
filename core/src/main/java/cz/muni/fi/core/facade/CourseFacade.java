package cz.muni.fi.core.facade;

import cz.muni.fi.commons.dto.course.CourseDetailedDto;
import cz.muni.fi.commons.dto.course.CourseDto;
import cz.muni.fi.commons.dto.course.CourseUpdateDto;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import cz.muni.fi.core.data.mapper.CourseMapper;
import cz.muni.fi.core.data.mapper.ExerciseMapper;
import cz.muni.fi.core.data.mapper.LectureMapper;
import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.service.CourseEnrollmentService;
import cz.muni.fi.core.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class CourseFacade {
    private final CourseService courseService;
    private final CourseEnrollmentService courseEnrollmentService;
    private final CourseMapper courseMapper;
    private final LectureMapper lectureMapper;
    private final ExerciseMapper exerciseMapper;

    @Autowired
    public CourseFacade(
            CourseService courseService,
            CourseEnrollmentService courseEnrollmentService,
            CourseMapper courseMapper,
            LectureMapper lectureMapper,
            ExerciseMapper exerciseMapper
    ) {
        this.courseService = courseService;
        this.courseEnrollmentService = courseEnrollmentService;
        this.courseMapper = courseMapper;
        this.lectureMapper = lectureMapper;
        this.exerciseMapper = exerciseMapper;
    }

    @Transactional(readOnly = true)
    public CourseDto findById(UUID id) {
        return courseMapper.mapToDto(courseService.findById(id));
    }

    @Transactional(readOnly = true)
    public CourseDetailedDto findByIdDetailed(UUID id) {
        Course foundCourse = courseService.findById(id);
        CourseDetailedDto courseDetailedDto = courseMapper.mapToDetailedDto(foundCourse);
        courseDetailedDto.setStudents(courseEnrollmentService.findStudentsByCourse(courseService.findById(id)).stream().toList());
        courseDetailedDto.setLectures(lectureMapper.toDtoList(foundCourse.getLectures()));
        courseDetailedDto.setExercises(exerciseMapper.toDtoList(foundCourse.getExercises()));
        return courseDetailedDto;
    }

    @Transactional(readOnly = true)
    public Page<CourseDto> findAll(Pageable pageable) {
        return courseMapper.mapToPage(courseService.findAll(pageable));
    }

    @Transactional(readOnly = true)
    public List<CourseDto> findByIds(Set<UUID> ids) {
        return courseMapper.mapToList(courseService.findByIds(ids));
    }


    @Transactional(readOnly = true)
    public List<CourseDto> findByProficiencyLevel(ProficiencyLevel proficiencyLevel) {
        return courseMapper.mapToList(courseService.findByProficiencyLevel(proficiencyLevel));
    }

    @Transactional(readOnly = true)
    public List<CourseDto> findByLanguage(Language language) {
        return courseMapper.mapToList(courseService.findByLanguage(language));
    }

    @Transactional
    public CourseDto create(CourseDto courseDto) {
        return courseMapper.mapToDto(courseService.create(courseMapper.mapToEntity(courseDto)));
    }

    @Transactional
    public CourseDto delete(UUID id) {
        return courseMapper.mapToDto(courseService.delete(id));
    }

    @Transactional
    public CourseDto update(UUID id, CourseUpdateDto courseUpdateDto) {
        return courseMapper.mapToDto(courseService.update(id, courseMapper.mapToEntity(courseUpdateDto)));
    }
}
