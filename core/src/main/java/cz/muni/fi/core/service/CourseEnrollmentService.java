package cz.muni.fi.core.service;

import cz.muni.fi.commons.dto.user.student.StudentDto;
import cz.muni.fi.commons.enums.EnrollmentState;
import cz.muni.fi.commons.exceptions.ResourceNotFoundException;
import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.data.model.CourseEnrollment;
import cz.muni.fi.core.data.repository.CourseEnrollmentRepository;
import cz.muni.fi.core.service.api.StudentApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CourseEnrollmentService {
    private final CourseEnrollmentRepository courseEnrollmentRepository;
    private final StudentApiService studentApiService;

    @Autowired
    public CourseEnrollmentService(CourseEnrollmentRepository courseEnrollmentRepository, StudentApiService studentApiService) {
        this.courseEnrollmentRepository = courseEnrollmentRepository;
        this.studentApiService = studentApiService;
    }

    @Transactional
    public CourseEnrollment findById(UUID id) {
        return courseEnrollmentRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("CourseEnrollment with id: " + id + " was not found."));
    }

    @Transactional
    public CourseEnrollment findByCourseAndStudent(UUID courseId, UUID studentId) {
        return courseEnrollmentRepository
                .findByCourseIdAndStudentId(courseId, studentId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        "CourseEnrollment for course id: "
                                + courseId
                                + " and student id: "
                                + studentId
                                + " was not found."
                ));
    }

    @Transactional
    public Set<StudentDto> findStudentsByCourse(Course course) {
        List<CourseEnrollment> enrollments = courseEnrollmentRepository.findByCourse(course);
        if (enrollments.isEmpty()) {
            return new HashSet<>();
        }
        Set<UUID> studentIds = enrollments.stream()
                .map(CourseEnrollment::getStudentId)
                .collect(Collectors.toSet());
        return studentApiService.findStudentsByIds(studentIds);
    }

    @Transactional
    public List<CourseEnrollment> findByStudentId(UUID studentId) {
        return courseEnrollmentRepository.findByStudentId(studentId);
    }

    @Transactional
    public CourseEnrollment create(CourseEnrollment courseEnrollment) {
        return courseEnrollmentRepository.save(courseEnrollment);
    }

    @Transactional
    public CourseEnrollment delete(UUID id) {
        CourseEnrollment courseEnrollment = courseEnrollmentRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("CourseEnrollment with id: " + id + " was not found."));

        courseEnrollmentRepository.deleteById(id);
        return courseEnrollment;
    }

    @Transactional
    public CourseEnrollment updateEnrollmentState(UUID id, EnrollmentState enrollmentState) {
        CourseEnrollment courseEnrollment = courseEnrollmentRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("CourseEnrollment with id: " + id + " was not found."));

        courseEnrollment.setEnrollmentState(enrollmentState);

        return courseEnrollmentRepository.save(courseEnrollment);
    }

    public boolean checkIfStudentIsEnrolled(UUID courseId, UUID studentId) {
        return courseEnrollmentRepository.checkIfStudentIsEnrolled(courseId, studentId);
    }
}
