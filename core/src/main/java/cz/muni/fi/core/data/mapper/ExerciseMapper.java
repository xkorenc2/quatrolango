package cz.muni.fi.core.data.mapper;

import cz.muni.fi.commons.dto.exercise.ExerciseDto;
import cz.muni.fi.core.data.model.Exercise;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ExerciseMapper {
    List<ExerciseDto> toDtoList(List<Exercise> exercises);

    default Page<ExerciseDto> toDtoPage(Page<Exercise> exercises) {
        return new PageImpl<>(toDtoList(exercises.getContent()), exercises.getPageable(), exercises.getTotalPages());
    }

    @Mapping(source = "course.id", target = "courseId")
    ExerciseDto mapToDto(Exercise exercise);

    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "course", ignore = true)
    Exercise mapToEntity(ExerciseDto exerciseDTO);

}

