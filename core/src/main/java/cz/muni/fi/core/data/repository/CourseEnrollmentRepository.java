package cz.muni.fi.core.data.repository;

import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.data.model.CourseEnrollment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface CourseEnrollmentRepository extends JpaRepository<CourseEnrollment, UUID> {
    List<CourseEnrollment> findByStudentId(UUID studentId);

    List<CourseEnrollment> findByCourse(Course course);

    Optional<CourseEnrollment> findByCourseIdAndStudentId(UUID courseId, UUID studentId);

    @Query("SELECT COUNT(ce) > 0 FROM CourseEnrollment ce WHERE ce.course.id = :courseId AND ce.studentId = :studentId")
    boolean checkIfStudentIsEnrolled(@Param("courseId") UUID courseId, @Param("studentId") UUID studentId);
}
