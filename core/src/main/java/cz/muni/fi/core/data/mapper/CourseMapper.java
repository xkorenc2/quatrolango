package cz.muni.fi.core.data.mapper;

import cz.muni.fi.commons.dto.course.CourseDetailedDto;
import cz.muni.fi.commons.dto.course.CourseDto;
import cz.muni.fi.commons.dto.course.CourseUpdateDto;
import cz.muni.fi.core.data.model.Course;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CourseMapper {
    @Mapping(target = "lectures", ignore = true)
    @Mapping(target = "exercises", ignore = true)
    @Mapping(target = "students", ignore = true)
    CourseDetailedDto mapToDetailedDto(Course course);

    @Named("toCourseDto")
    CourseDto mapToDto(Course course);

    @Mapping(target = "lectures", ignore = true)
    @Mapping(target = "exercises", ignore = true)
    @Mapping(target = "courseEnrollments", ignore = true)
    Course mapToEntity(CourseDto courseDto);

    @Named("fromCourseUpdateDto")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "lectures", ignore = true)
    @Mapping(target = "exercises", ignore = true)
    @Mapping(target = "courseEnrollments", ignore = true)
    Course mapToEntity(CourseUpdateDto courseUpdateDto);

    List<CourseDto> mapToList(List<Course> courses);

    default Page<CourseDto> mapToPage(Page<Course> courses) {
        return new PageImpl<>(mapToList(courses.getContent()), courses.getPageable(), courses.getTotalPages());
    }
}
