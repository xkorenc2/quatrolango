package cz.muni.fi.core.rest;

import cz.muni.fi.commons.dto.lecture.LectureDto;
import cz.muni.fi.core.facade.LectureFacade;
import io.micrometer.observation.annotation.Observed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@RestController
@Tag(name = "Lecture service", description = "Service for handling lectures in language school")
@RequestMapping(value = "/lectures", produces = MediaType.APPLICATION_JSON_VALUE)
public class LectureRestController {
    private final LectureFacade lectureFacade;

    @Autowired
    public LectureRestController(LectureFacade lectureFacade) {
        this.lectureFacade = lectureFacade;
    }

    @Operation(summary = "Get all lectures", security = @SecurityRequirement(name = "Bearer"), description = "Get list of all lectures")
    @GetMapping(path = "/")
    @Observed(name = "findAllLectures")
    public ResponseEntity<Page<LectureDto>> findAllLectures(Pageable pageable) {
        return ResponseEntity.ok(lectureFacade.findAll(pageable));
    }

    @Operation(summary = "Get lecture by id", security = @SecurityRequirement(name = "Bearer"), description = "Get lecture by id")
    @GetMapping(path = "/{id}")
    @Observed(name = "findLectureById")
    public ResponseEntity<LectureDto> findLectureById(@PathVariable UUID id) {
        return Optional.ofNullable(lectureFacade.findById(id))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @Operation(summary = "Get lectures by lecturer", security = @SecurityRequirement(name = "Bearer"), description = "Get lectures by lecturer")
    @GetMapping(path = "/lecturer/{lecturerId}")
    @Observed(name = "findLecturesByLecturer")
    public ResponseEntity<List<LectureDto>> findLecturesByLecturer(@PathVariable UUID lecturerId) {
        return ResponseEntity.ok(lectureFacade.findByLecturer(lecturerId));
    }

    @Operation(summary = "Get lectures by date", security = @SecurityRequirement(name = "Bearer"), description = "Get lectures by date")
    @GetMapping(path = "/date/{date}")
    @Observed(name = "findLecturesByDate")
    public ResponseEntity<List<LectureDto>> findLecturesByDate(@PathVariable LocalDate date) {
        return ResponseEntity.ok(lectureFacade.findByDate(date));
    }

    @Operation(summary = "Get lectures by course ID", security = @SecurityRequirement(name = "Bearer"), description = "Get lectures by course ID")
    @GetMapping(path = "/course/{courseId}")
    @Observed(name = "findLecturesByCourse")
    public ResponseEntity<List<LectureDto>> findLecturesByCourse(@PathVariable UUID courseId) {
        return ResponseEntity.ok(lectureFacade.findByCourse(courseId));
    }

    @Operation(summary = "Get lectures by IDs", security = @SecurityRequirement(name = "Bearer"), description = "Get lectures by IDs")
    @PostMapping(path = "/ids")
    @Observed(name = "findLecturesByIds")
    public ResponseEntity<List<LectureDto>> findLecturesByIds(@RequestBody Set<UUID> ids) {
        return ResponseEntity.ok(lectureFacade.findByIds(ids));
    }

    @Operation(summary = "Create lecture", security = @SecurityRequirement(name = "Bearer"), description = "Create lecture")
    @PostMapping(path = "/{courseId}")
    @Observed(name = "createLecture")
    public ResponseEntity<LectureDto> createLecture(@PathVariable UUID courseId,
                                                    @RequestBody @Valid LectureDto lectureDto) {
        return new ResponseEntity<>(lectureFacade.create(lectureDto, courseId), HttpStatus.CREATED);
    }

    @Operation(summary = "Update lecture", security = @SecurityRequirement(name = "Bearer"), description = "Update lecture")
    @PutMapping(path = "/{id}")
    @Observed(name = "updateLecture")
    public ResponseEntity<LectureDto> updateLecture(@RequestBody @Valid LectureDto lectureDto,
                                                    @PathVariable UUID id) {
        return Optional.ofNullable(lectureFacade.update(lectureDto, id))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @Operation(summary = "Delete lecture", security = @SecurityRequirement(name = "Bearer"), description = "Delete lecture")
    @DeleteMapping(path = "/{id}")
    @Observed(name = "deleteLecture")
    public ResponseEntity<LectureDto> deleteLecture(@PathVariable UUID id) {
        return Optional.ofNullable(lectureFacade.remove(id))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }
}
