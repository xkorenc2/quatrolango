package cz.muni.fi.core.service;

import cz.muni.fi.commons.dto.user.lecturer.LecturerDto;
import cz.muni.fi.commons.exceptions.ResourceNotFoundException;
import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.data.model.Lecture;
import cz.muni.fi.core.data.repository.LectureRepository;
import cz.muni.fi.core.service.api.LecturerApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
public class LectureService {
    private final LectureRepository lectureRepository;
    private final CourseService courseService;
    private final LecturerApiService lecturerApiService;

    @Autowired
    public LectureService(LectureRepository lectureRepository,
                          CourseService courseService,
                          LecturerApiService lecturerApiService) {
        this.lectureRepository = lectureRepository;
        this.courseService = courseService;
        this.lecturerApiService = lecturerApiService;
    }

    @Transactional
    public Page<Lecture> findAll(Pageable pageable) {
        return lectureRepository.findAll(pageable);
    }

    @Transactional
    public Lecture findById(UUID id) {
        return lectureRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Lecture with id: " + id + " was not found."));
    }

    @Transactional
    public List<Lecture> findByIds(Set<UUID> ids) {
        return lectureRepository.findByIds(ids);
    }

    @Transactional
    public List<Lecture> findByCourse(UUID courseId) {
        List<Lecture> lectures = lectureRepository.findByCourseId(courseId);
        if (lectures.isEmpty()) {
            throw new ResourceNotFoundException("No lectures found for course with id: " + courseId);
        }
        return lectures;
    }

    @Transactional
    public List<Lecture> findByLecturer(UUID lecturerId) {
        checkIfLecturerExists(lecturerId);
        return lectureRepository.findByLecturerId(lecturerId);
    }

    @Transactional
    public List<Lecture> findByDate(LocalDate date) {
        return lectureRepository.findByStartTimeAndEndTime(date.atStartOfDay(), date.atStartOfDay().plusDays(1).minusNanos(1));
    }

    @Transactional
    public Lecture create(Lecture lecture, UUID courseId) {
        Course course = findCourseIfExists(courseId);
        checkIfLecturerExists(lecture.getLecturerId());
        lecture.setCourse(course);
        return lectureRepository.save(lecture);
    }

    @Transactional
    public Lecture remove(UUID id) {
        Optional<Lecture> lecture = lectureRepository.findById(id);
        if (lecture.isEmpty()) {
            throw new ResourceNotFoundException("Lecture with id: " + id +
                    " was not found, course was not deleted");
        }
        lectureRepository.deleteById(id);
        return lecture.get();
    }

    @Transactional
    public Lecture update(Lecture lecture, UUID id) {
        if (lecture == null) {
            throw new IllegalArgumentException("Lecture cannot be null");
        }
        if (!lectureRepository.existsById(id)) {
            throw new ResourceNotFoundException("Lecture with id: " + id +
                    " was not found, lecture was not updated");
        }

        checkIfLecturerExists(lecture.getLecturerId());
        lecture.setId(id);
        return lectureRepository.save(lecture);
    }

    Course findCourseIfExists(UUID courseId) throws ResourceNotFoundException {
        return courseService.findById(courseId);
    }

    void checkIfLecturerExists(UUID lecturerId) {
        Optional<LecturerDto> lecturer = lecturerApiService.findLecturerById(lecturerId);
        if (lecturer.isEmpty()) {
            throw new ResourceNotFoundException("Lecturer with id: " + lecturerId + " was not found.");
        }
    }
}
