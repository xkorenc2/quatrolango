package cz.muni.fi.core.facade;

import cz.muni.fi.commons.dto.course.CourseEnrollmentDto;
import cz.muni.fi.commons.enums.EnrollmentState;
import cz.muni.fi.core.data.mapper.CourseEnrollmentMapper;
import cz.muni.fi.core.service.CourseEnrollmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class CourseEnrollmentFacade {
    private final CourseEnrollmentService courseEnrollmentService;
    private final CourseEnrollmentMapper courseEnrollmentMapper;

    @Autowired
    public CourseEnrollmentFacade(CourseEnrollmentService courseEnrollmentService, CourseEnrollmentMapper courseEnrollmentMapper) {
        this.courseEnrollmentService = courseEnrollmentService;
        this.courseEnrollmentMapper = courseEnrollmentMapper;
    }


    @Transactional(readOnly = true)
    public CourseEnrollmentDto findById(UUID id) {
        return courseEnrollmentMapper.mapToDto(courseEnrollmentService.findById(id));
    }

    @Transactional(readOnly = true)
    public List<CourseEnrollmentDto> findByStudentId(UUID studentId) {
        return courseEnrollmentMapper.mapToDtoList(courseEnrollmentService.findByStudentId(studentId));
    }

    @Transactional(readOnly = true)
    public CourseEnrollmentDto findByCourseIdAndStudentId(UUID courseId, UUID studentId) {
        return courseEnrollmentMapper.mapToDto(courseEnrollmentService.findByCourseAndStudent(courseId, studentId));
    }

    @Transactional
    public CourseEnrollmentDto create(CourseEnrollmentDto courseEnrollmentDto) {
        return courseEnrollmentMapper.mapToDto(courseEnrollmentService.create(courseEnrollmentMapper.mapToEntity(courseEnrollmentDto)));
    }

    @Transactional
    public CourseEnrollmentDto delete(UUID id) {
        return courseEnrollmentMapper.mapToDto(courseEnrollmentService.delete(id));
    }

    @Transactional
    public CourseEnrollmentDto updateEnrollmentState(UUID id, EnrollmentState enrollmentState) {
        return courseEnrollmentMapper.mapToDto(courseEnrollmentService.updateEnrollmentState(id, enrollmentState));
    }
}
