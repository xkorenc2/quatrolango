package cz.muni.fi.core.data.seeding;

import cz.muni.fi.commons.dto.user.lecturer.LecturerDto;
import cz.muni.fi.commons.dto.user.student.StudentDto;
import cz.muni.fi.commons.enums.EnrollmentState;
import cz.muni.fi.commons.enums.ExerciseDifficulty;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.data.model.CourseEnrollment;
import cz.muni.fi.core.data.model.Exercise;
import cz.muni.fi.core.data.model.Lecture;
import cz.muni.fi.core.data.model.LectureEnrollment;
import cz.muni.fi.core.data.repository.CourseEnrollmentRepository;
import cz.muni.fi.core.data.repository.CourseRepository;
import cz.muni.fi.core.data.repository.ExerciseRepository;
import cz.muni.fi.core.data.repository.LectureEnrollmentRepository;
import cz.muni.fi.core.data.repository.LectureRepository;
import cz.muni.fi.core.service.api.LecturerApiService;
import cz.muni.fi.core.service.api.StudentApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

@Profile("dev")
@Component
public class CoreDataSeeder implements CommandLineRunner {
    private final CourseRepository courseRepository;
    private final LectureRepository lectureRepository;
    private final ExerciseRepository exerciseRepository;
    private final CourseEnrollmentRepository courseEnrollmentRepository;
    private final LectureEnrollmentRepository lectureEnrollmentRepository;
    private final List<LecturerDto> lecturers;
    private final List<StudentDto> students;

    @Autowired
    public CoreDataSeeder(
            CourseRepository courseRepository,
            LectureRepository lectureRepository,
            ExerciseRepository exerciseRepository,
            LecturerApiService lecturerApiService,
            StudentApiService studentApiService,
            CourseEnrollmentRepository courseEnrollmentRepository,
            LectureEnrollmentRepository lectureEnrollmentRepository
    ) {
        this.courseRepository = courseRepository;
        this.exerciseRepository = exerciseRepository;
        this.lectureRepository = lectureRepository;
        this.lectureEnrollmentRepository = lectureEnrollmentRepository;
        this.courseEnrollmentRepository = courseEnrollmentRepository;
        this.lecturers = lecturerApiService.findAll(15);
        this.students = studentApiService.findAll(50);
    }

    private static final String[] courseNames = {
            "English Essentials: Beginner's Course",
            "Conversational English: Intermediate Level",
            "Business English for Professionals",
            "German Basics: Beginner's Course",
            "German in Daily Life: Advanced Course",
            "German for Professionals: Business Communication",
            "Basic Spanish: Course for Beginners",
            "Conversations in Spanish: Intermediate Level",
            "Business Spanish"
    };

    private static final String[] descriptions = {
            "Kickstart your journey into the English language! Designed for beginners, this course covers basic grammar, vocabulary, and conversational phrases to help you start speaking with confidence.",
            "Enhance your spoken English skills! This course focuses on improving your fluency and understanding of everyday English through interactive and practical speaking sessions.",
            "Aimed at business professionals, this course teaches the nuances of business communication in English, including writing emails, making presentations, and negotiating skills.",
            "Dive into the basics of German! Learn essential grammar and vocabulary to form sentences and engage in simple conversations. Perfect for those starting their German language journey.",
            "Develop your German proficiency to express complex ideas and improve comprehension. This course includes cultural nuances and idiomatic expressions to help you speak like a native.",
            "Tailored for professionals who require German in their workplace, this course covers specialized vocabulary and practical skills for effective communication in a German business environment.",
            "Start your Spanish learning with foundational grammar, essential vocabulary, and basic conversational skills, setting the stage for effective communication in everyday scenarios.",
            "Improve your Spanish speaking and listening skills through guided conversations on a variety of topics. This course is designed to boost your confidence and fluency.",
            "Prepare to navigate the business world in Spanish-speaking countries. Learn the specific language used in business negotiations, presentations, and corporate communication."
    };

    private static final Language[] language = {
            Language.ENGLISH,
            Language.ENGLISH,
            Language.ENGLISH,
            Language.GERMAN,
            Language.GERMAN,
            Language.GERMAN,
            Language.SPANISH,
            Language.SPANISH,
            Language.SPANISH
    };

    private static final String[] exerciseNames = {
            "Vocabulary exercises",
            "Grammar exercises - sentence construction",
            "Grammar exercises - word formation",
            "Listening comprehension",
            "Phonetic drills",
            "Reading comprehension",
            "Writing exercise - essay",
            "Journal writing",
            "Peer review",
    };

    private static final String[] exerciseDescriptions = {
            "Engage in activities that enhance vocabulary retention and usage, including the use of flashcards, word matching games, and sorting exercises.",
            "Construct sentences using specific grammar rules to enhance understanding of syntactical structure and verb conjugation.",
            "Focus on creating words from given prefixes, suffixes, or roots to enhance understanding of word formation and expand vocabulary.",
            "Listen to recordings or watch videos in the target language, followed by answering questions to improve listening skills and understanding of spoken language.",
            "Practice pronunciation, intonation, and stress patterns through repetitive speaking exercises, tongue twisters, and choral repetition to improve spoken clarity and fluency.",
            "Read various texts like articles, stories, or essays and answer comprehension questions to improve reading skills and text interpretation.",
            "Develop writing skills by crafting essays on given topics, focusing on structure, coherence, and the application of language concepts learned.",
            "Maintain a regular diary or journal in the target language, encouraging daily writing practice to enhance writing fluency and reflective thinking in the language.",
            "Write short pieces and exchange them with classmates for feedback; revise based on the critiques received to improve writing quality and learn from peers."
    };


    private static final ProficiencyLevel[] proficiency = {
            ProficiencyLevel.A1,
            ProficiencyLevel.B1,
            ProficiencyLevel.C1,
            ProficiencyLevel.A1,
            ProficiencyLevel.B1,
            ProficiencyLevel.C1,
            ProficiencyLevel.A1,
            ProficiencyLevel.B1,
            ProficiencyLevel.C1
    };
    private List<Course> courses;


    @Override
    public void run(String... args) {
        System.out.println("SEEDING COURSES...");
        seedCourse();
        System.out.printf("Created %d courses.%n", courseRepository.count());

        System.out.println("SEEDING LECTURES...");
        seedLectures();
        System.out.printf("Created %d lectures.%n", lectureRepository.count());

        System.out.println("SEEDING EXERCISES...");
        seedExercises();
        System.out.printf("Created %d exercises.%n", exerciseRepository.count());

        System.out.println("SEEDING COURSE ENROLLMENTS...");
        seedCourseEnrollment();
        System.out.printf("Created %d course enrollments.%n", courseEnrollmentRepository.count());

        System.out.println("SEEDING LECTURE ENROLLMENTS...");
        seedLectureEnrollment();
        System.out.printf("Created %d lecture enrollments.%n", lectureEnrollmentRepository.count());
    }

    public void seedLectures() {
        for (int i = 0; i < courses.size(); i++) {
            createLecturesForCourse(i);
        }
    }

    public void createLecturesForCourse(int courseIndex) {
        Course course = courses.get(courseIndex);

        for (int i = 1; i <= 8; i++) {
            Lecture lecture = new Lecture();
            lecture.setId(UUID.randomUUID());
            lecture.setName(course.getName() + ": Lecture " + i);
            lecture.setDescription(descriptions[courseIndex]);
            lecture.setStartTime(generateRandomDateTime());
            lecture.setEndTime(lecture.getStartTime().plusHours(2)); // assuming a 2-hour fixed duration
            lecture.setLecturerId(lecturers.get(ThreadLocalRandom.current().nextInt(lecturers.size())).getId());
            lecture.setCapacity(30 + ThreadLocalRandom.current().nextInt(20)); // 30 to 50

            lecture.setCourse(course);
            lectureRepository.save(lecture);
        }
    }

    private void createCourse(int courseIndex) {
        Course course = new Course();

        course.setId(UUID.randomUUID());
        course.setName(courseNames[courseIndex]);
        course.setCapacity(ThreadLocalRandom.current().nextInt(20, 30));
        course.setLanguage(language[courseIndex]);
        course.setProficiencyLevel(proficiency[courseIndex]);
        courseRepository.save(course);
    }

    private void seedCourse() {
        for (int i = 0; i < courseNames.length; i++) {
            createCourse(i);
        }
        this.courses = courseRepository.findAll();
    }

    private void createExercisesForCourse(int courseIndex) {
        Course course = courses.get(courseIndex);
        for (int i = 0; i < 5; i++) {
            Exercise exercise = new Exercise();
            exercise.setCreatedAt(LocalDateTime.now());
            exercise.setId(UUID.randomUUID());

            int difficulty = ThreadLocalRandom.current().nextInt(ExerciseDifficulty.values().length);
            exercise.setExerciseDifficulty(ExerciseDifficulty.values()[difficulty]);

            int exerciseIndex = ThreadLocalRandom.current().nextInt(9);
            exercise.setName(exerciseNames[exerciseIndex]);
            exercise.setDescription(exerciseDescriptions[exerciseIndex]);
            exercise.setCourse(course);
            exercise.setUpdatedAt(LocalDateTime.now());
            exerciseRepository.save(exercise);
        }
    }

    private void seedExercises() {
        for (int i = 0; i < courses.size(); i++) {
            createExercisesForCourse(i);
        }
    }

    private void seedCourseEnrollment() {
        List<StudentDto> activeStudents = students.stream().filter(StudentDto::isActive).toList();
        for (StudentDto student : activeStudents) {
            for (Course course: courses) {
                boolean enrollStudent = ThreadLocalRandom.current().nextBoolean();
                if (!enrollStudent) {
                    continue;
                }
                CourseEnrollment enrollment = new CourseEnrollment();
                enrollment.setId(UUID.randomUUID());
                enrollment.setCourse(course);
                enrollment.setStudentId(student.getId());
                int stateIndex = ThreadLocalRandom.current().nextInt(EnrollmentState.values().length);
                enrollment.setEnrollmentState(EnrollmentState.values()[stateIndex]);
                courseEnrollmentRepository.save(enrollment);
            }
        }
    }

    private void seedLectureEnrollment() {
        List<StudentDto> activeStudents = students.stream().filter(StudentDto::isActive).toList();
        for (StudentDto student : activeStudents) {
            List<CourseEnrollment> coursesList = courseEnrollmentRepository.findByStudentId(student.getId());
            List<UUID> courses = coursesList.stream().map(enrollment -> enrollment.getCourse().getId() ).toList();
            List<Lecture> eligibleLectures = lectureRepository
                    .findAll()
                    .stream()
                    .takeWhile(l -> courses.contains(l.getCourse().getId()))
                    .toList();
            for (Lecture l: eligibleLectures) {
                LectureEnrollment enrollment = new LectureEnrollment();
                enrollment.setId(UUID.randomUUID());
                enrollment.setLecture(l);
                enrollment.setStudentId(student.getId());
                lectureEnrollmentRepository.save(enrollment);
            }
        }
    }

    private static LocalDateTime generateRandomDateTime() {
        return LocalDateTime.now()
                .plusDays(ThreadLocalRandom.current().nextInt(1, 30))
                .withHour(ThreadLocalRandom.current().nextInt(8, 18))
                .withMinute(0);
    }
}