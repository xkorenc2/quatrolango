package cz.muni.fi.core.rest;

import cz.muni.fi.commons.dto.exercise.ExerciseDto;
import cz.muni.fi.commons.enums.ExerciseDifficulty;
import cz.muni.fi.core.facade.ExerciseFacade;
import io.micrometer.observation.annotation.Observed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@RequestMapping(path = "/exercises", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@Tag(name = "Exercise service", description = "Service for operations on Exercise model")
public class ExerciseRestController {
    private final ExerciseFacade exerciseFacade;

    @Autowired
    public ExerciseRestController(ExerciseFacade exerciseFacade) {
        this.exerciseFacade = exerciseFacade;
    }

    @GetMapping(path = "/{uuid}")
    @Operation(summary = "Find Exercise by UUID", security = @SecurityRequirement(name = "Bearer"), description = "Finds an exercise by the provided UUID")
    @Observed(name = "findExerciseById")
    public ResponseEntity<ExerciseDto> findById(@PathVariable("uuid") UUID uuid) {
        return Optional.ofNullable(exerciseFacade.findById(uuid))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping(path = "/")
    @Operation(summary = "Find All Exercises", security = @SecurityRequirement(name = "Bearer"), description = "Finds all exercises")
    @Observed(name = "findAllExercises")
    public ResponseEntity<Page<ExerciseDto>> findAll(Pageable pageable) {
        return ResponseEntity.ok(exerciseFacade.findAll(pageable));
    }

    @Operation(summary = "Get exercises by IDs", security = @SecurityRequirement(name = "Bearer"), description = "Get exercises by IDs")
    @PostMapping(path = "/ids")
    @Observed(name = "findExercisesByIds")
    public ResponseEntity<List<ExerciseDto>> findLecturesByIds(@RequestBody Set<UUID> ids) {
        return ResponseEntity.ok(exerciseFacade.findByIds(ids));
    }

    @GetMapping(path = "/difficulty/{exerciseDifficulty}")
    @Operation(summary = "Find Exercises by provided difficulty", security = @SecurityRequirement(name = "Bearer"), description = "Finds exercises by difficulty")
    @Observed(name = "findExercisesByDifficulty")
    public ResponseEntity<List<ExerciseDto>> findByExerciseDifficulty(@PathVariable("exerciseDifficulty") ExerciseDifficulty exerciseDifficulty) {
        return ResponseEntity.ok(exerciseFacade.findByExerciseDifficulty(exerciseDifficulty));
    }

    @PostMapping(path = "/{studentUuid}/{exerciseUuid}")
    @Operation(summary = "Pick an exercise for a student by exercise id", security = @SecurityRequirement(name = "Bearer"), description = "Pick an exercise for a student")
    @Observed(name = "getExerciseForStudent")
    public ResponseEntity<ExerciseDto> getExerciseForStudent(@PathVariable UUID studentUuid,
                                                             @PathVariable UUID exerciseUuid) {
        return Optional.ofNullable(exerciseFacade.getExerciseForStudent(studentUuid, exerciseUuid))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping(path = "/{courseUuid}")
    @Operation(summary = "Create Exercise", security = @SecurityRequirement(name = "Bearer"), description = "Creates a new exercise")
    @Observed(name = "createExercise")
    public ResponseEntity<ExerciseDto> create(@PathVariable UUID courseUuid,
                                              @Valid @RequestBody ExerciseDto exerciseDTO) {
        return new ResponseEntity<>(exerciseFacade.create(exerciseDTO, courseUuid), HttpStatus.CREATED);
    }

    @DeleteMapping(path = "/{uuid}")
    @Operation(summary = "Delete Exercise by UUID", security = @SecurityRequirement(name = "Bearer"), description = "Deletes a exercise by the provided UUID")
    @Observed(name = "deleteExercise")
    public ResponseEntity<ExerciseDto> delete(@PathVariable UUID uuid) {
        return Optional.ofNullable(exerciseFacade.delete(uuid))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PutMapping(path = "/{uuid}")
    @Operation(summary = "Update Exercise by UUID", security = @SecurityRequirement(name = "Bearer"), description = "Updates an exercise by the provided UUID")
    @Observed(name = "updateExercise")
    public ResponseEntity<ExerciseDto> update(@PathVariable("uuid") UUID uuid, @Valid @RequestBody ExerciseDto exerciseDTO) {
        return Optional.ofNullable(exerciseFacade.update(uuid, exerciseDTO))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

}

