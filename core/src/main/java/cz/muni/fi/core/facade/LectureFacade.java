package cz.muni.fi.core.facade;

import cz.muni.fi.commons.dto.lecture.LectureDto;
import cz.muni.fi.core.data.mapper.LectureMapper;
import cz.muni.fi.core.service.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class LectureFacade {
    private final LectureMapper mapper;
    private final LectureService lectureService;

    @Autowired
    public LectureFacade(LectureService lectureService, LectureMapper mapper) {
        this.lectureService = lectureService;
        this.mapper = mapper;
    }

    @Transactional(readOnly = true)
    public LectureDto findById(UUID id) {
        return mapper.toDto(lectureService.findById(id));
    }

    @Transactional(readOnly = true)
    public Page<LectureDto> findAll(Pageable pageable) {
        return mapper.toDtoPage(lectureService.findAll(pageable));
    }

    @Transactional(readOnly = true)
    public List<LectureDto> findByIds(Set<UUID> ids) {
        return mapper.toDtoList(lectureService.findByIds(ids));
    }

    @Transactional(readOnly = true)
    public List<LectureDto> findByLecturer(UUID lecturerId) {
        return mapper.toDtoList(lectureService.findByLecturer(lecturerId));
    }

    @Transactional(readOnly = true)
    public List<LectureDto> findByDate(LocalDate date) {
        return mapper.toDtoList(lectureService.findByDate(date));
    }


    @Transactional(readOnly = true)
    public List<LectureDto> findByCourse(UUID courseId) {
        return mapper.toDtoList(lectureService.findByCourse(courseId));
    }

    @Transactional
    public LectureDto create(LectureDto lectureDto, UUID courseId) {
        return mapper.toDto(lectureService.create(mapper.toEntity(lectureDto), courseId));
    }

    @Transactional
    public LectureDto remove(UUID id) {
        return mapper.toDto(lectureService.remove(id));

    }

    @Transactional
    public LectureDto update(LectureDto lectureDto, UUID id) {
        return mapper.toDto(lectureService.update(mapper.toEntity(lectureDto), id));
    }
}
