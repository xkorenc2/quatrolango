package cz.muni.fi.core.data.mapper;

import cz.muni.fi.commons.dto.lecture.LectureDto;
import cz.muni.fi.core.data.model.Lecture;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LectureMapper {
    List<LectureDto> toDtoList(List<Lecture> lectures);

    default Page<LectureDto> toDtoPage(Page<Lecture> lectures) {
        return new PageImpl<>(toDtoList(lectures.getContent()), lectures.getPageable(), lectures.getTotalPages());
    }

    @Mapping(source = "course.id", target = "courseId")
    LectureDto toDto(Lecture lecture);

    @Mapping(source = "courseId", target = "course.id")
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "lectureEnrollments", ignore = true)
    Lecture toEntity(LectureDto lectureDto);
}
