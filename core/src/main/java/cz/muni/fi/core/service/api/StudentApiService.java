package cz.muni.fi.core.service.api;

import cz.muni.fi.commons.dto.CustomPageImpl;
import cz.muni.fi.commons.dto.user.student.StudentDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.AbstractOAuth2Token;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Service
public class StudentApiService {

    private final WebClient client;

    public StudentApiService(@Value("${student.api.url}") String baseUrl) {
        this.client = WebClient.create(baseUrl);
    }

    public Set<StudentDto> findStudentsByIds(Set<UUID> studentIds) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        AbstractOAuth2Token token = (AbstractOAuth2Token) authentication.getCredentials();

        var students = client
                .post()
                .uri("/ids")
                .headers(httpHeaders -> httpHeaders.setBearerAuth(token.getTokenValue()))
                .bodyValue(studentIds)
                .retrieve()
                .bodyToFlux(StudentDto.class);
        return new HashSet<>(Objects.requireNonNull(students.collectList().block()));
    }

    public List<StudentDto> findAll(int size) {
        return client
                .get()
                .uri("/?page=0&size=" + size)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<CustomPageImpl<StudentDto>>() {
                })
                .block()
                .getContent();
    }
}
