package cz.muni.fi.core.data.repository;

import cz.muni.fi.core.data.model.Lecture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Repository
public interface LectureRepository extends JpaRepository<Lecture, UUID> {

    @Query("SELECT l FROM Lecture l WHERE l.lecturerId = :lecturerId")
    List<Lecture> findByLecturerId(UUID lecturerId);

    @Query("SELECT l FROM Lecture l WHERE l.course.id = :courseId")
    List<Lecture> findByCourseId(UUID courseId);

    @Query("SELECT l FROM Lecture l WHERE l.startTime BETWEEN :startOfDay AND :endOfDay")
    List<Lecture> findByStartTimeAndEndTime(@Param("startOfDay") LocalDateTime startOfDay, @Param("endOfDay") LocalDateTime endOfDay);

    @Query("SELECT l FROM Lecture l WHERE l.id IN :ids")
    List<Lecture> findByIds(@Param("ids") Set<UUID> ids);
}
