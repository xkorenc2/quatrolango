package cz.muni.fi.core.facade;

import cz.muni.fi.commons.dto.lecture.LectureEnrollmentDto;
import cz.muni.fi.core.data.mapper.LectureEnrollmentMapper;
import cz.muni.fi.core.service.LectureEnrollmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class LectureEnrollmentFacade {
    private final LectureEnrollmentService lectureEnrollmentService;

    private final LectureEnrollmentMapper lectureEnrollmentMapper;

    @Autowired
    public LectureEnrollmentFacade(LectureEnrollmentService lectureEnrollmentService, LectureEnrollmentMapper lectureEnrollmentMapper) {
        this.lectureEnrollmentService = lectureEnrollmentService;
        this.lectureEnrollmentMapper = lectureEnrollmentMapper;
    }


    @Transactional(readOnly = true)
    public LectureEnrollmentDto findById(UUID id) {
        return lectureEnrollmentMapper.mapToDto(lectureEnrollmentService.findById(id));
    }

    @Transactional(readOnly = true)
    public List<LectureEnrollmentDto> findByStudentId(UUID studentId) {
        return lectureEnrollmentMapper.mapToDtoList(lectureEnrollmentService.findByStudentId(studentId));
    }

    @Transactional
    public LectureEnrollmentDto create(LectureEnrollmentDto lectureEnrollmentDto) {
        return lectureEnrollmentMapper.mapToDto(lectureEnrollmentService.create(lectureEnrollmentMapper.mapToEntity(lectureEnrollmentDto)));
    }

    @Transactional
    public LectureEnrollmentDto delete(UUID id) {
        return lectureEnrollmentMapper.mapToDto(lectureEnrollmentService.delete(id));
    }

    public boolean checkIfStudentIsEnrolled(UUID studentId, UUID lectureId) {
        return lectureEnrollmentService.checkIfStudentIsEnrolled(studentId, lectureId);
    }
}
