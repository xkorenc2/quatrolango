package cz.muni.fi.core.repository;

import cz.muni.fi.commons.enums.EnrollmentState;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.data.model.CourseEnrollment;
import cz.muni.fi.core.data.repository.CourseEnrollmentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
class CourseEnrollmentRepositoryTest {

    @Autowired
    private CourseEnrollmentRepository courseEnrollmentRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    private Course course;
    private Course course2;
    private final UUID studentId1 = UUID.randomUUID();
    private final UUID studentId2 = UUID.randomUUID();

    @BeforeEach
    void initData() {
        course = createCourse();
        course2 = createCourse();
        createCourseEnrollment(course, studentId1);
        createCourseEnrollment(course, studentId2);
    }

    private Course createCourse() {
        Course course = new Course();
        course.setName("Test Course");
        course.setProficiencyLevel(ProficiencyLevel.B1);
        course.setLanguage(Language.ENGLISH);
        course.setCapacity(60);
        return testEntityManager.persistAndFlush(course);
    }

    private void createCourseEnrollment(Course course, UUID studentId) {
        CourseEnrollment courseEnrollment = new CourseEnrollment();
        courseEnrollment.setCourse(course);
        courseEnrollment.setStudentId(studentId);
        courseEnrollment.setEnrollmentState(EnrollmentState.ENROLLED);
        testEntityManager.persistAndFlush(courseEnrollment);
    }

    @Test
    @Transactional
    void findByStudentId_studentValid_returnsEnrollment() {
        List<CourseEnrollment> enrollments = courseEnrollmentRepository.findByStudentId(studentId1);
        assertEquals(1, enrollments.size());
        assertEquals(studentId1, enrollments.getFirst().getStudentId());
    }

    @Test
    @Transactional
    void findByStudentId_studentInValid_returnsEmpty() {
        List<CourseEnrollment> enrollments = courseEnrollmentRepository.findByStudentId(UUID.randomUUID());
        assertEquals(0, enrollments.size());
    }

    @Test
    @Transactional
    void findByCourse_courseValid_returnsEnrollment() {
        List<CourseEnrollment> enrollments = courseEnrollmentRepository.findByCourse(course);
        assertEquals(2, enrollments.size());
        assertEquals(studentId1, enrollments.getFirst().getStudentId());
    }

    @Test
    @Transactional
    void findByCourse_courseInvalid_returnsNone() {
        List<CourseEnrollment> enrollments = courseEnrollmentRepository.findByCourse(course2);
        assertEquals(0, enrollments.size());
    }

    @Test
    @Transactional
    void findByCourseIdAndStudentId_bothValid_returnsEnrollment() {
        Optional<CourseEnrollment> enrollment = courseEnrollmentRepository.findByCourseIdAndStudentId(course.getId(), studentId1);
        assertNotNull(enrollment);
        assertFalse(enrollment.isEmpty());
        assertEquals(studentId1, enrollment.get().getStudentId());
        assertEquals(course, enrollment.get().getCourse());
    }

    @Test
    @Transactional
    void findByCourseIdAndStudentId_invalidCourse_returnsNone() {
        Optional<CourseEnrollment> enrollment = courseEnrollmentRepository.findByCourseIdAndStudentId(course2.getId(), studentId1);
        assertTrue(enrollment.isEmpty());
    }

    @Test
    @Transactional
    void findByCourseIdAndStudentId_invalidStudent_returnsNone() {
        Optional<CourseEnrollment> enrollment = courseEnrollmentRepository.findByCourseIdAndStudentId(course.getId(), UUID.randomUUID());
        assertTrue(enrollment.isEmpty());
    }

    @Test
    @Transactional
    void findByCourseIdAndStudentId_bothInvalid_returnsNone() {
        Optional<CourseEnrollment> enrollment = courseEnrollmentRepository.findByCourseIdAndStudentId(course2.getId(), UUID.randomUUID());
        assertTrue(enrollment.isEmpty());
    }

    @Test
    @Transactional
    void checkIfStudentIsEnrolled_studentValid_returnsTrue() {
        assertTrue(courseEnrollmentRepository.checkIfStudentIsEnrolled(course.getId(), studentId1));
    }

    @Test
    @Transactional
    void checkIfStudentIsEnrolled_studentInvalid_returnsFalse() {
        assertFalse(courseEnrollmentRepository.checkIfStudentIsEnrolled(course.getId(), UUID.randomUUID()));
    }
}
