package cz.muni.fi.core.facade;

import cz.muni.fi.commons.BaseEntity;
import cz.muni.fi.commons.dto.course.CourseDetailedDto;
import cz.muni.fi.commons.dto.course.CourseDto;
import cz.muni.fi.commons.dto.course.CourseUpdateDto;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import cz.muni.fi.core.data.mapper.CourseMapper;
import cz.muni.fi.core.data.mapper.ExerciseMapper;
import cz.muni.fi.core.data.mapper.LectureMapper;
import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.factory.TestDataFactory;
import cz.muni.fi.core.service.CourseEnrollmentService;
import cz.muni.fi.core.service.CourseService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CourseFacadeTest {
    @Mock
    private CourseService courseService;
    @Mock
    private CourseMapper courseMapper;
    @Mock
    private LectureMapper lectureMapper;
    @Mock
    private ExerciseMapper exerciseMapper;
    @Mock
    private CourseEnrollmentService courseEnrollmentService;
    @Mock
    private LectureFacade lectureFacade;
    @Mock
    private ExerciseFacade exerciseFacade;
    @InjectMocks
    private CourseFacade courseFacade;

    @Test
    void findById_courseFound_returnsCourse() {
        Course course = TestDataFactory.createSampleCourse();
        CourseDto courseDto = TestDataFactory.createSampleCourseDto();
        when(courseService.findById(course.getId())).thenReturn(course);
        when(courseMapper.mapToDto(course)).thenReturn(courseDto);

        var result = courseFacade.findById(course.getId());

        assertThat(result).isEqualTo(courseDto);
    }

    @Test
    void findAll_coursesFound_returnsCourses() {
        Pageable pageable = Pageable.unpaged();
        List<CourseDto> courseDtos = TestDataFactory.createSampleListOfCourseDtos();
        List<Course> courses = TestDataFactory.createSampleListOfCourses();
        Page<Course> coursePage = new PageImpl<>(courses);
        Page<CourseDto> courseDtoPage = new PageImpl<>(courseDtos);

        when(courseService.findAll(pageable)).thenReturn(coursePage);
        when(courseMapper.mapToPage(coursePage)).thenReturn(courseDtoPage);

        var result = courseFacade.findAll(pageable);

        assertThat(result).isEqualTo(courseDtoPage);
    }

    @Test
    void findByProficiencyLevel_coursesFound_returnsCourses() {
        List<CourseDto> courseDtos = TestDataFactory.createSampleListOfCourseDtos();
        List<Course> courses = TestDataFactory.createSampleListOfCourses();

        when(courseService.findByProficiencyLevel(ProficiencyLevel.A1)).thenReturn(courses);
        when(courseMapper.mapToList(courses)).thenReturn(courseDtos);

        var result = courseFacade.findByProficiencyLevel(ProficiencyLevel.A1);

        assertThat(result).isEqualTo(courseDtos);
    }

    @Test
    void findByLanguage_coursesFound_returnsCourses() {
        List<CourseDto> courseDtos = TestDataFactory.createSampleListOfCourseDtos();
        List<Course> courses = TestDataFactory.createSampleListOfCourses();

        when(courseService.findByLanguage(Language.ENGLISH)).thenReturn(courses);
        when(courseMapper.mapToList(courses)).thenReturn(courseDtos);

        var result = courseFacade.findByLanguage(Language.ENGLISH);

        assertThat(result).isEqualTo(courseDtos);
    }

    @Test
    void create_courseValid_returnsCreated() {
        Course course = TestDataFactory.createSampleCourse();
        CourseDto courseDto = TestDataFactory.createSampleCourseDto();
        when(courseService.create(course)).thenReturn(course);
        when(courseMapper.mapToDto(course)).thenReturn(courseDto);
        when(courseMapper.mapToEntity(any(CourseDto.class))).thenReturn(course);

        var result = courseFacade.create(courseDto);

        assertThat(result).isEqualTo(courseDto);
    }

    @Test
    void delete_courseExists_returnsDeleted() {
        Course course = TestDataFactory.createSampleCourse();
        CourseDto courseDto = TestDataFactory.createSampleCourseDto();
        when(courseService.delete(course.getId())).thenReturn(course);
        when(courseMapper.mapToDto(course)).thenReturn(courseDto);

        var result = courseFacade.delete(courseDto.getId());

        assertThat(result).isEqualTo(courseDto);
    }

    @Test
    void update_courseDoesNotExist_returnsUpdated() {
        Course course = TestDataFactory.createSampleCourse();
        CourseDto courseDto = TestDataFactory.createSampleCourseDto();
        CourseUpdateDto courseUpdateDto = TestDataFactory.createSampleCourseUpdateDto();
        when(courseService.update(course.getId(), course)).thenReturn(course);
        when(courseMapper.mapToDto(course)).thenReturn(courseDto);
        when(courseMapper.mapToEntity(courseUpdateDto)).thenReturn(course);

        var result = courseFacade.update(courseDto.getId(), courseUpdateDto);

        assertThat(result).isEqualTo(courseDto);
    }


    @Test
    void findByDetailed_courseExists_returnsCourseDetailed() {
        Course course = TestDataFactory.createSampleCourse();
        CourseDto courseDto = TestDataFactory.createSampleCourseDto();
        var sampleStudents = Set.of(TestDataFactory.createSampleStudentDto());
        var sampleLectures = List.of(TestDataFactory.createSampleLectureDto());
        var sampleExercises = List.of(TestDataFactory.createSampleExerciseDto());

        CourseDetailedDto courseDetailedDto = TestDataFactory.getCourseDetailedDtoFactory();
        courseDetailedDto.setExercises(sampleExercises);
        courseDetailedDto.setLectures(sampleLectures);
        courseDetailedDto.setStudents(sampleStudents.stream().toList());

        when(courseService.findById(course.getId())).thenReturn(course);
        when(courseMapper.mapToDetailedDto(course)).thenReturn(courseDetailedDto);
        when(courseEnrollmentService.findStudentsByCourse(any())).thenReturn(sampleStudents);

        var result = courseFacade.findByIdDetailed(courseDto.getId());

        assertThat(result).isEqualTo(courseDetailedDto);
        assertThat(result.getExercises()).isEqualTo(courseDetailedDto.getExercises());
        assertThat(result.getStudents()).isEqualTo(courseDetailedDto.getStudents());
        assertThat(result.getLectures()).isEqualTo(courseDetailedDto.getLectures());
    }

    @Test
    void findByIds_validIds_returnsListOfCourseDtos() {
        List<Course> courses = TestDataFactory.createSampleListOfCourses();
        List<CourseDto> expectedDtos = TestDataFactory.createSampleListOfCourseDtos();

        Set<UUID> ids = courses.stream().map(BaseEntity::getId).collect(Collectors.toSet());

        when(courseService.findByIds(ids)).thenReturn(courses);
        when(courseMapper.mapToList(courses)).thenReturn(expectedDtos);

        List<CourseDto> result = courseFacade.findByIds(ids);

        assertEquals(expectedDtos, result);
    }
}
