package cz.muni.fi.core.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.commons.dto.lecture.LectureDto;
import cz.muni.fi.commons.dto.course.CourseDetailedDto;
import cz.muni.fi.commons.dto.course.CourseDto;
import cz.muni.fi.commons.enums.ExerciseDifficulty;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import cz.muni.fi.core.data.mapper.CourseMapper;
import cz.muni.fi.core.data.mapper.ExerciseMapper;
import cz.muni.fi.core.data.mapper.LectureMapper;
import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.data.model.Exercise;
import cz.muni.fi.core.data.model.Lecture;
import cz.muni.fi.core.data.repository.CourseRepository;
import cz.muni.fi.core.data.repository.ExerciseRepository;
import cz.muni.fi.core.data.repository.LectureRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WithMockUser(username = "admin", authorities = {"SCOPE_test_1", "SCOPE_test_2", "SCOPE_test_3"})
class CourseRestControllerIT {
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private LectureRepository lectureRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private ExerciseRepository exerciseRepository;

    @Autowired
    private CourseMapper courseMapper;

    @Autowired
    private LectureMapper lectureMapper;

    @Autowired
    private ExerciseMapper exerciseMapper;
    private Course course1;
    private Course course2;
    private Lecture lecture;
    private Exercise exercise;

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;

    @BeforeAll
    void setup() {
        objectMapper.findAndRegisterModules();
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @BeforeEach
    void setupBeforeEach() {
        courseRepository.deleteAll();

        // Create a course
        Course c1 = new Course();
        c1.setName("Test course 1");
        c1.setLanguage(Language.GERMAN);
        c1.setProficiencyLevel(ProficiencyLevel.B1);
        c1.setCapacity(100);
        course1 = courseRepository.save(c1);

        Course c2 = new Course();
        c2.setName("Test course 2");
        c2.setLanguage(Language.ENGLISH);
        c2.setProficiencyLevel(ProficiencyLevel.B2);
        c2.setCapacity(1000);
        course2 = courseRepository.save(c2);

        objectMapper.findAndRegisterModules();

        // Create a lecture
        Lecture l = new Lecture();
        l.setCapacity(100);
        l.setCourse(course1);
        l.setDescription("Test lecture");
        l.setLecturerId(UUID.randomUUID()); // TODO
        l.setStartTime(java.time.LocalDateTime.now());
        l.setEndTime(java.time.LocalDateTime.now().plusHours(1));
        l.setName("Test lecture");
        lecture = lectureRepository.save(l);

        // Create an exercise
        Exercise e = new Exercise();
        e.setCourse(course1);
        e.setDescription("Test exercise");
        e.setName("Test exercise");
        e.setExerciseDifficulty(ExerciseDifficulty.HARD);
        exercise = exerciseRepository.save(e);
    }

    @Test
    void findById_courseExists_returnsCourse() throws Exception {
        // Arrange
        CourseDto expected = courseMapper.mapToDto(course1);

        // Act
        String result = mockMvc.perform(MockMvcRequestBuilders.get("/courses/" + course1.getId()).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON).with(csrf()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        CourseDto actual = objectMapper.readValue(result, CourseDto.class);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void findById_courseDoesNotExist_returnsNotFound() throws Exception {
        // Arrange
        UUID id = UUID.randomUUID();
        // Act
        mockMvc.perform(MockMvcRequestBuilders.get("/courses/" + id).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void findByIds_coursesExist_returnsCourses() throws Exception {
        // Arrange
        Set<UUID> ids = Set.of(course1.getId(), course2.getId());
        List<CourseDto> expected = List.of(courseMapper.mapToDto(course1), courseMapper.mapToDto(course2));

        // Act
        String result = mockMvc.perform(MockMvcRequestBuilders.post("/courses/ids").with(csrf())
                        .content(objectMapper.writeValueAsBytes(ids))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        List<CourseDto> actual = Arrays.asList(objectMapper.readValue(result, CourseDto[].class));

        // Assert
        assertEquals(expected.size(), actual.size(), "The returned list of courses does not match the length of expected list");
        for (int i = 0; i < 2; i++) {
            assertEquals(expected.stream().map(CourseDto::getId).sorted().toList(),
                    actual.stream().map(CourseDto::getId).sorted().toList(),
                    "The returned list of courses does not match the expected list");
        }
    }

    @Test
    void findByIdDetailed_courseExists_returnsCourse() throws Exception {
        // Arrange
        CourseDto expected = courseMapper.mapToDto(course1);

        // Act
        String result = mockMvc.perform(MockMvcRequestBuilders.get("/courses/detailed/" + course1.getId()).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        CourseDetailedDto actual = objectMapper.readValue(result, CourseDetailedDto.class);

        // Assert
        assertEquals(actual.getClass(), CourseDetailedDto.class);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getLanguage(), actual.getLanguage());
        assertEquals(expected.getProficiencyLevel(), actual.getProficiencyLevel());
        assertEquals(expected.getCapacity(), actual.getCapacity());
        assertEquals(1, actual.getLectures().size());
        assertEquals(1, actual.getExercises().size());
        assertEquals(0, actual.getStudents().size());
    }

    @Test
    void findByIdDetailed_courseDoesNotExist_returnsNotFound() throws Exception {
        // Arrange
        UUID id = UUID.randomUUID();
        // Act
        mockMvc.perform(MockMvcRequestBuilders.get("/courses/detailed/" + id).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void findByProficiencyLevel_coursesExist_returnsCourses() throws Exception {
        // Arrange
        List<CourseDto> expected = List.of(courseMapper.mapToDto(course2));

        // Act
        String result = mockMvc.perform(MockMvcRequestBuilders.get("/courses/levels/" + ProficiencyLevel.B2).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        CourseDto[] actual = objectMapper.readValue(result, CourseDto[].class);

        // Assert
        assertEquals(expected.size(), actual.length);
        assertEquals(expected.getFirst(), actual[0]);
    }

    @Test
    void findByProficiencyLevel_noCourseMatching_returnsEmptyList() throws Exception {

        String result = mockMvc.perform(MockMvcRequestBuilders.get("/courses/levels/" + ProficiencyLevel.C2).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        CourseDto[] actual = objectMapper.readValue(result, CourseDto[].class);

        assertEquals(0, actual.length);
    }

    @Test
    void findByProficiencyLevel_invalidProficiencyLevel_returnsInternalError() throws Exception {
        // Arrange
        String invalidProficiencyLevel = "INVALID";
        // Act
        mockMvc.perform(MockMvcRequestBuilders.get("/courses/levels/" + invalidProficiencyLevel).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void findByLanguage_coursesExist_returnsCourses() throws Exception {
        // Arrange
        List<CourseDto> expected = List.of(courseMapper.mapToDto(course2));

        // Act
        String result = mockMvc.perform(MockMvcRequestBuilders.get("/courses/languages/" + Language.ENGLISH).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        CourseDto[] actual = objectMapper.readValue(result, CourseDto[].class);

        // Assert
        assertEquals(expected.size(), actual.length);
        assertEquals(expected.getFirst(), actual[0]);
    }

    @Test
    void findByLanguage_noCourseMatching_returnsEmptyList() throws Exception {
        String result = mockMvc.perform(MockMvcRequestBuilders.get("/courses/languages/" + Language.SPANISH).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        CourseDto[] actual = objectMapper.readValue(result, CourseDto[].class);

        // Assert
        assertEquals(0, actual.length);
    }

    @Test
    void findByLanguage_invalidLanguage_returnsInternalError() throws Exception {
        // Arrange
        String invalidLanguage = "INVALID";
        // Act
        mockMvc.perform(MockMvcRequestBuilders.get("/courses/languages/" + invalidLanguage).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void createCourse_validCourse_returnsCreated() throws Exception {
        // Arrange
        CourseDto courseDto = new CourseDto();
        courseDto.setName("Test course 3");
        courseDto.setLanguage(Language.SPANISH);
        courseDto.setProficiencyLevel(ProficiencyLevel.A1);
        courseDto.setCapacity(100);

        // Act
        String result = mockMvc.perform(MockMvcRequestBuilders.post("/courses/")
                        .content(objectMapper.writeValueAsBytes(courseDto))
                        .contentType(MediaType.APPLICATION_JSON).with(csrf()))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString();

        CourseDto actual = objectMapper.readValue(result, CourseDto.class);

        // Assert
        assertEquals(courseDto.getName(), actual.getName());
        assertEquals(courseDto.getLanguage(), actual.getLanguage());
        assertEquals(courseDto.getProficiencyLevel(), actual.getProficiencyLevel());
        assertEquals(courseDto.getCapacity(), actual.getCapacity());
    }

    @Test
    void createCourse_invalidCourse_returnsBadRequest() throws Exception {
        // Arrange
        CourseDto courseDto = new CourseDto();
        courseDto.setName("Test course 3");
        courseDto.setLanguage(Language.SPANISH);
        courseDto.setProficiencyLevel(ProficiencyLevel.A1);
        courseDto.setCapacity(-1);

        // Act
        mockMvc.perform(MockMvcRequestBuilders.post("/courses/").with(csrf())
                        .content(objectMapper.writeValueAsBytes(courseDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void updateCourse_validCourse_returnsUpdated() throws Exception {
        // Arrange
        CourseDto courseDto = new CourseDto();
        courseDto.setName("Test course updated");
        courseDto.setLanguage(Language.GERMAN);
        courseDto.setProficiencyLevel(ProficiencyLevel.C1);
        courseDto.setCapacity(100);

        // Act
        String result = mockMvc.perform(MockMvcRequestBuilders.put("/courses/" + course2.getId()).with(csrf())
                        .content(objectMapper.writeValueAsBytes(courseDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        CourseDto actual = objectMapper.readValue(result, CourseDto.class);

        // Assert
        assertEquals(courseDto.getName(), actual.getName());
        assertEquals(courseDto.getLanguage(), actual.getLanguage());
        assertEquals(courseDto.getProficiencyLevel(), actual.getProficiencyLevel());
        assertEquals(courseDto.getCapacity(), actual.getCapacity());
    }

    @Test
    void updateCourse_invalidCourseId_returnsNotFound() throws Exception {
        // Arrange
        CourseDto courseDto = new CourseDto();
        courseDto.setName("Test course updated");
        courseDto.setLanguage(Language.GERMAN);
        courseDto.setProficiencyLevel(ProficiencyLevel.C1);
        courseDto.setCapacity(100);

        // Act
        mockMvc.perform(MockMvcRequestBuilders.put("/courses/" + UUID.randomUUID()).with(csrf())
                        .content(objectMapper.writeValueAsBytes(courseDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void updateCourse_invalidCourse_returnsBadRequest() throws Exception {
        // Arrange
        CourseDto courseDto = new CourseDto();
        courseDto.setName("Test course updated");
        courseDto.setLanguage(Language.GERMAN);
        courseDto.setProficiencyLevel(ProficiencyLevel.C1);
        courseDto.setCapacity(-1);

        // Act
        mockMvc.perform(MockMvcRequestBuilders.put("/courses/" + course2.getId()).with(csrf())
                        .content(objectMapper.writeValueAsBytes(courseDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void deleteCourse_validCourseId_returnsIsOk() throws Exception {
        // Arrange
        // Act
        mockMvc.perform(MockMvcRequestBuilders.delete("/courses/" + course2.getId()).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        // Assert
        assertEquals(1, courseRepository.count());
    }

    @Test
    void deleteCourse_invalidCourseId_returnsNotFound() throws Exception {
        // Arrange
        // Act
        mockMvc.perform(MockMvcRequestBuilders.delete("/courses/" + UUID.randomUUID()).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        // Assert
        assertEquals(2, courseRepository.count());
    }

    @Test
    void findAll_lecturesExist_returnsLectures() throws Exception {
        // Arrange
        LectureDto expected = lectureMapper.toDto(lecture);

        // Act
        mockMvc.perform(MockMvcRequestBuilders.get("/lectures/").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$.content[0].id").value(expected.getId().toString()))
                .andExpect(jsonPath("$.content[0].name").value(expected.getName()));

    }

    @Test
    void findAll_coursesExist_returnsCourses() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/courses/").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].id").value(course1.getId().toString()))
                .andExpect(jsonPath("$.content[0].name").value(course1.getName()));
    }

    @Test
    void findByDate_lecturesExist_returnsLectures() throws Exception {
        // Arrange
        LectureDto expected = lectureMapper.toDto(lecture);


        // Act
        String response = mockMvc.perform(MockMvcRequestBuilders.get("/lectures/date/" +
                                lecture.getStartTime().toLocalDate()).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        LectureDto[] actual = objectMapper.readValue(response, LectureDto[].class);

        // Assert
        assertEquals(expected, actual[0]);
    }
}