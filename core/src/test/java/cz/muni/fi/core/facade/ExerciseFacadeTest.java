package cz.muni.fi.core.facade;

import cz.muni.fi.commons.dto.exercise.ExerciseDto;
import cz.muni.fi.commons.enums.ExerciseDifficulty;
import cz.muni.fi.core.data.mapper.ExerciseMapper;
import cz.muni.fi.core.data.model.Exercise;
import cz.muni.fi.core.factory.TestDataFactory;
import cz.muni.fi.core.service.ExerciseService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ExerciseFacadeTest {
    @Mock
    private ExerciseService exerciseService;
    @Mock
    private ExerciseMapper exerciseMapper;
    @InjectMocks
    private ExerciseFacade exerciseFacade;

    @Test
    void findById_existingId_returnsExerciseDto() {
        Exercise exercise = TestDataFactory.createSampleExercise();
        UUID id = exercise.getId();

        ExerciseDto expectedExerciseDto = TestDataFactory.createSampleExerciseDto();

        when(exerciseService.findById(id)).thenReturn(exercise);
        when(exerciseMapper.mapToDto(exercise)).thenReturn(expectedExerciseDto);

        ExerciseDto result = exerciseFacade.findById(id);

        assertThat(expectedExerciseDto).isEqualTo(result);

        verify(exerciseService).findById(id);
        verify(exerciseMapper).mapToDto(exercise);
    }

    @Test
    void findAll_exercisesFound_returnsListOfExerciseDto() {
        Pageable pageable = Pageable.unpaged();

        List<Exercise> exercises = TestDataFactory.getExerciseEntities();
        List<ExerciseDto> exerciseDtos = TestDataFactory.getExerciseDtos();
        Page<Exercise> exercisePage = new PageImpl<>(exercises);
        Page<ExerciseDto> exerciseDtoPage = new PageImpl<>(exerciseDtos);

        when(exerciseService.findAll(pageable)).thenReturn(exercisePage);
        when(exerciseMapper.toDtoPage(exercisePage)).thenReturn(exerciseDtoPage);

        Page<ExerciseDto> result = exerciseFacade.findAll(pageable);

        assertEquals(exerciseDtoPage, result);

        verify(exerciseService).findAll(pageable);
        verify(exerciseMapper).toDtoPage(exercisePage);
    }

    @Test
    void findByIds_exercisesFound_returnsListOfExerciseDto() {
        List<ExerciseDto> exerciseDtos = TestDataFactory.getExerciseDtos();
        List<Exercise> exercises = TestDataFactory.getExerciseEntities();
        Set<UUID> ids = new HashSet<>();

        for (var exercise : exercises) {
            ids.add(exercise.getId());
        }

        when(exerciseService.findByIds(ids)).thenReturn(exercises);
        when(exerciseMapper.toDtoList(exercises)).thenReturn(exerciseDtos);

        List<ExerciseDto> result = exerciseFacade.findByIds(ids);
        assertEquals(exerciseDtos, result);

        verify(exerciseService).findByIds(ids);
        verify(exerciseMapper).toDtoList(exercises);
    }

    @Test
    void findByExerciseDifficulty_exercisesFound_returnsListOfExerciseDto() {
        ExerciseDifficulty difficulty = ExerciseDifficulty.EASY;
        List<Exercise> exercises = TestDataFactory.getExerciseEntities();
        List<ExerciseDto> exerciseDtos = TestDataFactory.getExerciseDtos();

        when(exerciseService.findByExerciseDifficulty(difficulty)).thenReturn(exercises);
        when(exerciseMapper.toDtoList(exercises)).thenReturn(exerciseDtos);

        List<ExerciseDto> result = exerciseFacade.findByExerciseDifficulty(difficulty);

        assertEquals(exerciseDtos, result);

        verify(exerciseService).findByExerciseDifficulty(difficulty);
        verify(exerciseMapper).toDtoList(exercises);
    }

    @Test
    void pickForStudent_exercisePicked_returnsExerciseDto() {
        UUID studentUuid = UUID.randomUUID();

        Exercise exercise = TestDataFactory.createSampleExercise();
        UUID exerciseUuid = exercise.getId();
        ExerciseDto expectedExerciseDto = TestDataFactory.createSampleExerciseDto();


        when(exerciseService.getExerciseForStudent(studentUuid, exerciseUuid)).thenReturn(exercise);
        when(exerciseMapper.mapToDto(exercise)).thenReturn(expectedExerciseDto);

        ExerciseDto result = exerciseFacade.getExerciseForStudent(studentUuid, exerciseUuid);

        assertEquals(expectedExerciseDto, result);

        verify(exerciseService).getExerciseForStudent(studentUuid, exerciseUuid);
        verify(exerciseMapper).mapToDto(exercise);
    }

    @Test
    void create_createdSuccessfully_returnsExerciseDto() {
        UUID courseUuid = UUID.randomUUID();
        ExerciseDto exerciseDto = TestDataFactory.createSampleExerciseDto();
        Exercise exercise = TestDataFactory.createSampleExercise();

        when(exerciseMapper.mapToEntity(exerciseDto)).thenReturn(exercise);
        when(exerciseService.create(exercise, courseUuid)).thenReturn(exercise);
        when(exerciseMapper.mapToDto(exercise)).thenReturn(exerciseDto);

        ExerciseDto result = exerciseFacade.create(exerciseDto, courseUuid);

        assertEquals(exerciseDto, result);

        verify(exerciseMapper).mapToEntity(exerciseDto);
        verify(exerciseService).create(exercise, courseUuid);
        verify(exerciseMapper).mapToDto(exercise);
    }

    @Test
    void delete_deletedSuccessfully_returnsExerciseDto() {
        Exercise exercise = TestDataFactory.createSampleExercise();
        UUID id = exercise.getId();
        ExerciseDto expectedExerciseDto = TestDataFactory.createSampleExerciseDto();

        when(exerciseService.delete(id)).thenReturn(exercise);
        when(exerciseMapper.mapToDto(exercise)).thenReturn(expectedExerciseDto);

        ExerciseDto result = exerciseFacade.delete(id);

        assertEquals(expectedExerciseDto, result);

        verify(exerciseService).delete(id);
        verify(exerciseMapper).mapToDto(exercise);
    }

    @Test
    void update_updatedSuccessfully_returnsExerciseDto() {
        ExerciseDto exerciseDto = TestDataFactory.createSampleExerciseDto();
        exerciseDto.setName("New");
        exerciseDto.setExerciseDifficulty(ExerciseDifficulty.EASY);
        Exercise exercise = TestDataFactory.createSampleExercise();
        UUID id = exercise.getId();

        ExerciseDto expectedExerciseDto = TestDataFactory.createSampleExerciseDto();

        when(exerciseMapper.mapToEntity(exerciseDto)).thenReturn(exercise);
        when(exerciseService.update(id, exercise)).thenReturn(exercise);
        when(exerciseMapper.mapToDto(exercise)).thenReturn(expectedExerciseDto);

        ExerciseDto result = exerciseFacade.update(id, exerciseDto);

        assertEquals(expectedExerciseDto, result);

        verify(exerciseMapper).mapToEntity(exerciseDto);
        verify(exerciseService).update(id, exercise);
        verify(exerciseMapper).mapToDto(exercise);
    }

    @Test
    void create_invalidExerciseDto_returnsNull() {
        ExerciseDto invalidDto = new ExerciseDto();

        ExerciseDto result = exerciseFacade.create(invalidDto, UUID.randomUUID());

        assertNull(result);
    }

    @Test
    void update_nonExistingId_returnsNull() {
        ExerciseDto nonExistingDto = new ExerciseDto();
        nonExistingDto.setId(UUID.randomUUID());

        ExerciseDto result = exerciseFacade.update(UUID.randomUUID(), nonExistingDto);

        assertNull(result);
    }

    @Test
    void remove_nonExistingId_returnsNull() {
        UUID nonExistingId = UUID.randomUUID();

        ExerciseDto result = exerciseFacade.delete(nonExistingId);

        assertNull(result);
    }

    @Test
    void create_nullExerciseDto_returnsNull() {
        ExerciseDto result = exerciseFacade.create(null, UUID.randomUUID());

        assertNull(result);
    }

    @Test
    void create_emptyExerciseDto_returnsNull() {
        ExerciseDto emptyDto = new ExerciseDto();

        ExerciseDto result = exerciseFacade.create(emptyDto, UUID.randomUUID());

        assertNull(result);
    }

    @Test
    void update_nullExerciseDto_returnsNull() {
        ExerciseDto result = exerciseFacade.update(UUID.randomUUID(), null);

        assertNull(result);
    }

    @Test
    void update_emptyExerciseDto_returnsNull() {
        ExerciseDto emptyDto = new ExerciseDto();

        ExerciseDto result = exerciseFacade.update(UUID.randomUUID(), emptyDto);

        assertNull(result);
    }

}
