package cz.muni.fi.core.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.commons.dto.exercise.ExerciseDto;
import cz.muni.fi.commons.enums.EnrollmentState;
import cz.muni.fi.commons.enums.ExerciseDifficulty;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import cz.muni.fi.core.data.mapper.ExerciseMapper;
import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.data.model.CourseEnrollment;
import cz.muni.fi.core.data.model.Exercise;
import cz.muni.fi.core.data.repository.CourseEnrollmentRepository;
import cz.muni.fi.core.data.repository.CourseRepository;
import cz.muni.fi.core.data.repository.ExerciseRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WithMockUser(username = "admin", authorities = {"SCOPE_test_1", "SCOPE_test_2", "SCOPE_test_3"})
class ExerciseRestControllerIT {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private ExerciseRepository exerciseRepository;

    @Autowired
    private CourseEnrollmentRepository courseEnrollmentRepository;

    @Autowired
    private ExerciseMapper exerciseMapper;

    private Course course1;
    private Exercise exercise;
    private UUID studentId;

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;

    @BeforeAll
    public void setUp() {
        objectMapper.findAndRegisterModules();
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        // Create a course
        Course c = new Course();
        c.setName("Test course 1");
        c.setLanguage(Language.GERMAN);
        c.setProficiencyLevel(ProficiencyLevel.B1);
        c.setCapacity(100);
        course1 = courseRepository.save(c);

        studentId = UUID.randomUUID();


        CourseEnrollment ce = new CourseEnrollment();
        ce.setStudentId(studentId);
        ce.setCourse(course1);
        ce.setEnrollmentState(EnrollmentState.ENROLLED);
        courseEnrollmentRepository.save(ce);
    }

    @BeforeEach
    public void setUpEach() {
        exerciseRepository.deleteAll();

        // Create an exercise
        Exercise e = new Exercise();
        e.setCourse(course1);
        e.setDescription("Test exercise");
        e.setName("Test exercise");
        e.setExerciseDifficulty(ExerciseDifficulty.HARD);
        exercise = exerciseRepository.save(e);

    }

    @Test
    void findById_exerciseExists_returnsExercise() throws Exception {
        // Arrange
        ExerciseDto expected = exerciseMapper.mapToDto(exercise);

        // Act
        String result = mockMvc.perform(MockMvcRequestBuilders.get("/exercises/" + exercise.getId()).with(csrf()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ExerciseDto actual = objectMapper.readValue(result, ExerciseDto.class);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void findById_exerciseDoesNotExist_returnsNotFound() throws Exception {
        // Act
        mockMvc.perform(MockMvcRequestBuilders.get("/exercises/" + "00000000-0000-0000-0000-000000000000").with(csrf()))
                .andExpect(status().isNotFound());
    }

    @Test
    void findByExerciseDifficulty_exercisesExist_returnsExercises() throws Exception {
        // Arrange
        ExerciseDto expected = exerciseMapper.mapToDto(exercise);

        // Act
        String result = mockMvc.perform(MockMvcRequestBuilders.get("/exercises/difficulty/" + ExerciseDifficulty.HARD).with(csrf()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ExerciseDto[] actual = objectMapper.readValue(result, ExerciseDto[].class);

        // Assert
        assertEquals(1, actual.length);
        assertEquals(expected, actual[0]);
    }

    @Test
    void findByExerciseDifficulty_exercisesDoNotExist_returnsEmptyList() throws Exception {
        // Arrange
        exerciseRepository.deleteAll();

        // Act
        String result = mockMvc.perform(MockMvcRequestBuilders.get("/exercises/difficulty/" + ExerciseDifficulty.HARD).with(csrf()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ExerciseDto[] actual = objectMapper.readValue(result, ExerciseDto[].class);

        // Assert
        assertEquals(0, actual.length);
    }

    @Test
    void findAll_exercisesDoNotExist_returnsEmptyList() throws Exception {
        // Arrange
        exerciseRepository.deleteAll();

        // Act
        mockMvc.perform(MockMvcRequestBuilders.get("/exercises/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content").isEmpty());
    }

    @Test
    void getExerciseForStudent_exerciseExists_returnsExercise() throws Exception {
        // Arrange
        ExerciseDto expected = exerciseMapper.mapToDto(exercise);

        // Act
        String result = mockMvc.perform(MockMvcRequestBuilders.post("/exercises/" + studentId + "/" + exercise.getId()).with(csrf()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ExerciseDto actual = objectMapper.readValue(result, ExerciseDto.class);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void findAll_exercisesExist_returnsExercises() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/exercises/").with(csrf()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content[0].id").value(exercise.getId().toString()))
                .andExpect(jsonPath("$.content[0].name").value(exercise.getName()));
    }


    @Test
    void getExerciseForStudent_studentNotEnrolled_returnsNotFound() throws Exception {
        // Act
        mockMvc.perform(MockMvcRequestBuilders.post("/exercises/" + UUID.randomUUID() + '/' + exercise.getId()).with(csrf()))
                .andExpect(status().isNotFound());
    }

    @Test
    void createExercise_courseExists_returnsExercise() throws Exception {
        // Arrange
        ExerciseDto expected = new ExerciseDto();
        expected.setCourseId(course1.getId());
        expected.setDescription("Test exercise");
        expected.setName("Test exercise");
        expected.setExerciseDifficulty(ExerciseDifficulty.HARD);

        // Act
        String result = mockMvc.perform(MockMvcRequestBuilders.post("/exercises/" + course1.getId()).with(csrf())
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(expected)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ExerciseDto actual = objectMapper.readValue(result, ExerciseDto.class);

        // Assert
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getExerciseDifficulty(), actual.getExerciseDifficulty());
    }

    @Test
    void updateExercise_validInput_returnsUpdatedExercise() throws Exception {
        // Arrange
        ExerciseDto expected = exerciseMapper.mapToDto(exercise);
        expected.setDescription("Updated description");
        expected.setName("Updated name");
        expected.setExerciseDifficulty(ExerciseDifficulty.EASY);

        // Act
        String result = mockMvc.perform(MockMvcRequestBuilders.put("/exercises/" + exercise.getId()).with(csrf())
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(expected)))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ExerciseDto actual = objectMapper.readValue(result, ExerciseDto.class);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void updateExercise_invalidInput_returnsNotFound() throws Exception {
        // Arrange
        ExerciseDto expected = exerciseMapper.mapToDto(exercise);
        expected.setDescription("Updated description");
        expected.setName("Updated name");
        expected.setExerciseDifficulty(ExerciseDifficulty.EASY);

        // Act
        mockMvc.perform(MockMvcRequestBuilders.put("/exercises/" + UUID.randomUUID()).with(csrf())
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(expected)))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteExercise_exerciseExists_returnsDeletedExercise() throws Exception {
        // Arrange
        ExerciseDto expected = exerciseMapper.mapToDto(exercise);

        // Act
        String result = mockMvc.perform(MockMvcRequestBuilders.delete("/exercises/" + exercise.getId()).with(csrf()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ExerciseDto actual = objectMapper.readValue(result, ExerciseDto.class);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void deleteExercise_exerciseDoesNotExist_returnsNotFound() throws Exception {
        // Act
        mockMvc.perform(MockMvcRequestBuilders.delete("/exercises/" + UUID.randomUUID()).with(csrf()))
                .andExpect(status().isNotFound());
    }
}
