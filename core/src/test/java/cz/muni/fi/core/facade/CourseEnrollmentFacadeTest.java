package cz.muni.fi.core.facade;

import cz.muni.fi.commons.dto.course.CourseEnrollmentDto;
import cz.muni.fi.commons.dto.user.student.StudentDto;
import cz.muni.fi.commons.enums.EnrollmentState;
import cz.muni.fi.core.data.mapper.CourseEnrollmentMapper;
import cz.muni.fi.core.data.model.CourseEnrollment;
import cz.muni.fi.core.factory.TestDataFactory;
import cz.muni.fi.core.service.CourseEnrollmentService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CourseEnrollmentFacadeTest {
    @Mock
    private CourseEnrollmentService courseEnrollmentService;
    @Mock
    private CourseEnrollmentMapper courseEnrollmentMapper;
    @InjectMocks
    private CourseEnrollmentFacade courseEnrollmentFacade;

    @Test
    void findById_validId_returnsCourseEnrollmentDto() {
        CourseEnrollmentDto expectedDto = TestDataFactory.getCourseEnrollmentDto();
        UUID id = expectedDto.getId();

        when(courseEnrollmentService.findById(id)).thenReturn(TestDataFactory.getCourseEnrollment());
        when(courseEnrollmentMapper.mapToDto(any())).thenReturn(expectedDto);

        CourseEnrollmentDto result = courseEnrollmentFacade.findById(id);

        assertEquals(expectedDto, result);
    }

    @Test
    void findByStudentId_validStudentId_returnsListOfCourseEnrollmentDto() {
        CourseEnrollment courseEnrollment = TestDataFactory.getCourseEnrollment();
        UUID studentId = courseEnrollment.getStudentId();
        List<CourseEnrollment> courseEnrollments = Collections.singletonList(TestDataFactory.getCourseEnrollment());

        StudentDto studentDto = TestDataFactory.createSampleStudentDto();
        studentDto.setId(studentId);
        CourseEnrollmentDto courseEnrollmentDto = TestDataFactory.getCourseEnrollmentDto();
        courseEnrollmentDto.setStudentDto(studentDto);
        List<CourseEnrollmentDto> expectedDtos = Collections.singletonList(courseEnrollmentDto);

        when(courseEnrollmentService.findByStudentId(studentId)).thenReturn(courseEnrollments);
        when(courseEnrollmentMapper.mapToDtoList(courseEnrollments)).thenReturn(expectedDtos);

        List<CourseEnrollmentDto> result = courseEnrollmentFacade.findByStudentId(studentId);

        assertEquals(expectedDtos, result);
    }

    @Test
    void create_validCourseEnrollmentDto_returnsCreatedCourseEnrollmentDto() {
        CourseEnrollmentDto inputDto = TestDataFactory.getCourseEnrollmentDto();
        CourseEnrollment inputEntity = TestDataFactory.getCourseEnrollment();

        when(courseEnrollmentMapper.mapToEntity(inputDto)).thenReturn(inputEntity);
        when(courseEnrollmentService.create(inputEntity)).thenReturn(inputEntity);
        when(courseEnrollmentMapper.mapToDto(inputEntity)).thenReturn(inputDto);

        CourseEnrollmentDto result = courseEnrollmentFacade.create(inputDto);

        assertEquals(inputDto, result);
    }

    @Test
    void delete_validId_returnsDeletedCourseEnrollmentDto() {
        CourseEnrollmentDto expectedDto = TestDataFactory.getCourseEnrollmentDto();
        UUID id = expectedDto.getId();

        when(courseEnrollmentService.delete(id)).thenReturn(TestDataFactory.getCourseEnrollment());
        when(courseEnrollmentMapper.mapToDto(any())).thenReturn(expectedDto);

        CourseEnrollmentDto result = courseEnrollmentFacade.delete(id);

        assertEquals(expectedDto, result);
    }


    @Test
    void findByCourseIdAndStudentId_validIds_returnsCourseEnrollmentDto() {
        UUID courseId = UUID.randomUUID();
        UUID studentId = UUID.randomUUID();
        CourseEnrollment courseEnrollment = TestDataFactory.getCourseEnrollment();
        CourseEnrollmentDto expectedDto = TestDataFactory.getCourseEnrollmentDto();

        when(courseEnrollmentService.findByCourseAndStudent(courseId, studentId)).thenReturn(courseEnrollment);
        when(courseEnrollmentMapper.mapToDto(courseEnrollment)).thenReturn(expectedDto);

        CourseEnrollmentDto result = courseEnrollmentFacade.findByCourseIdAndStudentId(courseId, studentId);

        assertEquals(expectedDto, result);
    }

    @Test
    void updateEnrollmentState_validIdAndState_returnsUpdatedCourseEnrollmentDto() {
        UUID id = UUID.randomUUID();
        EnrollmentState enrollmentState = EnrollmentState.PASSED;
        CourseEnrollment courseEnrollment = TestDataFactory.getCourseEnrollment();
        CourseEnrollmentDto expectedDto = TestDataFactory.getCourseEnrollmentDto();

        when(courseEnrollmentService.updateEnrollmentState(id, enrollmentState)).thenReturn(courseEnrollment);
        when(courseEnrollmentMapper.mapToDto(courseEnrollment)).thenReturn(expectedDto);

        CourseEnrollmentDto result = courseEnrollmentFacade.updateEnrollmentState(id, enrollmentState);

        assertEquals(expectedDto, result);
    }

}
