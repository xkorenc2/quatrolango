package cz.muni.fi.core.service;

import cz.muni.fi.commons.dto.user.student.StudentDto;
import cz.muni.fi.commons.enums.EnrollmentState;
import cz.muni.fi.commons.exceptions.ResourceNotFoundException;
import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.data.model.CourseEnrollment;
import cz.muni.fi.core.data.repository.CourseEnrollmentRepository;
import cz.muni.fi.core.factory.TestDataFactory;
import cz.muni.fi.core.service.api.StudentApiService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CourseEnrollmentServiceTest {
    @Mock
    private CourseEnrollmentRepository courseEnrollmentRepository;

    @InjectMocks
    private CourseEnrollmentService courseEnrollmentService;

    @Mock
    private StudentApiService studentApiService;

    @Test
    void findById_validId_returnsCourseEnrollment() {
        CourseEnrollment expectedCourseEnrollment = TestDataFactory.getCourseEnrollment();
        UUID id = expectedCourseEnrollment.getId();


        when(courseEnrollmentRepository.findById(id)).thenReturn(Optional.of(expectedCourseEnrollment));

        CourseEnrollment result = courseEnrollmentService.findById(id);

        assertEquals(expectedCourseEnrollment, result);
    }

    @Test
    void findByStudentId_validStudentId_returnsListOfCourseEnrollments() {
        CourseEnrollment courseEnrollment = TestDataFactory.getCourseEnrollment();
        UUID studentId = courseEnrollment.getStudentId();

        List<CourseEnrollment> expectedCourseEnrollments = Collections.singletonList(courseEnrollment);

        when(courseEnrollmentRepository.findByStudentId(studentId)).thenReturn(expectedCourseEnrollments);

        List<CourseEnrollment> result = courseEnrollmentService.findByStudentId(studentId);

        assertEquals(expectedCourseEnrollments, result);
    }

    @Test
    void create_validCourseEnrollment_returnsCreatedCourseEnrollment() {
        CourseEnrollment inputCourseEnrollment = TestDataFactory.getCourseEnrollment();


        when(courseEnrollmentRepository.save(inputCourseEnrollment)).thenReturn(inputCourseEnrollment);

        CourseEnrollment result = courseEnrollmentService.create(inputCourseEnrollment);

        assertEquals(inputCourseEnrollment, result);
    }

    @Test
    void delete_validId_returnsDeletedCourseEnrollment() {
        CourseEnrollment expectedCourseEnrollment = TestDataFactory.getCourseEnrollment();
        UUID id = expectedCourseEnrollment.getId();

        when(courseEnrollmentRepository.findById(id)).thenReturn(Optional.of(expectedCourseEnrollment));

        CourseEnrollment result = courseEnrollmentService.delete(id);

        assertEquals(expectedCourseEnrollment, result);
        verify(courseEnrollmentRepository).deleteById(id);
    }

    @Test
    void checkIfStudentIsEnrolled_validIds_returnsBoolean() {
        UUID courseId = UUID.randomUUID();
        UUID studentId = UUID.randomUUID();
        boolean expectedValue = true;

        when(courseEnrollmentRepository.checkIfStudentIsEnrolled(courseId, studentId)).thenReturn(expectedValue);

        boolean result = courseEnrollmentService.checkIfStudentIsEnrolled(courseId, studentId);

        assertEquals(expectedValue, result);
    }

    @Test
    void updateEnrollmentState_validIdAndState_courseEnrollmentUpdated() {
        CourseEnrollment existingEnrollment = TestDataFactory.getCourseEnrollment();
        UUID id = existingEnrollment.getId();
        EnrollmentState enrollmentState = EnrollmentState.PASSED;


        when(courseEnrollmentRepository.findById(id)).thenReturn(Optional.of(existingEnrollment));
        when(courseEnrollmentRepository.save(any(CourseEnrollment.class))).thenAnswer(invocation -> invocation.getArgument(0));

        CourseEnrollment updatedEnrollment = courseEnrollmentService.updateEnrollmentState(id, enrollmentState);

        assertEquals(id, updatedEnrollment.getId());
        assertEquals(enrollmentState, updatedEnrollment.getEnrollmentState());
        verify(courseEnrollmentRepository).findById(id);
        verify(courseEnrollmentRepository).save(existingEnrollment);
    }

    @Test
    void findByCourseAndStudent_whenCourseEnrollmentNotFound_shouldThrowResourceNotFoundException() {
        UUID courseId = UUID.randomUUID();
        UUID studentId = UUID.randomUUID();

        when(courseEnrollmentRepository.findByCourseIdAndStudentId(courseId, studentId))
                .thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () ->
                courseEnrollmentService.findByCourseAndStudent(courseId, studentId));
    }

    @Test
    void findStudentsByCourse_whenNoEnrollments_shouldReturnEmptySet() {
        Course course = TestDataFactory.createSampleCourse();

        when(courseEnrollmentRepository.findByCourse(course)).thenReturn(Collections.emptyList());

        Set<StudentDto> result = courseEnrollmentService.findStudentsByCourse(course);

        assertEquals(Collections.emptySet(), result);
    }

    @Test
    void findStudentsByCourse_whenEnrollmentsExist_shouldReturnStudents() {
        Course course = TestDataFactory.createSampleCourse();

        StudentDto studentDto1 = TestDataFactory.createSampleStudentDto();
        StudentDto studentDto2 = TestDataFactory.createSampleStudentDto();
        UUID studentId1 = studentDto1.getId();
        UUID studentId2 = studentDto2.getId();

        CourseEnrollment enrollment1 = TestDataFactory.getCourseEnrollment();
        enrollment1.setStudentId(studentId1);
        CourseEnrollment enrollment2 = TestDataFactory.getCourseEnrollment();
        enrollment2.setStudentId(studentId2);

        when(courseEnrollmentRepository.findByCourse(course))
                .thenReturn(List.of(enrollment1, enrollment2));
        
        Set<StudentDto> expectedStudents = new HashSet<>();
        expectedStudents.add(studentDto1);
        expectedStudents.add(studentDto2);

        when(studentApiService.findStudentsByIds(Set.of(studentId1, studentId2)))
                .thenReturn(expectedStudents);

        Set<StudentDto> result = courseEnrollmentService.findStudentsByCourse(course);

        assertEquals(expectedStudents, result);
    }
}
