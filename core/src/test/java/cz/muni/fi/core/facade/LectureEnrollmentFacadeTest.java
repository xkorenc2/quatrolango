package cz.muni.fi.core.facade;

import cz.muni.fi.commons.dto.lecture.LectureEnrollmentDto;
import cz.muni.fi.commons.dto.user.student.StudentDto;
import cz.muni.fi.core.data.mapper.LectureEnrollmentMapper;
import cz.muni.fi.core.data.model.LectureEnrollment;
import cz.muni.fi.core.factory.TestDataFactory;
import cz.muni.fi.core.service.LectureEnrollmentService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LectureEnrollmentFacadeTest {
    @Mock
    private LectureEnrollmentService lectureEnrollmentService;

    @Mock
    private LectureEnrollmentMapper lectureEnrollmentMapper;

    @InjectMocks
    private LectureEnrollmentFacade lectureEnrollmentFacade;

    @Test
    void findById_validId_returnsLectureEnrollmentDto() {
        LectureEnrollmentDto expectedDto = TestDataFactory.getLectureEnrollmentDto();
        UUID id = expectedDto.getId();

        when(lectureEnrollmentService.findById(id)).thenReturn(TestDataFactory.getLectureEnrollment());
        when(lectureEnrollmentMapper.mapToDto(any())).thenReturn(expectedDto);

        LectureEnrollmentDto result = lectureEnrollmentFacade.findById(id);

        assertEquals(expectedDto, result);
    }

    @Test
    void findByStudentId_validStudentId_returnsListOfLectureEnrollmentDto() {
        LectureEnrollment lectureEnrollment = TestDataFactory.getLectureEnrollment();
        UUID studentId = lectureEnrollment.getStudentId();
        List<LectureEnrollment> lectureEnrollments = Collections.singletonList(TestDataFactory.getLectureEnrollment());

        StudentDto studentDto = TestDataFactory.createSampleStudentDto();
        studentDto.setId(studentId);
        LectureEnrollmentDto lectureEnrollmentDto = TestDataFactory.getLectureEnrollmentDto();
        lectureEnrollmentDto.setStudentDto(studentDto);
        List<LectureEnrollmentDto> expectedDtos = Collections.singletonList(lectureEnrollmentDto);

        when(lectureEnrollmentService.findByStudentId(studentId)).thenReturn(lectureEnrollments);
        when(lectureEnrollmentMapper.mapToDtoList(lectureEnrollments)).thenReturn(expectedDtos);

        List<LectureEnrollmentDto> result = lectureEnrollmentFacade.findByStudentId(studentId);

        assertEquals(expectedDtos, result);
    }

    @Test
    void create_validLectureEnrollmentDto_returnsCreatedLectureEnrollmentDto() {
        LectureEnrollmentDto inputDto = TestDataFactory.getLectureEnrollmentDto();
        LectureEnrollment inputEntity = TestDataFactory.getLectureEnrollment();

        when(lectureEnrollmentMapper.mapToEntity(inputDto)).thenReturn(inputEntity);
        when(lectureEnrollmentService.create(inputEntity)).thenReturn(inputEntity);
        when(lectureEnrollmentMapper.mapToDto(inputEntity)).thenReturn(inputDto);

        LectureEnrollmentDto result = lectureEnrollmentFacade.create(inputDto);

        assertEquals(inputDto, result);
    }

    @Test
    void delete_validId_returnsDeletedLectureEnrollmentDto() {
        LectureEnrollmentDto expectedDto = TestDataFactory.getLectureEnrollmentDto();
        UUID id = expectedDto.getId();


        when(lectureEnrollmentService.delete(id)).thenReturn(TestDataFactory.getLectureEnrollment());
        when(lectureEnrollmentMapper.mapToDto(any())).thenReturn(expectedDto);

        LectureEnrollmentDto result = lectureEnrollmentFacade.delete(id);

        assertEquals(expectedDto, result);
    }

    @Test
    void checkIfStudentIsEnrolled_callsServiceMethodWithCorrectArguments() {
        UUID studentId = UUID.randomUUID();
        UUID lectureId = UUID.randomUUID();
        boolean expectedValue = true;

        when(lectureEnrollmentService.checkIfStudentIsEnrolled(studentId, lectureId)).thenReturn(expectedValue);

        boolean result = lectureEnrollmentFacade.checkIfStudentIsEnrolled(studentId, lectureId);

        verify(lectureEnrollmentService).checkIfStudentIsEnrolled(studentId, lectureId);

        assertEquals(expectedValue, result);
    }
}
