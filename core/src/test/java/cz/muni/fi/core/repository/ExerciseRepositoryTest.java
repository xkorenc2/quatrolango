package cz.muni.fi.core.repository;

import cz.muni.fi.commons.enums.ExerciseDifficulty;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.data.model.Exercise;
import cz.muni.fi.core.data.repository.ExerciseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
class ExerciseRepositoryTest {

    @Autowired
    private ExerciseRepository exerciseRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    private Course course;
    private Exercise exercise1;
    private Exercise exercise2;
    private Exercise exercise3;

    @BeforeEach
    void initData() {
        course = new Course();
        course.setName("Test Course");
        course.setProficiencyLevel(ProficiencyLevel.B1);
        course.setLanguage(Language.ENGLISH);
        course.setCapacity(60);
        testEntityManager.persistAndFlush(course);

        exercise1 = createExercise("Exercise 1", "Description 1", ExerciseDifficulty.EASY);
        exercise2 = createExercise("Exercise 2", "Description 2", ExerciseDifficulty.MEDIUM);
        exercise3 = createExercise("Exercise 3", "Description 3", ExerciseDifficulty.MEDIUM);
    }

    private Exercise createExercise(String name, String description, ExerciseDifficulty difficulty) {
        Exercise exercise = new Exercise();
        exercise.setName(name);
        exercise.setDescription(description);
        exercise.setExerciseDifficulty(difficulty);
        exercise.setCourse(course);
        return testEntityManager.persistFlushFind(exercise);
    }

    @Test
    @Transactional
    void findByExerciseDifficulty_exerciseExists_returnsExercise() {
        List<Exercise> exercises = exerciseRepository.findByExerciseDifficulty(ExerciseDifficulty.EASY);
        assertEquals(1, exercises.size());
        assertEquals(exercise1.getId(), exercises.get(0).getId());
    }

    @Test
    @Transactional
    void findByExerciseDifficulty_exerciseDoesNotExist_returnsNone() {
        List<Exercise> exercises = exerciseRepository.findByExerciseDifficulty(ExerciseDifficulty.HARD);
        assertEquals(0, exercises.size());
    }

    @Test
    @Transactional
    void findByIds_exercisesExist_returnsExercises() {
        Set<UUID> ids = new HashSet<>();
        ids.add(exercise1.getId());
        ids.add(exercise2.getId());
        List<Exercise> exercises = exerciseRepository.findByIds(ids);
        assertEquals(2, exercises.size());
        assertTrue(exercises.stream().anyMatch(exercise -> exercise.getId().equals(exercise1.getId())));
        assertTrue(exercises.stream().anyMatch(exercise -> exercise.getId().equals(exercise2.getId())));
    }

    @Test
    @Transactional
    void findByIds_exercisesDoNotExist_returnsNone() {
        Set<UUID> ids = new HashSet<>();
        ids.add(UUID.randomUUID());
        ids.add(UUID.randomUUID());
        List<Exercise> exercises = exerciseRepository.findByIds(ids);
        assertEquals(0, exercises.size());
    }
}