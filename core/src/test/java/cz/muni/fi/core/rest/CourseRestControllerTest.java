package cz.muni.fi.core.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.commons.dto.course.CourseDto;
import cz.muni.fi.commons.dto.course.CourseUpdateDto;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import cz.muni.fi.commons.exceptions.ResourceNotFoundException;
import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.facade.CourseFacade;
import cz.muni.fi.core.factory.TestDataFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@WebMvcTest(CourseRestController.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WithMockUser(username = "admin", authorities = {"SCOPE_test_1", "SCOPE_test_2", "SCOPE_test_3"})
public class CourseRestControllerTest {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    @MockBean
    private CourseFacade courseFacade;

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;

    @BeforeAll
    void setup() {
        objectMapper.findAndRegisterModules();
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    void findById_courseExists_isOk() throws Exception {
        Course course = TestDataFactory.createSampleCourse();
        CourseDto courseDto = TestDataFactory.createSampleCourseDto();
        Mockito.when(courseFacade.findById(course.getId())).thenReturn(courseDto);

        mockMvc.perform(MockMvcRequestBuilders.get("/courses/" + course.getId()).with(csrf())
                .contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void findById_courseDoesNotExist_isNotFound() throws Exception {
        Course course = TestDataFactory.createSampleCourse();
        Mockito.when(courseFacade.findById(course.getId())).thenThrow(new ResourceNotFoundException());

        mockMvc.perform(MockMvcRequestBuilders.get("/courses/" + course.getId()).with(csrf())
                .contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void findById_courseDoesNotExist_notFound() throws Exception {
        Course course = TestDataFactory.createSampleCourse();
        Mockito.when(courseFacade.findById(course.getId())).thenReturn(null);

        mockMvc.perform(MockMvcRequestBuilders.get("/courses/" + course.getId()).with(csrf())
                .contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void findByIdDetailed_courseExists_isOk() throws Exception {
        Course course = TestDataFactory.createSampleCourse();
        Mockito.when(courseFacade.findByIdDetailed(course.getId())).thenReturn(TestDataFactory.getCourseDetailedDtoFactory());

        mockMvc.perform(MockMvcRequestBuilders.get("/courses/detailed/" + course.getId()).with(csrf())
                .contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void findByIdDetailed_courseDoesNotExist_internalServerError() throws Exception {
        Course course = TestDataFactory.createSampleCourse();
        Mockito.when(courseFacade.findByIdDetailed(course.getId())).thenThrow(new ResourceNotFoundException());

        mockMvc.perform(MockMvcRequestBuilders.get("/courses/detailed/" + course.getId()).with(csrf())
                .contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void findAll_coursesExist_isOk() throws Exception {
        Pageable pageable = Pageable.unpaged();

        List<CourseDto> courseDtos = TestDataFactory.createSampleListOfCourseDtos();
        Mockito.when(courseFacade.findAll(pageable)).thenReturn(new PageImpl<>(courseDtos));

        mockMvc.perform(MockMvcRequestBuilders.get("/courses/").with(csrf())
                .contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void findByIds_coursesExist_isOk() throws Exception {
        List<CourseDto> courseDtos = TestDataFactory.createSampleListOfCourseDtos();
        Set<UUID> ids = Set.of(UUID.randomUUID(), UUID.randomUUID()); // Example IDs
        Mockito.when(courseFacade.findByIds(ids)).thenReturn(courseDtos);

        mockMvc.perform(MockMvcRequestBuilders.post("/courses/ids").with(csrf())
                        .content(objectMapper.writeValueAsBytes(ids))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void findByProficiency_courseExists_isOk() throws Exception {
        List<CourseDto> courseDtos = TestDataFactory.createSampleListOfCourseDtos();
        Mockito.when(courseFacade.findByProficiencyLevel(ProficiencyLevel.A1)).thenReturn(courseDtos);

        mockMvc.perform(MockMvcRequestBuilders.get("/courses/levels/A1").with(csrf())
                .contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void findByLanguage_courseExists_isOk() throws Exception {
        List<CourseDto> courseDtos = TestDataFactory.createSampleListOfCourseDtos();
        Mockito.when(courseFacade.findByLanguage(Language.ENGLISH)).thenReturn(courseDtos);

        mockMvc.perform(MockMvcRequestBuilders.get("/courses/languages/ENGLISH").with(csrf())
                .contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void createCourse_courseDoesNotExist_isOk() throws Exception {
        CourseDto courseDto = TestDataFactory.createSampleCourseDto();
        Mockito.when(courseFacade.create(courseDto)).thenReturn(courseDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/courses/").with(csrf()).content(objectMapper.writeValueAsBytes(courseDto))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    void createCourse_facadeIssue_isInternalServerError() throws Exception {
        CourseDto courseDto = TestDataFactory.createSampleCourseDto();
        Mockito.when(courseFacade.create(courseDto)).thenThrow(new RuntimeException());

        mockMvc.perform(MockMvcRequestBuilders.post("/courses/").with(csrf()).content(objectMapper.writeValueAsBytes(courseDto))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }

    @Test
    void createCourse_courseBadBody_badRequest() throws Exception {
        CourseDto badCourse = TestDataFactory.createSampleCourseDto();
        badCourse.setName("");

        mockMvc.perform(MockMvcRequestBuilders.post("/courses/").with(csrf()).content(objectMapper.writeValueAsBytes(badCourse))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void updateCourse_courseDoesNotExist_isOk() throws Exception {
        CourseUpdateDto courseUpdateDto = TestDataFactory.createSampleCourseUpdateDto();
        CourseDto courseDto = TestDataFactory.createSampleCourseDto();
        UUID id = UUID.randomUUID();
        Course course = TestDataFactory.createSampleCourse();
        Mockito.when(courseFacade.update(id, courseUpdateDto)).thenReturn(courseDto);

        mockMvc.perform(MockMvcRequestBuilders.put("/courses/" + course.getId()).with(csrf()).content(objectMapper.writeValueAsBytes(courseDto)).content(objectMapper.writeValueAsBytes(courseDto))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void updateCourse_facadeIssue_internalServerError() throws Exception {
        CourseUpdateDto courseDto = TestDataFactory.createSampleCourseUpdateDto();
        Course course = TestDataFactory.createSampleCourse();
        Mockito.when(courseFacade.update(course.getId(), courseDto)).thenThrow(new RuntimeException());

        mockMvc.perform(MockMvcRequestBuilders.put("/courses/" + course.getId()).with(csrf()).content(objectMapper.writeValueAsBytes(courseDto))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }

    @Test
    void deleteCourse_courseDoesExist_isOk() throws Exception {
        CourseDto courseDto = TestDataFactory.createSampleCourseDto();
        Mockito.when(courseFacade.delete(courseDto.getId())).thenReturn(courseDto);

        mockMvc.perform(MockMvcRequestBuilders.delete("/courses/" + courseDto.getId()).with(csrf()).content(objectMapper.writeValueAsBytes(courseDto))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void deleteCourse_facadeIssue_internalServerError() throws Exception {
        CourseDto courseDto = TestDataFactory.createSampleCourseDto();
        Mockito.when(courseFacade.delete(courseDto.getId())).thenThrow(new RuntimeException());

        mockMvc.perform(MockMvcRequestBuilders.delete("/courses/" + courseDto.getId()).with(csrf()).content(objectMapper.writeValueAsBytes(courseDto))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }
}
