package cz.muni.fi.core.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.commons.dto.exercise.ExerciseDto;
import cz.muni.fi.commons.enums.ExerciseDifficulty;
import cz.muni.fi.core.facade.ExerciseFacade;
import cz.muni.fi.core.factory.TestDataFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ExerciseRestController.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WithMockUser(username = "admin", authorities = {"SCOPE_test_1", "SCOPE_test_2", "SCOPE_test_3"})
class ExerciseRestControllerTest {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    @MockBean
    private ExerciseFacade exerciseFacade;

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;

    @BeforeAll
    void setup() {
        objectMapper.findAndRegisterModules();
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    void findById_exerciseFound_returnsExercise() throws Exception {
        ExerciseDto exerciseDto = TestDataFactory.createSampleExerciseDto();
        UUID exerciseDtoId = exerciseDto.getId();

        when(exerciseFacade.findById(exerciseDtoId)).thenReturn(exerciseDto);
        mockMvc.perform(get("/exercises/", exerciseDtoId).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void findById_shouldReturnNotFound_whenInvalidUuid() throws Exception {
        UUID uuid = UUID.randomUUID();
        when(exerciseFacade.findById(uuid)).thenReturn(null);

        mockMvc.perform(get("/exercises/{uuid}", uuid).with(csrf()))
                .andExpect(status().isNotFound());
    }

    @Test
    void findAll_exercisesFound_returnsListOfExerciseDto() throws Exception {

        ExerciseDto exerciseDto = TestDataFactory.createSampleExerciseDto();
        List<ExerciseDto> exerciseDtoList = List.of(exerciseDto);
        when(exerciseFacade.findAll(any(Pageable.class))).thenReturn(new PageImpl<>(exerciseDtoList));

        mockMvc.perform(get("/exercises/").with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void findExercisesByIds_exercisesFound_returnsListOfExerciseDto() throws Exception {
        Set<UUID> ids = new HashSet<>();
        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();
        ids.add(id1);
        ids.add(id2);
        List<ExerciseDto> exerciseDtos = new ArrayList<>();
        ExerciseDto exerciseDto = TestDataFactory.createSampleExerciseDto();
        exerciseDto.setId(id1);
        ExerciseDto exerciseDto2 = TestDataFactory.createSampleExerciseDto();
        exerciseDto2.setId(id2);

        exerciseDtos.add(exerciseDto);
        exerciseDtos.add(exerciseDto2);

        when(exerciseFacade.findByIds(ids)).thenReturn(exerciseDtos);

        mockMvc.perform(post("/exercises/ids").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(ids)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void findByExerciseDifficulty_exercisesFound_returnListOfExerciseDto() throws Exception {
        ExerciseDifficulty exerciseDifficulty = ExerciseDifficulty.EASY;
        ExerciseDto exerciseDto = TestDataFactory.createSampleExerciseDto();
        List<ExerciseDto> exerciseDtoSet = List.of(exerciseDto);
        when(exerciseFacade.findByExerciseDifficulty(exerciseDifficulty)).thenReturn(exerciseDtoSet);

        mockMvc.perform(get("/exercises/difficulty/{exerciseDifficulty}", exerciseDifficulty).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].id").exists());
    }

    @Test
    void pick_exercisePicked_returnsExerciseDto() throws Exception {
        UUID studentUuid = UUID.randomUUID();
        UUID exerciseUuid = UUID.randomUUID();
        ExerciseDto exerciseDto = TestDataFactory.createSampleExerciseDto();
        when(exerciseFacade.getExerciseForStudent(studentUuid, exerciseUuid)).thenReturn(exerciseDto);

        mockMvc.perform(post("/exercises/{studentUuid}/{exerciseUuid}", studentUuid, exerciseUuid).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    void create_createdSuccessfully_returnsExerciseDto() throws Exception {
        UUID courseUuid = UUID.randomUUID();
        ExerciseDto exerciseDto = TestDataFactory.createSampleExerciseDto();
        when(exerciseFacade.create(exerciseDto, courseUuid)).thenReturn(exerciseDto);

        mockMvc.perform(post("/exercises/{courseUuid}", courseUuid).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(exerciseDto)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    void delete_deletedSuccessfully_returnExerciseDto() throws Exception {
        UUID uuid = UUID.randomUUID();
        ExerciseDto exerciseDto = TestDataFactory.createSampleExerciseDto();
        when(exerciseFacade.delete(uuid)).thenReturn(exerciseDto);

        mockMvc.perform(delete("/exercises/{uuid}", uuid).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    void update_updatedSuccessfully_returnsExerciseDto() throws Exception {
        UUID uuid = UUID.randomUUID();
        ExerciseDto exerciseDto = TestDataFactory.createSampleExerciseDto();
        when(exerciseFacade.update(uuid, exerciseDto)).thenReturn(exerciseDto);

        mockMvc.perform(put("/exercises/{uuid}", uuid).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(exerciseDto)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    void delete_nonExistingExercise_returnsNotFound() throws Exception {
        UUID nonExistingId = UUID.randomUUID();

        when(exerciseFacade.delete(nonExistingId)).thenReturn(null);

        mockMvc.perform(MockMvcRequestBuilders.delete("/exercises/{uuid}", nonExistingId).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void createExercise_exceptionOccurs_returnsBadRequest() throws Exception {
        ExerciseDto exerciseDto = TestDataFactory.createSampleExerciseDto();

        UUID courseUuid = UUID.randomUUID();

        doThrow(RuntimeException.class).when(exerciseFacade).create(exerciseDto, courseUuid);

        mockMvc.perform(MockMvcRequestBuilders.post("/exercises/{courseUuid}", courseUuid).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(exerciseDto)))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }

    @Test
    void updateExercise_exceptionOccurs_returnsBadRequest() throws Exception {
        ExerciseDto exerciseDto = TestDataFactory.createSampleExerciseDto();

        doThrow(RuntimeException.class).when(exerciseFacade).update(exerciseDto.getId(), exerciseDto);

        mockMvc.perform(MockMvcRequestBuilders.put("/exercises/{uuid}", exerciseDto.getId()).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(exerciseDto)))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }

    @Test
    void pick_exceptionOccurs_returnsBadRequest() throws Exception {
        ExerciseDto exerciseDto = TestDataFactory.createSampleExerciseDto();
        UUID studentUuid = UUID.randomUUID();
        UUID exerciseUuid = UUID.randomUUID();

        doThrow(RuntimeException.class).when(exerciseFacade).getExerciseForStudent(studentUuid, exerciseUuid);

        mockMvc.perform(MockMvcRequestBuilders.post("/exercises/{studentUuid}/{exerciseUuid}", studentUuid, exerciseUuid).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(exerciseDto)))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }


}
