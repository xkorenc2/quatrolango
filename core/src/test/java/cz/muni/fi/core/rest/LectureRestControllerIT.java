package cz.muni.fi.core.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.commons.dto.lecture.LectureDto;
import cz.muni.fi.commons.dto.user.lecturer.LecturerDto;
import cz.muni.fi.commons.dto.user.lecturer.LecturerLanguageDto;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import cz.muni.fi.core.data.mapper.CourseMapper;
import cz.muni.fi.core.data.mapper.LectureMapper;
import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.data.model.Lecture;
import cz.muni.fi.core.data.model.LectureEnrollment;
import cz.muni.fi.core.data.repository.CourseRepository;
import cz.muni.fi.core.data.repository.LectureEnrollmentRepository;
import cz.muni.fi.core.data.repository.LectureRepository;
import cz.muni.fi.core.service.api.LecturerApiService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WithMockUser(username = "admin", authorities = {"SCOPE_test_1", "SCOPE_test_2", "SCOPE_test_3"})
class LectureRestControllerIT {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private LectureRepository lectureRepository;

    @Autowired
    private CourseRepository courseRepository;

    @MockBean
    private LecturerApiService lecturerApiService;

    @Autowired
    private LectureEnrollmentRepository lectureEnrollmentRepository;

    @Autowired
    private CourseMapper courseMapper;

    @Autowired
    private LectureMapper lectureMapper;

    private Course course;
    private Lecture lecture;
    private LecturerDto lecturer;

    private LectureEnrollment lectureEnrollment;

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;

    @BeforeAll
    void setup() {
        objectMapper.findAndRegisterModules();
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        // Create a course
        Course c = new Course();
        c.setName("Test course");
        c.setLanguage(Language.ENGLISH);
        c.setProficiencyLevel(ProficiencyLevel.B1);
        c.setCapacity(1000);
        course = courseRepository.save(c);
    }

    @BeforeEach
    void setupBeforeEach() {
        // Clear the lecture repository
        lectureRepository.deleteAll();

        lecturer = new LecturerDto();
        LecturerLanguageDto language = new LecturerLanguageDto();
        language.setLanguage(Language.SPANISH);


        lecturer.setLanguages(List.of(language));
        lecturer.setDateOfBirth(LocalDate.now());
        lecturer.setFirstName("john");
        lecturer.setSecondName("doe");
        lecturer.setEmail("johndoe@gmail.com");
        lecturer.setId(UUID.randomUUID());

        // Create a lecture
        Lecture l = new Lecture();
        l.setCapacity(100);
        l.setCourse(course);
        l.setDescription("Test lecture");
        l.setLecturerId(lecturer.getId());
        l.setStartTime(java.time.LocalDateTime.now());
        l.setEndTime(java.time.LocalDateTime.now().plusHours(1));
        l.setName("Test lecture");

        lecture = lectureRepository.save(l);

    }

    @Test
    void findById_lectureExists_returnsLecture() throws Exception {
        // Arrange
        LectureDto expected = lectureMapper.toDto(lecture);

        // Act
        String response = mockMvc.perform(get("/lectures/" + expected.getId()).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        LectureDto actual = objectMapper.readValue(response, LectureDto.class);

        // Assert
        assertEquals(expected, actual);

    }

    @Test
    void findById_lectureDoesNotExist_returnsNotFound() throws Exception {
        // Arrange
        UUID id = UUID.randomUUID();

        // Act
        mockMvc.perform(get("/lectures/" + id).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void findByIds_lecturesExist_returnsLectures() throws Exception {
        // Arrange
        LectureDto expected = lectureMapper.toDto(lecture);

        // Act
        String response = mockMvc.perform(post("/lectures/ids").with(csrf())
                        .content(objectMapper.writeValueAsBytes(new UUID[]{expected.getId()}))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        LectureDto[] actual = objectMapper.readValue(response, LectureDto[].class);

        // Assert
        assertEquals(expected, actual[0]);
    }

    @Test
    void findByCourse_lecturesExist_returnsLectures() throws Exception {
        // Arrange
        LectureDto expected = lectureMapper.toDto(lecture);

        // Act
        String response = mockMvc.perform(get("/lectures/course/" + course.getId()).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        LectureDto[] actual = objectMapper.readValue(response, LectureDto[].class);

        // Assert
        assertEquals(expected, actual[0]);

    }

    @Test
    void findByCourse_courseDoesNotExist_returnsNotFound() throws Exception {
        // Arrange
        UUID id = UUID.randomUUID();

        // Act
        mockMvc.perform(get("/lectures/course/" + id).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void findByLecturer_lecturesExist_returnsLectures() throws Exception {
        // Arrange
        LectureDto expected = lectureMapper.toDto(lecture);

        when(lecturerApiService.findLecturerById(lecturer.getId())).thenReturn(Optional.of(lecturer));

        // Act
        String response = mockMvc.perform(get("/lectures/lecturer/" + lecturer.getId()).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        LectureDto[] actual = objectMapper.readValue(response, LectureDto[].class);

        // Assert
        assertEquals(expected, actual[0]);
    }

    @Test
    void findByLecturer_lecturerDoesNotExist_returnsNotFound() throws Exception {
        // Arrange
        UUID id = UUID.randomUUID();

        // Act
        mockMvc.perform(get("/lectures/lecturer/" + id).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void findAll_lecturesDoNotExist_returnsEmptyList() throws Exception {
        lectureRepository.deleteAll();
        mockMvc.perform(get("/lectures/").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content").isEmpty());
    }

    @Test
    void create_lectureIsValid_returnsCreated() throws Exception {
        // Arrange
        LectureDto lectureDto = lectureMapper.toDto(lecture);
        lectureDto.setId(null);
        lectureDto.setCourseId(course.getId());

        when(lecturerApiService.findLecturerById(lecturer.getId())).thenReturn(Optional.of(lecturer));


        // Act
        String response = mockMvc.perform(post("/lectures/" + course.getId()).with(csrf())
                        .content(objectMapper.writeValueAsBytes(lectureDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString();

        LectureDto actual = objectMapper.readValue(response, LectureDto.class);
        lectureDto.setId(actual.getId());

        // Assert
        assertEquals(lectureDto, actual);
    }

    @Test
    void create_lectureIsInvalid_returnsBadRequest() throws Exception {
        // Arrange
        LectureDto lectureDto = lectureMapper.toDto(lecture);
        lectureDto.setId(null);
        lectureDto.setCourseId(course.getId());
        lectureDto.setName(null);

        // Act
        mockMvc.perform(post("/lectures/" + course.getId()).with(csrf())
                        .content(objectMapper.writeValueAsBytes(lectureDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void update_lectureIsValid_returnsOk() throws Exception {
        // Arrange
        LectureDto lectureDto = lectureMapper.toDto(lecture);
        lectureDto.setId(lecture.getId());
        lectureDto.setCourseId(lecture.getCourse().getId());
        lectureDto.setName("Updated name");
        lectureDto.setEndTime(lectureDto.getEndTime().plusHours(4));

        when(lecturerApiService.findLecturerById(lecturer.getId())).thenReturn(Optional.of(lecturer));

        // Act
        String response = mockMvc.perform(put("/lectures/" + lecture.getId()).with(csrf())
                        .content(objectMapper.writeValueAsBytes(lectureDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        LectureDto actual = objectMapper.readValue(response, LectureDto.class);

        // Assert
        assertEquals(lectureDto, actual);
    }

    @Test
    void update_lectureIsInvalid_returnsBadRequest() throws Exception {
        // Arrange
        LectureDto lectureDto = lectureMapper.toDto(lecture);
        lectureDto.setCourseId(lecture.getCourse().getId());
        lectureDto.setLecturerId(null);

        // Act
        mockMvc.perform(put("/lectures/" + lectureDto.getId()).with(csrf())
                        .content(objectMapper.writeValueAsBytes(lectureDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void delete_lectureExists_returnsOk() throws Exception {
        // Arrange
        LectureDto lectureDto = lectureMapper.toDto(lecture);

        // Act
        String response = mockMvc.perform(delete("/lectures/" + lectureDto.getId()).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        LectureDto actual = objectMapper.readValue(response, LectureDto.class);

        // Assert
        assertEquals(lectureDto, actual);
        assertEquals(0, lectureRepository.count());
    }

    @Test
    void delete_lectureDoesNotExist_returnsNotFound() throws Exception {
        // Arrange
        UUID id = UUID.randomUUID();

        // Act
        mockMvc.perform(delete("/lectures/" + id).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}

