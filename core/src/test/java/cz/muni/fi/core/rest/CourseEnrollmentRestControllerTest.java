package cz.muni.fi.core.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.commons.dto.course.CourseEnrollmentDto;
import cz.muni.fi.commons.enums.EnrollmentState;
import cz.muni.fi.core.facade.CourseEnrollmentFacade;
import cz.muni.fi.core.factory.TestDataFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CourseEnrollmentRestController.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WithMockUser(username = "admin", authorities = {"SCOPE_test_1", "SCOPE_test_2", "SCOPE_test_3"})
class CourseEnrollmentRestControllerTest {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    @MockBean
    private CourseEnrollmentFacade courseEnrollmentFacade;
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;

    @BeforeAll
    void setup() {
        objectMapper.findAndRegisterModules();
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    void findById_ValidId_ReturnsCourseEnrollmentDto() throws Exception {
        CourseEnrollmentDto expectedDto = TestDataFactory.getCourseEnrollmentDto();
        UUID id = expectedDto.getId();

        when(courseEnrollmentFacade.findById(id)).thenReturn(expectedDto);

        mockMvc.perform(get("/course-enrollments/{id}", id).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(id.toString()));
    }

    @Test
    void findById_InvalidId_ReturnsNotFound() throws Exception {
        UUID id = UUID.randomUUID();

        when(courseEnrollmentFacade.findById(id)).thenReturn(null);

        mockMvc.perform(get("/course-enrollments/{id}", id).with(csrf()))
                .andExpect(status().isNotFound());
    }

    @Test
    void findByStudentId_ValidStudentId_ReturnsListOfCourseEnrollmentDto() throws Exception {
        CourseEnrollmentDto courseEnrollmentDto = TestDataFactory.getCourseEnrollmentDto();
        UUID studentId = courseEnrollmentDto.getStudentDto().getId();
        List<CourseEnrollmentDto> expectedList = List.of(courseEnrollmentDto);

        when(courseEnrollmentFacade.findByStudentId(studentId)).thenReturn(expectedList);

        mockMvc.perform(get("/course-enrollments/students/{studentId}", studentId).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").exists());
    }

    @Test
    void create_ValidCourseEnrollmentDto_ReturnsCreatedCourseEnrollmentDto() throws Exception {
        CourseEnrollmentDto inputDto = TestDataFactory.getCourseEnrollmentDto();
        CourseEnrollmentDto expectedDto = TestDataFactory.getCourseEnrollmentDto();

        when(courseEnrollmentFacade.create(inputDto)).thenReturn(expectedDto);

        mockMvc.perform(post("/course-enrollments/").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(inputDto)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(expectedDto.getId().toString()));
    }

    @Test
    void delete_ValidId_ReturnsDeletedCourseEnrollmentDto() throws Exception {
        CourseEnrollmentDto expectedDto = TestDataFactory.getCourseEnrollmentDto();
        UUID id = expectedDto.getId();

        when(courseEnrollmentFacade.delete(id)).thenReturn(expectedDto);

        mockMvc.perform(delete("/course-enrollments/{id}", id).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(expectedDto.getId().toString()));
    }

    @Test
    void findByCourseIdAndStudentId_validIds_returnsCourseEnrollmentDto() throws Exception {
        CourseEnrollmentDto expectedDto = TestDataFactory.getCourseEnrollmentDto();
        UUID courseId = UUID.randomUUID();
        UUID studentId = UUID.randomUUID();
        expectedDto.getCourseDto().setId(courseId);
        expectedDto.getStudentDto().setId(studentId);

        when(courseEnrollmentFacade.findByCourseIdAndStudentId(courseId, studentId)).thenReturn(expectedDto);

        mockMvc.perform(get("/course-enrollments/courses/" + courseId + "/students/" + studentId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void updateEnrollmentState_validIdAndState_returnsUpdatedCourseEnrollmentDto() throws Exception {
        CourseEnrollmentDto expectedDto = TestDataFactory.getCourseEnrollmentDto();
        UUID id = expectedDto.getId();
        EnrollmentState enrollmentState = EnrollmentState.PASSED;

        when(courseEnrollmentFacade.updateEnrollmentState(id, enrollmentState)).thenReturn(expectedDto);

        mockMvc.perform(patch("/course-enrollments/" + id).with(csrf())
                        .param("enrollmentState", enrollmentState.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }
}
