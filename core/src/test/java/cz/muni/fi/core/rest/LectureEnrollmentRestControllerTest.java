package cz.muni.fi.core.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.commons.dto.lecture.LectureEnrollmentDto;
import cz.muni.fi.core.facade.LectureEnrollmentFacade;
import cz.muni.fi.core.factory.TestDataFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(LectureEnrollmentRestController.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WithMockUser(username = "admin", authorities = {"SCOPE_test_1", "SCOPE_test_2", "SCOPE_test_3"})
class LectureEnrollmentRestControllerTest {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    @MockBean
    private LectureEnrollmentFacade lectureEnrollmentFacade;
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;

    @BeforeAll
    void setup() {
        objectMapper.findAndRegisterModules();
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    void findById_ValidId_ReturnsLectureEnrollmentDto() throws Exception {
        LectureEnrollmentDto expectedDto = TestDataFactory.getLectureEnrollmentDto();
        UUID id = expectedDto.getId();

        when(lectureEnrollmentFacade.findById(id)).thenReturn(expectedDto);

        mockMvc.perform(get("/lecture-enrollments/{id}", id).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(id.toString()));
    }

    @Test
    void findById_InvalidId_ReturnsNotFound() throws Exception {
        UUID id = UUID.randomUUID();

        when(lectureEnrollmentFacade.findById(id)).thenReturn(null);

        mockMvc.perform(get("/lecture-enrollments/{id}", id).with(csrf()))
                .andExpect(status().isNotFound());
    }

    @Test
    void findByStudentId_ValidStudentId_ReturnsListOfLectureEnrollmentDto() throws Exception {
        LectureEnrollmentDto lectureEnrollmentDto = TestDataFactory.getLectureEnrollmentDto();
        UUID studentId = lectureEnrollmentDto.getStudentDto().getId();
        List<LectureEnrollmentDto> expectedList = List.of(lectureEnrollmentDto);

        when(lectureEnrollmentFacade.findByStudentId(studentId)).thenReturn(expectedList);

        mockMvc.perform(get("/lecture-enrollments/students/{studentId}", studentId).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").exists());
    }

    @Test
    void create_ValidLectureEnrollmentDto_ReturnsCreatedLectureEnrollmentDto() throws Exception {
        LectureEnrollmentDto inputDto = TestDataFactory.getLectureEnrollmentDto();
        LectureEnrollmentDto expectedDto = TestDataFactory.getLectureEnrollmentDto();

        when(lectureEnrollmentFacade.create(inputDto)).thenReturn(expectedDto);

        mockMvc.perform(post("/lecture-enrollments/").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(inputDto)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(expectedDto.getId().toString()));
    }

    @Test
    void delete_ValidId_ReturnsDeletedLectureEnrollmentDto() throws Exception {
        LectureEnrollmentDto expectedDto = TestDataFactory.getLectureEnrollmentDto();
        UUID id = expectedDto.getId();

        when(lectureEnrollmentFacade.delete(id)).thenReturn(expectedDto);

        mockMvc.perform(delete("/lecture-enrollments/{id}", id).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(expectedDto.getId().toString()));
    }

    @Test
    void checkIfStudentIsEnrolled_ValidStudentAndLectureId_ReturnsTrue() throws Exception {
        UUID studentId = UUID.randomUUID();
        UUID lectureId = UUID.randomUUID();

        when(lectureEnrollmentFacade.checkIfStudentIsEnrolled(studentId, lectureId)).thenReturn(true);

        mockMvc.perform(get("/lecture-enrollments/students/{studentId}/lectures/{lectureId}", studentId, lectureId).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("true"));
    }
}
