package cz.muni.fi.core.factory;

import cz.muni.fi.commons.dto.course.CourseEnrollmentDto;
import cz.muni.fi.commons.dto.exercise.ExerciseDto;
import cz.muni.fi.commons.dto.lecture.LectureDto;
import cz.muni.fi.commons.dto.lecture.LectureEnrollmentDto;
import cz.muni.fi.commons.dto.user.lecturer.LecturerDto;
import cz.muni.fi.commons.dto.user.lecturer.LecturerLanguageDto;
import cz.muni.fi.commons.dto.course.CourseDetailedDto;
import cz.muni.fi.commons.dto.course.CourseDto;
import cz.muni.fi.commons.dto.course.CourseUpdateDto;
import cz.muni.fi.commons.dto.user.student.StudentDetailedDto;
import cz.muni.fi.commons.dto.user.student.StudentDto;
import cz.muni.fi.commons.enums.EnrollmentState;
import cz.muni.fi.commons.enums.ExerciseDifficulty;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.data.model.CourseEnrollment;
import cz.muni.fi.core.data.model.Exercise;
import cz.muni.fi.core.data.model.Lecture;
import cz.muni.fi.core.data.model.LectureEnrollment;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class TestDataFactory {

    private static final UUID USER_UUID = UUID.randomUUID();

    private TestDataFactory() {
    }

    public static UUID getExistingUUID() {
        return UUID.fromString("aed7bf53-2047-4090-9b54-321c5527862a");
    }

    public static Course createSampleCourse() {
        Course course = getCourseWithEmptySetsObjectFactory();

        var lecture = new Lecture();
        lecture.setId(getExistingUUID());
        var exercise = new Exercise();
        exercise.setId(getExistingUUID());

        course.setLectures(new ArrayList<>(List.of(lecture)));
        course.setExercises(new ArrayList<>(List.of(exercise)));

        return course;
    }

    public static Course getCourseWithEmptySetsObjectFactory() {
        Course course = new Course();
        course.setId(USER_UUID);
        course.setName("English course");
        course.setProficiencyLevel(ProficiencyLevel.A1);
        course.setCapacity(20);
        course.setLanguage(Language.ENGLISH);
        course.setExercises(new ArrayList<>());
        course.setLectures(new ArrayList<>());
        return course;
    }

    public static CourseDto createSampleCourseDto() {
        CourseDto courseDto = new CourseDto();
        courseDto.setId(USER_UUID);
        courseDto.setName("English course");
        courseDto.setProficiencyLevel(ProficiencyLevel.A1);
        courseDto.setCapacity(20);
        courseDto.setLanguage(Language.ENGLISH);

        return courseDto;
    }

    public static CourseUpdateDto createSampleCourseUpdateDto() {
        CourseUpdateDto courseDto = new CourseUpdateDto();
        courseDto.setName("English course");
        courseDto.setProficiencyLevel(ProficiencyLevel.A1);
        courseDto.setCapacity(20);
        courseDto.setLanguage(Language.ENGLISH);

        return courseDto;
    }

    public static CourseDetailedDto getCourseDetailedDtoFactory() {
        CourseDetailedDto courseDto = new CourseDetailedDto();
        courseDto.setId(USER_UUID);
        courseDto.setName("English course");
        courseDto.setProficiencyLevel(ProficiencyLevel.A1);
        courseDto.setCapacity(20);
        courseDto.setLanguage(Language.ENGLISH);

        return courseDto;
    }

    public static List<CourseDto> createSampleListOfCourseDtos() {
        List<CourseDto> courseDtos = new ArrayList<>();
        CourseDto courseDto = new CourseDto();
        courseDto.setId(UUID.randomUUID());
        courseDto.setName("English course");
        courseDto.setProficiencyLevel(ProficiencyLevel.A1);
        courseDto.setCapacity(20);
        courseDto.setLanguage(Language.ENGLISH);


        CourseDto courseDto2 = new CourseDto();
        courseDto2.setId(UUID.randomUUID());
        courseDto2.setName("German course");
        courseDto2.setProficiencyLevel(ProficiencyLevel.B1);
        courseDto2.setCapacity(40);
        courseDto2.setLanguage(Language.GERMAN);

        courseDtos.add(courseDto);
        courseDtos.add(courseDto2);

        return courseDtos;
    }

    public static List<Course> createSampleListOfCourses() {
        List<Course> courses = new ArrayList<>();
        Course course = new Course();
        course.setId(UUID.randomUUID());
        course.setName("English course");
        course.setProficiencyLevel(ProficiencyLevel.A1);
        course.setCapacity(20);
        course.setLanguage(Language.ENGLISH);


        Course course2 = new Course();
        course2.setId(UUID.randomUUID());
        course2.setName("German course");
        course2.setProficiencyLevel(ProficiencyLevel.B1);
        course2.setCapacity(40);
        course2.setLanguage(Language.GERMAN);

        courses.add(course);
        courses.add(course2);

        return courses;
    }


    public static StudentDto createSampleStudentDto() {
        StudentDto student = new StudentDto();
        student.setId(UUID.randomUUID());
        student.setFirstName("John");
        student.setSecondName("Doe");
        student.setEmail("test@gmail.com");
        student.setDateOfBirth(LocalDate.of(2000, 1, 1));
        return student;
    }

    public static LectureDto createSampleLectureDto() {
        LectureDto lectureDto = new LectureDto();
        lectureDto.setName("Sample Lecture");
        lectureDto.setDescription("This is a sample lecture description.");
        lectureDto.setStartTime(LocalDateTime.now());
        lectureDto.setEndTime(LocalDateTime.now().plusHours(1));
        lectureDto.setCourseId(UUID.randomUUID());
        lectureDto.setLecturerId(UUID.randomUUID());
        lectureDto.setCapacity(50);
        return lectureDto;
    }

    public static Lecture createSampleLecture() {
        Lecture lecture = new Lecture();
        lecture.setId(UUID.randomUUID());
        lecture.setName("Sample Lecture");
        lecture.setDescription("This is a sample lecture description.");
        lecture.setStartTime(LocalDateTime.now());
        lecture.setEndTime(LocalDateTime.now().plusHours(1));
        lecture.setCourse(new Course());
        lecture.setLecturerId(UUID.randomUUID());
        lecture.setCapacity(50);
        return lecture;
    }

    public static LecturerDto createSampleLecturer() {
        LecturerDto lecturer = new LecturerDto();
        lecturer.setId(UUID.randomUUID());
        lecturer.setFirstName("John");
        lecturer.setSecondName("Doe");
        lecturer.setEmail("test@gmail.com");
        lecturer.setDateOfBirth(LocalDate.of(2000, 1, 1));
        LecturerLanguageDto language = new LecturerLanguageDto();
        language.setLanguage(Language.SPANISH);
        lecturer.setLanguages(List.of(language));
        return lecturer;
    }

    public static List<Lecture> createSampleLectureList() {
        return List.of(createSampleLecture(), createSampleLecture());
    }

    public static StudentDetailedDto createSampleStudentDetailedDto() {
        StudentDetailedDto student = new StudentDetailedDto();
        student.setFirstName("John");
        student.setSecondName("Doe");
        Set<CourseDto> courses = Set.of(createSampleCourseDto());
        List<LectureDto> lecturesList = List.of(createSampleLectureDto(), createSampleLectureDto());
        for (LectureDto lecture : lecturesList) {
            lecture.setId(UUID.randomUUID());
        }
        Set<LectureDto> lectures = new HashSet<>(lecturesList);
        Set<ExerciseDto> exercises = Set.of(createSampleExerciseDto(), createSampleExerciseDto());
        List<UUID> courseIds = courses.stream().map(CourseDto::getId).toList();
        List<UUID> lectureIds = lectures.stream().map(LectureDto::getId).toList();
        return student;
    }

    public static ExerciseDto createSampleExerciseDto() {
        ExerciseDto eDto = new ExerciseDto();
        eDto.setId(UUID.randomUUID());
        eDto.setCourseId(UUID.randomUUID());
        eDto.setName("Sample Exercise");
        eDto.setDescription("This is a sample exercise description.");
        eDto.setExerciseDifficulty(ExerciseDifficulty.MEDIUM);
        return eDto;
    }

    public static Exercise createSampleExercise() {
        Exercise e = new Exercise();
        e.setId(UUID.randomUUID());
        e.setCourse(new Course());
        e.setName("Sample Exercise");
        e.setDescription("This is a sample exercise description.");
        e.setExerciseDifficulty(ExerciseDifficulty.MEDIUM);
        return e;
    }

    public static CourseDetailedDto createSampleCourseDetailedViewDto() {
        CourseDetailedDto cd = new CourseDetailedDto();
        cd.setId(UUID.randomUUID());
        cd.setName("English course");
        cd.setProficiencyLevel(ProficiencyLevel.A1);
        cd.setCapacity(20);
        cd.setLanguage(Language.ENGLISH);
        cd.setStudents(new ArrayList<>());
        cd.setExercises(new ArrayList<>());
        cd.setLectures(new ArrayList<>());
        return cd;
    }

    public static List<Exercise> getExerciseEntities() {
        Exercise exercise1 = createSampleExercise();
        Exercise exercise2 = createSampleExercise();
        Exercise exercise3 = createSampleExercise();

        return List.of(exercise1, exercise2, exercise3);
    }

    public static List<ExerciseDto> getExerciseDtos() {
        ExerciseDto exercise1 = createSampleExerciseDto();
        ExerciseDto exercise2 = createSampleExerciseDto();
        ExerciseDto exercise3 = createSampleExerciseDto();

        return List.of(exercise1, exercise2, exercise3);
    }

    public static CourseEnrollment getCourseEnrollment() {
        Course course = createSampleCourse();

        CourseEnrollment courseEnrollment = new CourseEnrollment();
        courseEnrollment.setId(UUID.randomUUID());
        courseEnrollment.setCourse(course);
        courseEnrollment.setStudentId(UUID.randomUUID());
        courseEnrollment.setEnrollmentState(EnrollmentState.ENROLLED);
        return courseEnrollment;
    }

    public static LectureEnrollment getLectureEnrollment() {
        Lecture lecture = createSampleLecture();

        LectureEnrollment lectureEnrollment = new LectureEnrollment();
        lectureEnrollment.setId(UUID.randomUUID());
        lectureEnrollment.setLecture(lecture);
        lectureEnrollment.setStudentId(UUID.randomUUID());
        return lectureEnrollment;
    }

    public static CourseEnrollmentDto getCourseEnrollmentDto() {
        CourseDto courseDto = createSampleCourseDto();
        StudentDto studentDto = createSampleStudentDto();

        CourseEnrollmentDto courseEnrollmentDto = new CourseEnrollmentDto();
        courseEnrollmentDto.setCourseDto(courseDto);
        courseEnrollmentDto.setId(UUID.randomUUID());
        courseEnrollmentDto.setStudentDto(studentDto);
        courseEnrollmentDto.setEnrollmentState(EnrollmentState.ENROLLED);
        return courseEnrollmentDto;
    }

    public static LectureEnrollmentDto getLectureEnrollmentDto() {
        LectureDto lectureDto = createSampleLectureDto();
        StudentDto studentDto = createSampleStudentDto();

        LectureEnrollmentDto lectureEnrollmentDto = new LectureEnrollmentDto();
        lectureEnrollmentDto.setLectureDto(lectureDto);
        lectureEnrollmentDto.setId(UUID.randomUUID());
        lectureEnrollmentDto.setStudentDto(studentDto);
        return lectureEnrollmentDto;
    }
}
