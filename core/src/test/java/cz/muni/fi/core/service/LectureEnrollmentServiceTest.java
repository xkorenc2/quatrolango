package cz.muni.fi.core.service;

import cz.muni.fi.core.data.model.LectureEnrollment;
import cz.muni.fi.core.data.repository.LectureEnrollmentRepository;
import cz.muni.fi.core.factory.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LectureEnrollmentServiceTest {
    @Mock
    private LectureEnrollmentRepository lectureEnrollmentRepository;

    @InjectMocks
    private LectureEnrollmentService lectureEnrollmentService;

    @Test
    void findById_validId_returnsLectureEnrollment() {
        LectureEnrollment expectedLectureEnrollment = TestDataFactory.getLectureEnrollment();
        UUID id = expectedLectureEnrollment.getId();

        when(lectureEnrollmentRepository.findById(id)).thenReturn(Optional.of(expectedLectureEnrollment));

        LectureEnrollment result = lectureEnrollmentService.findById(id);

        assertEquals(expectedLectureEnrollment, result);
    }

    @Test
    void findByStudentId_validStudentId_returnsListOfLectureEnrollments() {
        LectureEnrollment lectureEnrollment = TestDataFactory.getLectureEnrollment();
        List<LectureEnrollment> expectedLectureEnrollments = Collections.singletonList(lectureEnrollment);
        UUID studentId = lectureEnrollment.getStudentId();

        when(lectureEnrollmentRepository.findByStudentId(studentId)).thenReturn(expectedLectureEnrollments);

        List<LectureEnrollment> result = lectureEnrollmentService.findByStudentId(studentId);

        assertEquals(expectedLectureEnrollments, result);
    }

    @Test
    void create_validLectureEnrollment_returnsCreatedLectureEnrollment() {
        LectureEnrollment inputLectureEnrollment = TestDataFactory.getLectureEnrollment();
        LectureEnrollment expectedLectureEnrollment = TestDataFactory.getLectureEnrollment();

        when(lectureEnrollmentRepository.save(inputLectureEnrollment)).thenReturn(expectedLectureEnrollment);

        LectureEnrollment result = lectureEnrollmentService.create(inputLectureEnrollment);

        assertEquals(expectedLectureEnrollment, result);
    }

    @Test
    void delete_validId_returnsDeletedLectureEnrollment() {
        LectureEnrollment expectedLectureEnrollment = TestDataFactory.getLectureEnrollment();
        UUID id = expectedLectureEnrollment.getId();
        when(lectureEnrollmentRepository.findById(id)).thenReturn(Optional.of(expectedLectureEnrollment));

        LectureEnrollment result = lectureEnrollmentService.delete(id);

        assertEquals(expectedLectureEnrollment, result);
        verify(lectureEnrollmentRepository).deleteById(id);
    }

    @Test
    void checkIfStudentIsEnrolled_validIds_returnsBoolean() {
        UUID lectureId = UUID.randomUUID();
        UUID studentId = UUID.randomUUID();
        boolean expectedValue = true;

        when(lectureEnrollmentRepository.checkIfStudentIsEnrolled(lectureId, studentId)).thenReturn(expectedValue);

        boolean result = lectureEnrollmentService.checkIfStudentIsEnrolled(lectureId, studentId);

        assertEquals(expectedValue, result);
    }
}
