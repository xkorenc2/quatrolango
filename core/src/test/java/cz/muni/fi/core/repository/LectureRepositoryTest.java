package cz.muni.fi.core.repository;

import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.data.model.Lecture;
import cz.muni.fi.core.data.repository.LectureRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
class LectureRepositoryTest {

    @Autowired
    private LectureRepository lectureRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    private Course course;
    private Lecture lecture1;
    private Lecture lecture2;
    private Lecture lecture3;

    @BeforeEach
    void initData() {
        // Persisting a course
        course = new Course();
        course.setName("Test Course");
        course.setProficiencyLevel(ProficiencyLevel.B1);
        course.setLanguage(Language.ENGLISH);
        course.setCapacity(60);
        testEntityManager.persistAndFlush(course);

        // Persisting lectures
        lecture1 = createLecture("Lecture 1", "Description 1", LocalDateTime.of(2024, 4, 21, 10, 0),
                LocalDateTime.of(2024, 4, 21, 12, 0), 10);
        lecture2 = createLecture("Lecture 2", "Description 2", LocalDateTime.of(2024, 4, 22, 10, 0),
                LocalDateTime.of(2024, 4, 22, 12, 0), 15);
        lecture3 = createLecture("Lecture 3", "Description 3", LocalDateTime.of(2024, 4, 23, 10, 0),
                LocalDateTime.of(2024, 4, 23, 12, 0), 20);
    }

    private Lecture createLecture(String name, String description, LocalDateTime startTime, LocalDateTime endTime, int capacity) {
        Lecture lecture = new Lecture();
        lecture.setName(name);
        lecture.setDescription(description);
        lecture.setStartTime(startTime);
        lecture.setEndTime(endTime);
        lecture.setCourse(course);
        lecture.setLecturerId(UUID.randomUUID());
        lecture.setCapacity(capacity);
        return testEntityManager.persistFlushFind(lecture);
    }

    @Test
    @Transactional
    void findByLecturerId_lecturerExists_returnsCourse() {
        List<Lecture> lectures = lectureRepository.findByLecturerId(lecture1.getLecturerId());
        assertEquals(1, lectures.size());
        assertEquals(lecture1.getId(), lectures.getFirst().getId());
    }

    @Test
    @Transactional
    void findByLecturerId_lecturerDoesNotExist_returnsNone() {
        List<Lecture> lectures = lectureRepository.findByLecturerId(UUID.randomUUID());
        assertEquals(0, lectures.size());
    }

    @Test
    @Transactional
    void findByCourseId_courseExists_returnsCourse() {
        List<Lecture> lectures = lectureRepository.findByCourseId(course.getId());
        assertEquals(3, lectures.size());
        assertTrue(lectures.stream().anyMatch(lecture -> lecture.getId().equals(lecture1.getId())));
        assertTrue(lectures.stream().anyMatch(lecture -> lecture.getId().equals(lecture2.getId())));
        assertTrue(lectures.stream().anyMatch(lecture -> lecture.getId().equals(lecture3.getId())));
    }

    @Test
    @Transactional
    void findByCourseId_courseDoesNotExist_returnsNone() {
        List<Lecture> lectures = lectureRepository.findByCourseId(UUID.randomUUID());
        assertEquals(0, lectures.size());
    }

    @Test
    @Transactional
    void findByStartTime_courseExists_returnsCourse() {
        LocalDateTime localDateTime = LocalDateTime.of(2024, 4, 21, 10, 0);
        List<Lecture> lectures = lectureRepository.findByStartTimeAndEndTime(
                localDateTime.toLocalDate().atStartOfDay(),
                localDateTime.toLocalDate().atStartOfDay().plusDays(1).minusNanos(1));
        assertEquals(1, lectures.size());
        assertEquals(lecture1.getId(), lectures.getFirst().getId());
    }

    @Test
    @Transactional
    void findByStartTime_courseDoesNotExist_returnsNone() {
        LocalDateTime localDateTime = LocalDateTime.of(2025, 4, 21, 10, 0);
        List<Lecture> lectures = lectureRepository.findByStartTimeAndEndTime(
                localDateTime.toLocalDate().atStartOfDay(),
                localDateTime.toLocalDate().atStartOfDay().plusDays(1).minusNanos(1));
        assertEquals(0, lectures.size());
    }

    @Test
    @Transactional
    void findByIds_coursesExist_returnsCourses() {
        Set<UUID> ids = new HashSet<>();
        ids.add(lecture1.getId());
        ids.add(lecture2.getId());
        List<Lecture> lectures = lectureRepository.findByIds(ids);
        assertEquals(2, lectures.size());
        assertTrue(lectures.stream().anyMatch(lecture -> lecture.getId().equals(lecture1.getId())));
        assertTrue(lectures.stream().anyMatch(lecture -> lecture.getId().equals(lecture2.getId())));
    }

    @Test
    @Transactional
    void findByIds_coursesDoNotExist_returnsNone() {
        Set<UUID> ids = new HashSet<>();
        ids.add(UUID.randomUUID());
        ids.add(UUID.randomUUID());
        List<Lecture> lectures = lectureRepository.findByIds(ids);
        assertEquals(0, lectures.size());
    }
}
