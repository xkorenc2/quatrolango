package cz.muni.fi.core.service;

import cz.muni.fi.commons.enums.ExerciseDifficulty;
import cz.muni.fi.commons.exceptions.ResourceNotFoundException;
import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.data.model.Exercise;
import cz.muni.fi.core.data.repository.ExerciseRepository;
import cz.muni.fi.core.factory.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ExerciseServiceTest {
    @Mock
    private ExerciseRepository exerciseRepository;
    @Mock
    private CourseService courseService;
    @Mock
    private CourseEnrollmentService courseEnrollmentService;
    @InjectMocks
    private ExerciseService exerciseService;

    @Test
    void findById_exerciseFound_returnsExercise() {
        Exercise exercise = TestDataFactory.createSampleExercise();
        UUID id = exercise.getId();

        when(exerciseRepository.findById(id)).thenReturn(Optional.of(exercise));

        Exercise foundExercise = exerciseService.findById(id);

        assertThat(foundExercise).isEqualTo(exercise);
    }

    @Test
    void findById_exerciseNotFound_throwsResourceNotFoundException() {
        UUID id = UUID.randomUUID();
        when(exerciseRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> exerciseService.findById(id));
    }

    @Test
    void findAll_allExercisesFound_returnsAllExercises() {
        Pageable pageable = Pageable.unpaged();

        List<Exercise> exercises = new ArrayList<>();
        Page<Exercise> exercisePage = new PageImpl<>(exercises);
        exercises.add(TestDataFactory.createSampleExercise());
        exercises.add(TestDataFactory.createSampleExercise());
        when(exerciseRepository.findAll(pageable)).thenReturn(exercisePage);

        Page<Exercise> result = exerciseService.findAll(pageable);

        verify(exerciseRepository).findAll(pageable);
        assert result != null;
        assertThat(result).isEqualTo(exercisePage);
    }


    @Test
    void findAll_exercisesNotFound_returnsEmptyList() {
        Pageable pageable = Pageable.unpaged();

        List<Exercise> exercises = new ArrayList<>();
        when(exerciseRepository.findAll(pageable)).thenReturn(new PageImpl<>(exercises));

        Page<Exercise> result = exerciseService.findAll(pageable);

        verify(exerciseRepository).findAll(pageable);
        assert result != null;
        assert result.isEmpty();
        assertThat(result).isEqualTo(new PageImpl<>(exercises));
    }

    @Test
    void findByExerciseDifficulty_exercisesFound_returnsListOfExercises() {
        ExerciseDifficulty difficulty = ExerciseDifficulty.EASY;
        List<Exercise> expectedExercises = new ArrayList<>();
        expectedExercises.add(TestDataFactory.createSampleExercise());
        expectedExercises.add(TestDataFactory.createSampleExercise());
        when(exerciseRepository.findByExerciseDifficulty(difficulty)).thenReturn(expectedExercises);


        List<Exercise> result = exerciseService.findByExerciseDifficulty(difficulty);

        verify(exerciseRepository).findByExerciseDifficulty(difficulty);
        assertThat(expectedExercises).hasSameSizeAs(result).isEqualTo(result);
    }


    @Test
    void findByExerciseDifficulty_exercisesNotFound_returnsEmptyList() {
        ExerciseDifficulty difficulty = ExerciseDifficulty.EASY;
        List<Exercise> expectedExercises = new ArrayList<>();
        when(exerciseRepository.findByExerciseDifficulty(difficulty)).thenReturn(expectedExercises);


        List<Exercise> result = exerciseService.findByExerciseDifficulty(difficulty);

        verify(exerciseRepository).findByExerciseDifficulty(difficulty);
        assert result != null;
        assert result.isEmpty();
        assertThat(result).isEqualTo(expectedExercises);
    }

    @Test
    void pickForStudent_exerciseNotFound_throwsResourceNotFoundException() {
        UUID studentUuid = UUID.randomUUID();
        UUID exerciseUuid = UUID.randomUUID();

        when(exerciseRepository.findById(exerciseUuid)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> exerciseService.getExerciseForStudent(studentUuid, exerciseUuid));
    }

    @Test
    void create_courseExists_shouldCreateExerciseAndAddToCourse() {
        UUID courseUuid = UUID.randomUUID();
        Exercise exercise = TestDataFactory.createSampleExercise();
        when(courseService.findById(courseUuid)).thenReturn(TestDataFactory.createSampleCourse());
        when(exerciseRepository.save(exercise)).thenReturn(exercise);

        Exercise createdExercise = exerciseService.create(exercise, courseUuid);


        assertThat(exercise).isEqualTo(createdExercise);

        verify(exerciseRepository).save(exercise);
        verify(courseService, times(1)).findById(courseUuid);
    }

    @Test
    void create_courseDoesNotExist_shouldThrowException() {
        UUID courseUuid = UUID.randomUUID();

        when(courseService.findById(courseUuid)).thenThrow(new ResourceNotFoundException());

        assertThrows(ResourceNotFoundException.class, () -> exerciseService.create(TestDataFactory.createSampleExercise(), courseUuid));
        verify(courseService, times(1)).findById(courseUuid);
    }

    @Test
    void update_exerciseExists_shouldUpdateExercise() {
        UUID id = UUID.randomUUID();
        Exercise updateData = new Exercise();
        updateData.setName("New");
        updateData.setExerciseDifficulty(ExerciseDifficulty.EASY);

        Exercise updatedExercise = TestDataFactory.createSampleExercise();
        when(exerciseRepository.save(updateData)).thenReturn(updatedExercise);
        when(exerciseRepository.findById(id)).thenReturn(Optional.of(updateData));

        Exercise result = exerciseService.update(id, updateData);

        assertThat(updatedExercise).isEqualTo(result);

        verify(exerciseRepository).save(updateData);
    }

    @Test
    void checkIfCourseExists_courseNotFound_throwsResourceNotFoundException() {
        UUID courseId = UUID.randomUUID();

        when(courseService.findById(courseId)).thenThrow(new ResourceNotFoundException());

        assertThrows(ResourceNotFoundException.class, () -> exerciseService.checkIfCourseExists(courseId));

        verify(courseService).findById(courseId);
    }

    @Test
    void checkIfCourseExists_courseExists_throwsNothing() {
        UUID courseId = UUID.randomUUID();

        when(courseService.findById(courseId)).thenReturn(TestDataFactory.createSampleCourse());

        assertDoesNotThrow(() -> exerciseService.checkIfCourseExists(courseId));

        verify(courseService).findById(courseId);
    }

    @Test
    void delete_exerciseExists_deletesExercise() {
        UUID id = UUID.randomUUID();
        Exercise exercise = TestDataFactory.createSampleExercise();

        when(exerciseRepository.findById(id)).thenReturn(Optional.of(exercise));

        Exercise deletedExercise = exerciseService.delete(id);

        assertThat(exercise).isEqualTo(deletedExercise);

        verify(exerciseRepository).deleteById(id);
    }

    @Test
    void delete_exerciseDoesNotExist_throwsResourceNotFoundException() {
        UUID id = UUID.randomUUID();

        when(exerciseRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> exerciseService.delete(id));
    }


    @Test
    void findByIds_shouldReturnExercises_whenIdsProvided() {
        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();
        UUID id3 = UUID.randomUUID();

        Exercise exercise1 = TestDataFactory.createSampleExercise();
        exercise1.setId(id1);
        Exercise exercise2 = TestDataFactory.createSampleExercise();
        exercise2.setId(id2);
        Exercise exercise3 = TestDataFactory.createSampleExercise();
        exercise3.setId(id3);

        Set<UUID> ids = Set.of(id1, id2, id3);


        List<Exercise> expectedExercises = List.of(exercise1, exercise2, exercise3);

        when(exerciseRepository.findByIds(ids)).thenReturn(expectedExercises);

        List<Exercise> result = exerciseService.findByIds(ids);

        assertThat(expectedExercises).isEqualTo(result);

        verify(exerciseRepository).findByIds(ids);
    }

    @Test
    void findByIds_noIdsProvided_returnsEmptyList() {
        Set<UUID> ids = new HashSet<>();

        when(exerciseRepository.findByIds(ids)).thenReturn(new ArrayList<>());

        List<Exercise> result = exerciseService.findByIds(ids);

        assertThat(result).isEmpty();

        verify(exerciseRepository).findByIds(ids);
    }

    @Test
    void getExerciseForStudent_studentIsEnrolled_returnsExercise() {
        UUID studentUuid = UUID.randomUUID();

        Exercise exercise = TestDataFactory.createSampleExercise();
        UUID exerciseUuid = UUID.randomUUID();
        Course course = TestDataFactory.createSampleCourse();
        exercise.setCourse(course);

        when(exerciseRepository.findById(exerciseUuid)).thenReturn(Optional.of(exercise));
        when(courseEnrollmentService.checkIfStudentIsEnrolled(course.getId(), studentUuid)).thenReturn(true);

        Exercise result = exerciseService.getExerciseForStudent(studentUuid, exerciseUuid);

        assertNotNull(result);
        assertEquals(exercise, result);
    }

    @Test
    void getExerciseForStudent_studentIsNotEnrolled_throwsException() {
        UUID studentUuid = UUID.randomUUID();
        UUID exerciseUuid = UUID.randomUUID();

        Exercise exercise = TestDataFactory.createSampleExercise();
        Course course = TestDataFactory.createSampleCourse();
        exercise.setCourse(course);

        when(exerciseRepository.findById(exerciseUuid)).thenReturn(Optional.of(exercise));
        when(courseEnrollmentService.checkIfStudentIsEnrolled(course.getId(), studentUuid)).thenReturn(false);

        assertThrows(ResourceNotFoundException.class, () -> {
            exerciseService.getExerciseForStudent(studentUuid, exerciseUuid);
        });
    }

    @Test
    void update_exerciseNotFound_throwsResourceNotFoundException() {
        UUID exerciseId = UUID.randomUUID();
        Exercise updateData = TestDataFactory.createSampleExercise();
        updateData.setId(exerciseId);

        when(exerciseRepository.findById(exerciseId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> exerciseService.update(exerciseId, updateData));
    }
}
