package cz.muni.fi.core;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@ExtendWith(MockitoExtension.class)
@EnableTransactionManagement
class CoreApplicationTests {

    @Test
    void contextLoads() {
    }

}
