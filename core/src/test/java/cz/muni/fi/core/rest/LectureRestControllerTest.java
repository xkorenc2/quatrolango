package cz.muni.fi.core.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.commons.dto.lecture.LectureDto;
import cz.muni.fi.core.facade.LectureFacade;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static cz.muni.fi.core.factory.TestDataFactory.createSampleLectureDto;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@WebMvcTest(LectureRestController.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WithMockUser(username = "admin", authorities = {"SCOPE_test_1", "SCOPE_test_2", "SCOPE_test_3"})
class LectureRestControllerTest {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    @MockBean
    private LectureFacade lectureFacade;
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;

    @BeforeAll
    void setup() {
        objectMapper.findAndRegisterModules();
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    void findAll_lecturesFound_returnsLectures() throws Exception {
        Pageable pageable = Pageable.unpaged();

        List<LectureDto> lectureDtos = new ArrayList<>();
        lectureDtos.add(createSampleLectureDto());

        when(lectureFacade.findAll(pageable)).thenReturn(new PageImpl<>(lectureDtos));

        mockMvc.perform(MockMvcRequestBuilders.get("/lectures/").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void findById_lectureFound_returnsLecture() throws Exception {

        LectureDto lectureDto = createSampleLectureDto();
        lectureDto.setId(UUID.randomUUID());
        UUID lectureId = lectureDto.getId();

        when(lectureFacade.findById(lectureId)).thenReturn(lectureDto);

        mockMvc.perform(MockMvcRequestBuilders.get("/lectures/" + lectureId).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void findById_lectureNotFound_returns404() throws Exception {
        UUID nonExistingId = UUID.randomUUID();

        when(lectureFacade.findById(nonExistingId)).thenReturn(null);

        mockMvc.perform(MockMvcRequestBuilders.get("/lectures/" + nonExistingId).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }


    @Test
    void findByLecturer_lectureFound_returnsLecture() throws Exception {
        UUID lecturerId = UUID.randomUUID();
        List<LectureDto> lectures = new ArrayList<>();
        LectureDto lecture = createSampleLectureDto();
        lecture.setLecturerId(lecturerId);
        lectures.add(lecture);

        when(lectureFacade.findByLecturer(lecturerId)).thenReturn(lectures);

        // Perform the GET request
        mockMvc.perform(MockMvcRequestBuilders.get("/lectures/lecturer/" + lecturerId).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void findByLecturer_nonExistingLecturer_returnsEmptyList() throws Exception {
        UUID nonExistingLecturerId = UUID.randomUUID();

        when(lectureFacade.findByLecturer(nonExistingLecturerId)).thenReturn(new ArrayList<>());

        mockMvc.perform(MockMvcRequestBuilders.get("/lectures/lecturer/" + nonExistingLecturerId).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void findByDate_lecturesFound_returnsLectures() throws Exception {
        LocalDate date = LocalDate.now();
        List<LectureDto> lectureDtos = new ArrayList<>();

        when(lectureFacade.findByDate(date)).thenReturn(lectureDtos);
        mockMvc.perform(MockMvcRequestBuilders.get("/lectures/date/" + date).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void findByDate_nonExistingDate_returnsEmptyList() throws Exception {
        LocalDate nonExistingDate = LocalDate.of(2024, 12, 31);

        when(lectureFacade.findByDate(nonExistingDate)).thenReturn(new ArrayList<>());

        mockMvc.perform(MockMvcRequestBuilders.get("/lectures/date/" + nonExistingDate).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void create_lectureCreated_returnsLecture() throws Exception {
        LectureDto lectureDto = createSampleLectureDto();

        when(lectureFacade.create(lectureDto, lectureDto.getCourseId())).thenReturn(lectureDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/lectures/" + lectureDto.getCourseId()).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(lectureDto)))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    void create_lectureInvalidInput_returnsBadRequest() throws Exception {
        LectureDto lectureDto = createSampleLectureDto();
        lectureDto.setName(""); // Setting invalid name

        mockMvc.perform(MockMvcRequestBuilders.post("/lectures/" + UUID.randomUUID()).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(lectureDto)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void update_lectureUpdated_returnsLecture() throws Exception {
        LectureDto lectureDto = createSampleLectureDto();
        lectureDto.setId(UUID.randomUUID());

        when(lectureFacade.update(lectureDto, lectureDto.getId())).thenReturn(lectureDto);

        mockMvc.perform(MockMvcRequestBuilders.put("/lectures/" + lectureDto.getId()).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(lectureDto)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void update_nonExistingLecture_returnsNotFound() throws Exception {
        LectureDto lectureDto = createSampleLectureDto();
        UUID nonExistingId = UUID.randomUUID();

        when(lectureFacade.update(lectureDto, nonExistingId)).thenReturn(null);

        mockMvc.perform(MockMvcRequestBuilders.put("/lectures/" + nonExistingId).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(lectureDto)))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void delete_lectureDeleted_returnsLecture() throws Exception {
        LectureDto lectureDto = createSampleLectureDto();
        lectureDto.setId(UUID.randomUUID());

        when(lectureFacade.remove(lectureDto.getId())).thenReturn(lectureDto);

        mockMvc.perform(MockMvcRequestBuilders.delete("/lectures/" + lectureDto.getId()).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void findByCourse_lecturesFound_returnsLectures() throws Exception {
        UUID courseId = UUID.randomUUID();
        List<LectureDto> lectures = new ArrayList<>();
        LectureDto lecture = createSampleLectureDto();
        lecture.setCourseId(courseId);
        lectures.add(lecture);

        when(lectureFacade.findByCourse(courseId)).thenReturn(lectures);

        mockMvc.perform(MockMvcRequestBuilders.get("/lectures/course/" + courseId).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void findByCourse_nonExistingCourse_returnsEmptyList() throws Exception {
        UUID nonExistingCourseId = UUID.randomUUID();

        when(lectureFacade.findByCourse(nonExistingCourseId)).thenReturn(new ArrayList<>());

        mockMvc.perform(MockMvcRequestBuilders.get("/lectures/course/" + nonExistingCourseId).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void findLecturesByIds_lecturesFound_returnsLectures() throws Exception {
        Set<UUID> ids = Set.of(UUID.randomUUID(), UUID.randomUUID());
        List<LectureDto> lectures = new ArrayList<>();
        lectures.add(createSampleLectureDto());
        lectures.add(createSampleLectureDto());

        when(lectureFacade.findByIds(ids)).thenReturn(lectures);

        mockMvc.perform(MockMvcRequestBuilders.post("/lectures/ids").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(ids)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void findLecturesByIds_noLecturesFound_returnsEmptyList() throws Exception {
        Set<UUID> ids = Set.of(UUID.randomUUID(), UUID.randomUUID());

        when(lectureFacade.findByIds(ids)).thenReturn(new ArrayList<>());

        mockMvc.perform(MockMvcRequestBuilders.post("/lectures/ids").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(ids)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void delete_nonExistingLecture_returnsNotFound() throws Exception {
        UUID nonExistingId = UUID.randomUUID();

        when(lectureFacade.remove(nonExistingId)).thenReturn(null);

        mockMvc.perform(MockMvcRequestBuilders.delete("/lectures/" + nonExistingId).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void createLecture_exceptionOccurs_returnsBadRequest() throws Exception {
        LectureDto lectureDto = createSampleLectureDto();

        doThrow(RuntimeException.class).when(lectureFacade).create(lectureDto, lectureDto.getCourseId());

        mockMvc.perform(MockMvcRequestBuilders.post("/lectures/").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(lectureDto)))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }

    @Test
    void updateLecture_exceptionOccurs_returnsBadRequest() throws Exception {
        LectureDto lectureDto = createSampleLectureDto();
        lectureDto.setId(UUID.randomUUID());

        doThrow(RuntimeException.class).when(lectureFacade).update(lectureDto, lectureDto.getId());

        mockMvc.perform(MockMvcRequestBuilders.put("/lectures/" + lectureDto.getId()).with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(lectureDto)))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }
}