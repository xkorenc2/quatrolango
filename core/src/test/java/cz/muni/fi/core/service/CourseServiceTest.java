package cz.muni.fi.core.service;

import cz.muni.fi.commons.dto.course.CourseDto;
import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import cz.muni.fi.commons.exceptions.ResourceNotFoundException;
import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.data.repository.CourseRepository;
import cz.muni.fi.core.factory.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CourseServiceTest {
    @Mock
    private CourseRepository courseRepository;
    @Mock
    private LectureService lectureService;
    @InjectMocks
    private CourseService courseService;

    @Test
    void findById_courseFound_returnsCourse() {
        CourseDto courseDto = TestDataFactory.createSampleCourseDto();
        Course course = TestDataFactory.createSampleCourse();
        when(courseRepository.findById(courseDto.getId())).thenReturn(Optional.of(course));

        Course foundCourse = courseService.findById(courseDto.getId());

        assertThat(foundCourse).isEqualTo(course);
    }

    @Test
    void findById_courseNotFound_throwsResourceNotFoundException() {
        CourseDto courseDto = TestDataFactory.createSampleCourseDto();
        assertThrows(ResourceNotFoundException.class, () -> courseService.findById(courseDto.getId()));
    }

    @Test
    void findByIds_validIds_returnsCourses() {
        Course course1 = TestDataFactory.createSampleCourse();
        Course course2 = TestDataFactory.createSampleCourse();


        UUID courseId1 = course1.getId();
        UUID courseId2 = course2.getId();

        Set<UUID> ids = new HashSet<>(Arrays.asList(courseId1, courseId2));
        when(courseRepository.findByIds(ids)).thenReturn(Arrays.asList(course1, course2));

        List<Course> result = courseService.findByIds(ids);

        assertEquals(2, result.size());
        assertEquals(course1, result.get(0));
        assertEquals(course2, result.get(1));
    }

    @Test
    void findAll_coursesFound_returnsAllCourses() {
        Pageable pageable = Pageable.unpaged();

        List<Course> courses = TestDataFactory.createSampleListOfCourses();
        Page<Course> coursePage = new PageImpl<>(courses);
        when(courseRepository.findAll(pageable)).thenReturn(coursePage);

        Page<Course> foundCourses = courseService.findAll(pageable);

        assertThat(foundCourses).isEqualTo(coursePage);
    }

    @Test
    void findAll_coursesNotFound_returnsEmptyList() {
        Pageable pageable = Pageable.unpaged();
        List<Course> courses = new ArrayList<>();
        Page<Course> coursePage = new PageImpl<>(courses);

        when(courseRepository.findAll(pageable)).thenReturn(coursePage);

        Page<Course> foundCourses = courseService.findAll(pageable);

        assertThat(foundCourses).isEqualTo(coursePage);
    }

    @Test
    void findByLanguage_coursesFound_returnsListOfCourses() {
        List<Course> courses = TestDataFactory.createSampleListOfCourses();
        when(courseRepository.findByLanguage(Language.ENGLISH)).thenReturn(courses);

        List<Course> foundCourses = courseService.findByLanguage(Language.ENGLISH);

        assertThat(foundCourses).isEqualTo(courses);
    }

    @Test
    void findByProficiencyLevel_coursesFound_returnsListOfCourses() {
        List<Course> courses = TestDataFactory.createSampleListOfCourses();
        when(courseRepository.findByProficiencyLevel(ProficiencyLevel.A1)).thenReturn(courses);

        List<Course> foundCourses = courseService.findByProficiencyLevel(ProficiencyLevel.A1);

        assertThat(foundCourses).isEqualTo(courses);
    }

    @Test
    void create_courseIsValid_courseReturned() {
        Course course = TestDataFactory.createSampleCourse();
        when(courseRepository.save(course)).thenReturn(course);

        var resultCourse = courseService.create(course);

        assertThat(resultCourse).isEqualTo(course);
    }

    @Test
    void update_courseIsValidAndDoesNotExist_courseReturned() {
        Course course = TestDataFactory.createSampleCourse();
        CourseDto courseDto = TestDataFactory.createSampleCourseDto();

        when(courseRepository.existsById(course.getId())).thenReturn(true);
        when(courseRepository.save(course)).thenReturn(course);

        var result = courseService.update(courseDto.getId(), course);

        assertThat(result).isEqualTo(course);
    }

    @Test
    void update_courseIsValidAndDoesExist_courseReturned() {
        Course course = TestDataFactory.createSampleCourse();
        when(courseRepository.existsById(course.getId())).thenReturn(true);
        when(courseRepository.save(course)).thenReturn(course);

        var result = courseService.update(course.getId(), course);

        assertThat(result).isEqualTo(course);
    }

    @Test
    void delete_courseExists_deletedReturned() {
        Course course = TestDataFactory.createSampleCourse();
        CourseDto courseDto = TestDataFactory.createSampleCourseDto();
        when(courseRepository.findById(courseDto.getId())).thenReturn(Optional.of(course));

        var removed = courseService.delete(courseDto.getId());

        assertThat(removed).isEqualTo(course);
        verify(courseRepository, times(1)).deleteById(courseDto.getId());
    }

    @Test
    void delete_courseDoesNotExists_throwsResourceNotFoundException() {
        CourseDto courseDto = TestDataFactory.createSampleCourseDto();
        when(courseRepository.findById(courseDto.getId())).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> courseService.delete(courseDto.getId()));
    }

    @Test
    void update_courseNotFound_throwsResourceNotFoundException() {
        UUID courseId = UUID.randomUUID();
        Course updateData = TestDataFactory.createSampleCourse();
        updateData.setId(courseId);

        when(courseRepository.existsById(courseId)).thenReturn(false);

        assertThrows(ResourceNotFoundException.class, () -> courseService.update(courseId, updateData));
    }

}
