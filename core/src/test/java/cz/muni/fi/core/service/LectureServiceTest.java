package cz.muni.fi.core.service;

import cz.muni.fi.commons.dto.user.lecturer.LecturerDto;
import cz.muni.fi.commons.exceptions.ResourceNotFoundException;
import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.data.model.Lecture;
import cz.muni.fi.core.data.repository.LectureRepository;
import cz.muni.fi.core.factory.TestDataFactory;
import cz.muni.fi.core.service.api.LecturerApiService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static cz.muni.fi.core.factory.TestDataFactory.createSampleLecture;
import static cz.muni.fi.core.factory.TestDataFactory.createSampleLecturer;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LectureServiceTest {

    @Mock
    private LectureRepository lectureRepository;
    @Mock
    private CourseService courseService;

    @Mock
    private LecturerApiService lecturerApiService;

    @InjectMocks
    private LectureService lectureService;

    @Test
    void findById_existingId_returnsLecture() {
        UUID existingId = UUID.randomUUID();
        Lecture expectedLecture = createSampleLecture();

        when(lectureRepository.findById(existingId)).thenReturn(Optional.of(expectedLecture));

        Lecture result = lectureService.findById(existingId);

        assertEquals(expectedLecture, result);
    }

    @Test
    void findAll_existingLectures_returnsListOfLectures() {
        Pageable pageable = Pageable.unpaged();

        List<Lecture> existingLectures = new ArrayList<>();
        existingLectures.add(createSampleLecture());
        existingLectures.add(createSampleLecture());
        Page<Lecture> lecturePage = new PageImpl<>(existingLectures);

        when(lectureRepository.findAll(pageable)).thenReturn(lecturePage);

        Page<Lecture> result = lectureService.findAll(pageable);

        assertEquals(lecturePage, result);
    }

    @Test
    void findByLecturer_existingLecturer_returnsListOfLectures() {
        LecturerDto lecturer = createSampleLecturer();

        List<Lecture> lecturesByLecturer = new ArrayList<>();
        lecturesByLecturer.add(createSampleLecture());
        lecturesByLecturer.add(createSampleLecture());

        when(lectureRepository.findByLecturerId(lecturer.getId())).thenReturn(lecturesByLecturer);
        when(lecturerApiService.findLecturerById(lecturer.getId())).thenReturn(Optional.of(lecturer));

        List<Lecture> result = lectureService.findByLecturer(lecturer.getId());

        assertEquals(lecturesByLecturer, result);
    }

    @Test
    void findByDate_existingDate_returnsListOfLectures() {
        LocalDate date = LocalDate.now();
        LocalDateTime dateTime = date.atStartOfDay();
        List<Lecture> lecturesByDate = new ArrayList<>();
        lecturesByDate.add(createSampleLecture());
        lecturesByDate.add(createSampleLecture());

        when(lectureRepository.findByStartTimeAndEndTime(dateTime, dateTime.plusDays(1).minusNanos(1))).thenReturn(lecturesByDate);

        List<Lecture> result = lectureService.findByDate(date);

        assertEquals(lecturesByDate, result);
    }

    @Test
    void create_validLecture_returnsCreatedLecture() {
        Lecture lectureToCreate = createSampleLecture();
        lectureToCreate.setId(TestDataFactory.getExistingUUID());
        when(courseService.findById(lectureToCreate.getCourse().getId()))
                .thenReturn(TestDataFactory.createSampleCourse());

        when(lecturerApiService.findLecturerById(lectureToCreate.getLecturerId()))
                .thenReturn(Optional.of(new LecturerDto()));

        when(lectureRepository.save(lectureToCreate)).thenReturn(lectureToCreate);

        Lecture result = lectureService.create(lectureToCreate, lectureToCreate.getCourse().getId());

        assertEquals(lectureToCreate, result);
    }

    @Test
    void findById_nonExistingId_throwsResourceNotFoundException() {
        UUID nonExistingId = UUID.randomUUID();

        when(lectureRepository.findById(nonExistingId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> lectureService.findById(nonExistingId));
    }

    @Test
    void remove_nonExistingId_throwsResourceNotFoundException() {
        UUID nonExistingId = UUID.randomUUID();

        when(lectureRepository.findById(nonExistingId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> lectureService.remove(nonExistingId));
    }

    @Test
    void update_nullLecture_throwsIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> lectureService.update(null, UUID.randomUUID()));
    }

    @Test
    void update_lectureNotFound_throwsResourceNotFoundException() {
        assertThrows(ResourceNotFoundException.class, () -> lectureService.update(createSampleLecture(), UUID.randomUUID()));
    }

    @Test
    void findByCourse_existingCourse_returnsListOfLectures() {
        UUID courseId = UUID.randomUUID();
        List<Lecture> lecturesByCourse = new ArrayList<>();
        lecturesByCourse.add(createSampleLecture());
        lecturesByCourse.add(createSampleLecture());

        when(lectureRepository.findByCourseId(courseId)).thenReturn(lecturesByCourse);

        List<Lecture> result = lectureService.findByCourse(courseId);

        assertEquals(lecturesByCourse, result);
    }

    @Test
    void findByCourse_nonExistingCourse_throwsResourceNotFoundException() {
        UUID nonExistingCourseId = UUID.randomUUID();

        when(lectureRepository.findByCourseId(nonExistingCourseId)).thenReturn(new ArrayList<>());

        assertThrows(ResourceNotFoundException.class, () -> lectureService.findByCourse(nonExistingCourseId));
    }

    @Test
    void createLecture_invalidCourse_throwsResourceNotFoundException() {
        Lecture lectureToCreate = createSampleLecture();
        lectureToCreate.getCourse().setId(UUID.randomUUID());

        when(courseService.findById(lectureToCreate.getCourse().getId()))
                .thenThrow(new ResourceNotFoundException());

        assertThrows(ResourceNotFoundException.class, () -> lectureService.create(lectureToCreate, lectureToCreate.getCourse().getId()));
    }

//    @Test TODO
//    void create_invalidLecturer_throwsResourceNotFoundException() {
//        Lecture lectureToCreate = createSampleLecture();
//        lectureToCreate.setLecturerId(UUID.randomUUID());
//
//        when(courseService.findById(lectureToCreate.getCourse().getId()))
//                .thenReturn(new Course());
//
////        when(lecturerApiService.findByLecturer(lectureToCreate.getLecturerId())) TODO
////                .thenReturn(Optional.empty());
//
////        assertThrows(ResourceNotFoundException.class, () -> lectureService.create(lectureToCreate));
//    }

    @Test
    void findByDate_nonExistingDate_returnsEmptyList() {
        LocalDate nonExistingDate = LocalDate.of(2024, 4, 1);
        LocalDateTime nonExistingDateTime = nonExistingDate.atStartOfDay();

        when(lectureRepository.findByStartTimeAndEndTime(nonExistingDateTime, nonExistingDateTime.plusDays(1).minusNanos(1))).thenReturn(new ArrayList<>());

        List<Lecture> result = lectureService.findByDate(nonExistingDate);

        assertEquals(0, result.size());
    }

    @Test
    void remove_existingId_returnsLecture() {
        UUID existingId = UUID.randomUUID();
        Lecture expectedLecture = createSampleLecture();

        when(lectureRepository.findById(existingId)).thenReturn(Optional.of(expectedLecture));

        Lecture result = lectureService.remove(existingId);

        assertEquals(expectedLecture, result);
    }

    @Test
    void remove_existingId_callsRepositoryRemoveOnce() {
        UUID existingId = UUID.randomUUID();
        Lecture sampleLec = createSampleLecture();
        when(lectureRepository.findById(existingId)).thenReturn(Optional.of(sampleLec));

        lectureService.remove(existingId);

        verify(lectureRepository, times(1)).deleteById(existingId);
    }

    @Test
    void update_validLecture_returnsUpdatedLecture() {
        LecturerDto lecturer = createSampleLecturer();
        Lecture lectureToUpdate = createSampleLecture();
        lectureToUpdate.setLecturerId(lecturer.getId());
        lectureToUpdate.setId(TestDataFactory.getExistingUUID());

        when(lecturerApiService.findLecturerById(lectureToUpdate.getLecturerId()))
                .thenReturn(Optional.of(lecturer));
        when(lectureRepository.existsById(lectureToUpdate.getId())).thenReturn(true);
        when(lectureRepository.save(lectureToUpdate)).thenReturn(lectureToUpdate);

        Lecture result = lectureService.update(lectureToUpdate, lectureToUpdate.getId());

        assertEquals(lectureToUpdate, result);
    }

    @Test
    void checkIfCourseExists_existingCourse_doesNotThrowException() {
        when(courseService.findById(any())).thenReturn(new Course());

        assertDoesNotThrow(() -> lectureService.findCourseIfExists(UUID.randomUUID()));
    }

    @Test
    void checkIfCourseExists_nonExistingCourse_throwsResourceNotFoundException() {
        when(courseService.findById(any())).thenThrow(new ResourceNotFoundException());

        assertThrows(ResourceNotFoundException.class, () -> lectureService.findCourseIfExists(UUID.randomUUID()));
    }

    @Test
    void checkIfLecturerExists_existingLecturer_doesNotThrowException() {
        when(lecturerApiService.findLecturerById(any())).thenReturn(Optional.of(new LecturerDto()));

        assertDoesNotThrow(() -> lectureService.checkIfLecturerExists(UUID.randomUUID()));
    }

    @Test
    void checkIfLecturerExists_nonExistingLecturer_throwsResourceNotFoundException() {
        when(lecturerApiService.findLecturerById(any())).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> lectureService.checkIfLecturerExists(UUID.randomUUID()));
    }

    @Test
    void findByIds_validIds_returnsListOfLectures() {
        Set<UUID> validIds = Set.of(UUID.randomUUID(), UUID.randomUUID());
        List<Lecture> expectedLectures = List.of(createSampleLecture(), createSampleLecture());

        when(lectureRepository.findByIds(validIds)).thenReturn(expectedLectures);

        List<Lecture> result = lectureService.findByIds(validIds);

        assertEquals(expectedLectures, result);
    }

    @Test
    void findByIds_emptyIdsList_returnsEmptyList() {
        List<Lecture> result = lectureService.findByIds(Collections.emptySet());

        assertTrue(result.isEmpty());
    }

    @Test
    void findByIds_invalidIds_returnsEmptyList() {
        Set<UUID> invalidIds = Set.of(UUID.randomUUID(), UUID.randomUUID());

        when(lectureRepository.findByIds(invalidIds)).thenReturn(Collections.emptyList());

        List<Lecture> result = lectureService.findByIds(invalidIds);

        assertTrue(result.isEmpty());
    }
}
