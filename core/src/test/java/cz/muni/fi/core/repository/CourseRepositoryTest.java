package cz.muni.fi.core.repository;

import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.data.model.Lecture;
import cz.muni.fi.core.data.model.Exercise;
import cz.muni.fi.core.data.repository.CourseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
class CourseRepositoryTest {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    private Course course1;
    private Course course2;
    private Course course3;
    private Lecture lecture1;
    private Lecture lecture2;
    private Lecture lecture3;
    private Exercise exercise1;
    private Exercise exercise2;
    private Exercise exercise3;

    @BeforeEach
    void initData() {
        // Persisting courses
        course1 = createCourse("Course 1", 60, Language.ENGLISH, ProficiencyLevel.B1);
        course2 = createCourse("Course 2", 80, Language.GERMAN, ProficiencyLevel.B2);
        course3 = createCourse("Course 3", 50, Language.SPANISH, ProficiencyLevel.A2);

        // Persisting lectures
        lecture1 = createLecture("Lecture 1", "Description 1", LocalDateTime.of(2024, 4, 21, 10, 0),
                LocalDateTime.of(2024, 4, 21, 12, 0),10, course1);
        lecture2 = createLecture("Lecture 2", "Description 2", LocalDateTime.of(2024, 4, 22, 10, 0),
                LocalDateTime.of(2024, 4, 21, 12, 0),15, course2);
        lecture3 = createLecture("Lecture 3", "Description 3", LocalDateTime.of(2024, 4, 23, 10, 0),
                LocalDateTime.of(2024, 4, 21, 12, 0),20, course3);

        // Persisting exercises
        exercise1 = createExercise("Exercise 1", "Description 1", course1);
        exercise2 = createExercise("Exercise 2", "Description 2", course2);
        exercise3 = createExercise("Exercise 3", "Description 3", course3);
    }

    private Course createCourse(String name, int capacity, Language language, ProficiencyLevel proficiencyLevel) {
        Course course = new Course();
        course.setName(name);
        course.setCapacity(capacity);
        course.setLanguage(language);
        course.setProficiencyLevel(proficiencyLevel);
        return testEntityManager.persistAndFlush(course);
    }

    private Lecture createLecture(String name, String description, LocalDateTime start, LocalDateTime end, int capacity, Course course) {
        Lecture lecture = new Lecture();
        lecture.setName(name);
        lecture.setDescription(description);
        lecture.setStartTime(start);
        lecture.setEndTime(end);
        lecture.setCourse(course);
        lecture.setLecturerId(UUID.randomUUID());
        lecture.setCapacity(capacity);
        return testEntityManager.persistAndFlush(lecture);
    }

    private Exercise createExercise(String name, String description, Course course) {
        Exercise exercise = new Exercise();
        exercise.setName(name);
        exercise.setDescription(description);
        exercise.setExerciseDifficulty(null);
        exercise.setCourse(course);
        return testEntityManager.persistAndFlush(exercise);
    }

    @Test
    @Transactional
    void findByIds_validIds_returnsCourses() {
        Set<UUID> ids = new HashSet<>();
        ids.add(course1.getId());
        ids.add(course2.getId());
        List<Course> courses = courseRepository.findByIds(ids);
        assertEquals(2, courses.size());
        assertTrue(courses.stream().anyMatch(course -> course.getId().equals(course1.getId())));
        assertTrue(courses.stream().anyMatch(course -> course.getId().equals(course2.getId())));
    }

    @Test
    @Transactional
    void findByIds_invalidIds_returnsNone() {
        Set<UUID> ids = new HashSet<>();
        ids.add(UUID.randomUUID());
        List<Course> courses = courseRepository.findByIds(ids);
        assertEquals(0, courses.size());
    }

    @Test
    @Transactional
    void findByProficiencyLevel_courseExists_returnsCourse() {
        List<Course> courses = courseRepository.findByProficiencyLevel(ProficiencyLevel.B1);
        assertEquals(1, courses.size());
        assertEquals(course1.getId(), courses.get(0).getId());
    }

    @Test
    @Transactional
    void findByProficiencyLevel_courseDoesNotExist_returnsNone() {
        List<Course> courses = courseRepository.findByProficiencyLevel(ProficiencyLevel.C2);
        assertEquals(0, courses.size());
    }

    @Test
    @Transactional
    void findByLanguage_courseExists_returnsCourse() {
        List<Course> courses = courseRepository.findByLanguage(Language.GERMAN);
        assertEquals(1, courses.size());
        assertEquals(course2.getId(), courses.get(0).getId());
    }

    @Test
    @Transactional
    void findByLanguage_courseDoesNotExist_returnsNone() {
        List<Course> courses = courseRepository.findByLanguage(Language.MANDARIN);
        assertEquals(0, courses.size());
    }
}
