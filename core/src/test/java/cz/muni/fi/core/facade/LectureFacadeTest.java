package cz.muni.fi.core.facade;

import cz.muni.fi.commons.dto.lecture.LectureDto;
import cz.muni.fi.core.data.mapper.LectureMapper;
import cz.muni.fi.core.data.model.Lecture;
import cz.muni.fi.core.service.LectureService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static cz.muni.fi.core.factory.TestDataFactory.createSampleLecture;
import static cz.muni.fi.core.factory.TestDataFactory.createSampleLectureDto;
import static cz.muni.fi.core.factory.TestDataFactory.createSampleLectureList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LectureFacadeTest {

    @Mock
    private LectureService lectureService;

    @Mock
    private LectureMapper lectureMapper;

    @InjectMocks
    private LectureFacade lectureFacade;

    @Test
    void findById_existingId_returnsLectureDto() {

        UUID existingId = UUID.randomUUID();
        Lecture expectedLecture = createSampleLecture();

        when(lectureService.findById(existingId)).thenReturn(expectedLecture);

        LectureDto result = lectureFacade.findById(existingId);

        assertEquals(lectureMapper.toDto(expectedLecture), result);
    }

    @Test
    void findAll_existingLectures_returnsListOfLectureDtos() {
        Pageable pageable = Pageable.unpaged();

        List<Lecture> existingLectures = createSampleLectureList();
        List<LectureDto> expectedLectureDtos = existingLectures.stream().map(lectureMapper::toDto).toList();

        Page<Lecture> lecturePage = new PageImpl<>(existingLectures);
        Page<LectureDto> lectureDtos = new PageImpl<>(expectedLectureDtos);


        when(lectureService.findAll(pageable)).thenReturn(lecturePage);
        when(lectureMapper.toDtoPage(lecturePage)).thenReturn(lectureDtos);

        Page<LectureDto> result = lectureFacade.findAll(pageable);

        assertEquals(lectureDtos, result);
    }

    @Test
    void findByLecturer_existingLectures_returnsListOfLectureDtos() {
        UUID lecturerId = UUID.randomUUID();
        List<Lecture> existingLectures = createSampleLectureList();
        List<LectureDto> expectedLectureDtos = existingLectures.stream().map(lectureMapper::toDto).toList();

        when(lectureService.findByLecturer(lecturerId)).thenReturn(existingLectures);
        when(lectureMapper.toDtoList(existingLectures)).thenReturn(expectedLectureDtos);

        List<LectureDto> result = lectureFacade.findByLecturer(lecturerId);

        assertEquals(expectedLectureDtos, result);
    }

    @Test
    void findByDate_existingLectures_returnsListOfLectureDtos() {
        LocalDate date = LocalDate.now();
        List<Lecture> existingLectures = createSampleLectureList();
        List<LectureDto> expectedLectureDtos = existingLectures.stream().map(lectureMapper::toDto).toList();

        when(lectureService.findByDate(date)).thenReturn(existingLectures);
        when(lectureMapper.toDtoList(existingLectures)).thenReturn(expectedLectureDtos);

        List<LectureDto> result = lectureFacade.findByDate(date);

        assertEquals(expectedLectureDtos, result);
    }

    @Test
    void findByCourse_existingLectures_returnsListOfLectureDtos() {
        UUID courseId = UUID.randomUUID();
        List<Lecture> existingLectures = createSampleLectureList();
        List<LectureDto> expectedLectureDtos = existingLectures.stream().map(lectureMapper::toDto).toList();

        when(lectureService.findByCourse(courseId)).thenReturn(existingLectures);
        when(lectureMapper.toDtoList(existingLectures)).thenReturn(expectedLectureDtos);

        List<LectureDto> result = lectureFacade.findByCourse(courseId);

        assertEquals(expectedLectureDtos, result);
    }

    @Test
    void findByCourse_noLectures_returnsEmptyList() {
        UUID nonExistingCourseId = UUID.randomUUID();

        when(lectureService.findByCourse(nonExistingCourseId)).thenReturn(new ArrayList<>());

        List<LectureDto> result = lectureFacade.findByCourse(nonExistingCourseId);

        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    void findLecturesByIds_existingLectures_returnsListOfLectureDtos() {
        Set<UUID> ids = new HashSet<>();
        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();
        ids.add(id1);
        ids.add(id2);
        List<Lecture> existingLectures = createSampleLectureList();
        List<LectureDto> expectedLectureDtos = existingLectures.stream().map(lectureMapper::toDto).toList();

        when(lectureService.findByIds(ids)).thenReturn(existingLectures);
        when(lectureMapper.toDtoList(existingLectures)).thenReturn(expectedLectureDtos);

        List<LectureDto> result = lectureFacade.findByIds(ids);

        assertEquals(expectedLectureDtos, result);
    }

    @Test
    void findLecturesByIds_noLectures_returnsEmptyList() {
        Set<UUID> ids = new HashSet<>();
        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();
        ids.add(id1);
        ids.add(id2);

        when(lectureService.findByIds(ids)).thenReturn(new ArrayList<>());

        List<LectureDto> result = lectureFacade.findByIds(ids);

        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    void create_validLectureDto_returnsCreatedLectureDto() {
        LectureDto lectureDto = createSampleLectureDto();

        Lecture expectedLecture = lectureMapper.toEntity(lectureDto);

        when(lectureService.create(expectedLecture, lectureDto.getCourseId())).thenReturn(expectedLecture);
        when(lectureMapper.toDto(expectedLecture)).thenReturn(lectureDto);

        LectureDto result = lectureFacade.create(lectureDto, lectureDto.getCourseId());

        assertEquals(lectureDto, result);
    }

    @Test
    void update_validLectureDto_returnsUpdatedLectureDto() {
        LectureDto lectureDto = createSampleLectureDto();
        lectureDto.setId(UUID.randomUUID());

        Lecture expectedLecture = lectureMapper.toEntity(lectureDto);

        when(lectureService.update(expectedLecture, lectureDto.getId())).thenReturn(expectedLecture);
        when(lectureMapper.toDto(expectedLecture)).thenReturn(lectureDto);

        LectureDto result = lectureFacade.update(lectureDto, lectureDto.getId());

        assertEquals(lectureDto, result);
    }

    @Test
    void findById_nonExistingId_returnsNull() {
        UUID nonExistingId = UUID.randomUUID();

        when(lectureService.findById(nonExistingId)).thenReturn(null);

        LectureDto result = lectureFacade.findById(nonExistingId);

        assertNull(result);
    }

//    @Test
//    void findAll_noLectures_returnsEmptyList() {
//        Pageable pageable = Pageable.unpaged();
//
//        List<Lecture> emptyList = new ArrayList<>();
//        Page<Lecture> lecturePage = new PageImpl<>(emptyList);
//        when(lectureService.findAll(pageable)).thenReturn(lecturePage);
//
//        Page<LectureDto> result = lectureFacade.findAll(pageable);
//
//        assertNotNull(result);
//        assertTrue(result.isEmpty());
//    }

    @Test
    void findByLecturer_noLectures_returnsEmptyList() {
        UUID lecturerId = UUID.randomUUID();
        List<Lecture> emptyList = new ArrayList<>();
        when(lectureService.findByLecturer(lecturerId)).thenReturn(emptyList);

        List<LectureDto> result = lectureFacade.findByLecturer(lecturerId);

        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    void findByDate_noLectures_returnsEmptyList() {
        LocalDate date = LocalDate.now();
        List<Lecture> emptyList = new ArrayList<>();
        when(lectureService.findByDate(date)).thenReturn(emptyList);

        List<LectureDto> result = lectureFacade.findByDate(date);

        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    void create_invalidLectureDto_returnsNull() {
        LectureDto invalidDto = new LectureDto();

        LectureDto result = lectureFacade.create(invalidDto, UUID.randomUUID());

        assertNull(result);
    }

    @Test
    void update_nonExistingId_returnsNull() {
        LectureDto nonExistingDto = new LectureDto();
        nonExistingDto.setId(UUID.randomUUID());

        LectureDto result = lectureFacade.update(nonExistingDto, UUID.randomUUID());

        assertNull(result);
    }

    @Test
    void remove_nonExistingId_returnsNull() {
        UUID nonExistingId = UUID.randomUUID();

        LectureDto result = lectureFacade.remove(nonExistingId);

        assertNull(result);
    }

    @Test
    void create_nullLectureDto_returnsNull() {
        LectureDto result = lectureFacade.create(null, UUID.randomUUID());

        assertNull(result);
    }

    @Test
    void create_emptyLectureDto_returnsNull() {
        LectureDto emptyDto = new LectureDto();

        LectureDto result = lectureFacade.create(emptyDto, UUID.randomUUID());

        assertNull(result);
    }

    @Test
    void update_nullLectureDto_returnsNull() {
        LectureDto result = lectureFacade.update(null, UUID.randomUUID());

        assertNull(result);
    }

    @Test
    void update_emptyLectureDto_returnsNull() {
        LectureDto emptyDto = new LectureDto();

        LectureDto result = lectureFacade.update(emptyDto, UUID.randomUUID());

        assertNull(result);
    }
}