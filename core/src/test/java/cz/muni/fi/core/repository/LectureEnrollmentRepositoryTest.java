package cz.muni.fi.core.repository;

import cz.muni.fi.commons.enums.Language;
import cz.muni.fi.commons.enums.ProficiencyLevel;
import cz.muni.fi.core.data.model.Course;
import cz.muni.fi.core.data.model.Lecture;
import cz.muni.fi.core.data.model.LectureEnrollment;
import cz.muni.fi.core.data.repository.LectureEnrollmentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
class LectureEnrollmentRepositoryTest {

    @Autowired
    private LectureEnrollmentRepository lectureEnrollmentRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    private Lecture lecture;
    private final UUID studentId1 = UUID.randomUUID();
    private final UUID studentId2 = UUID.randomUUID();

    @BeforeEach
    void initData() {
        lecture = createLecture();

        createLectureEnrollment(lecture, studentId1);
        createLectureEnrollment(lecture, studentId2);
    }

    private Lecture createLecture() {
        Course course = new Course();
        course.setName("Test Course");
        course.setProficiencyLevel(ProficiencyLevel.B1);
        course.setLanguage(Language.ENGLISH);
        course.setCapacity(60);
        course = testEntityManager.persistAndFlush(course);

        Lecture lecture = new Lecture();
        lecture.setName("Test Lecture");
        lecture.setDescription("Sample Description");
        lecture.setStartTime(LocalDateTime.of(2024, 4, 21, 10, 0));
        lecture.setEndTime(LocalDateTime.of(2024, 4, 21, 12, 0));
        lecture.setCapacity(10);
        lecture.setLecturerId(UUID.randomUUID());
        lecture.setCourse(course); // Set course
        return testEntityManager.persistAndFlush(lecture);
    }

    private void createLectureEnrollment(Lecture lecture, UUID studentId) {
        LectureEnrollment lectureEnrollment = new LectureEnrollment();
        lectureEnrollment.setLecture(lecture);
        lectureEnrollment.setStudentId(studentId);
        testEntityManager.persistAndFlush(lectureEnrollment);
    }

    @Test
    @Transactional
    void findByStudentId_studentExists_returnsStudent() {
        List<LectureEnrollment> enrollments = lectureEnrollmentRepository.findByStudentId(studentId1);
        assertEquals(1, enrollments.size());
        assertEquals(studentId1, enrollments.get(0).getStudentId());
    }

    @Test
    @Transactional
    void findByStudentId_studentDoesNotExist_returnsNone() {
        List<LectureEnrollment> enrollments = lectureEnrollmentRepository.findByStudentId(UUID.randomUUID());
        assertEquals(0, enrollments.size());
    }

    @Test
    @Transactional
    void CheckIfStudentIsEnrolled_validLectureAndStudentId_returnsTrue() {
        assertTrue(lectureEnrollmentRepository.checkIfStudentIsEnrolled(lecture.getId(), studentId1));
    }

    @Test
    @Transactional
    void CheckIfStudentIsEnrolled_invalidStudentId_returnsFalse() {
        assertFalse(lectureEnrollmentRepository.checkIfStudentIsEnrolled(lecture.getId(), UUID.randomUUID()));
    }

    @Test
    @Transactional
    void CheckIfStudentIsEnrolled_invalidLectureId_returnsFalse() {
        assertFalse(lectureEnrollmentRepository.checkIfStudentIsEnrolled(UUID.randomUUID(), studentId1));
    }

    @Test
    @Transactional
    void CheckIfStudentIsEnrolled_invalidBoth_returnsFalse() {
        assertFalse(lectureEnrollmentRepository.checkIfStudentIsEnrolled(UUID.randomUUID(), UUID.randomUUID()));
    }
}
